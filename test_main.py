from distributed import Client
client = Client()
import modin.pandas as pd

#import pandas as pd
import modin.pandas as md_pd
import numpy as np
from random import randint, sample
import datetime
import time
import calendar



# # Generate a sample dataframe
# n = 1000000

def generate_random_dates(start, end, n):
    dates = md_pd.Series(np.zeros(n))
    for i in range(n):
        dates[i] = start + datetime.timedelta(seconds=randint(0, int((end - start).total_seconds())))
    return(dates)


def test_main():
    #freeze_support()
    n = 1000000

    df = md_pd.DataFrame()
    print(df.info(verbose = True))

    start_time = time.time()
    df = md_pd.DataFrame({'user_id': sample(range(90000000, 99999999), n),
                    'order_id': np.random.choice(range(1000000, 2000000), n, replace=False),
                    'order_date': generate_random_dates(datetime.date(2015, 1, 1), 
                                                       datetime.date(2017, 12, 31), 
                                                       n),
                    'number_of_products': np.random.choice(range(20), n, replace=True),
                    'total_amount': np.round(np.random.uniform(1, 5000, n), 2)})

    # adding day of week variable
    df = df.assign(day_of_week = df.order_date.apply(lambda x: calendar.day_name[x.weekday()]))
    # changing type of id's to str
    df.user_id = df.user_id.astype('str')
    df.order_id = df.order_id.astype('str')
    create_df_time = time.time() - start_time

    print(df.info(verbose=True))
    print(df.head(10))
    print('Time to create df ', create_df_time)




    # start_time = time.time()
    # df = md_pd.DataFrame({'user_id': sample(range(90000000, 99999999), n),
    #                 'order_id': np.random.choice(range(1000000, 2000000), n, replace=False),
    #                 'order_date': generate_random_dates(datetime.date(2015, 1, 1), 
    #                                                     datetime.date(2017, 12, 31), 
    #                                                     n),
    #                 'number_of_products': np.random.choice(range(20), n, replace=True),
    #                 'total_amount': np.round(np.random.uniform(1, 5000, n), 2)})

    # # adding day of week variable
    # df = df.assign(day_of_week = df.order_date.apply(lambda x: calendar.day_name[x.weekday()]))
    # # changing type of id's to str
    # df.user_id = df.user_id.astype('str')
    # df.order_id = df.order_id.astype('str')
    # create_df_time = time.time() - start_time

    # print(df.info(verbose=True))
    # print(df.head(10))
    # print('Time to create df ', create_df_time)
