#set up the path in init to the scrapers and the core modules
import __init__

import os
from pathlib import Path
import pandas as pd

from CoreScraperDescriptions import update_attributes_descriptions_and_output
from CoreScraperConstants import default_params

#toppath to OUTPUT FOLDER,
#also controls whether TEST or production database is used when downloading from BQ (if folder contains TESt then TEST BQ db is used)
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Spb-attributes/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/attributes/'


if __name__ == '__main__':
    params = default_params.copy()
    
#     # specify retailers to process
    retailers = [ "JackWills",
    "WhiteStuff",
"AllSaints",
"FatFace",
"Boden",
"Crew",
"Joules",
"Pimkie",
"Superdry",
"ZaraFR",
"ZaraGB",
"MangoFR",
"HandMFR",
#"NewLook",
"Stradivarius",
"Bershka",
"PullandBear",
"GoOutdoors",
"HandMGB",
"MangoGB",
"Millets",
"Blacks",
"Trespass",
"CotswoldOutdoor",
"MountainWarehouse",
#"Asos",
#"JohnLewis",
]
    #retailers = [ "JackWills"] 
    #retailers = [ "WhiteStuff"] 
    retailers = [ "ZaraGB"] 


    row_limit = -1  # set row_limit to -1 or delete to extract all data, for testing set to a value
    replace_bq_data = False # default is to append bq data, not overwrite the database, when updating can append then remove duplicates
    process_new_data_only = False # default is True only processes data where sparkbox catagory is null, to force processing of all data set to False
    local_only = False # default False, when True does not write to BQ database
    remove_duplicates = True # default False, set to True when doing maintenance / updating following a logic change etc
    update_BQ_descriptions = True # default is False, set to True when downloading existing descriptions from BQ and updating 
    #test_skulist = ['35045103']
    test_skulist = []
    attributes_subset = ['sub_category', 'category', 'department', 'fabric', 'style', 'colour']  # set to empty list to update all
    #attributes_subset = ['sub_category', 'category', 'department']  # set to empty list to update all
    attributes_subset =  ['department']  # set to empty list to update all

    for r in retailers:
        descriptions_df = pd.DataFrame()
        #process_attributes = True
        #if it is the last retailer in the list, remove the duplicates if set as a parameter
        if r == retailers[-1]:
            remove_duplicates_retailer = remove_duplicates
        else:
            remove_duplicates_retailer = False

        output_data = update_attributes_descriptions_and_output( descriptions_df, toppath, params,
                                    retailer = r, row_limit = row_limit, replace_bq_data = replace_bq_data, 
                                    process_new_data_only = process_new_data_only, local_only = local_only , remove_duplicates = remove_duplicates_retailer, 
                                    test_skulist = test_skulist, attributes_subset = attributes_subset,
                                    google_project = "", google_credentials = "", update_BQ_descriptions = update_BQ_descriptions,
                                    )

        #save a local copy of attributes output
        print('Writing the Output')
        attributes_fname = str(Path( toppath)  / ( 'attributes_out_' + r + '.csv'))
        print('Output to ', attributes_fname)
        output_data.to_csv(attributes_fname, index = False)
