#init for scrapers dir and test scrapers dir
import os, sys 
from pathlib import Path
current_path = Path(os.path.dirname(os.path.realpath(__file__)))
parent_path = (current_path.parent)
core_scraper_path = Path( str(parent_path) + '//core_scraper')
sys.path.append(str(core_scraper_path))