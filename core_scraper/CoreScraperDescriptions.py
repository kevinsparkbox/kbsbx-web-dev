import time
import google
import csv
from  more_itertools import unique_everseen
import pandas as pd
import numpy as np
from datetime import datetime
from pathlib import Path
import sys
from retrying import retry
import logging

from google.cloud import translate_v2 as translate

from CoreScraperOutputModules import outputDataFrameToDatabase, remove_duplicates_descriptions
from CoreScraper_utils import configure_googleapi_credentials
from Corescraper_fetch_BQ_data import fetch_descriptions_from_bq, fetch_keywords_config_from_GCS
from CoreScraper_attributes import assign_subcat_cat_div, assign_department, match_long_description_fabric_keywords
from CoreScraper_attributes import match_similar_type_keywords, match_type_keywords, match_long_description_type_keywords
from CoreScraper_attributes import extract_style_entities, match_similar_fabric_keywords, extract_colour, match_fabric_keywords
from CoreScraperConstants import MINIMUM_RUNTIME, RETAILERS_TO_TRANSLATE, COLS_TO_TRANSLATE, DISABLE_TRANSLATION

import json
import requests

class Descr:
    def __init__(self, descriptions_df, toppath, retailer, row_limit, replace_bq_data, process_new_data_only, local_only, 
                    remove_duplicates, test_skulist, google_project, google_credentials, params):
        self.starttime = time.time()
        self.descriptions_df = descriptions_df
        self.toppath = toppath
        self.retailer = retailer
        self.row_limit = row_limit
        self.replace_bq_data = replace_bq_data
        self.process_new_data_only = process_new_data_only
        self.output_to_database = not local_only
        self.remove_duplicates = remove_duplicates
        self.test_skulist = test_skulist
        self.params = params
        self.output = pd.DataFrame()

        if len(sys.argv) > 1 : #toppath has been passed as an argument which takes priority
            self.toppath = sys.argv[1]
        if len(sys.argv) > 2 : #reset duplicates has been passed as an argument (not used in description scrape)
            #params['resetduplicatelinks'] = sys.argv[2]
            self.resetduplicatelinks = sys.argv[2]
        if len(sys.argv) > 3 : #run time limit has been passed as an argument
            self.run_time_limit = sys.argv[3]
        else:
            self.run_time_limit =  MINIMUM_RUNTIME
        if len(sys.argv) > 4 : #google credentials have been passed as an argument
            self.google_project = sys.argv[4]["google_project"]
            self.google_credentials = sys.argv[4]["google_credentials"]
            logging.info(('Google Credentials passed as arguments', self.google_project, self.google_credentials))
        elif google_credentials != "" and google_credentials != "" :
            self.google_project = google_project
            self.google_credentials = google_credentials
        else:
            google_credentials, google_project = configure_googleapi_credentials()
            logging.info(('Google Credentials generated using auth ', google_project, google_credentials))
            self.google_project = google_project
            self.google_credentials = google_credentials

    def get_descriptions_from_BQ(self):
        #for testing set the row_limit, for production set to -1
        row_limit = self.row_limit
        data_df = fetch_descriptions_from_bq( row_limit, self.retailer, self.toppath, self.google_project, self.google_credentials, self.params)
        logging.info(('Fetched descriptions for ', self.retailer, len(data_df)))
        #descriptions_df = pd.concat([self.descriptions, data_df])
        self.descriptions = data_df

    def get_csv_keywords(self):
        """Read in the type (sub_cat, category, division lookup) and the fabric keywords
        Convert the keywords to lower case
        """
        logging.info(('Reading the keywords and retailer attributes'))
        departmentsdf, categoriesdf, fabricdf, spbx_coloursdf, rgb_coloursdf, retailer_name, retailer_region, default_department, category_dict  =  \
                                        fetch_keywords_config_from_GCS( self.toppath, self.google_project, self.google_credentials, self.retailer) 

        fabric_keys = fabricdf['Keywords'].str.lower()
        #get the type keys from the category and type mapping
        type_keys = []
        for cat in categoriesdf.columns:
            for type_val in categoriesdf[cat].dropna().values:
                type_keys.append(type_val.lower())

        self.type_keys = type_keys
        self.fabric_keys = fabric_keys
        self.categories = categoriesdf
        self.category_dict = category_dict
        self.department_keywords = departmentsdf
        self.spbx_colours = spbx_coloursdf
        self.rgb_colours = rgb_coloursdf
        self.retailer_name = retailer_name
        self.retailer_region = retailer_region
        self.default_department = default_department
        logging.info(('retailer name', self.retailer_name))

    def select_and_index_data(self):
        data = self.descriptions
        #if attributes are not currently in the data, then it is all data that requires processing
        #otherwise if process_new_data_only = True then split the data into data that requires processing and data that has already been processed
        if self.process_new_data_only and 'spbx_category' in data.columns:
            selected_data = data[ data['spbx_category'].isnull()]
            existing_attributes = data[ data['spbx_category'].notnull()]
        else:
            selected_data = data
            existing_attributes = pd.DataFrame()

        #select a specific sku for testing
        if len(self.test_skulist ) > 0:
            selected_data = selected_data[ selected_data['skuid'].isin(self.test_skulist )]

        #ensure there are not any Nan/nulls in the text columns to process
        for c in COLS_TO_TRANSLATE:
            selected_data[c] = selected_data[c].fillna(" ")

        self.extracted_attributes = selected_data.sort_values(['retailer', 'skuid'], ignore_index = True)
        self.existing_attributes = existing_attributes

    def find_spbx_subcat_category_division(self):
        """ find the sparkbox category and division from the sub category
        """
        logging.info('Assigning subcat, categories, divisions ...')
        extracted_attributes = assign_subcat_cat_div(self.extracted_attributes,self.categories, self.category_dict)
        logging.info('Done Assigning Categories')
        self.extracted_attributes = extracted_attributes
    
    def find_spbx_department(self):
        """ find the sparkbox department from the input dept, cat, subcat
        """
        logging.info('Assigning  departments...')
        extracted_attributes = assign_department(self.extracted_attributes,self.department_keywords, self.default_department)
        logging.info('Done Assigning Departments')
        self.extracted_attributes = extracted_attributes

    def find_spbx_sub_category_type(self):
        """ find the sparkbox sub category_type
        Broadly, compares the short description to the "type" keywords to find an exact match,
        If no exact match find the best type match between each of the words in the short description and the type keywords
        Before accepting as the best match, also check that the short description word is not a better match to one of the fabric keywords

        """
        logging.info('Appplying the exact type key words') # step 1
        self.extracted_attributes = match_type_keywords(self.extracted_attributes, self.type_keys)
        logging.info('Appplying the similar type key words') # step 2
        self.extracted_attributes = match_similar_type_keywords(self.extracted_attributes, self.type_keys, self.fabric_keys)
        logging.info('Appplying the exact type key words to the long_description') # step 3
        self.extracted_attributes = match_long_description_type_keywords(self.extracted_attributes, self.type_keys)
        self.extracted_attributes['spbx_subcat_type'] = self.extracted_attributes['spbx_subcat_type'].fillna("Other")


    def extract_style(self):
        extracted_type = self.extracted_attributes
        logging.info('Extracting style ...')
        subtype_desc = extract_style_entities(extracted_type)
        logging.info('Done')
        self.extracted_attributes = subtype_desc

    def extract_fabric(self):
        logging.info('Extracting Fabric ...')
        #first match the fabric type with any words in the short or long description, looking for an exact match
        logging.info('Fabric looking for an exact match in short descriptions')
        self.extracted_attributes = match_fabric_keywords(self.extracted_attributes, self.fabric_keys)
        #next match the fabric type with any words in the long description, looking for an exact match
        logging.info('Fabric looking for an exact match in long descriptions')
        self.extracted_attributes = match_long_description_fabric_keywords(self.extracted_attributes,  self.fabric_keys)
        logging.info('Fabric looking for a similar match in long descriptions')
        self.extracted_attributes = match_similar_fabric_keywords(self.extracted_attributes, self.fabric_keys, self.type_keys)

    def extract_colours(self):
        #get the colour mapping from RGB colours to sparkbox colours
        logging.info('Extracting colours')
        extracted_attributes = extract_colour(self.extracted_attributes, self.spbx_colours, self.rgb_colours)
        self.extracted_attributes = extracted_attributes


    # add a retry, as there are various Google API project rate limits whch may be exceeded if several processes attempt to use the translate
    #API at the same time - retry after 30 seconds, up to 10 times
    @retry(wait_fixed=30000, stop_max_attempt_number=10)
    def translate_text(self):
        descriptions_df = self.extracted_attributes
        translate_client = translate.Client(credentials = self.google_credentials)

        #create placeholder columns for the original text if not already created in the data 
        for c in COLS_TO_TRANSLATE:
            if c + '_orig' not in descriptions_df.columns:
                logging.info('Creating column')
                descriptions_df[c + '_orig'] = None

        #select the rows which need translating - retailer in retailers_to_translate and _orig is empty, (not already translated)
        descriptions_to_translate = descriptions_df[ (descriptions_df['retailer'].isin(RETAILERS_TO_TRANSLATE)  & 
                                                        descriptions_df['short_description_orig'].isnull()  ) ].copy()
        if len(descriptions_to_translate) > 0:
            descriptions_to_translate.reset_index(drop=True, inplace=True)
            for c in COLS_TO_TRANSLATE:
                descriptions_to_translate[c + '_orig'] = descriptions_to_translate[c]

            #translate the text to be translated, do it in chunks as there is a character limit on the google API translator 204800 bytes on the request payload
            #assume 2 bytes per char = 100,000 chars.  There is also a limit on the number of text segments which is 128 which is probably the dominant factor
            #calculate chunk size, do not make too small because there is a limit on the number of requests per minute, set a safety factor of 80% of payload limit
            translate_payload_limit = 100000
            number_text_segments_limit = 128
            payload_rate_limit = 3000000 #limit on number of characters per minute translated, Google limit is 6000000 characters per minute for the project
            #translate each column of the df
            for c in COLS_TO_TRANSLATE:
                logging.info(('Translating ', c))
                col_start_time = time.time()
                number_requests = 0
                cum_num_chars_payload = 0
                text_result = []
                #calculate the chunksize, needs to be as large as possible to minimise number of requests per minute but avoid exceeding the payload limit,
                #and less than the text_segments limit, requests per minute limit is 6000 per project
                col_text = descriptions_to_translate[c].to_list()
                numchars_col_text = sum(len(i) for i in col_text)
                chunk_size = int(translate_payload_limit * 0.8  / (numchars_col_text / len(descriptions_to_translate)) ) # payload limit / average chars per text type (c)
                chunk_size = min( number_text_segments_limit, chunk_size )
                for start in range(0, descriptions_to_translate.shape[0], chunk_size):
                    tr_start_time = time.time()
                    df_subset_text = descriptions_to_translate[c].iloc[start:start + chunk_size]
                    text = df_subset_text.to_list()
                    translated_text = pd.DataFrame(translate_client.translate(text, target_language="en"))
                    numchars_payload =   sum(len(i) for i in text)
                    cum_num_chars_payload = cum_num_chars_payload + numchars_payload
                    number_requests = number_requests +1 
                    #calculate if throttling is required to be under the payload rate limit
                    exec_time = time.time() - tr_start_time
                    chars_per_minute = int(numchars_payload/exec_time * 60)
                    excess_rate = chars_per_minute / payload_rate_limit
                    required_time = excess_rate * exec_time
                    wait_time = max( required_time - exec_time, 0)
                    time.sleep(wait_time)
                    #logging.info('payload', numchars_payload, 'exec_time', exec_time, 'chars per minute', chars_per_minute , 'wait time', wait_time)
                    text_result.append(translated_text)
                col_exec_time = time.time() - col_start_time
                logging.info(('Finished Translating ', c, 'number requests ', number_requests, 'cum num chars payload' , cum_num_chars_payload, 'exec time ', col_exec_time))
                #copy the translated text back into the descriptions
                translated_text= pd.concat(text_result)
                translated_text.reset_index(drop=True, inplace=True)
                descriptions_to_translate[c] = translated_text['translatedText']

            #merge the translated rows back into the original descriptions by concat and dropping duplicates
            merge_cols = ['retailer', 'skuid']
            descriptions_df = pd.concat([descriptions_df,descriptions_to_translate]).drop_duplicates(merge_cols, keep='last').sort_values(merge_cols)

        self.extracted_attributes = descriptions_df

    def generate_output(self):

        #combine new attributes with existing
        sort_cols = ['retailer', 'skuid']
        output_data = pd.concat([self.extracted_attributes, self.existing_attributes])
        output_data = output_data.sort_values(sort_cols, ignore_index = True).drop_duplicates(subset=sort_cols, ignore_index=True, keep='last')

        #before writing the data, add/update the date time stamp
        output_data['date'] = datetime.now().replace(microsecond=0)
        #add the retailer description and retailer region
        output_data['retailer_name'] = self.retailer_name
        output_data['retailer_region'] = self.retailer_region

        #set parameter to replace existing data or append to existing data
        if self.replace_bq_data:
            if_exists_operation = "replace" 
        else:
            if_exists_operation = "append" 

        #output the results to the database
        if self.output_to_database:
            if self.toppath.find('TEST') == -1:
                dataset = self.params['GBQ_DATASET']
                results_table = self.params['GBQ_TABLE_DESCRIPTIONS']
            else:
                dataset = self.params['TEST_GBQ_DATASET']
                results_table = self.params['TEST_GBQ_TABLE_DESCRIPTIONS']

            location = "Big_Query"
            logging.info(('Writing data to big query ', dataset, results_table, if_exists_operation))
            outputDataFrameToDatabase(output_data, dataset, results_table, location, if_exists_operation, self.google_project, self.google_credentials)
            #remove duplicates from the BQ database if requested
            if self.remove_duplicates:
                remove_duplicates_descriptions(dataset, results_table, self.google_project, self.google_credentials)

        #store the output_data
        self.output = output_data
        

# %% Main Code    

""" receive the descriptions (decriptions_df), process to update the attributes and generate the sparkbox hierarchies and then output to BQ
    only process the attributes if process_attributes is True, otherwise will waste processing time on retailers where the attributes are not yet defined, e.g. watches  """
def update_attributes_descriptions_and_output( descriptions_df, toppath, params, retailer = "", row_limit = -1, replace_bq_data = False, 
                                        process_new_data_only = True, local_only = False , remove_duplicates = False, test_skulist = [],
                                        google_project = "", google_credentials = "", update_BQ_descriptions = False,
                                        attributes_subset = []):

    default_attributes = ['sub_category', 'category', 'department', 'fabric', 'style', 'colour']
    if len(attributes_subset) == 0:
        attributes_to_update = default_attributes
    else:
        attributes_to_update = attributes_subset

    #initialise the descriptions attribute updater
    description = Descr(descriptions_df, toppath, retailer, row_limit, replace_bq_data, process_new_data_only , local_only, 
                                            remove_duplicates, test_skulist, google_project, google_credentials, params )

    #if descriptions have been passed, calculate the attributes on those, otherwise fetch descriptions from BQ if specified
    if update_BQ_descriptions:
        description.get_descriptions_from_BQ()
    else:
        description.descriptions = descriptions_df

    #prepare data for extracting attributes from descriptions, split the data into 2 datasets, one that requires
    #attribute extraction and one where attributes have previuosly been extracted (extracted_attributes and existing_attributes)
    description.select_and_index_data()

    #process the data where attributes are to be extracted and calculated
    if len(description.extracted_attributes) > 0 and params['process_attributes'] :
        #translate any non english text to english as the spacy nlp algorithms do not seem to work well on french product descriptions
        #DISABLE_TRANSLATION allows translations to be disabled to save cash
        if not DISABLE_TRANSLATION:
            description.translate_text()
        else:
            logging.info('Not doing translation')

        #reead in the key word definitions and hierarchies
        description.get_csv_keywords()

        #find the sparkbox sub_category_type, aka as the product "type"
        if ('sub_category' in attributes_to_update) or ('category' in attributes_to_update):
            #reset the current variables if already set
            description.extracted_attributes['spbx_subcat_type'] = np.nan
            description.extracted_attributes['spbx_subcat'] = np.nan
            description.find_spbx_sub_category_type()
        
        #find the sparkbox category sub_category and division
        if 'category' in attributes_to_update:
            #reset the current variables if already set
            description.extracted_attributes['spbx_category'] = np.nan
            description.find_spbx_subcat_category_division()

        #from the original hierarchy data find the product aparkbox department
        if 'department' in attributes_to_update:
            #reset the current variables if already set
            description.extracted_attributes['spbx_department'] = np.nan
            description.find_spbx_department()

        #from the long description and ahort description find the fabric
        if 'fabric' in attributes_to_update:
            #reset the current variables if already set
            description.extracted_attributes['fabric'] = np.nan
            description.extract_fabric()

        #from the long description and short description find the style of product
        if 'style' in attributes_to_update:
            #reset the current variables if already set
            description.extracted_attributes['style'] = np.nan
            description.extract_style()

        #map the retailer colour description to the closest sparkbox colour
        if 'colour' in attributes_to_update:
            #reset the current variables if already set
            description.extracted_attributes['spbx_colour'] = np.nan
            description.extract_colours()
            #replace not_found with Other
            description.extracted_attributes['spbx_colour'] = description.extracted_attributes['spbx_colour'].replace('not_found', 'Other')
            description.extracted_attributes['spbx_colour'] = description.extracted_attributes['spbx_colour'].fillna('Other')


        #recombine the newly processed attributes and the existing attributes into the output 
        #update the big query database if requested
        description.generate_output()

    else:
        logging.info(('Not processing attributes', len(description.extracted_attributes) , params['process_attributes']))

    return description.output

