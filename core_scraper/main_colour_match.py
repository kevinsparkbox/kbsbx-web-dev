import pandas as pd
import numpy as np
import os
import pickle
from fuzzywuzzy import fuzz
from colormath.color_objects import *
from colormath.color_conversions import convert_color
from scipy.special import softmax
from openpyxl.utils.dataframe import dataframe_to_rows
from openpyxl import Workbook, load_workbook
from scipy.special.orthogonal import laguerre

file_path = 'C:/Users/shaun/Data/sparkbox-dev/text-feature-extraction/color_matcher/data/'

spb_colours_file = 'sparkbox_base_colours.csv'
test_file1 = 'inputs.csv'
ref_file = 'color_names_consolidated.xlsx'
fuzzy_pickle_file = 'matched_colors.pkl'
final_results_file = 'final_results.csv'
final_results_macro_file = 'final_results.xlsm'
debug_results_file = 'debug_results.csv'
results_data_file = 'results_data.csv'

filter_protocol = {
    'matched_color_1': {
        'white': {
            'distance': 8
        },
        'grey': {
            'distance': 14
        },
        'silver': {
            'distance': 10
        }
    }
}


def read_input_data():

    sparkbox_colours = pd.read_csv(f"{file_path}/input/{spb_colours_file}")
    test1 = pd.read_csv(f"{file_path}/input/{test_file1}")

    ref_colors = pd.read_excel(io=f"{file_path}/input/{ref_file}", sheet_name='in')

    return sparkbox_colours, test1, ref_colors

def clean_test_color_names(test_colors):
    """
    Preprocess each color by:
    1. strip whitespaces
    2. split into "/"
    3. lowercase
    Also keep track of the ids for multiple colours for same row separated by "/"
    """
    all_colors, all_indices = [], []
    for idx, color_ in enumerate(test_colors['colour']):
        color_ = color_.strip()
        color_ = color_.lower()
        if "/" in color_:
            split_colors = color_.split("/")
            clean_color = split_colors[0]
            if "-colored" in clean_color or "-coloured" in clean_color:
                    clean_color = clean_color.split("-")[0]
            all_colors.append(clean_color.strip())
            all_indices.append(idx)
        elif "-colored" in color_ or "-coloured" in color_:
            c_ = color_.split("-")[0]
            all_colors.append(c_.strip())
            all_indices.append(idx)
        else:
            all_colors.append(color_)
            all_indices.append(idx)
    return all_colors, all_indices


def find_missing_matched_colors(large_test_colors, ref_colors):
    """
    Maps each test color to a ref colour: simple match
    """
    matched_colors = []
    for color_ in large_test_colors['colour']:
        if color_ not in ref_colors['preprocessed_name'].tolist():
            matched_colors.append(None)
        else:
            matched_colors.append(color_)

    large_test_colors['ref_colour'] = matched_colors

    return large_test_colors

def check_fuzzy_file_present():
    if fuzzy_pickle_file in os.listdir(path=f"{file_path}/input"):
        return True
    return False

def fuzzy_match_closest_color(large_test_colors, ref_colors):
    """
    Fuzzy match closest color
    Also keep track of colors that could not be matched
    Use partial ratio and token sort ratio and threshold of 90
    if fuzz.ratio(color_, ref_) > 90 or fuzz.token_sort_ratio(color_, ref_) > 90:
    """
    new_ref_colours = []
    for idx, color_ in enumerate(large_test_colors['colour']):
        if large_test_colors.iloc[idx]['ref_colour'] is None:
            match_found = False
            for ref_ in ref_colors['preprocessed_name']:
                if fuzz.ratio(color_, ref_) > 90 or fuzz.token_sort_ratio(color_, ref_) > 90:
                    match_found=True
                    new_ref_colours.append(ref_)
                    print (f"Fuzzy match for {color_} at idx: {idx} : {ref_}")
                    break
            if not match_found:
                new_ref_colours.append(None)
        else:
            new_ref_colours.append(large_test_colors.iloc[idx]['ref_colour'])

    large_test_colors['ref_colour'] = new_ref_colours

    #print (len(large_test_colors), len(new_ref_colours))

    return large_test_colors

def change_format(ref_colors):
    """
    Reformats the colors data into the test color format 
    """
    
    new_df = {
        'Color Name': [],
        'Hex Code #RRGGBB': [],
        'RGB Decimal': []
    }
    
    new_df['Color Name'] = list(ref_colors['ref_colour'])
    new_df['Hex Code #RRGGBB'] = [None] * len(ref_colors)
    
    R = [str(R) for R in ref_colors['R'].values.tolist()]
    G = [str(G) for G in ref_colors['G'].values.tolist()]
    B = [str(B) for B in ref_colors['B'].values.tolist()]
    
    for idx, R_ in enumerate(R):
        G_, B_ = G[idx], B[idx]
        RGB_ = f"({R_},{G_},{B_})"
        new_df['RGB Decimal'].append(RGB_)
    
    return pd.DataFrame(new_df)

def rgb2lab(inputColor):
    """
    [R,G,B] ---> [L,a,b] scale color conversion
    """

    num = 0
    RGB = [0, 0, 0]

    for value in inputColor:
        value = float(value) / 255

        if value > 0.04045:
            value = ((value + 0.055) / 1.055) ** 2.4
        else:
            value = value / 12.92

        RGB[num] = value * 100
        num = num + 1

    XYZ = [0, 0, 0, ]

    X = RGB[0] * 0.4124 + RGB[1] * 0.3576 + RGB[2] * 0.1805
    Y = RGB[0] * 0.2126 + RGB[1] * 0.7152 + RGB[2] * 0.0722
    Z = RGB[0] * 0.0193 + RGB[1] * 0.1192 + RGB[2] * 0.9505
    XYZ[0] = round(X, 4)
    XYZ[1] = round(Y, 4)
    XYZ[2] = round(Z, 4)

    # Observer= 2°, Illuminant= D65
    XYZ[0] = float(XYZ[0]) / 95.047         # ref_X =  95.047
    XYZ[1] = float(XYZ[1]) / 100.0          # ref_Y = 100.000
    XYZ[2] = float(XYZ[2]) / 108.883        # ref_Z = 108.883

    num = 0
    for value in XYZ:

        if value > 0.008856:
            value = value ** (0.3333333333333333)
        else:
            value = (7.787 * value) + (16 / 116)

        XYZ[num] = value
        num = num + 1

    Lab = [0, 0, 0]

    L = (116 * XYZ[1]) - 16
    a = 500 * (XYZ[0] - XYZ[1])
    b = 200 * (XYZ[1] - XYZ[2])

    Lab[0] = round(L, 4)
    Lab[1] = round(a, 4)
    Lab[2] = round(b, 4)

    return Lab

def compute_lab_distance(X1, X2):
    """
    Compute LAB dist bw 2 colors in the CIELAB scale
    """

    l1, l2, a1, a2, b1, b2 = X1[0], X2[0], X1[1], X2[1], X1[2], X2[2]
    deltaL = l2-l1
    deltaA = a2-a1
    deltaB = b2-b1
    d = (deltaL**2 + deltaA**2 + deltaB**2)**0.5
    return d

def compute_compu_phase_distance(C1, C2):
    """
    Compute the compuphase dist as described here: https://www.compuphase.com/cmetric.htm bw 2 colors in RGB scale
    """

    r1,r2, g1,g2, b1,b2 = C1[0],C2[0], C1[1],C2[1], C1[2],C2[2]
    
    r_bar = (r1 + r2)/2
    delta_r = (r1-r2)
    delta_g = (g1-g2)
    delta_b = (b1-b2)
    
    dist = ((2+r_bar/256)*(delta_r**2)) + (4*delta_g**2) + ((2 + (255-r_bar)/256)*delta_b**2)
    
    dist = dist**0.5
    
    return dist

def process_rgb_string(rgb_string):
    """
    From an RGB string, return [r, g, b] values as an array
    """
    rgb_string_clean = rgb_string.strip().replace("(", "").replace(")", "").replace(";", ",")
    r, g, b = int(float(rgb_string_clean.split(",")[0])), int(float(rgb_string_clean.split(",")[1])), int(float(rgb_string_clean.split(",")[2]))
    return [r, g, b]

def compute_test_lab_distances(test_tuples, main_tuples):
    """
    compute the LAB distance of each test color to each of the main(sbx) colors
    """
    test_color_distances = []

    for test_color in test_tuples:
        dist_array = []
        ## compute dist wrt each sbx color
        for sbx_color in main_tuples:
            dist_array.append(compute_lab_distance(test_color, sbx_color))
        test_color_distances.append(dist_array)

    test_color_distances = np.array(test_color_distances)

    assert test_color_distances.shape[0] == test_tuples.shape[0] and test_color_distances.shape[1] == main_tuples.shape[0]
    
    return test_color_distances


def compute_test_compuphase_distances(test_tuples, main_tuples, weights=[1,1,1]):
    """
    compute the compuphase distance of each test color to each of the main(sbx) colors
    """
    test_color_distances = []

    for test_color in test_tuples:
        dist_array = []
        ## compute dist wrt each sbx color
        for sbx_color in main_tuples:
            dist_array.append(compute_compu_phase_distance(test_color, sbx_color))
        test_color_distances.append(dist_array)

    test_color_distances = np.array(test_color_distances)

    assert test_color_distances.shape[0] == test_tuples.shape[0] and test_color_distances.shape[1] == main_tuples.shape[0]
    
    return test_color_distances

def create_greyish_ref(sparkbox_colours, greyish_colors = ['Black', 'White', 'Grey', 'Silver']):
    """
    Creates a reference of index -> greyish color
    """
    print ()
    indices = sparkbox_colours.loc[sparkbox_colours['Sparkbox Colour'].isin(greyish_colors)].index.tolist()
    names = sparkbox_colours.loc[sparkbox_colours['Sparkbox Colour'].isin(greyish_colors)]['Sparkbox Colour'].tolist()
    
    names = [name_.strip().lower() for name_ in names]
    
    return list(zip(indices, names))

def normalize_distances(arr, factor=1):
    arr = np.array(arr)
    arr = arr + 0.0001 ## adding a small bias to avoid divide by 0 error
    arr = arr*factor
    return ((1/arr)/(np.sum(1/arr)))

def find_min_distance_indices_scale1(test_colours, test_color_distances, rgb_tuples_main, k, greyish_indices_ref, filter_greyish, filter_protocol=None, protocol_colors=None):
    """
    Finds the dist in the CIELAB scale for each test color
    Finds the k nearest colors based on CIELAB dist
    Checks and filters for possible greyish color false positives
    Also if matched_color_1 is X filter based on custom rules of X
    Returns: k_dist_indices (indices of the k closest colors), 
            k_dist_values (dist values of the k closest colors)
            k_dist_rgb_values (rgb values of the k closest colors)
    """

    k_dist_indices = [] # for each test_colour, [... k ...] indices
    k_dist_rgb_values = [] # rgb value of the color at min distance
    k_dist_values = [] # for each test_colour, [... k ...] distance values
    k_dist_values_norm = [] # for each test_colour, [... k ...] NORMALIZED distance values (0->1)
    
    grey_indices = list(greyish_indices_ref.keys())
    grey_names = list(greyish_indices_ref.values())
    

    for test_idx, dist in enumerate(test_color_distances):
        
        min_k_dist_idx = np.argsort(dist)[:k] ### the k indices of minimum distance
        indices_to_keep = [] ### modified index list
        
        
        
        color_name = test_colours.iloc[test_idx, 0].lower().strip()
        
        # print (f"Initial selection: {min_k_dist_idx} for color: {color_name}")
        
        
        ### if ignore_greyish flag is False we dont need to do anything
        if not filter_greyish:
            # print ("Not filtering greyish colors...")
            indices_to_keep = min_k_dist_idx
        else:
            ## for each idx in the k indices found
            for kth_idx, min_idx in enumerate(min_k_dist_idx):
                ### if that index is not one of the grey colors, we can simply append
                if min_idx not in list(grey_indices):
                    indices_to_keep.append(min_idx)
                    # print (f"{min_idx} not greyish so added to {indices_to_keep} automatically")
                else:
                    # print (f"{min_idx} is greyish, need to check dist and col name and custom rules")
                    ### get distance
                    grey_distance = dist[min_idx]
                    grey_min_idx_name = greyish_indices_ref[min_idx]
                    
                    
                    ################## CUSTOM RULES START: only apply to the first matched color
                    if filter_protocol is not None and kth_idx == 0:
                        greyish_color = greyish_indices_ref[min_idx]
                        ### if the matched color is in the protocol_colors and the distance is less than the threshold for that color
                        if greyish_color in protocol_colors:
                            if grey_distance < filter_protocol['matched_color_1'][greyish_color]['distance']:
                                indices_to_keep.append(min_idx)
                                # print (f"{min_idx} added to {indices_to_keep} by filter protocol")
                            
                    ################## CUSTOM RULES ENDS
                    
                    #### TODO : if next index color is close to this greyish color, we dont need this greyish color
                    
                    elif grey_distance < 15: ### for any color except 1st matched color if grey dist < 15 keep it
                        ### add to list
                        # print (f"{kth_idx+1}th matched color is {greyish_indices_ref[min_idx]} and dist: {grey_distance} < 15 so adding to {indices_to_keep}")
                        indices_to_keep.append(min_idx)

                    ### check for color name: applies to all matched colors
                    if fuzz.ratio(color_name, grey_min_idx_name) > 90 or fuzz.partial_ratio(color_name, grey_min_idx_name) > 90 or fuzz.token_sort_ratio(color_name, grey_min_idx_name) > 90:
                        indices_to_keep.append(min_idx)
                        # print (f"test color: {color_name} || Grey color: {grey_min_idx_name}, so adding {min_idx} to {indices_to_keep}")
        
        ### sanitary check: filter for duplicates
        indices_to_keep = list(dict.fromkeys(indices_to_keep))
        
        ### at this stage, we have filtered the indices we want to keep; we might need to add additional one
        # print (indices_to_keep)
        
        if len(indices_to_keep) < k:
            diff = k - len(indices_to_keep)
            # print (f"Falling short by {diff} colors")
            ### need to get `diff` more indices
            diff_indices = np.argsort(dist)[k: k+diff]
            # print (f"New indices generated: {diff_indices}")
            for diff_ in diff_indices:
                indices_to_keep.append(diff_)
                    
        indices_to_keep = np.array(indices_to_keep)
        
        k_dist_indices.append(indices_to_keep)
        k_dist_values.append(dist[indices_to_keep])
        k_dist_values_norm.append(normalize_distances(dist[indices_to_keep]))
        k_dist_rgb_values.append(rgb_tuples_main[indices_to_keep])

    k_dist_indices = np.array(k_dist_indices)
    k_dist_rgb_values = np.array(k_dist_rgb_values)
    k_dist_values = np.array(k_dist_values)
    k_dist_values_norm = np.array(k_dist_values_norm)
    
    
    assert k_dist_indices.shape == (test_color_distances.shape[0], k) and k_dist_rgb_values.shape == (test_color_distances.shape[0], k, rgb_tuples_main.shape[1]) \
            and k_dist_values.shape == (test_color_distances.shape[0], k) and k_dist_values_norm.shape == (test_color_distances.shape[0], k) and np.isclose(np.sum(np.sum(k_dist_values_norm, axis=1)), test_color_distances.shape[0], atol=0.1)
    return k_dist_indices, k_dist_values, k_dist_values_norm, k_dist_rgb_values

def find_min_distance_indices_scale2(test_colours, test_color_distances, rgb_tuples_main, k, greyish_indices_ref, filter_greyish, filter_protocol=None, protocol_colors=None):
    
    """
    ######### To be applied after scale 1 ##########
    Finds the dist acc to COMPUPHASE metric for each test color
    Finds the k nearest colors based on COMPUPHASE metric
    Checks and filters for possible greyish color false positives
    Also if matched_color_1 is X filter based on custom rules of X
    Ignores name while matching color as this feature is already implemented in scale1

    Returns: k_dist_indices (indices of the k closest colors), 
            k_dist_values (dist values of the k closest colors)
            k_dist_rgb_values (rgb values of the k closest colors)
    """

    k_dist_indices = [] # for each test_colour, [... k ...] indices
    k_dist_rgb_values = [] # rgb value of the color at min distance
    k_dist_values = [] # for each test_colour, [... k ...] distance values
    k_dist_values_norm = [] # for each test_colour, [... k ...] NORMALIZED distance values (0->1)
    
    grey_indices = list(greyish_indices_ref.keys())
    grey_names = list(greyish_indices_ref.values())
    

    for test_idx, dist in enumerate(test_color_distances):
        
        min_k_dist_idx = np.argsort(dist)[:k] ### the k indices of minimum distance
        indices_to_keep = [] ### modified index list
        
        
        
        color_name = test_colours.iloc[test_idx, 0].lower().strip()
        
        # print (f"Initial selection: {min_k_dist_idx} for color: {color_name}")
        
        
        ### if ignore_greyish flag is False we dont need to do anything
        if not filter_greyish:
            # print ("Not filtering greyish colors...")
            indices_to_keep = min_k_dist_idx
        else:
            ## for each idx in the k indices found
            for kth_idx, min_idx in enumerate(min_k_dist_idx):
                ### if that index is not one of the grey colors, we can simply append
                if min_idx not in list(grey_indices):
                    indices_to_keep.append(min_idx)
                    # print (f"{min_idx} not greyish so added to {indices_to_keep} automatically")
                else:
                    # print (f"{min_idx} is greyish, need to check dist and col name and custom rules")
                    ### get distance
                    grey_distance = dist[min_idx]
                    grey_min_idx_name = greyish_indices_ref[min_idx]
                    
                    
                    ################## CUSTOM RULES START: only apply to the first matched color
                    if filter_protocol is not None and kth_idx == 0:
                        greyish_color = greyish_indices_ref[min_idx]
                        ### if the matched color is in the protocol_colors and the distance is less than the threshold for that color
                        if greyish_color in protocol_colors:
                            if grey_distance < filter_protocol['matched_color_1'][greyish_color]['distance']:
                                indices_to_keep.append(min_idx)
                                # print (f"{min_idx} added to {indices_to_keep} by filter protocol")
                            
                    ################## CUSTOM RULES ENDS
                    
                    #### TODO : if next index color is close to this greyish color, we dont need this greyish color
                    
                    elif grey_distance < 80: ### for any color except 1st matched color if grey dist < 15 keep it
                        ### add to list
                        # print (f"{kth_idx+1}th matched color is {greyish_indices_ref[min_idx]} and dist: {grey_distance} < 15 so adding to {indices_to_keep}")
                        indices_to_keep.append(min_idx)

                   
        ### sanitary check: filter for duplicates
        indices_to_keep = list(dict.fromkeys(indices_to_keep))
        
        ### at this stage, we have filtered the indices we want to keep; we might need to add additional one
        # print (indices_to_keep)
        
        if len(indices_to_keep) < k:
            diff = k - len(indices_to_keep)
            # print (f"Falling short by {diff} colors")
            ### need to get `diff` more indices
            diff_indices = np.argsort(dist)[k: k+diff]
            # print (f"New indices generated: {diff_indices}")
            for diff_ in diff_indices:
                indices_to_keep.append(diff_)
                    
        indices_to_keep = np.array(indices_to_keep)
        
        k_dist_indices.append(indices_to_keep)
        k_dist_values.append(dist[indices_to_keep])
        k_dist_values_norm.append(normalize_distances(dist[indices_to_keep]))
        k_dist_rgb_values.append(rgb_tuples_main[indices_to_keep])

    k_dist_indices = np.array(k_dist_indices)
    k_dist_rgb_values = np.array(k_dist_rgb_values)
    k_dist_values = np.array(k_dist_values)
    k_dist_values_norm = np.array(k_dist_values_norm)
    
    
    assert k_dist_indices.shape == (test_color_distances.shape[0], k) and k_dist_rgb_values.shape == (test_color_distances.shape[0], k, rgb_tuples_main.shape[1]) \
            and k_dist_values.shape == (test_color_distances.shape[0], k) and k_dist_values_norm.shape == (test_color_distances.shape[0], k) and np.isclose(np.sum(np.sum(k_dist_values_norm, axis=1)), test_color_distances.shape[0], atol=0.1)
    return k_dist_indices, k_dist_values, k_dist_values_norm, k_dist_rgb_values

def create_debug_results(test_colours, rgb_tuples_test, matched_colors, min_dist_rgb_value, min_dist_values_norm):
    """
    For each test color we write the corr matched colors, the RGB values and the probabilities into a df
    """
    color = test_colours['Color Name'].values.tolist()

    d = {'test_color' : color, 'rgb_test': [list(rgb) for rgb in rgb_tuples_test],
         'test_R': rgb_tuples_test[:, 0], 'test_G':  rgb_tuples_test[:, 1], 'test_B':rgb_tuples_test[:, 2],
         'rgb_test': [list(rgb) for rgb in rgb_tuples_test],

        }

    for col_idx, colors in enumerate(matched_colors):
        k = len(colors)
        for idx in range(1, k+1):
            var_name = f"matched_color_{idx}"
            if d.get(var_name) is None:
                d[var_name] = []
            d[var_name].append(colors[idx-1])

            var_name = f"matched_color_{idx}_R"
            if d.get(var_name) is None:
                d[var_name] = []
            d[var_name].append(min_dist_rgb_value[col_idx][idx-1][0])

            var_name = f"matched_color_{idx}_G"
            if d.get(var_name) is None:
                d[var_name] = []
            d[var_name].append(min_dist_rgb_value[col_idx][idx-1][1])

            var_name = f"matched_color_{idx}_B"
            if d.get(var_name) is None:
                d[var_name] = []
            d[var_name].append(min_dist_rgb_value[col_idx][idx-1][2])

            var_name = f"rgb_matched_color_{idx}"
            if d.get(var_name) is None:
                d[var_name] = []
            d[var_name].append(min_dist_rgb_value[col_idx][idx-1])

            var_name = f"probability_{idx}"
            if d.get(var_name) is None:
                d[var_name] = []
            d[var_name].append(min_dist_values_norm[col_idx][idx-1])

    prediction_df = pd.DataFrame(d)
    
    return prediction_df

def create_merged_debug_results(df1, df2):
    """
    Merge the results df for the 2 scales and op a final df
    """
    df2.drop(columns=['test_color', 'rgb_test', 'test_R', 'test_G', 'test_B'], inplace = True)
    df1.columns = [f"{col_}_scale1" if col_ not in ['test_color', 'rgb_test', 'test_R', 'test_G', 'test_B'] else col_ for col_ in df1.columns]
    df2.columns = [f"{col_}_scale2" for col_ in df2.columns]
    merged_df = pd.concat(objs=[df1, df2], join='inner', axis=1).reset_index()    
    ### check shape
    assert merged_df.shape[0] == df1.shape[0] == df2.shape[0]
    ### check the test_colors
    assert merged_df[['test_color']].equals(df1[['test_color']]) is True
    return merged_df


def find_best_matched_color(debug_results):
    
    """
    Simple: simply pick the max prob color
    """
    prob_cols1 = ['probability_1_scale1', 'probability_2_scale1', 'probability_3_scale1', 'probability_4_scale1', 'probability_5_scale1']
    prob_cols2 = ['probability_1_scale2', 'probability_2_scale2', 'probability_3_scale2', 'probability_4_scale2', 'probability_5_scale2']
    prob_cols = prob_cols1+prob_cols2
    cols_with_max_proba = debug_results[prob_cols].idxmax(axis=1).values.tolist()
    max_proba = debug_results[prob_cols].apply(func=np.max, axis=1).values.tolist()
    
    assert len(cols_with_max_proba) == len(max_proba) == debug_results.shape[0]    
    
    return cols_with_max_proba, max_proba

def create_final_results(debug_results, cols_with_max_proba, max_proba):
    
    d = {
        'test_color' : debug_results['test_color'],
        'rgb_test': debug_results['rgb_test'],
        'test_R': debug_results['test_R'],
        'test_G': debug_results['test_G'],
        'test_B': debug_results['test_B'],
        'matched_color': [],
        'matched_color_R': [],
        'matched_color_G': [],
        'matched_color_B': [],
        'probability': max_proba
    }
    
    for idx, col_ in enumerate(cols_with_max_proba):
        digits = [a for a in col_ if a.isdigit()]
        matched_col_ = f"matched_color_{digits[0]}_scale{digits[1]}"
        matched_col_R_ = f"matched_color_{digits[0]}_R_scale{digits[1]}"
        matched_col_G_ = f"matched_color_{digits[0]}_G_scale{digits[1]}"
        matched_col_B_ = f"matched_color_{digits[0]}_B_scale{digits[1]}"
        d['matched_color'].append(debug_results.loc[debug_results['index'] == idx][matched_col_].values[0])
        d['matched_color_R'].append(debug_results.loc[debug_results['index'] == idx][matched_col_R_].values[0])
        d['matched_color_G'].append(debug_results.loc[debug_results['index'] == idx][matched_col_G_].values[0])
        d['matched_color_B'].append(debug_results.loc[debug_results['index'] == idx][matched_col_B_].values[0])
    
    final_results = pd.DataFrame(d)
    
    return final_results

def generate_rgb_formula(row, col_prefix):
    return f"=myRGB({row[col_prefix+'R']}, {row[col_prefix+'G']}, {row[col_prefix+'B']})"


def write_to_macro_file(final_results):
    rows = dataframe_to_rows(final_results)
    wb = load_workbook(filename=f"{file_path}/output/final_results.xlsm", read_only=False, keep_vba=True)
    ws3 = wb.create_sheet(title="Final_Results")
    for r_idx, row in enumerate(rows, 1):
        for c_idx, value in enumerate(row, 1):
            ws3.cell(row=r_idx, column=c_idx, value=str(value))

    wb.save(f"{file_path}/output/final_results.xlsm")

    return



def main(extracted_items=None, spbx_colours=None, colours_reference=None):
    # sparkbox_colours, large_test_colors, ref_colors = read_input_data()

    #kb test outputs
    #extracted_items.to_pickle('extracted_items.pkl')
    #spbx_colours.to_csv('spbx_colours.csv')
    #colours_reference.to_csv('colours_reference.csv')

    if __name__ == "__main__":
        sparkbox_colours, large_test_colors, ref_colors = read_input_data()
    else:
        sparkbox_colours = spbx_colours.copy()
        large_test_colors = extracted_items.copy()
        ref_colors = colours_reference.copy()


    all_test_colors, all_test_indices = clean_test_color_names(large_test_colors)

    large_test_colors = pd.DataFrame(data={'id': all_test_indices, 'colour': all_test_colors}) ### rolled up test data with / split colors
    large_test_colors = find_missing_matched_colors(large_test_colors, ref_colors)

    ### TODO: check pickle file exists
    large_test_colors = fuzzy_match_closest_color(large_test_colors, ref_colors)
    # # pickle.dump(large_test_colors,  open(f"{file_path}/large_test_colors.pkl", "wb"))

    # large_test_colors = pickle.load(open(f"{file_path}/large_test_colors.pkl", "rb"))

    ### JOIN with ref_colours to get RGB values

    test_colors_merged = pd.merge(left=large_test_colors, right=ref_colors, how='left', left_on='ref_colour', right_on='preprocessed_name')[['id', 'colour', 'ref_colour', 'R', 'G', 'B']].reset_index(drop=False)
    ### there are 2 indices: id: to keep track of original split; index to keep track of sequence when we split again
    ### split into 2 parts : with None ref_colour and with ref_colour
    test_colors_with_ref = test_colors_merged.loc[test_colors_merged['ref_colour'].notnull()]

    ### TODO: no test colors with refrence return
    test_colors_with_NULL_ref = test_colors_merged.loc[test_colors_merged['ref_colour'].isnull()]

    if len(test_colors_with_ref) == 0:
        return pd.DataFrame(data={'index': test_colors_with_NULL_ref['index'], 'colour': test_colors_with_NULL_ref['colour'], 'sparkbox_colour': [None]*len(test_colors_with_NULL_ref)})


    filtered_test_colors = change_format(test_colors_with_ref)


    rgb_tuples_test = np.array([process_rgb_string(rgb_string) for rgb_string in filtered_test_colors['RGB Decimal'].tolist()])
    rgb_tuples_main = np.array([process_rgb_string(rgb_string) for rgb_string in sparkbox_colours['RGB Decimal'].tolist()])

    lab_tuples_test = np.array([rgb2lab(rgb_tuple) for rgb_tuple in rgb_tuples_test])
    lab_tuples_main = np.array([rgb2lab(rgb_tuple) for rgb_tuple in rgb_tuples_main])
    #print (lab_tuples_test)

    
    test_color_distances_scale1 = compute_test_lab_distances(lab_tuples_test, lab_tuples_main)
    test_color_distances_scale2 = compute_test_compuphase_distances(rgb_tuples_test, rgb_tuples_main)
    greyish_indices_ref = dict(create_greyish_ref(sparkbox_colours))

    min_dist_indices, min_dist_values, min_dist_values_norm, min_dist_rgb_value = find_min_distance_indices_scale1(filtered_test_colors, test_color_distances_scale1, rgb_tuples_main, 5, greyish_indices_ref, 
                                                                                                                     True, filter_protocol, list(filter_protocol['matched_color_1'].keys()))
    min_dist_indices2, min_dist_values2, min_dist_values_norm2, min_dist_rgb_value2 = find_min_distance_indices_scale2(filtered_test_colors, test_color_distances_scale2, rgb_tuples_main, 5, greyish_indices_ref, 
                                                                                                                     True, None, list(filter_protocol['matched_color_1'].keys()))


    matched_colors1 = [sparkbox_colours['Sparkbox Colour'][k_indices].tolist() for k_indices in min_dist_indices]
    matched_colors2 = [sparkbox_colours['Sparkbox Colour'][k_indices].tolist() for k_indices in min_dist_indices2]



    debug_results_df1 = create_debug_results(filtered_test_colors, rgb_tuples_test, matched_colors1, min_dist_rgb_value, min_dist_values_norm)
    debug_results_df2 = create_debug_results(filtered_test_colors, rgb_tuples_test, matched_colors2, min_dist_rgb_value2, min_dist_values_norm2)


    merged_debug_df = create_merged_debug_results(debug_results_df1, debug_results_df2)


    # merged_debug_df.to_csv(f"{file_path}/output/{debug_results_file}")

    cols_with_max_proba, max_proba = find_best_matched_color(merged_debug_df)

    final_results = create_final_results(merged_debug_df, cols_with_max_proba, max_proba)
    final_results['matched_color_viz'] = final_results.apply(func=generate_rgb_formula, axis=1, args=('matched_color_', ))
    final_results['test_color_viz'] = final_results.apply(func=generate_rgb_formula, axis=1, args=('test_', ))
    

    #final_results.to_csv(f"{file_path}/output/{final_results_file}")
    # write_to_macro_file(final_results)

    assert ( filtered_test_colors['Color Name'].tolist() == final_results['test_color'].tolist() == test_colors_with_ref['ref_colour'].tolist()) is True

    final_results['index'] = test_colors_with_ref['index'].tolist()
    final_results['test_color'] = test_colors_with_ref['colour'].tolist()


    ###########################
    results_to_return = {
        'index': final_results['index'].tolist() + test_colors_with_NULL_ref['index'].tolist(),
        'colour': final_results['test_color'].tolist() + test_colors_with_NULL_ref['colour'].tolist(),
        'sparkbox_colour': final_results['matched_color'].tolist() + [None] * len(test_colors_with_NULL_ref)
    }
    results_df = pd.DataFrame(results_to_return).sort_values(by='index')

    # results_df.to_csv(f"{file_path}/output/{results_data_file}")

    # print (results_df)

    
    return results_df

    

if __name__=="__main__":
    main()

