"""
Utility functions to support the core scraper modules
"""
import google.auth
import re
import time
import concurrent.futures as futures
import sys
import logging
from logging.handlers import TimedRotatingFileHandler
from pathlib import Path
import datetime
import psutil
import os

from CoreScraperConstants import RESTART_UBUNTU, RESTART_WINDOWS

class Formatter(logging.Formatter):
    """Custom formatter for logging.
    Format different by logging level
    """
    def format(self, record):
        if record.levelno == logging.INFO:
            self._style._fmt = '%(asctime)s - %(levelname)s - %(message)s'
        else:
            self._style._fmt = '%(asctime)s - %(levelname)s - %(module)s - %(lineno)d - %(message)s'
        return super().format(record)

def init_logging(filename, logpath):
    """ Configures logging to output to the screen and to a log file folder
    """
    logger = logging.getLogger() # get the root logger 
    for hdlr in logger.handlers[:]:  # remove the existing file handlers
        #print('Processing handler ', hdlr)
        if isinstance(hdlr,logging.FileHandler) or isinstance(hdlr,logging.StreamHandler) :
            logger.removeHandler(hdlr)
            #print('removing existing handler ', hdlr)
    #set logging level
    logger.setLevel(logging.INFO)
    #set console handler
    ch = logging.StreamHandler(sys.stdout)
    ch.flush = sys.stdout.flush
    ch.setFormatter(Formatter())
    logger.addHandler(ch)
    #set a file handler
    #todayDate = datetime.datetime.today()
    #todayDateStr = todayDate.strftime("%Y%m%d")
    Path(logpath).mkdir(parents=True, exist_ok=True) # this will create the logging folder if it does not already exist

    if "/" in filename:
        logfname = Path(logpath + filename.split('/')[-1].split('.')[0]  +  "_log.log")
    else:
        logfname = Path(logpath + filename.split('\\')[-1].split('.')[0] + "_log.log")
    print('Creating logging file handler ', logfname)
    #standard file name
    #fh = logging.FileHandler(logfname, 'a+')
    #using time rotating file handler
    fh = TimedRotatingFileHandler(filename=logfname, when='D', interval=1, backupCount=14, encoding='utf-8', delay=False)

    fh.setFormatter(Formatter())
    logger.addHandler(fh)
    
    return logger


## configure google credentials
def configure_googleapi_credentials():
    credentials, project = google.auth.default()
    logging.info(('Google Credentials ', credentials, project))
    return credentials, project

#restart computer
def restart_computer():
    """ Function to reboot the computer """
    if os.name == 'posix':
        if RESTART_UBUNTU :
            os.system('sudo reboot')
            #os.system('systemctl reboot -i')
    else:
        if RESTART_WINDOWS:
            os.system("shutdown /r /t 1")

# Helper function to remove invalid charaters 
def removeDisallowedFilenameChars(filename):
    value = re.sub('[^-a-zA-Z0-9_.() ]+', '', filename)
    return value

def closeDriver(driver):
    #if the driver currently is valid, close it
    try:
        session_id = driver.session_id
        #logging.info('Closing driver session  {}'.format(session_id))
        #driver.close()
        driver.quit()
    except Exception as e :
        #logging.info(e)
        #logging.info('Error closing driver {}'.format(e))
        pass


def timeout(timelimit):
    """ a decorator to generate a time out error on a function
    timelimit is in seconds
    """
    def decorator(func):
        def decorated(*args, **kwargs):
            with futures.ThreadPoolExecutor(max_workers=1) as executor:
                future = executor.submit(func, *args, **kwargs)
                try:
                    result = future.result(timelimit)
                except futures.TimeoutError:
                    logging.info('Timedout!')
                    #raise TimeoutError from None
                    sys.exit('Time out stopping !!')
                else:
                    pass
                    #logging.info(result)
                executor._threads.clear()
                futures.thread._threads_queues.clear()
                return result
        return decorated
    return decorator

def print_memory_usage():
    """Reports memory usage"""
    mem_avail = psutil.virtual_memory()[0]/(1024*1024*1024)
    mem_used = psutil.virtual_memory()[3] / (1024*1024*1024)
    pc_mem_used = psutil.virtual_memory()[2]
    logging.info('Memory Available {:.1f}GB, Memory Used {:.1f}GB, % Memory Used {}'.format(mem_avail, mem_used, pc_mem_used))
