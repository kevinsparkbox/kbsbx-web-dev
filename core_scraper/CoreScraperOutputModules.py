# use the runpy modules
import runpy
import datetime as dt
import os
from pathlib import Path
import sys
import traceback
import time
import pandas as pd
import numpy as np
from google.cloud import storage
from google.cloud import bigquery
from retrying import retry
import platform
import datetime
import logging

from CoreScraperConstants import GOOGLE_STORAGE_FILE_PATH, TEST_GOOGLE_STORAGE_FILE_PATH , BUCKET_NAME
from CoreScraperConstants import FRONT_PAGE_ONLY_FOLDER, PROGRESS_FOLDER, PROGRESS_FILE, OUTPUT_OPTION, WEB_RUNTIME_FILE, OUTPUT_TO_DATABASE, DEFAULT_ADD_DATE_TO_OUTPUT
from CoreScraperConstants import GCS_IMAGES_CHILD_FOLDER, GCS_IMAGES_PARENT_FOLDER
from InstaScraperConstants import INSTA_RUNTIME_FILE
from WeatherScraperConstants import WEATHER_RUNTIME_FILE
from CoreScraperErrorModules import send_email_notification, exit_if_exceeded_run_time_limit
from Corescraper_fetch_BQ_data import download_blob_delete, blob_delete
from bigquery_utils import upload_df_bq

#outputResults progress log, email if small number of results
def output_results_progress_log(retailer, toppath, fpath, resultsdf, last_description_resultsdf, starttime, params, problemUrls, google_project, google_credentials):
    opDate = params['todayDateStr']
    endtime = time.ctime()
    #duration in hours, mins, secs format
    #duration = time.strftime("%H:%M:%S",time.gmtime(time.time() - starttime))
    #duration in hours
    duration = (time.time() - starttime)/(60*60)
    if len(resultsdf) > 0:
        numResults = len(resultsdf)
        #before outputting the duplicates, reset the priority so that it contains the number of products from the original link to that webpage
        resultsdf['priority'] = resultsdf.groupby('webPage')['webPage'].transform('count')

        if 'include_date_in_outputcsv' in params:
            include_date = params['include_date_in_outputcsv']
        else:
            include_date = DEFAULT_ADD_DATE_TO_OUTPUT
        
        if include_date:
            opfname =  opDate + 'OutputdataIncDuplicates.csv'
            opfname_descr =  opDate + 'Descriptions.csv'
        else:
            opfname =  'OutputdataIncDuplicates.csv'
            opfname_descr =  'Descriptions.csv'
        #output all results inc duplicates
        if OUTPUT_OPTION == "LocalOnly" or OUTPUT_OPTION == "All":
            #for the local option do not include the date
            outputDataFrameToFile(resultsdf, fpath, 'OutputdataIncDuplicates.csv', "Local", google_project, google_credentials,
                                GOOGLE_STORAGE_FILE_PATH, TEST_GOOGLE_STORAGE_FILE_PATH)
            outputDataFrameToFile(last_description_resultsdf, fpath, 'Descriptions.csv', "Local", google_project, google_credentials,
                                GOOGLE_STORAGE_FILE_PATH, TEST_GOOGLE_STORAGE_FILE_PATH)
        if OUTPUT_OPTION == "GCSOnly" or OUTPUT_OPTION == "All":
            #for the GCS option  include the date if requested
            outputDataFrameToFile(resultsdf, fpath, opfname, "Google_Storage", google_project, google_credentials,
                                GOOGLE_STORAGE_FILE_PATH, TEST_GOOGLE_STORAGE_FILE_PATH)
            outputDataFrameToFile(last_description_resultsdf, fpath, opfname_descr, "Google_Storage", google_project, google_credentials,
                                GOOGLE_STORAGE_FILE_PATH, TEST_GOOGLE_STORAGE_FILE_PATH)
        
            
        #drop the duplicates and keep the lowest priority (when priority = number of products in web page = favours the more detailed category description)
        dedup_resultsdf = resultsdf.copy()
        dedup_resultsdf.sort_values(by = ['priority', 'webPage', 'skuid'], ascending=True, inplace=True) 
        dedup_resultsdf.drop_duplicates(subset=['skuid'], keep='first', inplace=True) # drop any duplicated items
        dedup_resultsdf.reset_index(drop=True, inplace=True)  
        logging.info(('num results before duplicates',numResults))
        logging.info(('num results after duplicates',len(dedup_resultsdf)))

        if len(dedup_resultsdf) <= 100:
            subject =  f"Small number of results Alert  {params['mainurl']} on { platform.node()}! "
            body = f"Got {len(dedup_resultsdf)} results for {params['mainurl']} on { platform.node()}!"
            send_email_notification(subject, body)

        if include_date:
            opfname =  opDate + 'Outputdata.csv'
        else:
            opfname =  'Outputdata.csv'
        if OUTPUT_OPTION == "LocalOnly" or OUTPUT_OPTION == "All":
            #for the local option do not include the date
            outputDataFrameToFile(dedup_resultsdf, fpath, 'Outputdata.csv', "Local", google_project, google_credentials,
                                    GOOGLE_STORAGE_FILE_PATH, TEST_GOOGLE_STORAGE_FILE_PATH)
        if OUTPUT_OPTION == "GCSOnly" or OUTPUT_OPTION == "All":
            #for the GCS option  include the date if requested
            outputDataFrameToFile(dedup_resultsdf, fpath, opfname, "Google_Storage", google_project, google_credentials,
                                    GOOGLE_STORAGE_FILE_PATH, TEST_GOOGLE_STORAGE_FILE_PATH) 

        #output the full results to the database
        if OUTPUT_TO_DATABASE:
            if fpath.find('TEST') == -1:
                dataset = params['GBQ_DATASET']
                results_table = params['GBQ_TABLE_RESULTS']
            else:
                dataset = params['TEST_GBQ_DATASET']
                results_table = params['TEST_GBQ_TABLE_RESULTS']

            location = "Big_Query"
            if_exists_operation = "append"
            outputDataFrameToDatabase(resultsdf, dataset, results_table, location, if_exists_operation, google_project, google_credentials)

        #output details to log file
        location = "Local"
        if not exit_if_exceeded_run_time_limit(params, "", None):
            textToOp = opDate + ',' + 'Succesful' +','   + endtime + ',' + str(duration) +'\n'
        else:
            textToOp = opDate + ',' + 'Exceeded RunTime' +','   + endtime + ',' + str(duration) +'\n'

        fname = 'LOG.csv'
        logging.info(('Outputting to LOG File', fpath, fname))
        outputToFile(fpath, fname, textToOp, "a", location, google_project, google_credentials) 
        
        #output details to progress file
        #update the progress file on GS, a convoluted progress as GS does not support append to file, read the local file, append, and then write to GS)
        logging.info(('Outputting to PROGRESS File', toppath))
        #create the output progress message
        if not exit_if_exceeded_run_time_limit(params, "", None):
            textToOp = opDate  + ',' + retailer +','  + 'Succesful' +','   + endtime + ',' + str(duration) + ',' + str(len(resultsdf)) +  ',' + platform.node() + '\n'
        else:
            textToOp = opDate + ','+ retailer +','  + 'Exceeded RunTime' +','   + endtime + ',' + str(duration) + ',' + str(len(resultsdf)) + ',' + platform.node() + '\n'
        fname =  PROGRESS_FILE
        #create the local output path if it does not already exist
        createOutputPath(PROGRESS_FOLDER, toppath, location)
        #configure the GS folder path
        if fpath.find('TEST') == -1:
            GSPath = GOOGLE_STORAGE_FILE_PATH
        else:
            GSPath = TEST_GOOGLE_STORAGE_FILE_PATH 
        folderpath = GSPath +  PROGRESS_FOLDER 
        #download the progress blob from GS
        download_success = download_blob_delete(google_credentials, google_project, BUCKET_NAME, folderpath, fname, toppath + PROGRESS_FOLDER + '/' +fname, False)
        if not download_success:
            #progress file does not exist on GS so start a new one, delete any existing local files
            try:
                os.remove(toppath + '/' + PROGRESS_FOLDER +  '/' + fname)
                logging.info('Deleted local progress file')
            except:
                logging.info('No local progress file to delete ')
        #append the latest progress info
        outputToFile(toppath + '/' + PROGRESS_FOLDER, fname, textToOp, "a", "Local", google_project, google_credentials) 
        #upload the local file back to GS
        upload_blob(toppath + PROGRESS_FOLDER + '/' +fname, folderpath + '/' + fname, google_project, google_credentials, BUCKET_NAME)
        logging.info('Uploaded PROGRESS File to GS')
        
        #if there are problem urls, output them to the problem urls file
        if len(problemUrls) > 0:
            for url in problemUrls:
                location = "Local"
                textToOp = opDate + ',' + url +'\n'
                fname = 'PROBLEMLOG.csv'
                outputToFile(fpath, fname, textToOp, "a", location, google_project, google_credentials)
    else:
        subject =  f"Small number of results Alert  {params['mainurl']} on { platform.node()}! "
        body = f"Got {len(resultsdf)} results for {params['mainurl']} on { platform.node()}!"
        send_email_notification(subject, body)

    return


# function to return today's date and day as strings
def get_today_date_day_str():
    #get todays date and day of week
    todayDate = datetime.datetime.today()
    #convert to strings
    todayDateStr = todayDate.strftime("%Y%m%d")
    return todayDateStr


#generate an error log and update an error notification to the progress file
def generate_error_log_update_progress(mod_name,outputpath, starttime, google_project, google_credentials,
                                        google_storage_file_path, test_google_storage_file_path):

    #duration = time.strftime("%H:%M:%S",time.gmtime(time.time() - starttime))
    duration = (time.time() - starttime)/(60*60)

    endtime = time.ctime()
    fpath = outputpath + '/' + mod_name
    #current, always write the error log to the local machine
    location = "Local"
    todayDateStr = get_today_date_day_str()
    outputErrorLog(fpath, todayDateStr, location, google_project, google_credentials)  

    #output error log details to progress file
    #update the progress file on GS, a convoluted progress as GS does not support append to file, read the local file, append, and then write to GS)
    logging.info(('Outputting ERROR MESSAGE to PROGRESS File', outputpath))
    error_message = "Error-see log"
    resultsdf=[]
    textToOp = todayDateStr + ','+ mod_name +','  + error_message +','   + endtime + ',' + str(duration) + ',' + str(len(resultsdf)) + ',' + platform.node() + '\n'
    fname =  PROGRESS_FILE
    #create the local output path if it does not already exist
    createOutputPath(PROGRESS_FOLDER, outputpath, location)
    #configure the GS folder path
    if fpath.find('TEST') == -1:
        GSPath = google_storage_file_path
    else:
        GSPath = test_google_storage_file_path 
    folderpath = GSPath +  PROGRESS_FOLDER 
    #download the progress blob from GS
    download_success = download_blob_delete(google_credentials, google_project, BUCKET_NAME, folderpath, fname, outputpath + PROGRESS_FOLDER + '/' +fname, False)
    if not download_success:
        #progress file does not exist on GS so start a new one, delete any existiong local files
        try:
            os.remove(outputpath + '/' + PROGRESS_FOLDER +  '/' + fname)
            logging.info('Deleted local progress file')
        except:
            logging.info('No local progress file to delete ')
    #append the latest progress info
    outputToFile(outputpath + '/' + PROGRESS_FOLDER, fname, textToOp, "a", "Local", google_project, google_credentials) 
    #upload the local file back to GS
    upload_blob(outputpath + PROGRESS_FOLDER + '/' +fname, folderpath + '/' + fname, google_project, google_credentials, BUCKET_NAME)
    logging.info('Uploaded PROGRESS File to GS')


#function to find which image files have already been extracted and stored in GCS
@retry(stop_max_delay=20000, wait_fixed=1000) #try for 20 seconds, wait 1 second between tries
def load_image_filenames_from_gcs(fpath, google_project, google_credentials):
    #construct the google storage path
    Retailer_folder = fpath.split('/')[-1].split('\\')[-1] #take the lowest level folder from the local path (toppath)
    GS_folder = Retailer_folder 
    #add image child and parent folders if the file type is wb
   
    if GCS_IMAGES_PARENT_FOLDER != "":
        GS_folder = GCS_IMAGES_PARENT_FOLDER  + '/' + GS_folder
    if GCS_IMAGES_CHILD_FOLDER != "":
        GS_folder = GS_folder + '/' + GCS_IMAGES_CHILD_FOLDER

    if fpath.find('TEST') == -1:
        GSPath = GOOGLE_STORAGE_FILE_PATH
    else:
        GSPath = TEST_GOOGLE_STORAGE_FILE_PATH     

    folderpath = GSPath + GS_folder +'/'

    storage_client = storage.Client(project=google_project, credentials=google_credentials)
    bucket = storage_client.get_bucket(BUCKET_NAME)
    blobs = bucket.list_blobs(prefix=str(folderpath))
    fnames = []
    for blob in blobs:
        #logging.info(blob.name)
        if (not blob.name.endswith("/")):
            fn = blob.name.split('/')[-1]
            fn = fn.split('.')[0]
            fnames.append(fn)
    return fnames

# function to check if output has already been generated
@retry(stop_max_delay=20000, wait_fixed=1000) #try for 20 seconds, wait 1 second between tries
def check_if_output_exists(fpath,nameToCheck, location, google_project, google_credentials ):
    if fpath.find('TEST') == -1:
        GSPath = GOOGLE_STORAGE_FILE_PATH
    else:
        GSPath = TEST_GOOGLE_STORAGE_FILE_PATH
        
    if location == "Local" :
        fileexists =  os.path.isfile(Path(fpath)/ (nameToCheck))
    elif location == "Google_Storage" :
        #construct the google storage path
        GS_folder = fpath.split('/')[-1].split('\\')[-1] #take the lowest level folder from the local path (toppath)
        folderpath =  GSPath + GS_folder +'/'
        gspath =  folderpath  + nameToCheck 
        storage_client = storage.Client(project=google_project, credentials=google_credentials)

        bucket = storage_client.get_bucket(BUCKET_NAME)
        blob = bucket.get_blob(gspath)
        try:
            fileexists = blob.exists()
        except:
            fileexists = False
    return fileexists

# function to find the interval in days to when the output was generated 
#add the retry decorator as GCP occassionaly generates 503 internal not available errors
@retry(stop_max_delay=20000, wait_fixed=1000) #try for 20 seconds, wait 1 second between tries
def find_days_since_output_updated(fpath,nameToCheck, location, google_project, google_credentials):
    from datetime import datetime , timezone

    if fpath.find('TEST') == -1:
        GSPath = GOOGLE_STORAGE_FILE_PATH
    else:
        GSPath = TEST_GOOGLE_STORAGE_FILE_PATH    
    
    todayDate = dt.datetime.today().date()
    if location == "Local" :
        if check_if_output_exists(fpath,nameToCheck, location, google_project, google_credentials):
            fileModTime = os.path.getmtime(Path(fpath)/ (nameToCheck)) 
            filemodDate = dt.datetime.fromtimestamp(fileModTime)
            filemodDate = filemodDate.date()
            daysSinceMod = (todayDate-filemodDate).days
            hoursSinceMod = 0 # not calculated TODO
        else:
            daysSinceMod = 9999
            hoursSinceMod = 9999
    elif location == "Google_Storage" :
        if check_if_output_exists(fpath,nameToCheck, location, google_project, google_credentials):
            #construct the google storage path
            GS_folder = fpath.split('/')[-1].split('\\')[-1] #take the lowest level folder from the local path (toppath)
            folderpath = GSPath + GS_folder +'/'
            gspath =  folderpath  + nameToCheck 
            
            #storage_client = storage.Client(credentials=credentials, project=project)
            storage_client = storage.Client(project=google_project, credentials=google_credentials)
            bucket = storage_client.get_bucket(BUCKET_NAME)
            blob = bucket.get_blob(gspath)
            updated = blob.updated # updated is a date time 
            filemodDate = updated.date()
            daysSinceMod = (todayDate-filemodDate).days
            current_time = datetime.now(timezone.utc)
            #daysSinceMod = (current_time - updated).days
            hoursSinceMod = ((current_time - updated).seconds)/(60*60)  # excluding days
        else:
            daysSinceMod = 9999
            hoursSinceMod = 9999
            
    return daysSinceMod, hoursSinceMod


# Function to output errors to an error log
def outputErrorLog(fpath, todayDateStr, location, google_project, google_credentials ): 
    """Output the error log to local storage """  
    if fpath.find('TEST') == -1:
        GSPath = GOOGLE_STORAGE_FILE_PATH
    else:
        GSPath = TEST_GOOGLE_STORAGE_FILE_PATH    

    if "insta_" in fpath.lower():
        #adjust the file path so that it does not include the insta_ part
        fpath = fpath.replace("insta_","").replace("Insta_", "") 
    
    opObject = "ERROR at " + todayDateStr
    if location == "Local" :
        Path(fpath).mkdir(parents=True, exist_ok=True) # this will create the directory if it does not already exist
        with open(Path(fpath)/ (todayDateStr + 'ErrorLog.txt'), 'a') as log:
            log.write(opObject)
            traceback.print_exc(file=log)

    return

# Function to output to a file, optype = w for text, wb for images etc, a to append
# NOTE Append is not supported on gcs as files are immutable, for now overwrite
# NOTE Special processing of GCS bucket folder if an image file, to have the option to store in a common folder, assume image file if filetype = "wb"
#add the retry decorator as GCP occassionaly generates 503 internal not available errors
@retry(stop_max_delay=20000, wait_fixed=1000) #try for 20 seconds, wait 1 second between tries
def outputToFile(fpath, fname, opObject, opType, location, google_project, google_credentials):   
    if fpath.find('TEST') == -1:
        GSPath = GOOGLE_STORAGE_FILE_PATH
    else:
        GSPath = TEST_GOOGLE_STORAGE_FILE_PATH      
    
    logging.info(('In OutputToFile', fname, opType, location))
    if location == "Local" :
        if opType == "wb": # image file
            fpath = fpath + '/Images' # store the images in a sub folder
            Path(fpath).mkdir(parents=True, exist_ok=True) # this will create the directory if it does not already exist
            output_name =  Path(fpath)/ (fname)
            with open(output_name, opType) as output_file:
                output_file.write(opObject)
        else:
            Path(fpath).mkdir(parents=True, exist_ok=True) # this will create the directory if it does not already exist
            output_name =  Path(fpath)/ (fname)
            with open(output_name, opType, encoding="utf-8") as output_file:
                output_file.write(opObject)
    elif location == "Google_Storage" :
        #Note - only used/tested for image upload - uses blob.upload_from_string 
        #construct the GCS output path
        Retailer_folder = fpath.split('/')[-1].split('\\')[-1] #take the lowest level folder from the local path (toppath)
        if opType == "wb":
            if GCS_IMAGES_PARENT_FOLDER != "":
                GS_folder = GCS_IMAGES_PARENT_FOLDER  + '/' + Retailer_folder
            if GCS_IMAGES_CHILD_FOLDER != "":
                GS_folder = GS_folder + '/' + GCS_IMAGES_CHILD_FOLDER
        else: # not an image
            GS_folder = Retailer_folder

        folderpath = GSPath + GS_folder +'/'

        storage_client = storage.Client()
        bucket = storage_client.get_bucket(BUCKET_NAME)
        blob = bucket.blob(folderpath  + fname)
        content_type = 'image'
        blob.upload_from_string(opObject, content_type=content_type)
        #blob.upload_from_string(opObject)

    return

# Function to write a dataframe to file
#add the retry decorator as GCP occassionaly generates 503 internal not available errors
@retry(stop_max_delay=20000, wait_fixed=1000) #try for 20 seconds, wait 1 second between tries
def outputDataFrameToFile(df, fpath, fname, location, google_project, google_credentials,
                         google_storage_file_path, test_google_storage_file_path):
    if fpath.find('TEST') == -1:
        GSPath = google_storage_file_path
    else:
        GSPath = test_google_storage_file_path     
    
    if location == "Local" :
        Path(fpath).mkdir(parents=True, exist_ok=True) # this will create the directory if it does not already exist
        df.to_csv( Path(fpath)/ (fname),index=False, encoding='utf-8')
    elif location == "Google_Storage" :
        GS_folder = fpath.split('/')[-1].split('\\')[-1] #take the lowest level folder from the local path (fpath)
        folderpath = GSPath + GS_folder +'/'
        #write the outut of the df.to_csv to Google Storage  NB Pandas works but produces an exra blak line after every row
        #gcs.Bucket(BUCKET_NAME).item(folderpath + fname).write_to(df.to_csv(index=False, encoding='utf-8'),'text/csv')
        #gspath = 'gs://' + BUCKET_NAME + '/' + folderpath +'2' + fname 
        #df.to_csv( gspath, index=False, encoding='utf-8', line_terminator='')
        #the gcs.Bucket approach does not work on Ubuntu comes up with encoding errors, so work around is to save locally then send then delete local copy
        temp_fname = str(Path(fpath)/ ('Temp.csv'))
        df.to_csv( temp_fname,index=False, encoding='utf-8')
        #upload it to google storage
        upload_blob(temp_fname, folderpath + fname, google_project, google_credentials, BUCKET_NAME)
        #delete the local copy
        os.remove(temp_fname)
    return


# Function to write a dataframe to google storage big query

#force the schema to match GCP table
#add the retry decorator as GCP occassionaly generates 503 internal not available errors
@retry(stop_max_delay=20000, wait_fixed=1000) #try for 20 seconds, wait 1 second between tries
def outputDataFrameToDatabase(df, dataset, table, location, if_exists_operation, google_project, google_credentials):

    if len(df) > 0:
        #test to see if destination is a results table
        if 'skuid' in df.columns and 'ticketprice' in df.columns:
        #force the dataframe to follow the big query schema for the results
            df['skuid'] = df['skuid'].astype('str')
            df['date'] = df['date'].astype('datetime64')
            df['department'] = df['department'].astype('str')
            df['category'] = df['category'].astype('str')
            df['subcategory'] = df['subcategory'].astype('str')
            df['description'] = df['description'].astype('str')
            df['brand'] = df['brand'].astype('str')
            df['ticketprice'] = df['ticketprice'].astype('float64')
            df['prevprice'] = df['prevprice'].astype('float64')
            df['banner'] = df['banner'].astype('str')
            df['reviewscore'] = df['reviewscore'].astype('str')
            df['reviewcount'] = df['reviewcount'].astype('str')
            df['url'] = df['url'].astype('str')
            df['imgurl'] = df['imgurl'].astype('str')
            df['promomessage'] = df['promomessage'].astype('str')
            df['webPage'] = df['webPage'].astype('str')
            df['priority'] = df['priority'].astype('int64')
            df['stock'] = df['stock'].astype('str')
            df['retailer'] = df['retailer'].astype('str')
            df['website_position'] = df['website_position'].astype('int64') 
        #test to see if destination is an instagram table
        elif 'post_url' in df.columns and 'post_text' in df.columns:
            df['retailer']  = df['retailer'].astype('str')
            df['date'] = df['date'].astype('str')
            df['post_url'] = df['post_url'].astype('str')
            df['likes'] = df['likes'].astype('int64')
            df['views'] = df['views'].astype('int64')
            df['post_text'] = df['post_text'].astype('str')
            df['post_products'] = df['post_products'].astype('str')
            df['post_hashtags'] = df['post_hashtags'].astype('str')
            df['media_url'] = df['media_url'].astype('str')
            df['media_type'] = df['media_type'].astype('str')
            df['post_date'] = df['post_date'].astype('str')
            df['post_time'] = df['post_time'].astype('str')
            df['comments'] = df['comments'].astype('str')
            df['comments_count'] = df['comments_count'].astype('int64')
            df['comments_hashtags'] = df['comments_hashtags'].astype('str')
            df['influencers'] = df['influencers'].astype('str')
            df['position'] = df['position'].astype('int64')
        #test to see if destination is a weather table
        elif 'temp_max' in df.columns and 'location' in df.columns:
            #force string columns to string and date to datetime
            for c in df.columns:
                if c in ['date', 'modified_date']:
                    df[c] = df[c].astype('datetime64')
                elif c in ['location']:
                    df[c] = df[c].astype('str')
                elif c in ['year', 'month']:
                    df[c] = df[c].astype('int')
                else:
                    df[c] = df[c].astype('float')
        else: # the destination is a descriptions table
            #add the attribute colummns if not already present
            attr_cols = [ 'department_orig','category_orig','subcategory_orig', 'colour_orig','short_description_orig', 'long_description_orig',
            'spbx_department','spbx_category','spbx_subcat','spbx_colour','style','fabric']
            for c in attr_cols:
                if c not in df.columns:
                    df[c] = None
            #force all columns to string, unless specified as something else
            for c in df.columns:
                if c in ['date']:
                    df[c] = df[c].astype('datetime64')
                else:
                    df[c] = df[c].astype('str')
            
        if location == "Big_Query" :    
            ref = dataset + '.' + table
            logging.info(('Outputting to GBQ', ref))
            if if_exists_operation == 'replace':
                replace = True
            else:
                replace = False
            upload_df_bq(df, table_id=ref, replace=replace, wait_for_finish=False)
            #gbq.to_gbq(df, ref, project_id=google_project, credentials=google_credentials, if_exists=if_exists_operation) # if exists = 'fail', 'replace', or 'append'
    else:
        logging.info(('Empty DF NOT outputting df to BQ ', dataset, table)) #if there are no new images or descriptions this will be the case

    return


#Function to read a dataframe to google storage big query
#add the retry decorator as GCP occassionaly generates 503 internal not available errors
@retry(stop_max_delay=20000, wait_fixed=1000) #try for 20 seconds, wait 1 second between tries
def readDataFrameFromDatabase(dataset, table, location, limit, google_project):
    tablename = dataset + '.' + table
    #read the table if it exists, return empty dataframe if not
    try:
        if location == "Big_Query" :           
            query = " SELECT * FROM "  + tablename + " LIMIT " + str(limit) + "; "       
            #df = pd.read_gbq(query, project_id=GOOGLE_PROJECT, dialect='standard')
            df = pd.read_gbq(query, project_id=google_project, dialect='standard')
    except:
        logging.info(('Problem reading table ',tablename ))
        df = pd.DataFrame()
    return df


#%Function to read a dataframe from file
#add the retry decorator as GCP occassionaly generates 503 internal not available errors
@retry(stop_max_delay=20000, wait_fixed=1000) #try for 20 seconds, wait 1 second between tries
def readDataFrameFromFile( fpath, fname, location):
    if fpath.find('TEST') == -1:
        GSPath = GOOGLE_STORAGE_FILE_PATH
    else:
        GSPath = TEST_GOOGLE_STORAGE_FILE_PATH     
    
    if location == "Local" :
        df  = pd.read_csv( Path(fpath)/ (fname), parse_dates=True, dayfirst = True)
    elif location == "Google_Storage" :
        GS_folder = fpath.split('/')[-1].split('\\')[-1] #take the lowest level folder from the local path (toppath)
        folderpath = GSPath + GS_folder +'/'
        gspath = 'gs://' + BUCKET_NAME + '/' + folderpath + fname
        df = pd.read_csv(gspath,  parse_dates=True, dayfirst = True)
    return df    


# Function to save Pillow Screenshot images to PDF
#add the retry decorator as GCP occassionaly generates 503 internal not available errors
@retry(stop_max_delay=20000, wait_fixed=1000) #try for 20 seconds, wait 1 second between tries
def saveScreenShottoPDF(screenshot, fpath, fname, location, google_project, google_credentials):
    if fpath.find('TEST') == -1:
        GSPath = GOOGLE_STORAGE_FILE_PATH
    else:
        GSPath = TEST_GOOGLE_STORAGE_FILE_PATH  

    OP_folder = fpath.split('/')[-1].split('\\')[-1] #take the lowest level folder from the local path (toppath)
    if location == "Local" :
        #create the path if it does not yet exist
        Path(fpath).mkdir(parents=True, exist_ok=True)
        opname = Path(fpath )/ (fname)

        #PDF version
        #screenshot.save(opname)
        #Jpeg version
        jpgopname = str(opname).replace('pdf','jpg')
        logging.info(('Saving SS Local', str(jpgopname) ))
        screenshot.save(jpgopname, 'JPEG', optimize=True, quality=95)
    elif location == "Google_Storage" :
        if 'fp_' in OP_folder:
            folderpath = GSPath + FRONT_PAGE_ONLY_FOLDER + '/' + OP_folder  + '/'
        else:
            folderpath = GSPath  + OP_folder +'/FrontPage/'
        logging.info(('Saving SS to GCS', folderpath  ))
        #may be a better way but this works
        #Save the PDF version
        #save the screenshot to a temporary file
        temp_name = str(Path(fpath)/ ('temp_' + fname))
        #screenshot.save(temp_name)
        #upload it to google storage
        #upload_blob(temp_name, folderpath + fname, project, google_credentials, BUCKET_NAME)
        #delete the local copy
        #os.remove(temp_name)
        #Save the jpg version
        jpgopname = str(fname).replace('pdf','jpg')
        jpgtemp_name = (temp_name).replace('pdf','jpg')
        screenshot.save(jpgtemp_name, 'JPEG', optimize=True, quality=95)
        #upload it to google storage
        upload_blob(jpgtemp_name, folderpath + jpgopname, google_project, google_credentials, BUCKET_NAME)
        #delete the local copy
        os.remove(jpgtemp_name)
    return

# Creates an output path with  toppath as the parent folder, and creates a sub folder with the same name as the Python file
def createOutputPath(pythonFname, toppath, location):
    fpath = ""
    if location == "Local" :
        if pythonFname.find('\\') > -1 :
            currpythonFname =  pythonFname.split('\\')[-1].split('.')[0]
        else:
            currpythonFname =  pythonFname.split('/')[-1].split('.')[0]
        fpath = toppath + currpythonFname
        Path(fpath).mkdir(parents=True, exist_ok=True) # this will create the directory if it does not already exist
        Path(fpath+'/Images/').mkdir(parents=True, exist_ok=True)
        Path(fpath+'/Text/').mkdir(parents=True, exist_ok=True)
        logging.info(('Output saving to ', fpath))
    elif location == "Google_Storage" :
        #Not required with Google storage as folder path is part of blob name
        pass
    return fpath


# Function to upload file from local folder onto Google Storage destination
#Upload file from source to destination
#add the retry decorator as GCP occassionaly generates 503 internal not available errors
@retry(stop_max_delay=60000, wait_fixed=2000) #try for 60 seconds, wait 2 second between tries
def upload_blob(source_file_name, destination_blob_name, google_project, google_credentials, bucket_name):
    storage_client = storage.Client(project=google_project, credentials=google_credentials)
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)
    blob.upload_from_filename(source_file_name)
    return

#Function to iterate through all files in a local directory and upload one by one using the same filename
def upload_files(local_directory, bucket_directory, google_project, google_credentials ):
    for filename in os.listdir(local_directory):
        upload_blob(local_directory + filename, bucket_directory + filename, google_project, google_credentials, BUCKET_NAME)
    return "File uploaded!"

#def output a dummy file to indicate the run start time
def output_running_file(fpath, google_project, google_credentials, google_storage_file_path, test_google_storage_file_path):
    logging.info(('Output Running File, Scraper Name', fpath))
    if "insta_" in fpath.lower():
        #configure the name of the runtime file
        opfname = INSTA_RUNTIME_FILE
    elif "weather_" in fpath.lower():
        #configure the name of the runtime file
        opfname = WEATHER_RUNTIME_FILE
    else:
        opfname =  WEB_RUNTIME_FILE
    opfdata = pd.DataFrame()
    if OUTPUT_OPTION == "LocalOnly" or OUTPUT_OPTION == "All":
        outputDataFrameToFile(opfdata, fpath, opfname, "Local", google_project, google_credentials,
                             google_storage_file_path, test_google_storage_file_path)
    if OUTPUT_OPTION == "GCSOnly" or OUTPUT_OPTION == "All":
        outputDataFrameToFile(opfdata, fpath, opfname, "Google_Storage", google_project, google_credentials,
                            google_storage_file_path, test_google_storage_file_path)      

#delete the a dummy file to indicate the run start time
def delete_running_file(fpath, google_project, google_credentials, google_storage_file_path, test_google_storage_file_path ):
    if "insta_" in fpath.lower():
        #configure the name of the runtime file
        opfname = INSTA_RUNTIME_FILE
    elif "weather_" in fpath.lower():
        #configure the name of the runtime file
        opfname = WEATHER_RUNTIME_FILE
    else:
        opfname =  WEB_RUNTIME_FILE

    if fpath.find('TEST') == -1:
        GSPath = google_storage_file_path
    else:
        GSPath = test_google_storage_file_path     
     
    GS_folder = fpath.split('/')[-1].split('\\')[-1] #take the lowest level folder from the local path (fpath)
    folderpath = GSPath + GS_folder 
    print('Attempting to delete from : ', folderpath)

    #delete the run time marker file on GS so that the scraper can run again
    blob_delete(google_credentials, google_project, BUCKET_NAME, folderpath, opfname)




#function to remove duplicates from the descriptions big query database
@retry(stop_max_delay=60000, wait_fixed=10000) #try for 60 seconds, wait 10 second between tries
def remove_duplicates_descriptions(dataset, results_table,project_id, credentials, ):

    logging.info('Removing Duplicates from BQ database')
    query_string = """
            #deduplicate on skuid and retailer, keeping the latest using date time column
            CREATE OR REPLACE TABLE XXDESCRIPTIONS_TABLEXX
            AS
            SELECT descr.* FROM (
            SELECT ARRAY_AGG(
            t ORDER BY t.date DESC LIMIT 1
            ) [OFFSET(0)] descr
            FROM XXDESCRIPTIONS_TABLEXX t
            GROUP BY retailer, skuid
            )
    """

    query_string = query_string.replace( "XXDESCRIPTIONS_TABLEXX", dataset + "." + results_table )
    # Make client nd run query job
    bqclient = bigquery.Client(credentials= credentials, project=project_id )
    bqclient._http.adapters['https://']._pool_connections=100
    bqclient._http.adapters['https://'].pool_maxsize=100
    bq_job = bqclient.query(query_string, project=project_id)
    bq_job.result()  # Waits for job to complete.

    logging.info(('{} Removed duplicates in {} project '.format(dataset + "." + results_table, project_id)))
    return      

#function to remove duplicates from the weather big query database
@retry(stop_max_delay=60000, wait_fixed=10000) #try for 60 seconds, wait 10 second between tries
def remove_duplicates_weather(dataset, results_table,project_id, credentials, ):

    logging.info('Removing Duplicates from BQ database')
    query_string = """
            #deduplicate on location and date, keeping the latest using modified date time column
            CREATE OR REPLACE TABLE XXWEATHER_TABLEXX
            AS
            SELECT descr.* FROM (
            SELECT ARRAY_AGG(
            t ORDER BY t.modified_date DESC LIMIT 1
            ) [OFFSET(0)] descr
            FROM XXWEATHER_TABLEXX t
            GROUP BY location, date
            )
    """

    query_string = query_string.replace( "XXWEATHER_TABLEXX", dataset + "." + results_table )
    # Make client nd run query job
    bqclient = bigquery.Client(credentials= credentials, project=project_id )
    bqclient._http.adapters['https://']._pool_connections=100
    bqclient._http.adapters['https://'].pool_maxsize=100
    bq_job = bqclient.query(query_string, project=project_id)
    bq_job.result()  # Waits for job to complete.

    logging.info(('{} Removed duplicates in {} project '.format(dataset + "." + results_table, project_id)))
    return      