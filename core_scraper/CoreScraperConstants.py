# -*- coding: utf-8 -*-

import os

RETAILERS_TO_TRANSLATE = ['PullandBear', 'HandMFR', 'MangoFR', 'Stradivarius', 'Bershka', 'Pimkie']
COLS_TO_TRANSLATE =  ['department', 'category', 'subcategory', 'colour', 'short_description', 'long_description'] 

#USER_AGENT fetch from https://www.iplocation.net/
USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.75 Safari/537.36"

NUM_PROCESSORS_MULTIPLIER = 1.0 # run X as many cpus processes in Pool or Threads in ThreadPool
RESTART_UBUNTU = True
RESTART_WINDOWS = False

EMAIL_NOTIFICATION_RECIPIENTS = ['kevin@sparkbox.ai',  'kevin.d.blackmore@gmail.com']

#folder with chrome data cookies etc  USER_AGENT fetch from https://www.iplocation.net/
if os.name == 'posix':
    #MASTER_CHROME_DATA_PATH = "/home/kevin_d_blackmore/kbsbx-web-dev/"  # the path to the code repro
    MASTER_CHROME_DATA_PATH = "/_chrome-data/" #the path in the code repo to the chrome data files
    MASTER_CHROME_DATA_DIR = "chrome-data"
    CHROME_USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36"
else:
    #MASTER_CHROME_DATA_PATH = "C:\\Users\\kevin\\OneDrive\\Documents\\Repos\\kbsbx-web-dev\\" #the path to the code repro
    MASTER_CHROME_DATA_PATH = "/_chrome-data/" #the path in the code repo to the chrome data files
    MASTER_CHROME_DATA_DIR = "chrome-data_win"
    CHROME_USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.75 Safari/537.36"


#configure toppath - for compatibality with older versions
toppath = 'placeholder'

#link files and logs etc are written to the local machine
#OUTPUT_OPTION = "LocalOnly"
#OUTPUT_OPTION = "GCSOnly"
OUTPUT_OPTION = "All"

#option to write to a database (currently Google Big Query)
OUTPUT_TO_DATABASE = True

BUCKET_NAME = 'sparkbox-competition-data'
GOOGLE_STORAGE_FILE_PATH =  "data/"
TEST_GOOGLE_STORAGE_FILE_PATH =  "testdata/"
FRONT_PAGE_ONLY_FOLDER = "AAA_FRONT_PAGE_ONLY"
PROGRESS_FOLDER = "AAA_PROGRESS"
PROGRESS_FILE = "progress.csv"
WEB_RUNTIME_FILE = "web_running.csv"
WEB_RESULTS_FILE = "Outputdata.csv"
WEBSITES_FOLDER = "AAA_WEBSITES"
WEBSITES_FILE = 'webpages_priority.csv'
KEYWORDS_FOLDER = "AAA_KEYWORDS"
CONFIGURATION_FILE = 'configuration.csv'
SPARKBOX_COLOURS_FILE = 'sparkbox_base_colours.csv'
REFERENCE_COLOURS_FILE = 'color_names_consolidated.xlsx'

GCS_IMAGES_PARENT_FOLDER = "AAA_IMAGES"
GCS_IMAGES_CHILD_FOLDER = ""

#for testing ignore existing images, force saving of images and descriptions
IGNORE_EXISTING_IMAGES = False
#IGNORE_EXISTING_IMAGES = True

#disable translation
DISABLE_TRANSLATION = True

#define whether or not to add the date to the output csvs stored on google storage
DEFAULT_ADD_DATE_TO_OUTPUT = False

#to loop continuously set to True, otherwise runs a single loop (all high and medium priority and 1st low priority)
CONTINUOUS_LOOP = True

#run time limit constants
RUNTIME_MULTIPLIER = 1.0
MINIMUM_RUNTIME = 60*60*24*5  #change to 5 days, is overwritten when run in batch mode by parameter in csv config file

#default parameter definitions set up in params
default_params = {
    'mainurl' : 'https://www.retailerwebsite.com',
    #dcontrol the generation of web links  , links generated based on the 'linkurls' list of starting urls
    'sitemap_links' : [],  #if empty search through the linkurls, if defined use the sitemap links
    'validtags' : ['www.'],  # a valid link must contain one of these strings
    'invalidtags' : ['brand', 'wcs', 'app', 'content', 'travel-money', ], # if a link contains one of these it is invalid
    'resetduplicatelinks' : True, # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks' : [], # starting list of duplicate links, these links are alwas excluded from the web scrape
    'starting_links' : [],    #additional links to process  #set any starting links that will not be picked up by the find starting pages process
    'tagref' : 'a', # look for hrefs in this class
    'remove_switches_link' : True,  # if True, remove ? and # switches from link (recommended unless required)
    'url_to_prefix_to_link' : None, # string prefixed to links if http is missing, default is to prefix the mainurl
    'departmentlevel' : 3, #the nth level in the link
    'categorylevel' : 4,  #the nth level in the link
    'subcategorylevel' : 5,  #the nth level in the link
    'minimumdepth' : 4 ,  #a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    'maximumdepth' : 999 ,  #a page link has not to exceed the maximum depth
    'max_link_depth' : 1,  # the depth of url links to search in the links searching process
    #define how the web page is accessed
    'htmlParser' :  'lxml', # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    # url and redirect parameters
    'urlSwitch' :  '',    #add an option to the url,e.g. 48 items per page
    'invalidRedirect' : '', # if the redirected url contains this value the product pages do not load correctly
    #webdriver parameters
    #'webdriverpath' : 'dummy' , # path to the webdriver executable SET UP IN CoreScraper.py
    'displayOff' : False,  # whether display is switched off or visible
    'displayMinimised' :  False,  # whether to minimise the window if display is on
    'resetDriver' : False,  #whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'resetCookies' : False, #if True cookies are copied across from a saved Cookies folder (ignored for vpn = always copied), also if True cookies copied across on each product page
    'disable_image_loading' : False,  #if True stops images from loading, saving download bandwidth
    #page waits, scrolling and next page
    'waitForPageLoad' : 3, #typ set to half webPageWait, when scrolling, time to wait for page to load before comparing 2 subsequent loads to see if scrolled page has finished loading
    'webPageWait' : 3,  # time between subsequent web pages (next link, next page, next scroll down page)
    'webPageScrollWait' : 2, # time to scroll up and down at the end of a scroll page
    'short_webPageWait' : 1, #  #define a shorter time to wait for when loading individal product pages
    'scrollDownPage' : False,  # whether to scroll down the page to find more products, e.g. Topshop
    'pgkey_page_scroll' : False, # scroll down using page down key
    'slow_page_scroll': False, # scroll down by scrolling N pixels at a time - set to N to use this
    'end_page_scroll' : False, #scroll to the end of the page to force the html to load, but do not look for additional pages
    'findNextPage' : True, # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    'comparehtml'  : 'direct', # if 'direct' assumes pages are equal if html is identical, if 'product' compares product details - used to determine if page loaded
    #number of screenshots to save, if there are more than 1 linkurl, can save a single svreenshot for each link page
    'numscreenshots' : 1, # number of screnshots to save
    #define any buttons to be clicked on screen
    'buttonXpaths' : [], # a list of XPaths
    'buttoniFrameXpaths' : [],
    #set up variables to hold values between pages
    'num_products' : 0, # initialise the number of products found
    'page_url'  :'', # placeholder to store current page_url
    #used internally to hold/transfer variables
    'todayDate' : 'dateplaceholder',
    'todayDateStr' : 'datestrplaceholder',
    'starttime'  : 'starttimeplaceholder', # time process started, used to apply maximum runtime limit
    'run_time_limit' : 0, # the run time limit in seconds
    'opfpath' : 'webscrape', # output path
    'descriptions_reset_interval' : 999999, # how often to reset the driver when collecting descriptions
    'existing_product_descriptions' : [], #skuids of products that have images and descriptions extracted
    'google_project' : '', #placeholder  for GCS project
    'google_credentials' : '', #placeholder for GCS credentials
    #output formatting
    'include_date_in_outputcsv' : False, # True to include the date in the filename, when not defined defaults to True
    'proxy_location': '',
    # Give multiple country like this, 'GB|US'. country codes: https://laendercode.net/en/2-letter-list.html
    'popup_remove_button': [],  # give xpath of popup removal button
    'popup_remove_button_wait': 5 , #time to wait for pop up to be removed
    'show_proxy_info': False,
    'vpn_location': '', # specify the vpn location, '' if no vpn
    'vpn_location_list' : [], # specify a set of vpn location choices, empty if not used
    'wrong_country_popup': [],
    'luminati_proxy': '',
    'pre_newdriver_wait' : 2, #time to wait between copying chrome data and configuring new driver - waiting for copy to complete
    'post_newdriver_wait' : 3, #time to wait after new driver has been configured and launched before getting data (important on VM!)
    'user-data-dir' : '', #place holder for the chrome configuration folder if set
    #process the attributes, category, sub catagory fabric etc
    'process_attributes' : True,
    #default google big query datasets and tables
    'GBQ_DATASET' : 'competitive_data',
    'GBQ_TABLE_RESULTS' : 'results',
    'GBQ_TABLE_DESCRIPTIONS' : 'descriptions',
    'TEST_GBQ_DATASET' : 'testcompetitive_data',
    'TEST_GBQ_TABLE_RESULTS' : 'testresults',
    'TEST_GBQ_TABLE_DESCRIPTIONS' : 'testdescriptions',
    #extra description info - if False searches for the default product info, if True searches for the additional ebay product info
    'ADD_EBAY_PRODUCT_DESCRIPTIONS' : False,
    }

#legacy params variables
params = default_params.copy()