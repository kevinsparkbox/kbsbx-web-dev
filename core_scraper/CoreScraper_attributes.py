import pandas as pd
import numpy as np
import spacy
import yake
import string
import re
from tqdm import tqdm
from fuzzywuzzy import fuzz
from fuzzywuzzy import process  

#from matplotlib import colors as mcolors
#from sklearn.neighbors import KNeighborsClassifier

from main_colour_match import main as find_the_sparkbox_colour

#nlp = spacy.load('en_core_web_sm')
#python -m spacy download en_core_web_sm

import warnings
warnings.filterwarnings('ignore')

def match_fabric_keywords(extracted_keywords, keywords):
    """
    Check if any of the fabric keywords are in the short description 
    
    """
    
    to_be_processed = extracted_keywords.copy()
    for index, row in tqdm(to_be_processed.iterrows()):
        fabric = None
        found = False
        description = row['short_description']
        #if not pd.isna(description):
        description = description.lower().replace('-', '')
        description = description.translate(str.maketrans('', '', string.punctuation))    
        #convert to a list of words so that the in operator does not find a partial match
        description = description.split(' ')   
        for key in keywords:
            if found:
                break
            if key in description:
                fabric = key
                found = True

        extracted_keywords.loc[index, 'fabric'] = fabric

    return extracted_keywords

def match_type_keywords(extracted_keywords, keywords):
    """
    Check if any of the type keywords are in the short description , check for an exact match
    
    """
    to_be_processed = extracted_keywords.copy()
    for index, row in tqdm(to_be_processed.iterrows()):
        spbx_subcat_type = None
        found = False
        description = row['short_description']
        #if not pd.isna(description):
        description = description.lower().replace('-', '')
        description = description.translate(str.maketrans('', '', string.punctuation)) 
        #convert to a list of words so that the in operator does not find a partial match
        description = description.split(' ')   
        for key in keywords:
            if found:
                break
            if key in description:
                spbx_subcat_type = key
                found = True

        extracted_keywords.loc[index, 'spbx_subcat_type'] = spbx_subcat_type

    return extracted_keywords

def match_long_description_fabric_keywords(extracted_keywords,  keywords):
    
    """
    Check if any of the fabric keywords are in the long description 
    
    """
    #process the rows where fabric has not been found
    not_processed_keywords = extracted_keywords[ extracted_keywords['fabric'].isnull() ].copy()
    for index, row in tqdm(not_processed_keywords.iterrows()):
        fabric = None
        found = False
        description = row['long_description']
        #if not pd.isna(description):
        #description = description.lower().replace('-', '')
        description = description.replace('-', '')
        description = description.translate(str.maketrans('', '', string.punctuation))   
        #convert to a list of words so that the in operator does not find a partial match
        #seperate words if upper case in middle of word, e.g. helloThere > hello There
        sep_upcase = re.findall("[a-zA-Z][^A-Z]*", description) 
        #seperate into list using space as seperator
        descr = ' '.join(sep_upcase).lower().replace('  ',' ').split(' ')
        for key in keywords:
            if found:
                break
            if key in descr:
                fabric = key
                found = True

        extracted_keywords.loc[index, 'fabric'] = fabric

    return extracted_keywords

def match_long_description_type_keywords(extracted_keywords,  keywords):
    
    """
    Check if any of the type keywords are in the long description, looking for an exact match
    
    """
    #process the rows where fabric has not been found
    not_processed_keywords = extracted_keywords[ extracted_keywords['spbx_subcat_type'].isnull() ].copy()
    for index, row in tqdm(not_processed_keywords.iterrows()):
        subcat_type = None
        found = False
        description = row['long_description']
        #if not pd.isna(description):
        #description = description.lower().replace('-', '')
        description = description.replace('-', '')
        description = description.translate(str.maketrans('', '', string.punctuation))   
        #convert to a list of words so that the in operator does not find a partial match
        #seperate words if upper case in middle of word, e.g. helloThere > hello There
        sep_upcase = re.findall("[a-zA-Z][^A-Z]*", description) 
        #seperate into list using space as seperator
        descr = ' '.join(sep_upcase).lower().replace('  ',' ').split(' ')
        for key in keywords:
            if found:
                break
            if key in descr:
                subcat_type = key
                found = True

        extracted_keywords.loc[index, 'spbx_subcat_type'] = subcat_type

    return extracted_keywords

def match_similar_type_keywords(extracted_keywords, type_keywords, fabric_keywords):
    
    """
    for each word in the short decription find the best match from the type keywords, 
    use the match score to determine if it is the best match found so far
    if it is the best_match found so far and the match score is higher than matching with any of the fabric keywords, take this as the best match so far
    
    """
    similar_match_threshold = 90

    #process the rows where the type (sub_catagory) has not been found
    not_processed_keywords = extracted_keywords[ extracted_keywords['spbx_subcat_type'].isnull() ].copy()
    
    for index, row in tqdm(not_processed_keywords.iterrows()):
        max_found = 0
        k_found = None
        description = row['short_description']
        #if not pd.isna(description):
        description = description.lower().replace('-', '')
        description = description.translate(str.maketrans('', '', string.punctuation))    
        description_list = description.split(' ')
        #print('Description ', description_list)
        for d in description_list:
            #print('Looking for a match with  ', d)
            if len(d) == 0:
                continue
            #find the best match between the description word (d) and the type keywords
            t_match = process.extractOne(d, type_keywords)
            t_match_value  = t_match[1]
            t_match_keyword = t_match[0]
            #print('match ' , t_match_keyword, t_match_value)
            if t_match_value >similar_match_threshold and t_match_value > max_found:
                #found the best match, check it is a lower match with a fabric value before accepting
                f_match = process.extractOne(d, fabric_keywords)
                f_match_value  = f_match[1]
                f_match_keyword = f_match[0]
                #if the fabric match is lower than the type match then class as the best type match so far
                #print('Best match so far - fabric matches ' , f_match_keyword, f_match_value)
                if f_match_value < t_match_value:
                    k_found = t_match_keyword
                    max_found = t_match_value
                    #print('Setting as best match ', k_found, max_found)


        extracted_keywords.loc[index, 'spbx_subcat_type'] = k_found
    return extracted_keywords



def OLDextract_type_POS(extracted_keywords, type_keys, fabric_keys): 
    """
    apply part of speech tagging to the short description to find the type.  
    this step is after the match_type_keywords step so is only applied  where the match type has failed
    sets the type to a word which matches a noun if it is similar to one of the type keywords and not similar to a fabric keyword  

    """
    to_be_processed = extracted_keywords[ extracted_keywords['spbx_subcat_type'].isnull()].copy()

    for index, row in tqdm(to_be_processed.iterrows()):
        type_found = False
        description = row['short_description']
        #if not pd.isna(description):
        description = description.lower().replace('-', '')
        description = description.translate(str.maketrans('', '', string.punctuation))  
        doc = nlp(description)
        for token in doc:
            if type_found:
                continue
            if ((token.dep_ == 'nsubj' and token.pos_ == 'NOUN') or (token.dep_ == 'ROOT' and token.pos_ == 'NOUN') or (token.dep_ == 'dobj' and token.pos_ == 'NOUN') or
                (token.dep_ == 'ROOT' and token.pos_ == 'PROPN' ) or (token.dep_ == 'dobj' and token.pos_ == 'PROPN'  )): 
                key = token.text
                type_sim = max_type_sim(key,  type_keys)
                fabric_sim = max_fabric_sim(key, fabric_keys)
                #if more similar to fabric continue
                if fabric_sim > type_sim:
                    continue
                #if more similar to type and above a similarity threshold type has been found
                if type_sim > 0.8:  
                    extracted_keywords.loc[index, 'spbx_subcat_type'] = key
                    type_found = True
       
    return extracted_keywords

def max_type_sim(key,type_keywords):
    """
    Calculate maximum similarity with type
    """
    key = nlp(key)
    type_sim = []
    for type_key in list(type_keywords): 
        key2 = nlp(type_key)
        type_sim.append(key.similarity(key2))
    max_sim = max(type_sim)
    return max_sim 

def max_fabric_sim(key,fabric_keywords):
    """
    Calculate maximum similarity with fabric keywords
    """
    key = nlp(key)
    fabric_sim = []
    for fabric_key in list(fabric_keywords): 
        key2 = nlp(fabric_key)
        fabric_sim.append(key.similarity(key2))
    max_sim = max(fabric_sim)
    return max_sim 



def assign_subcat_cat_div(extracted_types,categories, category_dict):
    """
    Asign the sparkbox subcategory, category and division
    Search through the catagories definitions to find the matching sub category defined in the catagory keywords
    """
    copy_extracted_types = extracted_types.copy()
    for index, row in tqdm(copy_extracted_types.iterrows()):
        desc = row['spbx_subcat_type']
        spb_cat = "Other"
        desc = desc.lower()
    
        #search through the categories definitions, stop as soon as a matching subcat type is found, so priority is given to left top
        found = False
        for cat in categories.columns:
            for type_val in categories[cat].dropna().values:
                if type_val.lower() == desc:
                    spb_cat = cat
                    found = True
                    break
            if found:
                break
        extracted_types.loc[index, 'spbx_subcat'] = spb_cat
        if spb_cat != "Other" :
            extracted_types.loc[index, 'spbx_category'] = category_dict[spb_cat]['Category']
            extracted_types.loc[index, 'spbx_division'] = category_dict[spb_cat]['Division']
        else:
            extracted_types.loc[index, 'spbx_category'] = "Other"
            extracted_types.loc[index, 'spbx_division'] = "Other"

    return extracted_types

def assign_department(extracted_types,dept_keywords, default_department):
    """
    Asign the sparkbox department
    Search through the categories, url and descriptions definitions to find a Department that is found in the department type
    """
    #cols_to_search = ['department', 'category', 'subcategory', 'url']
    cols_to_search = ['department', 'category', 'subcategory']
    sample_data_df = extracted_types[cols_to_search].copy()
    #print('Default Dept', default_department)
    #print('Dept keywords ', dept_keywords)
    #print(sample_data_df)
    sample_data_df.fillna("Other", inplace=True)
    for index, row in tqdm(sample_data_df.iterrows()):
        text_row = row.values.flatten().tolist()
        if default_department == "":
            spbx_dept = "Other"
        else:
            spbx_dept = default_department
        found = False
        for t in text_row:
            t = t.lower()
            #print('PROCESSING t', t)
            for dept in dept_keywords.columns:
                for type_val in dept_keywords[dept].dropna().values:
                    #print('Testing', t, type_val, dept)
                    if type_val.lower() in t:
                        spbx_dept = dept
                        found = True
                        break
                if found:
                    break
            if found:
                break
        extracted_types.loc[index, 'spbx_department'] = spbx_dept
    #print(extracted_types)
    return extracted_types


def extract_style_entities(extracted_keywords):
    """
    Process the short description, eliminate words that are similar to fabric, colour, subcat, cat it is a style description
    
    """
    
    #simple_kwextractor = yake.KeywordExtractor(n=4, top=10)
    style_keywords_to_process = extracted_keywords.copy()
    style_keywords_to_process['colour'].fillna('Other', inplace=True)
    for index, row in tqdm(style_keywords_to_process.iterrows()):
        desc = row['short_description']
        if desc != "":
            desc = desc.lower().replace('-', '')
            desc = desc.translate(str.maketrans('', '', string.punctuation))
            fabric = row['fabric']
            fabric = fabric.lower()
            colour = row['colour']
            colour = colour.lower()
            subcat = row['spbx_subcat_type']
            subcat = subcat.lower()
            cat = row['spbx_category']
            cat = cat.lower()
            style = []
            for k in desc.split(' '):
                #append k to style if it is not similar to one of the extrtacted attributes
                if  max(    fuzz.partial_ratio(k, fabric), 
                            fuzz.partial_ratio(k, colour), 
                            fuzz.partial_ratio(k, subcat), 
                            fuzz.partial_ratio(k, cat) ) < 90 :
                    style.append(k)
            #convert style list into a string of words
            extracted_keywords.loc[index, 'style'] =  ' '.join(style) 
        else:
            extracted_keywords.loc[index, 'style'] =  'Other'


    return extracted_keywords
                



def match_similar_fabric_keywords(extracted_keywords, fabric_keywords, type_keywords):
    """
    Get fabric keywords first by omparing similarity of extracted keywords from the long description
    
    """
    #for the remaining items where fabric has not yet been found, look for similarity of the fabric key words in the long description
    simple_kwextractor = yake.KeywordExtractor(n=2, top=15)

    similar_match_threshold = 90

    #process the rows where the fabric has not been found
    not_processed_keywords = extracted_keywords[ extracted_keywords['fabric'].isnull() ].copy()
    print('Fabric looking for a similar match in long descriptions', len(not_processed_keywords))
    for index, row in tqdm(not_processed_keywords.iterrows()):
        max_found = 0
        k_found = "Other"
        description = row['long_description']
        #if not pd.isna(description):
        description = description.lower().replace('-', '')
        description = description.translate(str.maketrans('', '', string.punctuation)) 
        if len(description) > 0: 
            try: # trap the case when YAKE fails and generates an error
                keywords = simple_kwextractor.extract_keywords(description)
            except:
                print('YAKE failed on : ',description)
                keywords = []

            for k in keywords:
                if 'description' in k[0]:
                    continue
                #find the best "good" match between the description keyword (k) and the fabric keywords
                f_match = process.extractOne(k[0], fabric_keywords)
                f_match_value  = f_match[1]
                f_match_keyword = f_match[0]
                if f_match_value >= similar_match_threshold and f_match_value > max_found:
                    #found the best match, check it is a higher match than a type value before accepting
                    t_match = process.extractOne(k[0], type_keywords)
                    t_match_value  = t_match[1]
                    #t_match_keyword = t_match[0]
                    #if the fabric match is higher than type then class as the best type match so far
                    if f_match_value > t_match_value:
                        k_found = f_match_keyword
                        max_found = f_match_value
               
        extracted_keywords.loc[index, 'fabric'] = k_found


    return extracted_keywords

    
def extract_colour(extracted_items, spbx_colours, colours_reference):
    """
    Applies colour matching processing to match a colour in a retailer description (extracted items) to the closest sparkbox colour (spbx_colours). 
    The required functionality is to allow the sparkbox insights tool to report on colour trends etc of items scraped from the web, which requires mapping
    of the retailer colours to a common reporting colour palette

    Inputs:
    extracted_items a dataframe, each row is a product description, with various columns describing the product including
        column ['colour'] is the original colour of the item as described on the retailer website
        column['spbx_colour'] is a placeholder for the closest matched sparkbox colour (could be empty otr may already be populated from a previous version of colour matching)
    spbx_colours a dataframe of TBC columns where
        column{'spbx_colour'] describes the sparkbox colour and the other columns define the reference information describing the colour (e.g. RGB values)

    colur_reference a dataframe (containing reference information to support the colour matching)

    Outputs:
    the extracted_items data frame with the ['spbx_colour'] value updated.
    """

    #the find_sparkbox_colour is expecting an input dataframe of the format df['colour'], and it returns a matching  df with ['sparkbox_colour]
    extracted_items_updates = find_the_sparkbox_colour(extracted_items,  spbx_colours, colours_reference)
    #rename the output matched colour to spbx_colour
    extracted_items_updates = extracted_items_updates.rename(columns={'sparkbox_colour' : 'spbx_colour'})
    #update the extracted items
    extracted_items.update(extracted_items_updates['spbx_colour'], overwrite=True)    

    return extracted_items


    
    
