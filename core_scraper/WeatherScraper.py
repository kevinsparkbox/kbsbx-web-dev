# place username and password in the code. 
# #uncomment login function for the first time run. then it will save login data in cookies. then you can disable login function

import time
import os
import google
import platform
import csv
import re
from  more_itertools import unique_everseen  #pip install more_itertools - TODO add to requirements.txt
import pandas as pd
import datetime
from bs4 import BeautifulSoup
from pathlib import Path
import sys
import traceback
from retrying import retry
import logging
import bs4


from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager

from WeatherScraperConstants import TEST_GBQ_TABLE_WEATHER_RESULTS, GBQ_TABLE_WEATHER_RESULTS, GBQ_DATASET, TEST_GBQ_DATASET
from WeatherScraperConstants import WEATHER_RETAILER_CONFIG, WEATHER_CHROME_DATA_DIR, WEATHER_FORCE_NO_LOGIN, RESET_LOGIN
from WeatherScraperConstants import TEST_GOOGLE_STORAGE_FILE_PATH, GOOGLE_STORAGE_FILE_PATH, WEATHER_RESULTS_FILE

from CoreScraperOutputModules import createOutputPath, remove_duplicates_weather
from CoreScraperConstants import MINIMUM_RUNTIME
from CoreScraperConstants import OUTPUT_OPTION, OUTPUT_TO_DATABASE
from CoreScraperConstants import BUCKET_NAME, PROGRESS_FOLDER, PROGRESS_FILE
from CoreScraperConstants import CHROME_USER_AGENT, MASTER_CHROME_DATA_PATH
from CoreScraperOutputModules import outputDataFrameToFile, outputDataFrameToDatabase, createOutputPath, outputToFile, upload_blob
from CoreScraperOutputModules import generate_error_log_update_progress, output_running_file, delete_running_file
from CoreScraperErrorModules import  send_email_notification
from CoreScraper_utils import configure_googleapi_credentials
from Corescraper_fetch_BQ_data import download_blob_delete
from CoreScraper_getHtml import configure_reset_chrome_VPNextensions_cookies, configureNewDriver, getHtml, check_display_iplocation
from CoreScraper import init_logging


def create_weather_record(table):
    """ A function to convert the weather table html to a dataframe
    """
    weather_data = pd.DataFrame()
    thead = table.find('thead')
    tbody = table.find('tbody')
    #process tablke header for the maain tablle columns
    header = thead.find_all("td")
    header_cols = []
    for c in header:
        ctext = c.get_text()
        header_cols.append(ctext)
    #data is organised in blocks (columns) with up to 3 sub-columns within each block
    #print(tbody.prettify())
    data_cols = tbody.find_all("table")
    for col in range(0, len(data_cols)):
        rows = data_cols[col].find_all("tr")
        values = [ [], [], [] ]

        for r in rows:
            data = r.find_all("td")
            for x in range (0, len(data)):
                rtext = data[x].get_text().replace('\n', ' ').strip()
                values[x].append(rtext)
        #change the header to include the main header
        main_label = header_cols[col] 
        for x in range(0,3):
            if len(values[x]) > 0:
                values[x][0] = main_label + " " + values[x][0]
                weather_data[values[x][0]] = values[x][1:]

    #rename the columns to be big query compat names
    weather_data.columns = [ 'day', 'temp_max', 'temp_avg', 'temp_min', 'dewpoint_max', 'dewpoint_avg', 'dewpoint_min', 'humidity_max', 'humidity_avg', 'humidity_min', 
                                'windspeed_max', 'windspeed_avg', 'windspeed_min', 'pressure_max', 'pressure_avg', 'pressure_min', 'precipitation']

    return weather_data

def create_pws_weather_record(table):
    """ A function to convert the weather tabke html to a dataframe, where the table is from a pws weather station
    """
    weather_data = pd.DataFrame()
    tbody = table.find('tbody')
    weather_values = []
    rows = tbody.find_all("tr")
    #print(tbody.prettify())
    for r in rows:
        row_values = []
        data = r.find_all("td")
        for d in data:
            rtext = d.get_text().replace('\n', ' ').strip()
            #remove the formatting
            rtext = rtext.split()[0]
            row_values.append(rtext)
        weather_values.append(row_values)

    #configure the columns to be big query compat names - note there is no pressure_avg
    col_vals = [ 'date', 'temp_max', 'temp_avg', 'temp_min', 'dewpoint_max', 'dewpoint_avg', 'dewpoint_min', 'humidity_max', 'humidity_avg', 'humidity_min', 
                                'windspeed_max', 'windspeed_avg', 'windspeed_min', 'pressure_max',  'pressure_min', 'precipitation']
    weather_data = pd.DataFrame(weather_values, columns=col_vals)
    #create the pressure average for compatibility with non pws stations
    weather_data['pressure_avg'] = (weather_data['pressure_min'].astype(float) + weather_data['pressure_max'].astype(float) )/2

    return weather_data

def convert_weather_html_to_df(weather, params):
    weather_data = pd.DataFrame()
    for html_page in weather['html']:
        webSoup = bs4.BeautifulSoup(html_page , params['htmlParser'])
        table = webSoup.find('div', { 'class' : "observation-table" } )
        if table == None:
            msg = "WARNING observation table not loaded from web page"
            logging.warning(msg)
            valid_weather_data = False
        else:
            weather_data = create_weather_record(table)
            valid_weather_data = True
        #add the location and date information and delete noty required columns
        weather_data['year'] = weather['year']
        weather_data['month'] = weather['month']
        weather_data['location'] = params['location']
        weather_data['date'] = weather_data['year'].apply(str) + '-' + weather_data['month'].apply(str) +  '-'  + weather_data.iloc[:,0].apply(str)
        weather_data['date'] = pd.to_datetime(weather_data['date'], format = '%Y-%m-%d')
        #delete the day (first col) as its name is the month, and not required
        weather_data.drop(columns=weather_data.columns.to_list()[0], axis=1, inplace = True)
    return weather_data, valid_weather_data

def convert_pws_weather_html_to_df(weather, params):
    weather_data = pd.DataFrame()
    for html_page in weather['html']:
        webSoup = bs4.BeautifulSoup(html_page , params['htmlParser'])
        
        table = webSoup.find('div', { 'class' : "table-wrapper" } )
        if table == None:
            table = webSoup.find('div', { 'class' : "scrollable" } )
        if table != None:
            table = table.find('table', { 'class' : "desktop-table" } )
        if table == None:
            msg = "WARNING observation table not loaded from web page"
            logging.warning(msg)
            valid_weather_data = False
        else:
            weather_data = create_pws_weather_record(table)
            valid_weather_data = True
        weather_data['year'] = weather['year']
        weather_data['month'] = weather['month']
        weather_data['location'] = params['location']
        weather_data['date'] = pd.to_datetime(weather_data['date'], format = '%m/%d/%Y')
    return weather_data, valid_weather_data

class Weather:
    def __init__(self,username, password, url, params, toppath, location_from_fname, filename):
        self.username = username
        self.password = password
        self.url = url
        self.data_dir = WEATHER_CHROME_DATA_DIR
        self.starttime = time.time()
        self.params = params
        self.toppath = toppath
        self.location_from_fname = location_from_fname
        self.filename = filename
        self.driver = ''
        self.weather_html = ''
        self.ip = ''

        #determine the weather data format
        if 'pws' in filename:
            self.weather_station = 'pws'
        else:
            self.weather_station = 'inst'

        if len(sys.argv) > 1 : #toppath has been passed as an argument which takes priority
            self.toppath = sys.argv[1]
        if len(sys.argv) > 2 : #reset duplicates has been passed as an argument (not used in insta scrape)
            #params['resetduplicatelinks'] = sys.argv[2]
            self.resetduplicatelinks = sys.argv[2]
        if len(sys.argv) > 3 : #run time limit has been passed as an argument
            self.run_time_limit = sys.argv[3]
        else:
            self.run_time_limit =  MINIMUM_RUNTIME
        if len(sys.argv) > 4 : #google credentials have been passed as an argument
            self.google_project = sys.argv[4]["google_project"]
            self.google_credentials = sys.argv[4]["google_credentials"]
            print('Google Credentials passed as arguments', self.google_project, self.google_credentials)
        else:
            google_credentials, google_project = configure_googleapi_credentials()
            print('Google Credentials generated using auth ', google_project, google_credentials)
            self.google_project = google_project
            self.google_credentials = google_credentials

        params['google_project'] = self.google_project
        params['google_credentials'] = self.google_credentials
        #print ("The arguments are: " , str(sys.argv))
        starttime = time.time()
        params['starttime'] = starttime
        params['run_time_limit'] = self.run_time_limit
        #create the local output path if it does not already exist, folder name is same as module name
        opfpath =  createOutputPath(location_from_fname, toppath, "Local")
        params['opfpath'] = opfpath
        #add the start date string to the params
        todayDate = datetime.datetime.today()
        todayDateStr = todayDate.strftime("%Y%m%d")
        params['todayDate'] = todayDate.date()
        params['todayDateStr'] = todayDateStr

        #output a dummy file to indicate the run start time
        print('Outputting running file ', opfpath, GOOGLE_STORAGE_FILE_PATH )
        output_running_file(opfpath, self.google_project, self.google_credentials, GOOGLE_STORAGE_FILE_PATH, TEST_GOOGLE_STORAGE_FILE_PATH)

    def driver_config(self):
        #configure the browser extension for VPN parameters if required 
        self.params = configure_reset_chrome_VPNextensions_cookies(self.params)

    def new_driver(self):
        #configure the new driver
        self.driver = configureNewDriver(self.params,self.driver)

    def check_ip_connection(self, attempt):
        country, city, ip = check_display_iplocation(self.driver, self.params, attempt)
        self.country = country
        self.city = city
        self.ip = ip


    def scrape_weather_pages_pws(self):
        #format https://www.wunderground.com/dashboard/pws/IBERKSHI14/table/2017-02-01/2017-02-01/monthly
        webPage = self.url
        year = self.params['start_year']
        month = self.params['start_month']
        todayDate = datetime.datetime.today()
        current_year = todayDate.year
        current_month = todayDate.month
        weather_html = []
        finished = False
        all_weather_data = pd.DataFrame()
        max_attempts = 10
        while not finished:
            if month == current_month and year == current_year:
                finished = True
            valid_weather_data = False
            attempt = 0
            #fetch the html and convert to weather dataframe, retry if data not found
            while attempt < max_attempts and not valid_weather_data:
                #reset the driver - otherwise does not always keep the new page loaded
                self.driver = configureNewDriver(self.params,self.driver)
                #scrape the data
                logging.info('Weather scraping {} {} for {}, attempt {}'.format(year, month, self.location_from_fname, attempt))
                webPage = self.url.replace('YYYY', str(year)).replace('MM', str(month))
                logging.info('Scraping {} '.format(webPage))
                html, driver, currentUrl, problemUrl =  getHtml(webPage, self.driver, self.params,  clear_popups = True,
                                                            process_html_pages = True)
                self.driver = driver
                #store the results in a dict
                weather = {     'month' : month,
                                'year' : year,
                                'html' : html }
                #TODO alternative interpretation of pws pages
                weather_data, valid_weather_data = convert_pws_weather_html_to_df(weather, self.params)
                #append the results to the list of results
                if valid_weather_data:
                    all_weather_data = all_weather_data.append(weather_data)
                #if failed to collect data for a month, and exceeded max attempts exit with an error
                if valid_weather_data == False and attempt > max_attempts:
                    msg = "ERROR observation table not loaded from web page after {} attempts".format(attempt)
                    logging.warning(msg)
                    sys.exit(msg)
                attempt = attempt + 1
            #update the month and year to collect
            month = month +1
            if month ==13:
                year = year+1
                month = 1

        #put data into a standard order
        weath_cols = [ 'temp_max', 'temp_avg', 'temp_min', 'dewpoint_max', 'dewpoint_avg', 'dewpoint_min', 'humidity_max', 'humidity_avg', 'humidity_min', 
                        'windspeed_max', 'windspeed_avg', 'windspeed_min', 'pressure_max', 'pressure_avg', 'pressure_min', 'precipitation', 
                        'year', 'month', 'location', 'date']
        all_weather_data = all_weather_data[weath_cols]
        
        self.weather_data = all_weather_data



    def scrape_weather_pages(self):
        #format https://www.wunderground.com/history/monthly/gb/london/EGLC/date/2020-2
        webPage = self.url
        year = self.params['start_year']
        month = self.params['start_month']
        todayDate = datetime.datetime.today()
        current_year = todayDate.year
        current_month = todayDate.month
        weather_html = []
        finished = False
        all_weather_data = pd.DataFrame()
        max_attempts = 10
        while not finished:
            if month == current_month and year == current_year:
                finished = True
            valid_weather_data = False
            attempt = 0
            #fetch the html and convert to weather dataframe, retry if data not found
            while attempt < max_attempts and not valid_weather_data:
                #reset the driver - otherwise does not always keep the new page loaded
                self.driver = configureNewDriver(self.params,self.driver)
                #scrape the data
                logging.info('Weather scraping {} {} for {}, attempt {}'.format(year, month, self.location_from_fname, attempt))
                webPage = self.url + '/date/' + str(year) + '-' + str(month) 
                html, driver, currentUrl, problemUrl =  getHtml(webPage, self.driver, self.params,  clear_popups = True,
                                                            process_html_pages = True)
                self.driver = driver
                #store the results in a dict
                weather = {     'month' : month,
                                'year' : year,
                                'html' : html }
                weather_data, valid_weather_data = convert_weather_html_to_df(weather, self.params)
                #append the results to the list of results
                if valid_weather_data:
                    all_weather_data = all_weather_data.append(weather_data)
                #if failed to collect data for a month, and exceeded max attempts exit with an error
                if valid_weather_data == False and attempt > max_attempts:
                    msg = "ERROR observation table not loaded from web page after {} attempts".format(attempt)
                    logging.warning(msg)
                    sys.exit(msg)
                attempt = attempt + 1
            #update the month and year to collect
            month = month +1
            if month ==13:
                year = year+1
                month = 1

        #put data into a standard order
        weath_cols = [ 'temp_max', 'temp_avg', 'temp_min', 'dewpoint_max', 'dewpoint_avg', 'dewpoint_min', 'humidity_max', 'humidity_avg', 'humidity_min', 
                        'windspeed_max', 'windspeed_avg', 'windspeed_min', 'pressure_max', 'pressure_avg', 'pressure_min', 'precipitation', 
                        'year', 'month', 'location', 'date']
        all_weather_data = all_weather_data[weath_cols]

        self.weather_data = all_weather_data


    def write_csv_db(self):
        #create the output path if it does not already exist
        opfpath =  createOutputPath(self.location_from_fname, self.toppath, "Local")
        #before writing the data, add/update the date time stamp
        modified = datetime.datetime.now().replace(microsecond=0)
        self.weather_data['modified_date'] = modified
        #output the instagram results to database and csv
        output_weather_results(self.location_from_fname, self.toppath, opfpath, self.weather_data, self.starttime, self.google_project, self.google_credentials)





    def close_driver(self):
        try:
            self.driver.close()
        except:
            print('Unable to close driver')
            sys.stdout.flush()


#  Main Code    

def mainWeatherScrape(url, params , toppath, location_from_fname, filename):

    username = 'weather_user'
    password = 'weather_pwd'
 

    #set up the logging
    logpath =  toppath + 'AA_LOGS/'
    init_logging(filename, logpath)

    max_attempts = 10
    attempt = 1
    weath = Weather(username, password, url, params, toppath, location_from_fname, filename )
    #place in a try and catch and output any scraping errors
    try:
        weath.driver_config()
        #set up new driver until can obtain ip details, helping to solve issues where the vpn does not connct and asks for a user name
        while attempt < max_attempts and weath.ip == "":
            weath.new_driver()
            weath.check_ip_connection(attempt)
            attempt = attempt + 1
        if attempt == max_attempts:
            msg = "Unable to open the browser with connection and a valid ip, exiting"
            sys.exit(msg)
        if weath.weather_station == 'pws':
            weath.scrape_weather_pages_pws()
        else:
            weath.scrape_weather_pages()
        weath.close_driver()
        weath.write_csv_db()
    except : 
        weath.close_driver()
        msg = traceback.format_exc()
        print(msg)
        #delete the running file marker so that the scraper can start again
        print('Deleting the running file ' , weath.toppath, GOOGLE_STORAGE_FILE_PATH )
        delete_running_file(weath.params['opfpath'], weath.google_project, weath.google_credentials, GOOGLE_STORAGE_FILE_PATH, TEST_GOOGLE_STORAGE_FILE_PATH)
        #send notification and update error log
        generate_error_log_update_progress(location_from_fname,weath.toppath, weath.starttime, weath.google_project, weath.google_credentials,
                                        GOOGLE_STORAGE_FILE_PATH, TEST_GOOGLE_STORAGE_FILE_PATH)
        mod_name = url.strip().replace('.', '[.]')
        subject =  f"Error in Weather Scraping  {mod_name} on { platform.node()}! "
        body = f"Error for {mod_name} on { platform.node()} ! \n {traceback.format_exc()} "
        send_email_notification(subject, body)

    return

def output_weather_results(location, toppath, opfpath, weather_resultsdf, starttime, google_project, google_credentials):
    #toppath is the path to the results folder
    #opfpath is the path to the retailer specific folder within the results
    #retailer is the retailer name which is generally the same as the retailer specific folder name - some duplication caused by legacy codes
    #e.g. retailer = 'Pimkie'  = should match the folder used to store results from web scrape

    #get the current date
    todayDate = datetime.datetime.today()
    opDate = todayDate.strftime("%Y%m%d")

    endtime = time.ctime()
    duration = (time.time() - starttime)/(60*60)
    if len(weather_resultsdf) > 0:
        numResults = len(weather_resultsdf)
  
        opfname =  WEATHER_RESULTS_FILE
    
        #output weather results, for local outputs to the folder specified by opfpath, (configured as output path/retailer name)
        #for Google Storage outputs to the folder under the GOOGLE_STORAGE_FILE_PATH or TEST_GOOGLE_STORAGE_FILE_PATH
        if OUTPUT_OPTION == "LocalOnly" or OUTPUT_OPTION == "All":
            #for the local option 
            outputDataFrameToFile(weather_resultsdf, opfpath, opfname, "Local", google_project, google_credentials,
                                 GOOGLE_STORAGE_FILE_PATH, TEST_GOOGLE_STORAGE_FILE_PATH)
        if OUTPUT_OPTION == "GCSOnly" or OUTPUT_OPTION == "All":
            #for the GCS option 
            outputDataFrameToFile(weather_resultsdf, opfpath, opfname, "Google_Storage", google_project, google_credentials,
                                GOOGLE_STORAGE_FILE_PATH, TEST_GOOGLE_STORAGE_FILE_PATH)
        
        #output the full results to the database
        if OUTPUT_TO_DATABASE:
            if opfpath.find('TEST') == -1:
                dataset = GBQ_DATASET
                results_table = GBQ_TABLE_WEATHER_RESULTS
            else:
                dataset = TEST_GBQ_DATASET
                results_table = TEST_GBQ_TABLE_WEATHER_RESULTS

            #append and remove duplicates, keeping the most recent
            destination = "Big_Query"
            if_exists_operation = "append"
            outputDataFrameToDatabase(weather_resultsdf, dataset, results_table, destination, if_exists_operation, google_project, google_credentials)
            #deduplicate the weather database
            remove_duplicates_weather(dataset, results_table ,google_project, google_credentials )
        
        #output details to progress file
        #update the progress file on GS, a convoluted progress as GS does not support append to file, read the local file, append, and then write to GS)
        print('Outputting to PROGRESS File', toppath)
        #create the output progress message
        textToOp = opDate  + ',' + 'weather_' + location +','  + 'Succesful' +','   + endtime + ',' + str(duration) + ',' + str(len(weather_resultsdf)) +  ',' + platform.node() + '\n'
    
        fname =  PROGRESS_FILE
        #create the local output path if it does not already exist
        createOutputPath(PROGRESS_FOLDER, toppath, 'Local')
        #configure the GS folder path
        if opfpath.find('TEST') == -1:
            GSPath = GOOGLE_STORAGE_FILE_PATH
        else:
            GSPath = TEST_GOOGLE_STORAGE_FILE_PATH 
        folderpath = GSPath +  PROGRESS_FOLDER 
        #download the progress blob from GS
        download_success = download_blob_delete(google_credentials, google_project, BUCKET_NAME, folderpath, fname, toppath + PROGRESS_FOLDER + '/' +fname, False)
        if not download_success:
            #progress file does not exist on GS so start a new one, delete any existiong local files
            try:
                os.remove(toppath + '/' + PROGRESS_FOLDER +  '/' + fname)
                print('Deleted local progress file')
            except:
                print('No local progress file to delete ')
        #append the latest progress info
        outputToFile(toppath + '/' + PROGRESS_FOLDER, fname, textToOp, "a", "Local", google_project, google_credentials) 
        #upload the local file back to GS
        upload_blob(toppath + PROGRESS_FOLDER + '/' +fname, folderpath + '/' + fname, google_project, google_credentials, BUCKET_NAME)
        print('Uploaded PROGRESS File to GS')
        
    else:
        send_email_notification(numResults, 'weather scraper ' + location)

    return