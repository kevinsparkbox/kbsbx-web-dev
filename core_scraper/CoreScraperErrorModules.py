import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import time
import platform
import os
import datetime
import logging
import traceback

from CoreScraperConstants import EMAIL_NOTIFICATION_RECIPIENTS
from CoreScraper_utils import closeDriver

def send_email_notification(subject, body, recipients=""):
    """ Send an error message to an email address(es) (list of recipients)
    inputs subject line, formatted body text and list of email recipients
    uses gmail.com - prerequisite need to configure app access on gmail account and use app password
    https://stackoverflow.com/questions/73026671/how-do-i-now-since-june-2022-send-an-email-via-gmail-using-a-python-script
    """
    try:
        if recipients == "":
            recipients=EMAIL_NOTIFICATION_RECIPIENTS
        #remove any . from subject as otherwise generates an error
        subject = str(subject).replace('.', '[.]')
        msg = MIMEMultipart()
        fromaddr = 'sparkboxstatus@gmail.com'
        password = 'lnoskglgrygyxbco'
        msg['From'] = fromaddr
        msg['To'] = ", ".join(recipients)
        msg['Subject'] = subject
        msg.attach(MIMEText(body, 'plain'))
        s = smtplib.SMTP('smtp.gmail.com', 587)
        s.starttls()
        s.login(fromaddr, password)
        text = msg.as_string()
        s.sendmail(fromaddr, recipients, text)
        s.quit()
    except:
        logging.warning('Unable to send email notification')
        msg = traceback.format_exc()
        logging.warning(msg)


def exit_if_exceeded_run_time_limit(params, url, driver):
    #stop the process if exceeded the run time limit
    run_time = time.time() - params['starttime'] 
    if run_time > params['run_time_limit'] :
        logging.info( ('Exceeded run time limit ' + str(run_time )+ ' ' + str(params['run_time_limit']) + '\n' +
                    'Web Page ' + url))
        closeDriver(driver)
        exit_check = True
    else:
        exit_check = False
    return exit_check




