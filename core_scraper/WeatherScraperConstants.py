# -*- coding: utf-8 -*-

import os

#folder with chrome data cookies etc
if os.name == 'posix':
    WEATHER_CHROME_DATA_DIR = "chrome-insta-data"
else:
    WEATHER_CHROME_DATA_DIR = "chrome-insta-data_win"

#configuration, match instagram name with retailer name and configure matching string for finding products (reg ex)
WEATHER_RETAILER_CONFIG = {
    'Pimkie' :      {   'replace_str':  { 'find' : '#' , 'replace' :  ' #'},
                        'product_str' :  r"(\d\d\d\d\d\d\w\w\w\w\w\w)" ,
                    },
    'Boden' :       {   'replace_str':  { 'find' : '#' , 'replace' :  ' #'},
                        'product_str' :  r"(\d\d\d\d\d\d\w\w\w\w\w\w)" ,
                    },             
        }

#to reset instagram login
RESET_LOGIN = False

#to attempt to scrape with no login set True
WEATHER_FORCE_NO_LOGIN = False

#set up the table names
TEST_GBQ_TABLE_WEATHER_RESULTS = "test_weather_results"
GBQ_TABLE_WEATHER_RESULTS = "weather_results"

#set up runtime indication file for insta scrapes
WEATHER_RUNTIME_FILE = "web_running.csv"
WEATHER_RESULTS_FILE = "Outputdata.csv"

#big query data sets
GBQ_DATASET = 'weather_data'
TEST_GBQ_DATASET = 'test_weather_data'

#google storage top level data folder
GOOGLE_STORAGE_FILE_PATH =  "data/"
TEST_GOOGLE_STORAGE_FILE_PATH =  "test_data/"
