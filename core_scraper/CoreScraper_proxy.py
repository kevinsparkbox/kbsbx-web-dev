#Temp storage of code in case we need to get proxy working again

######################## Get Valid Proxy #############
import asyncio
#from proxybroker import Broker


existing_proxy = []
free_proxy_within_function = []


def get_free_proxy(country):
    global free_proxy_within_function
    free_proxy_within_function = []
    print("free proxy func called")
    proxy_list = []
    country = country.split("|")

    async def show(proxies):
        while True:
            proxy = await proxies.get()
            if proxy is None: break
            print('Found proxy: %s' % proxy)
            sep_proxy = str(proxy).split(' ')
            sep_proxy = sep_proxy[-1].replace('>', '')

            free_proxy_within_function.append(sep_proxy)
            proxy_list.append(sep_proxy)

    proxies = asyncio.Queue()
    broker = Broker(proxies)
    if country[0] == 'ALL':
        tasks = asyncio.gather(broker.find(types=['HTTP', 'HTTPS'], limit=5), show(proxies))
    else:
        tasks = asyncio.gather(broker.find(types=['HTTP', 'HTTPS'], countries=country, limit=10), show(proxies))

    loop = asyncio.get_event_loop()
    loop.run_until_complete(tasks)

    return proxy_list


def get_valid_proxy_from_existing(url, params, **kwargs):
    response = ''
    ret_proxy = ''
    ret_response = ''

    proxy_list = existing_proxy
    random.shuffle(proxy_list)

    for proxy in proxy_list:
        try:
            prox = proxy
            proxy = {'https': proxy}
            print(proxy)
            response = requests.request('get', url, proxies=proxy, timeout=5, **kwargs)
            if response.status_code == 200:
                if params['show_proxy_info']:
                    info_country = get_proxy_info(prox)
                    print("info country", info_country)
                    if info_country == "United Kingdom":
                        ret_proxy = proxy['https']
                        ret_response = response
                        print("got valid proxy", ret_proxy)
                        break
                else:
                    ret_proxy = proxy['https']
                    ret_response = response
                    print("got valid proxy", ret_proxy)
                    break
            else:
                # ret_proxy = ''
                print(response.status_code)
                continue
        except:
            pass
    # if params['show_proxy_info'] and ret_proxy != '':
    #     get_proxy_info(ret_proxy)
    print("before returning", ret_proxy)
    return ret_proxy, ret_response


def get_valid_proxy(url, country, params, **kwargs):
    global existing_proxy
    ret_proxy = ''
    response = ''
    ret_response = ''
    if country == 'ALL':
        proxy_url = 'http://list.didsoft.com/get?email=kevin@sparkbox.co&pass=dues9k&pid=http1000&showcountry=no&https=yes'
        # proxy_url = 'http://list.didsoft.com/get?email=kevin@sparkbox.co&pass=dues9k&pid=http3000&showcountry=no'
    else:
        print(f"Getting proxy from {country}")
        proxy_url = f'http://list.didsoft.com/get?email=kevin@sparkbox.co&pass=dues9k&pid=http1000&showcountry=no&country={country}'
        # proxy_url = f'http://list.didsoft.com/get?email=kevin@sparkbox.co&pass=dues9k&pid=http3000&showcountry=no&country={country}'
    try:
        res_flag = False
        response = None
        loop_counter = 0
        while not res_flag:
            if loop_counter < 5:
                res = requests.get(proxy_url)
                if res.status_code == 200:
                    response = res
                    res_flag = True
                loop_counter += 1
            else:
                res_flag = True
        paid_proxy_list = response.text.split("\n")[:-1]
    except:
        paid_proxy_list = []


    # class TimeOutExceptionNew(Exception):
    #     pass
    #
    # def alarm_handler(signum, frame):
    #     if len(free_proxy_within_function) < 10:
    #         print("get_free_proxy Timeout")
    #         raise TimeOutExceptionNew()
    #     else:
    #         pass
    #
    # signal.signal(signal.SIGALRM, alarm_handler)
    # signal.alarm(60 * 1)  # 1 minute
    #
    #
    # try:
    #     free_proxy_list = get_free_proxy(country)
    # except TimeOutExceptionNew as ex:
    #     print(ex)
    #     free_proxy_list = free_proxy_within_function
    #     print("in exception", free_proxy_list)

    free_proxy_list = []

    proxy_list = free_proxy_list + paid_proxy_list

    existing_proxy = proxy_list

    random.shuffle(proxy_list)

    for proxy in proxy_list:
        try:
            prox = proxy
            proxy = {'https': proxy}
            print(proxy)
            response = requests.request('get', url, proxies=proxy, timeout=5, **kwargs)
            if response.status_code == 200:
                if params['show_proxy_info']:
                    info_country = get_proxy_info(prox)
                    print("info country", info_country)
                    if info_country == "United Kingdom":
                        ret_proxy = proxy['https']
                        ret_response = response
                        print("got valid proxy", ret_proxy)
                        break
                else:
                    ret_proxy = proxy['https']
                    ret_response = response
                    print("got valid proxy", ret_proxy)
                    break
            else:
                # ret_proxy = ''
                print(response.status_code)
                continue
        except:
            pass

    # print("params...", params['show_proxy_info'])
    # if params['show_proxy_info'] and ret_proxy != '':
    #     get_proxy_info(ret_proxy)
    print("before returning", ret_proxy)
    return ret_proxy, ret_response

######################################################

####### use requests code #################
t = time.time()
#print('about to fetch webpage')
currentUrl = ''  # to remove 'res/currentUrl' reference error
# res = ''
try:
    # res = requests.get(webPage)
    print("working here")
    # res = request_mod(webPage, params)

    proxy = ''
    response_used = ''
    proxy, response_used = get_valid_proxy_from_existing(webPage, params)  # getting valid proxy from existing list
    print("in response", proxy)
    # if proxy == '':
    if not proxy:
        print('Getting response from new list')
        proxy, response_used = get_valid_proxy(webPage, params['proxy_location'], params)  # getting valid proxy
        print("after new proxy", proxy)

        if not proxy:
            for i in range(0, 5):
                print("No valid proxy in new list, waiting 5 minutes")
                time.sleep(60)  # if no valid proxy from new list wait 5 minutes
            getHtml(webPage, driver, params,)

    res = response_used

    print("got res at line number 779")
    html = res.text
    problemUrl = False # to remove 'problemUrl' reference error
    currentUrl = res.url

    try:
        res.raise_for_status()
    except Exception as exc:
        print('There was a problem: %s' % (exc))
        html = ''

except Exception as e:
    print("PROBLEM with requests.get exception", e)
    #TODO output to error log file
    print('PROBLEM with requests.get', webPage)
    html = html = ''
    problemUrl = True  # to remove 'problemUrl' reference error
    # getHtml(webPage, driver, params, link)
# try:
#     res.raise_for_status()
# except Exception as exc:
#     print('There was a problem: %s' % (exc))
#     html = ''
#get the current url so that we can track any redirects
# currentUrl = res.url
print(f'if finds anything, {currentUrl}')
if currentUrl.find(invalidRedirect) == -1  or invalidRedirect == '' :  # e.g. ?dual_run=monty& is not in the current url, then it is valid
    currentUrlValid = True
else:
    html = ''
    pageReloads = pageReloads + 1
#wait for some additional time so that min time between page loads is webPageWait
elapsed = time.time() - t
wait = max(webPageWait - elapsed,0)
time.sleep(wait)
#add the html to html pages
htmlPages.append(html)
print('fetched web page', currentUrl , 'htnl count ' , len(html), 'load time', elapsed, 'waiting', wait)

#orig proxy processing 
        # print('Getting proxy for', params['mainurl'])
        # proxy = ''
        # proxy, response_not_used = get_valid_proxy_from_existing(params['mainurl'], params)  # getting valid proxy from existing list
        # print("in configuration", proxy)
        # # if proxy == '':
        # if not proxy:
        #     print('Getting from new list')
        #     proxy, response_not_used = get_valid_proxy(params['mainurl'],
        #                                                params['proxy_location'], params)  # getting valid proxy
        #     print("after new proxy", proxy)

        #     if not proxy:
        #         for i in range(0, 5):
        #             print("No valid proxy in new list, waiting 5 minutes")
        #             time.sleep(60)  # if no valid proxy from new list wait 5 minutes
        #         driver = configureNewDriver(params, driver) #TODO ? TEST works properly

        # options.add_argument('--proxy-server=' + proxy)  # setting the proxy as an argument