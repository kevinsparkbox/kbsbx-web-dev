import os
from google.cloud import storage
from google.cloud import bigquery
from retrying import retry
import sys
from pathlib import Path
import pandas as pd
import numpy as np
import random
import string
import csv
import itertools

from CoreScraperConstants import BUCKET_NAME, GOOGLE_STORAGE_FILE_PATH, WEBSITES_FOLDER, WEBSITES_FILE, TEST_GOOGLE_STORAGE_FILE_PATH
from CoreScraperConstants import KEYWORDS_FOLDER, SPARKBOX_COLOURS_FILE, REFERENCE_COLOURS_FILE, CONFIGURATION_FILE

from bigquery_utils import download_bq_df

CREATE_EXTRACT_TABLE = {

    "descriptions" : {
         "query_string":  """
            SELECT  skuid, retailer
            FROM DATASET.SOURCETABLE
            WHERE  retailer in  ( ??retailer?? ) 
            ORDER BY  retailer, skuid
            LIMIT LIMITXX
            ;
            """}, 

    # "results" : {
    #      "query_string":  """
    #         CREATE OR REPLACE TABLE DATASET.TEMPTABLEID
    #          AS
    #         SELECT  skuid, retailer
    #         FROM DATASET.SOURCETABLE
    #         WHERE  retailer in ( ??retailer?? )
    #         ORDER BY  retailer, skuid
    #         LIMIT LIMITXX
    #         ;
    #         """},      

    "full_descriptions" : {
         "query_string":  """
            SELECT  *
            FROM DATASET.SOURCETABLE
            WHERE  retailer in  ( ??retailer?? ) 
            ORDER BY  retailer, skuid
            LIMIT LIMITXX
            ;
            """}, 
}
	
	
def fetch_skuids_from_bq_desc(bqtable, retailer, params, google_project, google_credentials):
    row_limit = -1
    if row_limit <= 0:
        row_limit = sys.maxsize

    local_folder = params['opfpath']
    if local_folder.find('TEST') == -1:
        dataset = params['GBQ_DATASET']
        sourcetable = bqtable
    else:
        dataset = params['TEST_GBQ_DATASET']
        sourcetable = 'test' + bqtable
 
    print("FETCHING SKUIDS FROM BQ", dataset, bqtable)

    ext = CREATE_EXTRACT_TABLE[bqtable]
    #create the query string, if a single retailer convert to a list first
    if type(retailer) is not list:
        retailer = [retailer]
    retailer_str = ', '.join('"{0}"'.format(r) for r in retailer)
    query_string = ext["query_string"].replace("??retailer??", retailer_str)
    query_string = query_string.replace( "SOURCETABLE", str(sourcetable) )
    query_string = query_string.replace( "DATASET", str(dataset) )
    #set up the row limit in the query
    query_string = query_string.replace( "LIMITXX", str(row_limit) )

    df = download_bq_df(query_string, google_credentials, google_project, remove_UTC = True)

    #drop all duplicates
    df = df.drop_duplicates(ignore_index=True)
    df['skuid'] = df['skuid'].astype(str) # make sure it is a string as some skuid can be interpreted as long ints

    return df
    
def fetch_descriptions_from_bq( row_limit, retailer, local_folder, google_project, google_credentials, params):
    if row_limit <= 0:
        row_limit = sys.maxsize
   
    temp_bucket_folder = 'temp_descr'
    query = "full_descriptions"

    if local_folder.find('TEST') == -1:
        dataset = params['GBQ_DATASET']
        sourcetable = params['GBQ_TABLE_DESCRIPTIONS']
    else:
        dataset = params['TEST_GBQ_DATASET']
        sourcetable = params['TEST_GBQ_TABLE_DESCRIPTIONS']
 
    print("FETCHING DESCRIPTIONS FROM BQ", dataset, sourcetable)

    ext = CREATE_EXTRACT_TABLE[query]
    query_string = ext["query_string"]
    if retailer == "" or len(retailer) == 0:
        query_string = query_string.replace( "WHERE  retailer in  ( ??retailer?? ) ", "")
    else:
        #create the query string, if a single retailer convert to a list first
        if type(retailer) is not list:
            retailer = [retailer]
        retailer_str = ', '.join('"{0}"'.format(r) for r in retailer)
        query_string = query_string.replace("??retailer??", retailer_str)
    query_string = query_string.replace( "SOURCETABLE", str(sourcetable) )
    query_string = query_string.replace( "DATASET", str(dataset) )
    #set up the row limit in the query
    query_string = query_string.replace( "LIMITXX", str(row_limit) )
    
    df = download_bq_df(query_string, google_credentials, google_project, remove_UTC = True)

    #drop all duplicate skuid retailer combinations
    df = df.drop_duplicates(subset=['skuid', 'retailer'], ignore_index=True, keep='last')
    df['skuid'] = df['skuid'].astype(str) # make sure it is a string as some skuid can be interpreted as long ints

    return df

@retry(stop_max_delay=120000, wait_fixed=10000) #try for 120 seconds, wait 10 second between tries, issue with xlsx open
def get_df_from_GCS(local_folder, bucket_folder, source_blob_name, google_credentials, google_project, header_row = 0):
    #set up to read csv files, the consolidated colours file (colors reference) is xlsx so at the moment "hard coded to read the input sheet"
    delete_blob = False # delete the temp file created on google storage
    destination_file_name = Path(local_folder)/ (source_blob_name)        
    download_blob_delete(google_credentials, google_project, BUCKET_NAME, bucket_folder, source_blob_name, destination_file_name, delete_blob) 
    if '.csv' in source_blob_name:
    #read in the csv
        df = pd.read_csv(destination_file_name, low_memory=False, header = header_row)
    else: # assumes it is the xlsx ref_colours file
        df = pd.read_excel(io=destination_file_name, sheet_name='in')

    #if header rows are required
    if header_row > 0:
        temp_df = pd.read_csv(destination_file_name, low_memory=False)
        temp_df = temp_df.head(header_row)
        #generate a category and division lookup table
        header_dict = {}
        for c in temp_df.columns:
            sub_cat_dict = {'Division' : str(temp_df[c].name).split('.')[0],
                            'Category' : temp_df[c].loc[0]
                            }
            sub_cat = temp_df[c].loc[1]
            header_dict[sub_cat] = sub_cat_dict
    else:
        header_dict = {}
        

    return df, header_dict

@retry(stop_max_delay=60000, wait_fixed=10000) #try for 60 seconds, wait 10 second between tries
def fetch_websites_from_GCS( local_folder, google_project, google_credentials):
    if "TEST" in local_folder:
        bucket_folder = TEST_GOOGLE_STORAGE_FILE_PATH + WEBSITES_FOLDER
    else:
        bucket_folder = GOOGLE_STORAGE_FILE_PATH + WEBSITES_FOLDER

    websitesdf = get_df_from_GCS( local_folder, bucket_folder, WEBSITES_FILE, google_credentials, google_project )

    return websitesdf

#@retry(stop_max_delay=120000, wait_fixed=10000) #try for 120 seconds, wait 10 second between tries, issue with xlsx open
def fetch_keywords_config_from_GCS( local_folder, google_project, google_credentials, retailer):

    if "TEST" in local_folder:
        bucket_folder = TEST_GOOGLE_STORAGE_FILE_PATH + KEYWORDS_FOLDER
    else:
        bucket_folder = GOOGLE_STORAGE_FILE_PATH + KEYWORDS_FOLDER

    configurationdf, header_dict = get_df_from_GCS( local_folder, bucket_folder, CONFIGURATION_FILE, google_credentials, google_project )
    #remove the TEST prefix if there
    retailer = retailer.replace("TEST","")
    #select the correct configuration
    config_row = configurationdf[ configurationdf['retailer'] == retailer].reset_index(drop = True)
    retailer_name = config_row['retailer_name'][0]
    retailer_region = config_row['retailer_region'][0]
    default_department = config_row['default_department'][0]
    keywords_categories = config_row['keywords_categories'][0]
    keywords_departments = config_row['keywords_departments'][0]
    keywords_fabrics = config_row['keywords_fabrics'][0]

    
    categoriesdf, category_dict = get_df_from_GCS( local_folder, bucket_folder, keywords_categories, google_credentials, google_project, header_row = 2 )
   
    departmentsdf, header_dict = get_df_from_GCS( local_folder, bucket_folder, keywords_departments, google_credentials, google_project )
    fabricdf, header_dict = get_df_from_GCS( local_folder, bucket_folder, keywords_fabrics, google_credentials, google_project )
    sparkbox_coloursdf, header_dict = get_df_from_GCS( local_folder, bucket_folder, SPARKBOX_COLOURS_FILE, google_credentials, google_project )
    rgb_coloursdf, header_dict = get_df_from_GCS( local_folder, bucket_folder, REFERENCE_COLOURS_FILE, google_credentials, google_project )

    return departmentsdf, categoriesdf, fabricdf, sparkbox_coloursdf, rgb_coloursdf, retailer_name, retailer_region, default_department, category_dict

#function to download a file object (blob) from google storage bucket
@retry(stop_max_delay=60000, wait_fixed=10000) #try for 60 seconds, wait 10 second between tries
def download_blob_delete(google_credentials, google_project, bucket_name, bucket_folder, source_blob_name, destination_file_name, delete_blob):
    storage_client = storage.Client(project = google_project, credentials = google_credentials)
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.get_blob(  bucket_folder + '/' + source_blob_name)
    try:
        blob.download_to_filename(destination_file_name)
        print('Blob {} downloaded to {}.'.format(source_blob_name, destination_file_name))
        if delete_blob:
            blob.delete()
        success = True
    except:
        print('Unable to download blob {} to {}.'.format(source_blob_name, destination_file_name))
        print('bucket name ', bucket_name)
        print('bucket folder', bucket_folder)
        success = False
    return success

#function to delete a file object (blob) from google storage bucket
@retry(stop_max_delay=60000, wait_fixed=10000) #try for 60 seconds, wait 10 second between tries
def blob_delete(google_credentials, google_project, bucket_name, bucket_folder, source_blob_name):
    storage_client = storage.Client(project = google_project, credentials = google_credentials)
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.get_blob(  bucket_folder + '/' + source_blob_name)
    try:
        blob.delete()
        success = True
    except:
        print('Unable to delete blob {} '.format(source_blob_name))
        success = False
    return success

