# -*- coding: utf-8 -*-

import os

#folder with chrome data cookies etc
if os.name == 'posix':
    INSTA_CHROME_DATA_DIR = "chrome-insta-data"
else:
    INSTA_CHROME_DATA_DIR = "chrome-insta-data_win"

#configuration, match instagram name with retailer name and configure matching string for finding products (reg ex)
INSTA_RETAILER_CONFIG = {
    'Pimkie' :      {   'replace_str':  { 'find' : '#' , 'replace' :  ' #'},
                        'product_str' :  r"(\d\d\d\d\d\d\w\w\w\w\w\w)" ,
                    },
    'Boden' :       {   'replace_str':  { 'find' : '#' , 'replace' :  ' #'},
                        'product_str' :  r"(\d\d\d\d\d\d\w\w\w\w\w\w)" ,
                    },             
        }

#to reset instagram login
RESET_LOGIN = False

#to attempt to scrape with no login set True
INSTA_FORCE_NO_LOGIN = False

#set up the table names
TEST_GBQ_TABLE_INSTA_RESULTS = "testinstaresults"
GBQ_TABLE_INSTA_RESULTS = "instaresults"

#set up runtime indication file for insta scrapes
INSTA_RUNTIME_FILE = "running.csv"
INSTA_RESULTS_FILE = "OutputInstaData.csv"
