#%% use the runpy modules
import runpy
import datetime as dt
import os
from pathlib import Path
import sys
import time
import random
import subprocess
import os
import pandas as pd
from datetime import datetime
import glob
from random import randint
import multiprocessing
from multiprocessing import Pool
from functools import partial
from multiprocessing.pool import ThreadPool
import shutil
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import logging

from CoreScraperOutputModules import check_if_output_exists, find_days_since_output_updated, createOutputPath
from CoreScraperOutputModules import  output_running_file
from Corescraper_fetch_BQ_data import fetch_websites_from_GCS
from CoreScraperConstants import CONTINUOUS_LOOP, RUNTIME_MULTIPLIER, MINIMUM_RUNTIME, OUTPUT_OPTION
from CoreScraperConstants import BUCKET_NAME, GOOGLE_STORAGE_FILE_PATH, TEST_GOOGLE_STORAGE_FILE_PATH, PROGRESS_FOLDER
from CoreScraper_utils import configure_googleapi_credentials, init_logging
from CoreScraperConstants import NUM_PROCESSORS_MULTIPLIER, PROGRESS_FILE, EMAIL_NOTIFICATION_RECIPIENTS
from CoreScraperConstants import  WEB_RUNTIME_FILE,  WEB_RESULTS_FILE
from InstaScraperConstants import INSTA_RUNTIME_FILE, INSTA_RESULTS_FILE
from WeatherScraperConstants import WEATHER_RUNTIME_FILE, WEATHER_RESULTS_FILE
from CoreScraper_utils import restart_computer


#%% function to change the directory to the same directory as this script, assuming other scripts are in the same place for run_module
def change_working_direcory():
    abspath = os.path.abspath(__file__)
    dname = os.path.dirname(abspath)
    os.chdir(dname)
    #logging.info('Changed working Directory to ', dname)    

# function to convert a day string to an integer
def convert_day_to_daystr(day):
    days = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun']
    daystr = days[day]
    return daystr

# function to return today's date and day as strings
def get_today_date_day():
    #get todays date and day of week
    todayDate = dt.datetime.today()
    weekday = int(todayDate.weekday())
    #convert to strings
    todayDay = convert_day_to_daystr(weekday)
    todayDateStr = todayDate.strftime("%Y%m%d")
    return todayDate, todayDateStr, todayDay

# function to check if duplicate links need to be reset according to the reset_interval specified in the web_pages_to_scrape
#if the interval is 0 the duplicate links are reset every run, if N, every N days (interval of N days)
def check_duplicates_reset(fpath, duplicateLinksFileName, reset_interval, todayDate, google_project, google_credentials):    
    reset_duplicates = False
    #updated look on GCS for suplicate links
    #location = "Local"   
    location = "Google_Storage"
    duplicatesFileexists =  check_if_output_exists(fpath,duplicateLinksFileName, location, google_project, google_credentials)
    if duplicatesFileexists:
        daysSinceMod, hoursSinceMod = find_days_since_output_updated(fpath,duplicateLinksFileName, location, google_project, google_credentials)
        if daysSinceMod >= reset_interval: 
            reset_duplicates = True
        else:
            reset_duplicates = False
        logging.info(('reset duplicates ', hoursSinceMod, daysSinceMod, reset_duplicates))
    else:  # if duplicates does not already exist then it needs to be gerenated
        reset_duplicates = True
    return reset_duplicates    
# process all of the webpages, return those that should be run now
# it is scheduled to run now if today is one of the days to run, if the time is after the start_time, if it has been run_interval days 
# since it produced an output, if it has not started a run already today
def is_scheduled_to_run_now(outputpath, web_pages, google_project, google_credentials ):
    scheduled_pages = []
    time_now = datetime.now().time()
    time_end = "23:59"
    for web_page_to_scrape in web_pages:
        #TODO check not end of csv file, i.e. web_page_to_scrape['module'] != nan
        logging.info(('Checking if scheduled to run now ' , web_page_to_scrape['module']) )
        #set the working directory to the parent dir to this script
        #change_working_direcory()

        mod_name = web_page_to_scrape["module"].strip()
        days_to_run = web_page_to_scrape["days"]
        run_interval = web_page_to_scrape["run_interval"]
        time_start = web_page_to_scrape["start_time"]
        
        #get today's date and day
        todayDate, todayDateStr, todayDay = get_today_date_day()
        #find out the interval to the last time this was run
        #location = "Local"
        location = "Google_Storage"  # look on google storage for the file
        #configure the output path and files to check for scheduled to run
        fpath = outputpath + '/' + mod_name
        if "insta_" in fpath.lower():
            #configure the name of the runtime file
            started_fname_to_check = INSTA_RUNTIME_FILE
            run_interval_fname_to_check = INSTA_RESULTS_FILE
            #adjust the file path so that it does not include the insta_ part
            #fpath = fpath.replace("insta_","").replace("Insta_", "") 
        if "weather_" in fpath.lower():
            #configure the name of the runtime file
            started_fname_to_check = WEATHER_RUNTIME_FILE
            run_interval_fname_to_check = WEATHER_RESULTS_FILE
            #adjust the file path so that it does not include the weather part
            #fpath = fpath.replace("weather_","").replace("Weather_", "") 
        else:
            started_fname_to_check = WEB_RUNTIME_FILE
            run_interval_fname_to_check = WEB_RESULTS_FILE

        logging.info('Checking if scheduled to run now in location {} started fname {}'.format(fpath, started_fname_to_check ))
        last_run_days, last_run_hours = find_days_since_output_updated(fpath, run_interval_fname_to_check, location, google_project, google_credentials)
        logging.info(('Time Since Last Run', last_run_days, last_run_hours))
        last_start_days, last_start_hours = find_days_since_output_updated(fpath, started_fname_to_check, location, google_project, google_credentials)
        logging.info(('Time Since Last Recently Started', last_start_days, last_start_hours))
        hours_since_recent_start =  last_start_hours + (24 * last_start_days)
        logging.info(('Hours since recently started ', hours_since_recent_start ))
        web_page_to_scrape["last_run_days"] = last_run_days
        web_page_to_scrape["last_run_hours"] = last_run_hours
        recent_start_test_duration = 1.05 * web_page_to_scrape["run_time"] 
        logging.info(('Run Time Limit ',  web_page_to_scrape["run_time"] , ' recent started duration ', recent_start_test_duration ))

        #check to see if it has been started within the recently started duration 
        if last_start_days == 9999 or  hours_since_recent_start >  recent_start_test_duration :
            has_recently_started = False
        else:
            has_recently_started = True

        #if today is a run day and it has not been run in the run_interval, then run the python module to scrape the web pages
        if  (todayDay in days_to_run and run_interval <= last_run_days and 
                            is_in_time_period(time_start, time_end, time_now) and not has_recently_started):
            logging.info((mod_name, last_run_days, 'Run Day is Today AND interval okay and in run_time and not started recently -  Run Now '))
            scheduled_pages.append(web_page_to_scrape)
        else:
            logging.info((mod_name, last_run_days, 'Recently Run or Run Day is Not Today or not at run time or recently started -  Do Not Run Again Today'))

    return scheduled_pages

#function to convert HH:MM:SS time string to seconds
def get_sec(time_str):
    # if a valid format get the h, m, s, else assume 0
    try:
        h, m, s = time_str.split(':')
    except:
        h = m = s = 0
    return int(h) * 3600 + int(m) * 60 + int(s)



 #%% Function to scrape a web page
def scrapeWeb(outputpath, google_project, google_credentials, web_page_to_scrape ):

    logging.info(('Processing Web Page' , web_page_to_scrape))
    #set up the variables to check for duplicate links
    duplicateLinksFileName = 'duplicatelinks.csv'
    #set the working directory to the same one as this script
    #change_working_direcory()
    starttime = time.time()
    #trap any errors so that the process continues with the next web page to scrape
    
    runtime = web_page_to_scrape["run_time"]*60*60
    if runtime > 0:
        run_time_limit = RUNTIME_MULTIPLIER * runtime
    else:
        run_time_limit =  MINIMUM_RUNTIME
    mod_name = web_page_to_scrape["module"].strip()
    fpath = outputpath + '/' + mod_name
    #find out if the duplicate links need resetting
    reset_interval = web_page_to_scrape['reset_interval']
    todayDate, todayDateStr, todayDay = get_today_date_day()
    reset_duplicates =  check_duplicates_reset(fpath, duplicateLinksFileName, reset_interval, todayDate, google_project, google_credentials)
    google_params = { 
        'google_project': google_project,
        'google_credentials' : google_credentials }
    sys.argv = ['', outputpath, reset_duplicates, run_time_limit, google_params] # pass the outputpath and reset_duplicates control and runtime limit as arguments to the module to be run
    runpy.run_module(mod_name, run_name='__main__')        

    return True




# Function to decide which web pages to scrape depending on priority and scrape interval
def is_in_time_period(time_start, time_end, nowTime):
    #for am pm time formats
    #time_start = '07:00AM'
    #time_end = '11:00PM'
    #endTime = dt.datetime.strptime(time_end, "%I:%M%p").time()
    #startTime = dt.datetime.strptime(time_start, "%I:%M%p").time()
    #for 24 hour clock formats 
    endTime = dt.datetime.strptime(time_end, "%H:%M").time()
    startTime = dt.datetime.strptime(time_start, "%H:%M").time()

    if startTime < endTime:
        return nowTime >= startTime and nowTime <= endTime
    else: #Over midnight
        return nowTime >= startTime or nowTime <= endTime

#runs in a continuous loop 
def scrapePagesInPriority(outputpath, pages_to_process):    
    #construct a list of web pages to scrape
    #intermediate conversion to dfs for prioritisation and selection

    google_credentials, google_project = configure_googleapi_credentials()
    #create the local output path if it does not already exist, folder name is file name (if running for first time on new machine or afer folder cleared)
    filename = ""
    createOutputPath(filename, outputpath, "Local")

    #remove all of the chrome cookies folders, try to avoid the issue of the browser being locked caused by an issue with the website
    #kill any open browser windows
    if os.name == 'posix':
        os.system("killall -9 'chromedriver'")
        os.system("killall -9 'chrome'")
    else:
        os.system("taskkill /im chrome.exe /f")
    search_path = outputpath + '**/chrome-data*/'
    pathList = glob.glob(search_path, recursive=True)
    for file_path in pathList:
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            logging.warning('Failed to delete %s. Reason: %s' % (file_path, e))


    continueLoop = True
    while continueLoop:
        #reset the logging
        logpath =  outputpath + 'AA_LOGS/'
        filename = "RunModules"
        init_logging(filename, logpath)
        #if a local filename is provided o pecify pages to process use it, otherwise download from GCS
        if len(pages_to_process) > 0:
            websitesdf = pd.read_csv(pages_to_process)
        else:
            websitesdf, header_dict = fetch_websites_from_GCS( outputpath,  google_project, google_credentials)
        web_pages = websitesdf.to_dict(orient='records')
        web_pages_to_scrape = is_scheduled_to_run_now(outputpath, web_pages, google_project, google_credentials )
        #convert from dict back to df
        web_pagesdf = pd.DataFrame.from_dict(web_pages_to_scrape)
        num_procs = multiprocessing.cpu_count()
        logging.info('Read in Web Pages to Process, Number of Available CPUs {}'.format(num_procs))
        nummber_of_scrapers_to_select = int(NUM_PROCESSORS_MULTIPLIER * num_procs)
        #nummber_of_scrapers_to_select = 2 # test
        logging.info(('Number of Scrapers to Select ', nummber_of_scrapers_to_select))
        #if there are pages to process, select the next "number_of_scrapers_to_select" pages, prioritise by priority and time since last run
        if len(web_pagesdf) > 0:
            web_pagesdf.sort_values(['priority',  'run_time'], ascending=[True,  True], inplace=True)
            web_pagesdf.reset_index(drop=True, inplace=True)
            selected_web_pagesdf = web_pagesdf.head(nummber_of_scrapers_to_select) 
            logging.info(('Selected web pages', selected_web_pagesdf))
            selected_web_pages = selected_web_pagesdf.to_dict(orient='records')
            logging.info(('Number of Web Pages to Process ' , len(selected_web_pages)))
            logging.info(('Prioritised Web Pages ' , selected_web_pagesdf))
            #set the running file for each selected scraper, do it here as well as in the main scraper in case there is a delay in starting 
            #the scraper in the multiprocessing pool, to help avoid 2 machines running the same scraper at the same time 
            for wp in selected_web_pages:
               output_running_file(outputpath + wp['module'], google_project, google_credentials, GOOGLE_STORAGE_FILE_PATH, TEST_GOOGLE_STORAGE_FILE_PATH )

            # #option to run sequentially for testing
            # for wp in selected_web_pages:
            #     scrapeWeb(outputpath, google_project, google_credentials, wp)
            p = Pool(int(nummber_of_scrapers_to_select ))
            #p = ThreadPool(int(NUM_PROCESSORS_MULTIPLIER * num_procs ))
            mp_worker_func = partial(scrapeWeb, outputpath, google_project, google_credentials)
            p.map(mp_worker_func, selected_web_pages)
            p.close()
            p.join()
       
        #close and flush the logging
        logging.shutdown()

        #repeat loop indefinitely if CONTINUOUS_LOOP
        if not CONTINUOUS_LOOP:
            continueLoop = False
        else:
            logging.info('XXX  Repeating Loop, waiting 200 to 400 seconds   XXXXXXXXX')
            time.sleep(randint(200,400))
            restart_computer()


