import math
import random
import bs4
import datetime as dt
import pandas as pd
from pandas.api.types import is_numeric_dtype
from pathlib import Path
import lxml
import time
import sys
import json
import html5lib
import os
import re, string
import unicodedata
import platform
import gc
import random
import traceback
import time
import logging
from io import StringIO

from CoreScraperOutputModules import check_if_output_exists, createOutputPath
from CoreScraperOutputModules import outputDataFrameToFile, readDataFrameFromFile, outputDataFrameToDatabase, delete_running_file
from CoreScraperOutputModules import load_image_filenames_from_gcs, output_running_file, generate_error_log_update_progress, output_results_progress_log
from CoreScraperErrorModules import  send_email_notification, exit_if_exceeded_run_time_limit
from CoreScraperConstants import MASTER_CHROME_DATA_DIR, MASTER_CHROME_DATA_PATH, MINIMUM_RUNTIME, PROGRESS_FOLDER, PROGRESS_FILE, FRONT_PAGE_ONLY_FOLDER
from CoreScraperConstants import IGNORE_EXISTING_IMAGES, EMAIL_NOTIFICATION_RECIPIENTS
from CoreScraperConstants import GOOGLE_STORAGE_FILE_PATH, TEST_GOOGLE_STORAGE_FILE_PATH 
from Corescraper_fetch_BQ_data import fetch_skuids_from_bq_desc, download_blob_delete
from CoreScraper_utils import configure_googleapi_credentials, closeDriver, init_logging
from CoreScraper_getHtml import getHtml, capture_save_screenshot, copy_cookies, WebPageItems, ProductDataFrame, configure_reset_chrome_VPNextensions_cookies
from CoreScraperDescriptions import update_attributes_descriptions_and_output
from CoreScraper_utils import restart_computer


# Interpret the webpage, find the products and store data in data frame
def find_products_on_webpage(next_page_found, webSoup, link, params, methods_to_run, driver):
    #logging.info("in find_products_on_webpage, next page found", next_page_found)
    opfpath = params['opfpath']
    #get the product items from the web soup
    webPage = WebPageItems(webSoup, link, driver, params)
    # output the soup for the entire web page
    if methods_to_run['output_webpage_to_file']: 
        webPage.output_file(opfpath, 'websoup.txt')
    #find all products on the webpage
    webPage.find_all_products()
    listofProducts = webPage.products
    #find the next page if switched on and it has not already been found
    if params['findNextPage'] and not next_page_found :  
        webPage.find_next_page_from_webpage()
    #get the promo message - default returns empty string
    promoMessage = webPage.get_promo_message()

    return webPage.nextPage, listofProducts, promoMessage  #returns empty df if nothing found


# Interpret the webpage, find the products and store data in data frame
def process_products_on_webpage(listofProducts, promoMessage, link, params, methods_to_run, driver):
    #logging.info("in process_products_on_webpage", len(listofProducts))
    reset_interval = params['descriptions_reset_interval'] # how many images/descriptions to force a driver reset
    existing_product_descriptions = params['existing_product_descriptions'] # get the descriptions and images already collected
    #reset the output dataframe
    opdf = pd.DataFrame()
    description_opdf = pd.DataFrame()
    descr_img_count = 0
    #process each product and add to the output dataframe
    for i, productSoup in enumerate(listofProducts):
        product = ProductDataFrame(productSoup, link,params, driver)
        product.pos = i
        #do the output first as useful when setting up new parameters
        if methods_to_run['output_product_to_screen'] : product.output_screen()
        #these are always run by default
        product.get_description()
        product.get_url()
        product.get_imageurl()
        product.get_ticket_previous_price()
        if promoMessage != "":  # promo mesage has been captured at web page level
            product.promoMessage = promoMessage
        else: #promo message from product details
            product.get_product_promo_message()
        product.get_banner()
        product.get_brand()
        product.get_reviewscore() 
        product.get_reviewcount()
        product.get_stock()
        product.get_skuid()
        #fetch and output output product descriptions, product image, after checking here if not already in the descriptions database
        #to get the image need to fetch the description first
        if ((methods_to_run['output_image_to_file'] or methods_to_run['output_description_to_file']) and
                             (str(product.skuid)  not in existing_product_descriptions) and  product.skuid != None and product.ticketprice > 0 ): 
            product.output_description_file()
            descr_img_count = descr_img_count + 1
            existing_product_descriptions.append(product.skuid)
            #if image is also selected then fetch it
            if methods_to_run['output_image_to_file'] :
                product.output_image_file()

            #reset the driver if required
            if descr_img_count%reset_interval == 0 and descr_img_count > 0 :
                logging.info(('Resetting driver in descriptions', reset_interval, descr_img_count))
                closeDriver(driver)
                driver = None
                product.driver = driver

        #output values to the screen
        if methods_to_run['output_product_values_to_screen'] : product.display_product_values()
        product.createOutputRow()
        #if the product skuid is defined, and the ticket price and prevprice are defined  add the product row to the dataframe of results
        if ( product.productdf.loc[0, 'skuid'] != None and float(product.productdf.loc[0, 'ticketprice']) > 0
                                                         and float(product.productdf.loc[0, 'prevprice']) > 0 ) :
            opdf = opdf.append(product.productdf, ignore_index = True)
        #if the product description is defined, add the product description to the data frame of description results
        if len(product.product_descriptiondf) > 0:
            description_opdf = description_opdf.append(product.product_descriptiondf, ignore_index = True)
        #update the driver with any value updated in the class product in case it has been reset in the image/description methods
        driver = product.driver

    return opdf, description_opdf, existing_product_descriptions, driver  #returns empty dfs if nothing found

#function to extract the first price in a string, return 0 if none
#copes with both £ and € and removes thousands seperator
def extractPrice(strval, currency):
    if currency == '€':
        thousand_sep='.'
        decimal_sep = ','
    else:
        thousand_sep=','
        decimal_sep = '.'
    #make sure it is a string
    strval = str(strval)    
    #remove any thousand seperator
    strval = strval.replace(thousand_sep, "")
    #put uk decimal seperator
    strval = strval.replace(decimal_sep, ".")
    numeric_const_pattern = '[-+]? (?: (?: \d* \. \d+ ) | (?: \d+ \.? ) )(?: [Ee] [+-]? \d+ ) ?'
    rx = re.compile(numeric_const_pattern, re.VERBOSE)
    op = rx.findall(strval)
    if len(op) > 0:
        op = float(op[0])
    else:
        op = 0
    return op

 

#check the tag to see if it contains a valid tag string and does not contain an invalid tag string
def checkValidityofTag_full(tag, params):
    validtags = params['validtags']
    invalidtags = params['invalidtags']
    duplicatelinks = params['duplicatelinks']
    minimumdepth = params['minimumdepth']
    maximumdepth = params['maximumdepth']
    # find if it is a potential valid tag, if it contains a valid tag string
    validtag = False
    depth = tag.count('/')
    for tagid in validtags:
        # check for correct depth, contains substring in validtags, not yet found
        if depth >= minimumdepth and depth <= maximumdepth and tag.find(tagid) >-1 and validtag == False:
            #preliminary set to valid, but reset if it contains an invalid tag substring
            validtag = True
            for tagid2 in invalidtags:
                if tag.find(tagid2) >-1:
                    validtag = False
            #also set as an invalid tag if it is one of the duplicate links
            for tagid2 in duplicatelinks:
                if tag == tagid2 :
                    logging.info(('Rejecting duplicate link', tag))
                    validtag = False
    return validtag

def checkValidityofTag_validstring(tag, params):
    validtags = params['validtags']
    invalidtags = params['invalidtags']
    # find if it is a potential valid tag, if it contains a valid tag string
    validtag = False
    for tagid in validtags:
        # check for correct depth, contains substring in validtags, not yet found
        if tag.find(tagid) >-1 and validtag == False:
            #preliminary set to valid, but reset if it contains an invalid tag substring
            validtag = True
            for tagid2 in invalidtags:
                if tag.find(tagid2) >-1:
                    validtag = False
    return validtag

#generate the tag info, url, department and category
def generateLink(tag, params):
    departmentlevel = params['departmentlevel']
    categorylevel = params['categorylevel']
    subcategorylevel = params['subcategorylevel']
    tagwords = tag.split('/')
    #department level
    if len(tagwords) > departmentlevel:
        department = tag.split('/')[departmentlevel]
    else:
        department = 'none'
    #category level
    if len(tagwords) > categorylevel:
        category = tag.split('/')[categorylevel]
    else:
        category = 'none'
    #subcategory level
    if len(tagwords) > subcategorylevel:
        subcategory = tag.split('/')[subcategorylevel]
    else:
        subcategory = 'none'
        
    #set up a priority = depth, but if contains "sale" set to 0,  used later when deleting duplicates
    #NB this is now overidden by number of products on the webpage
    if category.lower().find('sale') > -1 or category.lower().find('outlet') > -1:
        priority = 0
    else:
        priority = tag.count('/')

    link = {
        "topurl" : tag,
        "department" : department,
        "category" : category, 
        "subcategory" : subcategory,
        "depth" : tag.count('/'),
        'priority' : priority }
    return link

#function to reset the duplicate links, i.e. find the current set from the latest results, and write the duplicate links to csv
def resetDuplicateLinks(fpath, resultsdf, google_project, google_credentials):
    if not resultsdf.empty :
        logging.info('Processing to find duplicate/overlapping links')
        duplicatelinks = findLinksToDrop(resultsdf)
        logging.info(('links dropped ', duplicatelinks))
        #write the list of duplicate links to csv
        df = pd.DataFrame(duplicatelinks, columns=['links'])  
        #save duplicate links to both GCS and local
        location = "Local" 
        outputDataFrameToFile(df, fpath, 'duplicatelinks.csv', location, google_project, google_credentials, 
                            GOOGLE_STORAGE_FILE_PATH, TEST_GOOGLE_STORAGE_FILE_PATH )
        location = "Google_Storage" 
        outputDataFrameToFile(df, fpath, 'duplicatelinks.csv', location, google_project, google_credentials,
                            GOOGLE_STORAGE_FILE_PATH, TEST_GOOGLE_STORAGE_FILE_PATH )
    return

#find the links from the top level menus to process
def captureScreenshotsToplevel( webPages, params, google_project, google_credentials):
    fpath = params['opfpath']
    num_screenshots = params['numscreenshots']

    webPageCount = 0
    driver = None

    #save the current status of the scroll variables, change to fetch a single page, and ensure the scraping is set to download images
    current_scrollDownPage = params['scrollDownPage']
    current_disable_image_loading = params['disable_image_loading']
    params['scrollDownPage'] = False
    params['disable_image_loading'] = False

    #iterate through each web page
    for webPage in webPages: # loop over each link
        fullWebPage = createValidUrl(webPage, params['urlSwitch'])
        #capture and save the image 
        if webPageCount < num_screenshots :
            #open the webpage
            webHtml, driver, currentUrl, problemUrl =  getHtml(fullWebPage,  driver, params,  wait_for_page_load = True, clear_popups = True)
            logging.info(('Saving to fpath', fpath))
            time.sleep(params['waitForPageLoad'])
            driver = capture_save_screenshot(webPageCount, driver, params, fpath , 'MainPageImage', google_project, google_credentials)
            #driver check
            #str1 = driver.capabilities
            str1 = driver.capabilities['browserVersion']
            str2 = driver.capabilities['chrome']['chromedriverVersion'].split(' ')[0]
            #logging.info(('Browser ' , str1))
            #logging.info(('Driver ' , str2))
            if str1[0:2] != str2[0:2]: 
                logging.info("WARNING: please download correct chromedriver version")
        webPageCount = webPageCount + 1

    #finished screenshots reset the status of the scrollDownPage variables and download image settings
    params['scrollDownPage'] = current_scrollDownPage
    params['disable_image_loading']  = current_disable_image_loading
    closeDriver(driver) # close the driver

    return 

def getLinksfromWebPages(webPages, driver, params):
    mainurl = params['mainurl']
    htmlParser = params['htmlParser']
    tags = [] 
    rejecttags = []

    for webPage in webPages: # loop over each link
        fullWebPage = createValidUrl(webPage, params['urlSwitch'])
        #get the html from the page
        webHtml, driver, currentUrl, problemUrl =  getHtml(fullWebPage,  driver, params, clear_popups = True)
        #get the html dom
        webSoup = bs4.BeautifulSoup(webHtml[0] , htmlParser)
        # get all href links on the page
        href_tags = webSoup.find_all(params['tagref'], href=True)
        #logging.info('number of href tags', params['tagref'])
        #logging.info('XXX Finding Links for web page ', webPage)
        for a in href_tags:
            tag = a["href"].strip()
            # logging.info('tag', tag)
            if tag.find('http') == -1 : 
                if ('url_to_prefix_to_link' in params.keys()) and (params['url_to_prefix_to_link'] != None)  : # check if key is present for compatibility with older configurations
                    tag = params['url_to_prefix_to_link'] + tag
                else:
                    tag = mainurl + tag #if the link does not contain http add the main url
            #if the tag contains ?switches, or #switches  remove them
            if params["remove_switches_link"]:
                if tag.find('?') >-1:  tag = tag[:tag.find('?')]
                if tag.find('#') >-1:  tag = tag[:tag.find('#')]
            #if the tag contains a trailing '/' remove it , e.g. Bodens duplicate tags because sonme contain '/' some not:
                if len(tag) > 0 and tag[len(tag)-1] == '/':
                    tag = tag[0:len(tag)-1]
            #check that it is a valid link,  keep if it contains valid text, reject if it contains invalid text
            validtag = checkValidityofTag_validstring(tag, params)
            #logging.info(validtag, tag)
            if validtag:
                if tag not in tags:
                    tags.append(tag)
            elif tag not in rejecttags:
                rejecttags.append(tag)

    return tags, rejecttags, driver


#search through the webpages to find the links to process
def findLinkstoProcess( webPages, params, google_project, google_credentials):
    max_link_depth = params["max_link_depth"]
    fpath = params['opfpath']
    #if not resetting the duplicate links, read in the duplicate links from the duplicate links csv (if it exists)
    #when links are validty checked they are rejected if they are identified as a duplicate link
    if not params['resetduplicatelinks']:
        fname = 'duplicatelinks.csv'
        #location = "Local"  
        #updated look on Google Storage for duplicate links 
        location = "Google_Storage"    
        opexists = check_if_output_exists(fpath,fname, location, google_project, google_credentials)
        if opexists:
            df = readDataFrameFromFile( fpath, fname, location)
            duplicatelinks = df['links'].tolist()
        else:
            duplicatelinks = []
        #logging.info('links dropped ', duplicatelinks)
        params['duplicatelinks'].append(duplicatelinks)
    
    #save the current status of the scroll variables and change to use  single page
    current_scrollDownPage = params['scrollDownPage']
    params['scrollDownPage'] = False

    links = []
    rejectlinks = []
    orig_webPages = webPages
    #loop around for links, occassionally the VPN takes too long to connect and webpage is blank and zero links
    retries = 0
    while len(links) == 0  and retries < 5:
        logging.info(('Searching for Links ', retries))
        driver = None
        href_tags = [] 
        rejecthref_tags = []
        href_tag_depth = 0
        # if sitemap links are not defined,  get the href tags, searching down the link tree to max_link_depth
        if len(params['sitemap_links'] ) == 0:
            while href_tag_depth < max_link_depth:
                new_href_tags, new_rejecthref_tags, driver = getLinksfromWebPages(webPages, driver, params)
                for href_tag in new_href_tags:
                    if href_tag not in href_tags:
                        href_tags.append(href_tag)
                for href_tag in new_rejecthref_tags:
                    if href_tag not in rejecthref_tags:
                        rejecthref_tags.append(href_tag)
                href_tag_depth = href_tag_depth +1
                webPages = href_tags
                #logging.info('Link Search Depth ', href_tag_depth)
                #logging.info('Number of weblinks found from Search ', len(webPages))
                #logging.info('Valid web links found ', webPages)
        else:  # site map is defined
            #find links from the sitemap page (s)
            site_map_pages = params['sitemap_links']
            href_tags, rejecthref_tags, driver = getLinksfromWebPages(site_map_pages, driver, params)  
            #sort out invalid url combinations (chrono24)
            new_href = []
            for ref in href_tags:
                newref = ref.replace('..', '.').replace('./','/')
                new_href.append(newref)
            href_tags = new_href 
            #logging.info('Number of weblinks found from Sitemap', len(href_tags))
            #logging.info('Valid web links found ', href_tags)

        #apply the full validity checks to all href_tags, now include minimum depth and exclude duplicate links
        valid_href_tags = []
        for tag in href_tags: 
            #check that it is a valid link,  keep if it contains valid text, reject if it contains invalid text
            validtag = checkValidityofTag_full(tag, params)
            #logging.info(validtag)
            if validtag:
                if tag not in valid_href_tags:
                    valid_href_tags.append(tag)
            elif tag not in rejecthref_tags:
                rejecthref_tags.append(tag)

        #convert the href href_tags to url links
        valid_href_tags = params['starting_links'] + valid_href_tags
        for href_tag in valid_href_tags:
            link = generateLink(href_tag, params)
            links.append(link)

        for href_tag in rejecthref_tags:
            link = generateLink(href_tag, params)
            rejectlinks.append(link)
        
        #if no links are found, increment the retries, close the driver and wait before trying again
        if len(links) == 0:
            retries += 1 
            closeDriver(driver)
            webPages = orig_webPages
            logging.info('No links - wait for 10 and try again')
            time.sleep(10)

    logging.info('Outputting Links')
    #output the links to a local csv for checking , and to GCS bucket       
    df = pd.DataFrame(links)  
    location = "Local"
    outputDataFrameToFile(df, fpath, 'links.csv', location, google_project, google_credentials, GOOGLE_STORAGE_FILE_PATH, TEST_GOOGLE_STORAGE_FILE_PATH )
    location = "Google_Storage"   
    outputDataFrameToFile(df, fpath, 'links.csv', location, google_project, google_credentials, GOOGLE_STORAGE_FILE_PATH, TEST_GOOGLE_STORAGE_FILE_PATH )
    
    #output the reject links to a local csv for checking        
    df = pd.DataFrame(rejectlinks)  
    location = "Local"
    outputDataFrameToFile(df, fpath, 'rejectlinks.csv', location, google_project, google_credentials, GOOGLE_STORAGE_FILE_PATH, TEST_GOOGLE_STORAGE_FILE_PATH )
    location = "Google_Storage"   
    outputDataFrameToFile(df, fpath, 'rejectlinks.csv', location, google_project, google_credentials, GOOGLE_STORAGE_FILE_PATH, TEST_GOOGLE_STORAGE_FILE_PATH )

    #finished  link capture, reset the status of the scrollDownPage variables
    params['scrollDownPage'] = current_scrollDownPage 

    return links, driver


       




#%% Find links to drop
# identify which web links can be dropped without reducing the number of unique skuids
def findLinksToDrop(resultsdf):
    canBeDropped = []
    masterLink = []
    invalidlinks = []

    #process stage 1 - find each link, remove it, if the number of unique skus is the same, it is a candidate for being dropped
    orignumskus = resultsdf['skuid'].nunique()
    links = resultsdf['webPage'].unique()
    for link in links:
        newdf = resultsdf[resultsdf['webPage'] != link]
        numskus =newdf['skuid'].nunique()
        #logging.info('Original Sku Count ', orignumskus, numskus,' Without link',  link)
        if orignumskus == numskus:
            sizelink = len(resultsdf[resultsdf['webPage'] == link])
            canBeDropped.append([ orignumskus, numskus, link, sizelink])
        else:
            masterLink.append([ orignumskus, numskus, link])
                
    #sort the list of potential links to drop, largest first
    links_to_drop_df = pd.DataFrame(canBeDropped, columns = ['orig skus', 'after_drop_skus', 'link', 'skus_in_link' ])
    links_to_drop_df.sort_values(by=['skus_in_link'], ascending=False, inplace=True)

    #process each link, test if after removing it the number of unique skus is the same, if so drop it
    for index, row in links_to_drop_df.iterrows():
        test_link = row['link']
        newdf = resultsdf[resultsdf['webPage'] != test_link]
        numskus =newdf['skuid'].nunique()
        if orignumskus == numskus:
            #link can be dropped so drop it, but do not add the full link to the list, remove the ? switches incase they change on a rerun (Topshop)
            #logging.info('Dropping', test_link)
            if str(test_link).find('?') > -1 : 
                link_to_add = test_link[:test_link.find('?')]
            else:
                link_to_add = test_link
            invalidlinks.append(link_to_add)
            resultsdf = resultsdf[resultsdf['webPage'] != test_link]
            resultsdf.reset_index(drop=True, inplace=True)        
        else:
            #logging.info('Not Dropping ', test_link)
            pass

    logging.info(('Unique skus, orig', orignumskus, 'after dropping links ', resultsdf['skuid'].nunique()))
    return invalidlinks

#%% create valid url by removing switch
def createValidUrl(webPage, urlSwitch):
    if len(urlSwitch) > 0 :
        #remove any ? from the urlSwitch
        urlSwitch = urlSwitch.split('?')[-1]
        #case 1 webPage already has html switches 
        if webPage.find('?') > -1:
            validUrl = webPage + '&' + urlSwitch
        #case 2 no switches
        else:
            validUrl = webPage + '?' + urlSwitch
    else:
        validUrl = webPage
    return validUrl


#%% Main Scraping Modules
def scrapeFromListofUrls(listOfStartingPages, startPage, endPage, params, toppath, methods_to_run, driver, retailer_fname, google_project, google_credentials) :  
    #initailise and set up the variable to collect the results
    problemUrls = []
    resultsdf = pd.DataFrame()
    # convention for retailer name is characters after _ denote a sub scrape, e.g. retailerA_1, retailerA_2, the scrape has been split into two
    # in this module retailer is used to reference the results in the output databases so remove the _ and following characters
    retailer = retailer_fname.split('_')[0]
    #process each link 
    if not isinstance(endPage, int):
        endPage = len(listOfStartingPages)
    linksToProcess = listOfStartingPages[startPage:endPage]
    processing_first_link = True
    while len(linksToProcess) > 0 : # use a while loop as it allows links to be added
         #if collecting images and descriptions 
        #fetch the skuids of the product descriptions and images already collected, no need to collect them again
        if methods_to_run["output_image_to_file"] or methods_to_run["output_description_to_file"]:
            #option 1 use existig images stored on GCS
            #params['existing_product_descriptions'] = load_image_filenames_from_gcs(params['opfpath'])
            #option 2 get a list of sku ids stored in the big query descriptions database
            #logging.info('Fetching list of existing Product Descriptions')
            t = time.time()
            #incase running first time and database does not exist put in a try except
            if processing_first_link:
                try:
                    skuid_df = fetch_skuids_from_bq_desc('descriptions', retailer, params, google_project, google_credentials)
                    params['existing_product_descriptions']  = skuid_df['skuid'].tolist()
                    logging.info('Fetched {} descriptions, in {} seconds '.format(len( params['existing_product_descriptions'])  , time.time() - t ))  
                    processing_first_link = False   
                except:
                   logging.info('existing sku descriptions not available') 
                   params['existing_product_descriptions']  = []   
            #for testing to force it to fetch product images and descriptions set existing_product_descriptions = []
            if IGNORE_EXISTING_IMAGES:
                params['existing_product_descriptions']  = []                                                                                      
        #get the next link
        link = linksToProcess[0]
        #remove from the list to process
        linksToProcess = linksToProcess[1:]
        next_page_to_gethtml = link['topurl']
        webPage = createValidUrl(next_page_to_gethtml, params['urlSwitch'])
        #get the html for this page
        webHtml, driver, currentUrl, problemUrl =  getHtml(webPage, driver, params, clear_popups = True,
                                                    process_html_pages = True)
        print('SLEEPING 15 TO ENTER CODE - COPY URL, PRESS BACK, ENTER CODE, PASTE URL INTO BROWSER  ')
        time.sleep(15)
        link['webpage'] = currentUrl
        if problemUrl : problemUrls.append(webPage)
        #TODO - keep track of currentUrls, do not process if already processed
        #process each page of html , extracting the product, image and description info
        pageResultsdf = pd.DataFrame() # set up a data frame to collect the results from this web page link
        page_description_Resultsdf = pd.DataFrame() # set up a data frame to collect the results from this web page link
        page_description_Results_plus_attr_df = pd.DataFrame() # initialise the results plus attributes incase not updated
        list_of_products = []
        while next_page_to_gethtml != None:
            # process each next htmlpage until there are no more next pages to scrape
            logging.info(('Scrape from list of Urls Processing web page ', currentUrl, ' html pages ', len(webHtml), 'next page ', next_page_to_gethtml))
            #htmlPage_count = 0
            next_page_found = False
            next_page_to_gethtml = None # will be set to a value if a next page is found
            for htmlPage in webHtml:
                webSoup = bs4.BeautifulSoup(htmlPage , params['htmlParser'])
                next_page, products_on_page, promo_message = find_products_on_webpage(next_page_found, webSoup, link , params, methods_to_run, driver )
                list_of_products.extend(products_on_page)  
                #logging.info('Processed html page ', htmlPage_count, 'next page is ', next_page)   
                #htmlPage_count = htmlPage_count+1
                #if a next page has been found in this htmlPage then store it and set the found indicator
                if next_page != None:
                    next_page_to_gethtml = next_page 
                    next_page_found = True                           
            #get the next link page html
            if next_page_to_gethtml != None:
                webHtml, driver, currentUrl, problemUrl =  getHtml(next_page_to_gethtml, driver, params,
                                            clear_popups = True, process_html_pages = True)
                link['webpage'] = currentUrl
                if problemUrl : problemUrls.append(next_page_to_gethtml)

        #process the products found on this web page from this url (and the associated next pages)
        pageResultsdf, page_description_Resultsdf, updated_existing_product_descriptions, driver = process_products_on_webpage(list_of_products, promo_message, 
                                                                                                link, params, methods_to_run, driver)
        #update the list of existing product images and descriptions
        params['existing_product_descriptions'] = updated_existing_product_descriptions
        #drop any duplicates from this page of results, keep the first otherwise lose info on position on website
        if not pageResultsdf.empty:
            logging.info(('Number Results before dropping duplicates ', len(pageResultsdf)))
            pageResultsdf.drop_duplicates(subset=['skuid', 'ticketprice'], keep='first', inplace=True)
            logging.info(('Number Results after dropping duplicates ', len(pageResultsdf)))
        if not page_description_Resultsdf.empty:
            logging.info(('Number Descriptions before dropping duplicates ', len(page_description_Resultsdf)))
            page_description_Resultsdf.drop_duplicates(subset=['skuid'], keep='first', inplace=True)
        #add the retailer name to the resultsdf, and store the website position
        pageResultsdf['retailer'] = retailer
        pageResultsdf.reset_index(drop=True, inplace = True)
        pageResultsdf['website_position'] = pageResultsdf.index
        #completed this link, add the results to the total
        resultsdf = resultsdf.append(pageResultsdf)
        #if collecting images and descriptions 
        #output the description results to BQ (save what has been collected at each step so that if exceed runtime have at least saved so far)
        if (methods_to_run["output_image_to_file"] or methods_to_run["output_description_to_file"]) and len(page_description_Resultsdf) > 0:
            #process the attributes and add the sparkbox hierarchies before output
            page_description_Resultsdf['retailer'] = retailer
            page_description_Results_plus_attr_df = update_attributes_descriptions_and_output( page_description_Resultsdf, toppath , params, 
                                                    retailer = retailer, google_project = google_project, google_credentials = google_credentials)
        #print the size of the results df for debugging
        buf = StringIO()
        resultsdf.info(buf=buf, memory_usage='deep')
        logging.info(buf.getvalue())
        gc.collect()
        #check for exceeding runtime here, if exceeded run time do not process any more links
        if exit_if_exceeded_run_time_limit(params, webPage, driver):
            linksToProcess = []
    
    #try closing the driver if still open and a valid session            
    closeDriver(driver)
    return resultsdf, page_description_Results_plus_attr_df, problemUrls


#scrape the web pages
def scrapeWebPages(startPage, endPage, linkurls, params, filename, methods_to_run):
    #set up the logging
    logpath =  params['opfpath'] + 'AA_LOGS/'
    init_logging(filename, logpath)
    
    #if it is a front page, change the oppath to a sub folder
    if 'fp_' in filename:
        params["opfpath"] = params["opfpath"]  + FRONT_PAGE_ONLY_FOLDER +'/'

    #place in a try and except to trap any scraper errors
    try:
        toppath = params['opfpath']
        #overwrite path and reset variables with arguments passed to main module if passed
        if len(sys.argv) > 1 : #toppath has been passed as an argument which takes priority
            toppath = sys.argv[1]
        if len(sys.argv) > 2 : #reset duplicates has been passed as an argument which takes priority
            params['resetduplicatelinks'] = sys.argv[2]
        if len(sys.argv) > 3 : #run time limit has been passed as an argument
            run_time_limit = sys.argv[3]
        else:
            run_time_limit =  MINIMUM_RUNTIME
        if len(sys.argv) > 4 : #google credentials have been passed as an argument
            google_project = sys.argv[4]["google_project"]
            google_credentials = sys.argv[4]["google_credentials"]
            logging.info(('Google Credentials passed as arguments', google_project, google_credentials))
        else:
            google_credentials, google_project = configure_googleapi_credentials()
            logging.info(('Google Credentials generated using auth ', google_project, google_credentials))
        params['google_project'] = google_project
        params['google_credentials'] = google_credentials
        #print ("The arguments are: " , str(sys.argv))
        starttime = time.time()
        params['starttime'] = starttime
        params['run_time_limit'] = run_time_limit
        #create the local output path if it does not already exist, folder name is same as module name
        opfpath =  createOutputPath(filename, toppath, "Local")
        params['opfpath'] = opfpath
        #add the start date string to the params
        todayDate = dt.datetime.today()
        todayDateStr = todayDate.strftime("%Y%m%d")
        params['todayDate'] = todayDate.date()
        params['todayDateStr'] = todayDateStr
        #output a dummy file to indicate the run start time
        output_running_file(opfpath, google_project, google_credentials, GOOGLE_STORAGE_FILE_PATH, TEST_GOOGLE_STORAGE_FILE_PATH  )

        #fetch the directory of the python file
        #cwd = os.getcwd()
        dir_path = os.path.dirname(os.path.realpath(__file__))
        parent_path = str(Path(dir_path).parent)
        logging.info(('Current Directory Path is ', dir_path))
        logging.info(('Current Parent Path is ', parent_path))
        #configure the browser extension for VPN parameters if required 
        params = configure_reset_chrome_VPNextensions_cookies(params)

        #capture screenshots for each starting page
        captureScreenshotsToplevel( linkurls, params, google_project, google_credentials)
        
        # #create an empty dataframe for the results
        # resultsdf = pd.DataFrame()
        #find the links for each starting page
        logging.info('Looking for Web Links to process ')
        listOfStartingPages, driver = findLinkstoProcess(linkurls, params, google_project, google_credentials)
        #if there are no links, exit
        if len(listOfStartingPages) == 0:
            msg = "No WebPage Links found, list of staring pages empty,  Exiting"
            sys.exit(msg)
        #scrape the webpages for each link
        retailer =  filename.split('\\')[-1].split('/')[-1].split('.')[0]
        logging.info(('Collecting Prices for ', retailer))
        resultsdf,  last_description_resultsdf , problemUrls = scrapeFromListofUrls(listOfStartingPages, startPage, endPage, params, 
                                                                toppath, methods_to_run, driver, retailer, google_project, google_credentials) 
        #reset the duplicate links definition csv        
        if params['resetduplicatelinks']:
            #find and output / reset duplicate links if required
            resetDuplicateLinks(opfpath, resultsdf,  google_project, google_credentials)

        #output the results
        output_results_progress_log(retailer, toppath, opfpath, resultsdf, last_description_resultsdf, starttime, params,problemUrls, google_project, google_credentials )

        #message any problem urls
        if problemUrls == []: # scraping executed without any problem urls
            logging.info(('Executed Successfully', time.ctime()))
            run_time = time.time() - params['starttime']
            logging.info(('Run time duration ', run_time))
        else:
            logging.info('REMAIINING  PROBLEM URLS')
            for p in  problemUrls:
                logging.info(p)
            logging.info(('Executed with some problem Urls', time.ctime()))
        
        #check for exceeding runtime here, if exceeded run time generate an error message
        if exit_if_exceeded_run_time_limit(params, "", driver):
            run_time = time.time() - params['starttime']
            errmsg = ('Exceeded run time limit ' + str(run_time )+ ' ' + str(params['run_time_limit']) )
            logging.info(errmsg)
            closeDriver(driver)
            sys.exit(errmsg)

        #close and flush the logging
        logging.shutdown()
    except:
        #deal with the scraper error
        msg = traceback.format_exc()
        logging.info(msg)
        #delete the running file marker so that the scraper can start again
        delete_running_file(opfpath, google_project, google_credentials, GOOGLE_STORAGE_FILE_PATH, TEST_GOOGLE_STORAGE_FILE_PATH)
        #update the progress log with an error message
        filename = params['opfpath'].split('/')[-1]
        generate_error_log_update_progress(filename, toppath, starttime, google_project, google_credentials,
                                            GOOGLE_STORAGE_FILE_PATH, TEST_GOOGLE_STORAGE_FILE_PATH)
        #configure notification
        subject =  f"Error in Scraping  {filename} on { platform.node()}! "
        body = f"Error for {filename} on { platform.node()} ! \n {traceback.format_exc()} "

        #if the issue is because of chrome copying data, restart
        if '[Errno 17] File exists:' in body:
            subject =  f"RESTARTING - File Exists Error in Scraping  {filename} on { platform.node()}! "
            #body = f"Error for {filename} on { platform.node()} ! \n {traceback.format_exc()} "
            send_email_notification(subject, body)
            restart_computer()
        #if the issue is because of problem with browser or internet connection, restart
        elif 'Unable to open the browser' in body:
            subject =  f"RESTARTING - Browser Error in Scraping  {filename} on { platform.node()}! "
            #body = f"Error for {filename} on { platform.node()} ! \n {traceback.format_exc()} "
            send_email_notification(subject, body)
            restart_computer()
        #any other reason send the message with the original notification
        else:
            send_email_notification(subject, body)

        #close and flush the logging
        logging.shutdown()
    return    





