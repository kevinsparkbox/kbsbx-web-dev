# place username and password in the code. 
# #uncomment login function for the first time run. then it will save login data in cookies. then you can disable login function

import time
import os
import google
import platform
import csv
import re
from  more_itertools import unique_everseen  #pip install more_itertools - TODO add to requirements.txt
import pandas as pd
import datetime
from bs4 import BeautifulSoup
from pathlib import Path
import sys
import traceback
from retrying import retry

from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager

from CoreScraperOutputModules import createOutputPath
from CoreScraperConstants import MINIMUM_RUNTIME

from CoreScraperConstants import OUTPUT_OPTION, OUTPUT_TO_DATABASE
from CoreScraperConstants import TEST_GBQ_DATASET, GBQ_DATASET
from InstaScraperConstants import TEST_GBQ_TABLE_INSTA_RESULTS, GBQ_TABLE_INSTA_RESULTS
from CoreScraperConstants import BUCKET_NAME, PROGRESS_FOLDER, PROGRESS_FILE
from CoreScraperConstants import TEST_GOOGLE_STORAGE_FILE_PATH, GOOGLE_STORAGE_FILE_PATH
from InstaScraperConstants import INSTA_RETAILER_CONFIG, INSTA_CHROME_DATA_DIR, INSTA_FORCE_NO_LOGIN, RESET_LOGIN
from CoreScraperConstants import CHROME_USER_AGENT, MASTER_CHROME_DATA_PATH
from CoreScraperOutputModules import outputDataFrameToFile, outputDataFrameToDatabase, createOutputPath, outputToFile, upload_blob, generate_error_log_update_progress
from CoreScraperErrorModules import  send_email_notification
from CoreScraper_utils import configure_googleapi_credentials
from Corescraper_fetch_BQ_data import download_blob_delete
from CoreScraper_getHtml import copy_cookies



class Insta:
    def __init__(self, username, password, page_height, url, max_boxes, toppath, retailer_from_fname):
        self.username = username
        self.password = password
        self.max_page_height = page_height
        self.url = url
        self.data_dir = INSTA_CHROME_DATA_DIR
        self.unique_post_url = []
        self.soup_list = []
        self.post_list = []
        self.starttime = time.time()
        self.max_boxes = max_boxes
        self.toppath = toppath
        self.retailer_from_fname = retailer_from_fname
        # self.driver = ''

        if len(sys.argv) > 1 : #toppath has been passed as an argument which takes priority
            self.toppath = sys.argv[1]
        if len(sys.argv) > 2 : #reset duplicates has been passed as an argument (not used in insta scrape)
            #params['resetduplicatelinks'] = sys.argv[2]
            self.resetduplicatelinks = sys.argv[2]
        if len(sys.argv) > 3 : #run time limit has been passed as an argument
            self.run_time_limit = sys.argv[3]
        else:
            self.run_time_limit =  MINIMUM_RUNTIME
        if len(sys.argv) > 4 : #google credentials have been passed as an argument
            self.google_project = sys.argv[4]["google_project"]
            self.google_credentials = sys.argv[4]["google_credentials"]
            print('Google Credentials passed as arguments', self.google_project, self.google_credentials)
        else:
            google_credentials, google_project = configure_googleapi_credentials()
            print('Google Credentials generated using auth ', google_project, google_credentials)
            self.google_project = google_project
            self.google_credentials = google_credentials

    @retry(stop_max_delay=120000, wait_fixed=20000) #try for 120 seconds, wait 20 second between tries - ChromeDriverManager occassionally fails
    def driver_config(self):
        #when running several scrapers in parallel google chrome locks some of the files in the "cookies user data" folder
        #so approach is to copy them to scraper dir and then add them to the driver options as the user-data-dir in the scraper dir
        cwd = os.getcwd()
        if not RESET_LOGIN:
            target_chrome_data_dir = str(Path( self.toppath + '/' + self.retailer_from_fname + '/' + INSTA_CHROME_DATA_DIR ))
            #source_chrome_data_dir = str(Path( cwd + MASTER_CHROME_DATA_PATH)  / ( INSTA_CHROME_DATA_DIR ))
            source_chrome_data_dir = str(Path( MASTER_CHROME_DATA_PATH)  / ( INSTA_CHROME_DATA_DIR ))
            copy_cookies(target_chrome_data_dir, source_chrome_data_dir )
            self.data_dir = target_chrome_data_dir
            time.sleep(1) # wait for copy to finish
        options = webdriver.ChromeOptions()
        options.add_argument(f'user-agent={CHROME_USER_AGENT}' )
        if not INSTA_FORCE_NO_LOGIN:
            arg_string = "--user-data-dir=" + self.data_dir
            options.add_argument(arg_string)
        d = DesiredCapabilities.CHROME
        d['loggingPrefs'] = {'performance': 'ALL'}
        #self.driver = webdriver.Chrome(executable_path=CHROMEDRIVERPATH, options=options,
        #                               desired_capabilities=d)
        self.driver = webdriver.Chrome(ChromeDriverManager().install(), options=options,
                                       desired_capabilities=d)
        time.sleep(3) # wait for any web initialisations, e.g. VPN to start and connect

    def login(self):
        # driver.maximize_window()
        url = 'https://www.instagram.com/'
        self.driver.get(url)
        time.sleep(2)
        # username_box = self.driver.find_element_by_name("username")
        # password_box = self.driver.find_element_by_name("password")
        # username_box.send_keys(self.username)
        # time.sleep(1)
        # password_box.send_keys(self.password)
        # time.sleep(1)
        # password_box.send_keys(Keys.ENTER)
        # time.sleep(2)
        #wait for quite a while in case it asks for verification code, if so enter and close the browser
        time.sleep(200)
        #self.driver.find_element_by_xpath("//*[contains(text(), 'Not Now')]").click()

    def scrape_page(self):
        #url = 'https://www.instagram.com/boden_clothing/?hl=en'
        #self.url = 'https://www.instagram.com/pimkie/?hl=en'
        self.driver.get(self.url)
        time.sleep(10)
        boxes_list = []
        post_url_list = []
        flag = True
        while flag:
            boxes = []
            boxes = self.driver.find_elements_by_class_name("v1Nh3")
            print("boxes", len(boxes))
            boxes_list = boxes_list + boxes
            for box in boxes:
                try:
                    time.sleep(3)
                    post_url = box.find_element_by_tag_name("a").get_attribute('href')
                    post_url_list.append(post_url)
                except Exception as e:
                    print(e)
                    break

            page_height1 = self.driver.execute_script('return document.documentElement.scrollHeight')
            print("page_height1", page_height1)
            self.driver.find_element_by_tag_name('body').send_keys(Keys.END)
            time.sleep(10)
            page_height2 = self.driver.execute_script('return document.documentElement.scrollHeight')
            print('page_height2', page_height2)
            if page_height2 < page_height1 or page_height2 > self.max_page_height:
                flag = False
            #stop early if maximum number of boxes
            if len(boxes_list) > self.max_boxes :
                flag = False

        self.unique_post_url = list(unique_everseen(post_url_list)) #remove duplicates but preserve the order
        print(len(self.unique_post_url))

    def scrape_posts_soup(self):
        for post_url in self.unique_post_url:
            print('fetching post ', post_url)
            #TODO check valid page found etc, othrwise reset driver
            self.driver.get(post_url)
            print('fetched post ', post_url)
            time.sleep(2)
            flag = True
            i = 1
            while flag:
                try:
                    try:
                        remove_button = self.driver.find_element_by_xpath('//*[@id="react-root"]/section/nav/div[2]/div')
                        self.driver.execute_script("""var element = arguments[0];element.parentNode.removeChild(element);""", remove_button)
                    except:
                        pass
                    time.sleep(2)
                    last_comment1 = self.driver.find_elements_by_class_name("C4VMK")[-1].find_elements_by_tag_name("span")[1]
                    try:
                        button = self.driver.find_element_by_class_name("afkep")
                        self.driver.execute_script("arguments[0].scrollIntoView();", button)
                        print(i, "scrolled")
                        time.sleep(2)
                        button.click()
                        print(i, "clicked...")
                    except:
                        button = None
                    last_comment2 = self.driver.find_elements_by_class_name("C4VMK")[-1].find_elements_by_tag_name("span")[1]
                    if button == None or last_comment1 == last_comment2:
                        flag = False

                    #try clicking the views button to make the number of likes visible
                    try:
                        #find and click the views button
                        viewbutton = self.driver.find_element_by_class_name("vcOH2")
                        self.driver.execute_script("arguments[0].scrollIntoView();", viewbutton)
                        #print('video views', "scrolled")
                        time.sleep(2)
                        viewbutton.click()
                        #print('video views', "clicked...")
                    except:
                        pass

                    i += 1
                except Exception as e:
                    print('Exception in scrape posts ' , e)
                    flag = False

            soup = BeautifulSoup(self.driver.page_source, "html.parser")
            self.soup_list.append(soup)

    def get_likes_views(self, soup):
        media_type = ''
        #like_views = ''
        likes = ''
        views = ''
        info = soup.find('div', {'class': 'Nm9Fw'})
        if info != None:
            likes = info.find('button').get_text()
            #keep only the number
            likes = likes.split(' ')[0]
            #print("like", likes)
            media_type = 'Image'
            #print(media_type)
        else:
            info = soup.find('span', {'class': 'vcOH2'})
            if info != None:
                views = info.find('span').get_text()
                #print('views', views)
                media_type = 'Video'
                #print(media_type)
                subinfo = soup.find('div', {'class': 'vJRqr'})
                if subinfo != None:
                    likes = subinfo.find('span').get_text()
                    #keep only the number
                    likes = likes.split(' ')[0]
                    #print('Video Likes', likes)
        if likes != '':
            likes = int(likes.replace(',', ''))
        else:
            likes = 0        
        if views != '':
            views = int(views.replace(',', ''))
        else:
            views = 0
        return likes, views, media_type

    def get_retailer(self, soup):
        retailer = ''
        info = soup.find('div', {'class': 'e1e1d'})
        if info != None:
            span = info.find('span', {'class': 'Jv7Aj'})
            if span != None:
                retailer = span.find('a').get_text()
                #print("retailer", retailer)
        return retailer

    def get_post_text(self, soup):
        text = ''
        info = soup.find('span', attrs={"title": "Edited"})
        if info != None:
            text = info.get_text()
        else:
            info = soup.find('li', {'class': 'PpGvg'})
            if info != None:
                div = info.find('div', {'class': 'C4VMK'})
                if div != None:
                    text_arr = div.find_all('span')
                    if len(text) >= 3:
                        text = text_arr[2].get_text()
                    else:
                        text = text_arr[-1].get_text()
        #print("text", text)
        return text

    def get_post_products(self, text):
        #replace str defines characters that may need to be substituted, e.g. force a space between  #
        #product str defines the pattern to be matched for a product/skuid
        retailer = self.retailer_from_fname
        if retailer in INSTA_RETAILER_CONFIG.keys(): 
            replace_str = INSTA_RETAILER_CONFIG[retailer]['replace_str']
            text = text.replace( replace_str['find'], replace_str['replace'])
            product_str = replace_str = INSTA_RETAILER_CONFIG[retailer]['product_str']
            products = re.findall(product_str, text)
        else:
            products = []
        #print("text", text)
        #print('products in text ', products)
        return products


    def get_media_url(self, soup):
        media_url = ''
        info = soup.find('div', {'class': 'kPFhm'})
        if info != None:
            image_media_url = info.find('img', {'class': 'FFVAD'})
            if image_media_url != None:
                media_url = image_media_url.get('src')
                #print("image_url", media_url)
            else:
                video_media_url = info.find('video', {'class': 'tWeCl'})
                if video_media_url != None:
                    media_url = video_media_url.get('src')
                    #print(media_url)
        return media_url

    def get_date_time(self, soup):
        date = ''
        time_string = ''
        info = soup.find('time', {'class': '_1o9PC'})
        if info != None:
            #date_string = info.get('title')
            date_time_string = info.get('datetime') # time = Z time = Zulu = UTC
            #extract only time info from date_time_string
            date = date_time_string.split('T')[0]
            time_string = date_time_string.split('T')[1].split('.')[0]
            print('date', date)
            print('time string ', time_string)
        return date, time_string

    def get_comments(self, soup):
        info = soup.find_all('div', {'class': 'C4VMK'})
        comments = []
        if info != None:
            for i in range(1, len(info)):
                comment = info[i].find_all('span')[1].get_text()
                if comment != 'Verified':
                    comments.append(comment)
        print(comments)
        print(len(comments))
        return comments, len(comments)

    def get_post_hashtags(self, text):
        hashtags = re.findall(r"#(\w+)", text)
        #print("hashtags post", hashtags)
        return hashtags

    def get_comments_hashtags(self, comments):
        hashtags = re.findall(r"#(\w+)", str(comments))
        #print("hashtags comments", hashtags)
        return hashtags

    def get_influencers(self, comments):
        influencers = re.findall(r"@(\w+)", str(comments))
        #print("hashtags comments", influencers)
        return influencers

    def write_csv_db(self):
        post_df_cols = ['retailer', 'post_url', 'likes', 'views', 'post_text', 'post_products', 'post_hashtags','media_url', 'media_type', 'post_date', 'post_time', 'comments', 'comments_count',
                  'comments_hashtags', 'influencers', 'position']
        post_df = pd.DataFrame (self.post_list,columns=post_df_cols)
        post_df['date'] = datetime.datetime.today().strftime('%Y-%m-%d')
        new_cols =  ['retailer', 'date',  'post_url', 'likes', 'views', 'post_text', 'post_products', 'post_hashtags','media_url', 'media_type', 'post_date', 'post_time', 'comments', 'comments_count',
                  'comments_hashtags', 'influencers', 'position']
        post_df = post_df[new_cols]
        #set the retailer name
        post_df['retailer'] = self.retailer_from_fname
        self.post_df = post_df

        #create the output path if it does not already exist
        #e.g. retailer = 'Pimkie'  = should match the folder used to store results from web scrape
        opfpath =  createOutputPath(self.retailer_from_fname, self.toppath, "Local")
        #output the instagram results to database and csv
        output_insta_results(self.retailer_from_fname, self.toppath, opfpath, post_df, self.starttime, self.google_project, self.google_credentials)


    def iterate_over_soups(self):
        for i, soup in enumerate(self.soup_list):
            post_url = self.unique_post_url[i]
            likes, views, media_type = self.get_likes_views(soup)
            retailer = self.get_retailer(soup)
            text = self.get_post_text(soup)
            products = self.get_post_products(text)
            media_url = self.get_media_url(soup)
            date, time_string = self.get_date_time(soup)
            comments, comments_count = self.get_comments(soup)
            hashtags_post = self.get_post_hashtags(text)
            hashtags_comments = self.get_comments_hashtags(comments)
            influencers = self.get_influencers(comments)
            post = [retailer, post_url, likes, views, text, products, hashtags_post, media_url, media_type, date, time_string, comments, comments_count, 
                    hashtags_comments, influencers, i]
            self.post_list.append(post)


    def close_driver(self):
        try:
            self.driver.close()
        except:
            print('Unable to close driver')
            sys.stdout.flush()


    def write_error(self):
        #TODO see output error log in core scraper
        #
        #traceback.print_exc()
        pass

# %% Main Code    

def mainInstaScrape(url, max_boxes , toppath, retailer_from_fname):

    username = 'instagram_user'
    password = 'instagram0_pwd'
    page_height = 20000
    insta = Insta(username, password, page_height, url, max_boxes, toppath, retailer_from_fname )
    #place in a try and catch and output any scraping errors
    try:
        insta.driver_config()
        if RESET_LOGIN:
            insta.login()
        insta.scrape_page()
        #insta.unique_post_url = ['https://www.instagram.com/p/CAD6EiVFT01/']
        insta.scrape_posts_soup()
        insta.close_driver()
        insta.iterate_over_soups()
        insta.write_csv_db()
    except : 
        insta.close_driver()
        msg = traceback.format_exc()
        print(msg)
        #send notification and update error log
        insta.write_error()
        #update the progress log with an error message
        print(insta.retailer_from_fname ,insta.toppath, insta.starttime, insta.google_project, insta.google_credentials )
        generate_error_log_update_progress(retailer_from_fname,insta.toppath, insta.starttime, insta.google_project, insta.google_credentials,
                                            GOOGLE_STORAGE_FILE_PATH, TEST_GOOGLE_STORAGE_FILE_PATH)
        mod_name = url.strip().replace('.', '[.]')
        subject =  f"Error in Scraping  {mod_name} on { platform.node()}! "
        body = f"Error for {mod_name} on { platform.node()} ! \n {traceback.format_exc()} "
        send_email_notification(subject, body)


    return

#outputResults and log and if selected reset the duplicate links csv
#def outputResultsLog(retailer, toppath, opfpath, resultsdf, description_resultsdf, starttime, params, problemUrls, google_project, google_credentials):
def output_insta_results(retailer, toppath, opfpath, insta_resultsdf, starttime, google_project, google_credentials):
    #toppath is the path to the results folder
    #opfpath is the path to the retailer specific folder within the results
    #retailer is the retailer name which is generally the same as the retailer specific folder name - some duplication caused by legacy codes
    #e.g. retailer = 'Pimkie'  = should match the folder used to store results from web scrape

    #get the current date
    todayDate = datetime.datetime.today()
    opDate = todayDate.strftime("%Y%m%d")

    endtime = time.ctime()
    duration = (time.time() - starttime)/(60*60)
    if len(insta_resultsdf) > 0:
        numResults = len(insta_resultsdf)
  
        opfname =  'OutputInstaData.csv'
    
        #output insta results, for local outputs to the folder specified by opfpath, (configured as output path/retailer name)
        #for Google Storage outputs to the folder under the GOOGLE_STORAGE_FILE_PATH or TEST_GOOGLE_STORAGE_FILE_PATH
        if OUTPUT_OPTION == "LocalOnly" or OUTPUT_OPTION == "All":
            #for the local option 
            outputDataFrameToFile(insta_resultsdf, opfpath, opfname, "Local", google_project, google_credentials)
        if OUTPUT_OPTION == "GCSOnly" or OUTPUT_OPTION == "All":
            #for the GCS option 
            outputDataFrameToFile(insta_resultsdf, opfpath, opfname, "Google_Storage", google_project, google_credentials)
        
        #output the full results to the database
        if OUTPUT_TO_DATABASE:
            if opfpath.find('TEST') == -1:
                dataset = GBQ_DATASET
                results_table = GBQ_TABLE_INSTA_RESULTS
            else:
                dataset = TEST_GBQ_DATASET
                results_table = TEST_GBQ_TABLE_INSTA_RESULTS

            location = "Big_Query"
            if_exists_operation = "append"
            outputDataFrameToDatabase(insta_resultsdf, dataset, results_table, location, if_exists_operation, google_project, google_credentials)

             
        
        #output details to progress file
        #update the progress file on GS, a convoluted progress as GS does not support append to file, read the local file, append, and then write to GS)
        print('Outputting to PROGRESS File', toppath)
        #create the output progress message
        textToOp = opDate  + ',' + 'insta_' + retailer +','  + 'Succesful' +','   + endtime + ',' + str(duration) + ',' + str(len(insta_resultsdf)) +  ',' + platform.node() + '\n'
    
        fname =  PROGRESS_FILE
        #create the local output path if it does not already exist
        createOutputPath(PROGRESS_FOLDER, toppath, 'Local')
        #configure the GS folder path
        if opfpath.find('TEST') == -1:
            GSPath = GOOGLE_STORAGE_FILE_PATH
        else:
            GSPath = TEST_GOOGLE_STORAGE_FILE_PATH 
        folderpath = GSPath +  PROGRESS_FOLDER 
        #download the progress blob from GS
        download_success = download_blob_delete(google_credentials, google_project, BUCKET_NAME, folderpath, fname, toppath + PROGRESS_FOLDER + '/' +fname, False)
        if not download_success:
            #progress file does not exist on GS so start a new one, delete any existiong local files
            try:
                os.remove(toppath + '/' + PROGRESS_FOLDER +  '/' + fname)
                print('Deleted local progress file')
            except:
                print('No local progress file to delete ')
        #append the latest progress info
        outputToFile(toppath + '/' + PROGRESS_FOLDER, fname, textToOp, "a", "Local", google_project, google_credentials) 
        #upload the local file back to GS
        upload_blob(toppath + PROGRESS_FOLDER + '/' +fname, folderpath + '/' + fname, google_project, google_credentials, BUCKET_NAME)
        print('Uploaded PROGRESS File to GS')
        
    else:
        send_email_notification(numResults, 'insta scraper ' + retailer)

    return