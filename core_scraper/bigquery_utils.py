import time
from google.cloud import bigquery
from google.cloud import storage
from retrying import retry
from pandas.api.types import is_datetime64_any_dtype as is_datetime

#add the retry decorator incase google api fails
@retry(stop_max_delay=20000, wait_fixed=1000) #try for 20 seconds, wait 1 second between tries
def download_bq_df(sql, credentials, project_id, remove_UTC = True):
    """Download from big query
    Using the BigQuery Storage API client which creates temporary storage and speeds up download
    sql contains the sql script required to specify the content to be downloaded, e.g.
        sql = '''
            SELECT  CAST ( cal_date AS date) as cal_date , week, region, channel, department_id, department_desc, SUM(gsales_u_orig) AS gsales_u, SUM(gsales_v_orig) AS gsales_v, MAX(stores_count) as stores_count
            FROM DATASET.DATATABLE
            WHERE cal_date >= "MIN_DATE"  and pc_orig_sales_valid = 1
            GROUP BY  cal_date, region, week, channel,  department_desc, department_id
            ORDER BY  channel, department_desc, cal_date, week, region;
        '''
    if remove_UTC = True, the time zone aware date time stamps are converted to time zone niave timestamps
    """
    start_time = time.time()
    client = bigquery.Client(credentials=credentials, project=project_id)
    df = client.query(sql).to_dataframe(create_bqstorage_client=True)

    #when date times are downloaded from big query they are given a time zone aware UTC stamp, this conflicts with code later on where dates and times 
    # are time zone naive and cannot be compared.  So convert here to datetime[64]
    # dtype from bq is "datetime64[ns, UTC]"
    for c in df.columns:
        if is_datetime(df[c]) :
            df[c] = df[c].dt.tz_convert(None)

    t = time.time() - start_time
    print('Time Taken to download from bq', t)
    return df

#add the retry decorator incase google api fails
@retry(stop_max_delay=20000, wait_fixed=1000) #try for 20 seconds, wait 1 second between tries
def upload_df_bq(df, table_id, replace, wait_for_finish):
    """Upload from dataframe to bq table_id
    Using the BigQuery Storage API client to upload results.
    Append (replace = False) or Overwriting existing table if exists (replace=True)
    Option to wait for the upload to complete before moving on (wait_for_finish = True)
    """
    start_time = time.time()

    #force object columns to string to stop bq upload misclassifying type
    for c in df.columns:
        if df[c].dtypes == 'object':
            df[c] = df[c].astype('str')
    client = bigquery.Client()
    if replace:
        job_config = bigquery.LoadJobConfig(
            # Optionally, set the write disposition. BigQuery appends loaded rows
            # to an existing table by default, but with WRITE_TRUNCATE write
            # disposition it replaces the table with the loaded data.
            write_disposition="WRITE_TRUNCATE",
        )
    else:
        job_config = bigquery.LoadJobConfig()

    job = client.load_table_from_dataframe(df, table_id, job_config=job_config)
    if wait_for_finish:
        #wait for job to complete
        job.result()

    t = time.time() - start_time
    print('Time Taken to upload ', table_id  , t)
    return df

#add the retry decorator incase google api fails
@retry(stop_max_delay=20000, wait_fixed=1000) #try for 20 seconds, wait 1 second between tries
def delete_data_tobe_replaced(table_id, new_data_start_date, credentials, project):
    """Delete the data that is about to be incrementally overwritten from the table,
    """
    new_data_start_date_str = str(new_data_start_date)
    print('Deleting old Incremental data from BQ database, dates from ', new_data_start_date_str)
    start_time = time.time()

    query_string = """
            #keep the data that is not being incrementally updated (many times quicker than DELETE )
            CREATE OR REPLACE TABLE DATASET.DATATABLE
            AS
            SELECT * FROM DATASET.DATATABLE
            where cal_date < "NEW_INCR_DATE"
    """
    #sort param = parameter to sort on, e.g. time stam
    query_string = query_string.replace( "DATASET.DATATABLE", table_id)
    query_string = query_string.replace( "NEW_INCR_DATE", new_data_start_date_str)    

    # Make client nd run query job
    bqclient = bigquery.Client(credentials= credentials, project=project )
    bqclient._http.adapters['https://']._pool_connections=100
    bqclient._http.adapters['https://'].pool_maxsize=100
    bq_job = bqclient.query(query_string, project=project)
    bq_job.result()  # Waits for job to complete.

    t = time.time() - start_time
    print('Time Taken to delete old incremental data from ', table_id  , t)
    return

#add the retry decorator incase google api fails
@retry(stop_max_delay=20000, wait_fixed=1000) #try for 20 seconds, wait 1 second between tries
def remove_duplicates(table_id, order_param, dup_subset_string, credentials, project):
    """Revove duplicates from a data table keeping the last,
    sort by order_param, e.g. modified date
    select duplicates based on dup_subset_string, e.g. sku_id, cal_date
    """
    print('Removing Duplicates from BQ database')
    start_time = time.time()

    query_string = """
            #deduplicate on skuid and retailer, keeping the latest using date time column
            CREATE OR REPLACE TABLE DATASET.DATATABLE
            AS
            SELECT descr.* FROM (
            SELECT ARRAY_AGG(
            t ORDER BY t.ORDERPARAM DESC LIMIT 1
            ) [OFFSET(0)] descr
            FROM DATASET.DATATABLE t
            GROUP BY DUP_SUBSET_STRING
            )
    """
    #sort param = parameter to sort on, e.g. time stam
    query_string = query_string.replace( "DATASET.DATATABLE", table_id)
    query_string = query_string.replace( "ORDERPARAM", order_param)    
    query_string = query_string.replace( "DUP_SUBSET_STRING", dup_subset_string)

    # Make client nd run query job
    bqclient = bigquery.Client(credentials= credentials, project=project )
    bqclient._http.adapters['https://']._pool_connections=100
    bqclient._http.adapters['https://'].pool_maxsize=100
    bq_job = bqclient.query(query_string, project=project)
    bq_job.result()  # Waits for job to complete.

    t = time.time() - start_time
    print('Time Taken to remove duplicates from ', table_id  , t)
    return