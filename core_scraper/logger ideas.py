import logging
import sys
import datetime
from pathlib import Path


def init_logger(filename, logpath):
    logger = logging.getLogger(__file__)
    for hdlr in logger.handlers[:]:  # remove the existing file handlers
        print('Processing handler ', hdlr)
        if isinstance(hdlr,logging.FileHandler) or isinstance(hdlr,logging.StreamHandler) :
            logger.removeHandler(hdlr)
            print('removing existing handler ', hdlr)

    #set logging level
    logger.setLevel(logging.INFO)
    #set formatter
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    #formatter = logging.Formatter('%(asctime)-15s::%(levelname)s::%(filename)s::%(funcName)s::%(lineno)d::%(message)s')
    #formatter = logging.Formatter('%(asctime)-15s::%(levelname)s::%(funcName)s::%(lineno)d::%(message)s')

    #set console handler
    ch = logging.StreamHandler(sys.stdout)
    ch.flush = sys.stdout.flush
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    #set a file handler
    todayDate = datetime.datetime.today()
    todayDateStr = todayDate.strftime("%Y%m%d")
    Path(logpath).mkdir(parents=True, exist_ok=True) # this will create the logging folder if it does not already exist
    if "/" in filename:
        logfname = Path(logpath + filename.split('/')[-1].split('.')[0]  + "_" + todayDateStr + "_log.log")
    else:
        logfname = Path(logpath + filename.split('\\')[-1].split('.')[0] + "_" + todayDateStr + "_log.log")

    fh = logging.FileHandler(logfname, 'w+')
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    
    return logger

def init_logging(filename, logpath):
    logger = logging.getLogger() # get the root logger ?
    for hdlr in logger.handlers[:]:  # remove the existing file handlers
        print('Processing handler ', hdlr)
        if isinstance(hdlr,logging.FileHandler) or isinstance(hdlr,logging.StreamHandler) :
            logger.removeHandler(hdlr)
            print('removing existing handler ', hdlr)

    #set logging level
    logger.setLevel(logging.INFO)
    #set formatter
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    #formatter = logging.Formatter('%(asctime)-15s::%(levelname)s::%(filename)s::%(funcName)s::%(lineno)d::%(message)s')
    #formatter = logging.Formatter('%(asctime)-15s::%(levelname)s::%(funcName)s::%(lineno)d::%(message)s')

    #set console handler
    ch = logging.StreamHandler(sys.stdout)
    ch.flush = sys.stdout.flush
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    #set a file handler
    todayDate = datetime.datetime.today()
    todayDateStr = todayDate.strftime("%Y%m%d")
    Path(logpath).mkdir(parents=True, exist_ok=True) # this will create the logging folder if it does not already exist
    if "/" in filename:
        logfname = Path(logpath + filename.split('/')[-1].split('.')[0]  + "_" + todayDateStr + "_log.log")
    else:
        logfname = Path(logpath + filename.split('\\')[-1].split('.')[0] + "_" + todayDateStr + "_log.log")

    fh = logging.FileHandler(logfname, 'w+')
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    
    return logger

logpath =  'C:/Users/kevin/Data/Spb-web/Web Scrape TEST/'
filename1 =  'testlog1'
filename2 =  'testlog2'


init_logging(filename1, logpath)

logging.info('Sending a message to logging')
logging.warning('Sending a warning message to logging')

init_logging(filename2, logpath)

logging.info('Sending a message to logging 2')
logging.warning('Sending a warning message to logging 2')

print(causeanerror)

logger = init_logger(filename1, logpath)
logger.debug('...')

logger.info('Sending a message to logging')
logger.warning('Sending a warning message to logging')

logger = init_logger(filename2, logpath)

logger.info('Sending a message to logging 2')
logger.warning('Sending a warning message to logging 2')


logging.shutdown()

#logger.shutdown()
    
    # #if root logger has already been set need to reset the filehandler
    # #filehandler = logging.FileHandler(logfname, 'w+')
    # #formatter = logging.Formatter('%(asctime)-15s::%(levelname)s::%(filename)s::%(funcName)s::%(lineno)d::%(message)s')
    # #filehandler.setFormatter(formatter)
    # log = logging.getLogger()  # root logger - Good to get it only once
    # for hdlr in log.handlers[:]:  # remove the existing file handlers
    #     print('Processing handler ', hdlr)
    #     if isinstance(hdlr,logging.FileHandler):
    #         log.removeHandler(hdlr)
    #         print('removing existing file handler ', hdlr)
    # filehandler = logging.FileHandler(logfname, 'w+')
    # formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s')
    # filehandler.setFormatter(formatter)
    # log.addHandler(filehandler)      # set the new handler

    # print('set new handler ', filehandler)


    #logging.info("writing an info message")
    #logging.warning("writing a warning mssage")
    #logging.error("writing a bad error message")
    #logging.debug("writing some detailed debug messages")