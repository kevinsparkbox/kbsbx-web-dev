
import sys
from retrying import retry
import time
import platform
from pathlib import Path
import datetime as dt
import bs4
import os
import shutil
import requests
import pandas as pd
import random
import logging

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import DesiredCapabilities ### for logging
from selenium.webdriver.common.by import By
from selenium.common.exceptions import ElementClickInterceptedException  # for button click exception
from webdriver_manager.chrome import ChromeDriverManager

from PIL import Image  # package pillow
from io import BytesIO

from CoreScraperConstants import USER_AGENT, MASTER_CHROME_DATA_DIR, MASTER_CHROME_DATA_PATH, OUTPUT_OPTION
from CoreScraper_utils import closeDriver, timeout
from CoreScraperErrorModules import exit_if_exceeded_run_time_limit
from CoreScraperOutputModules import saveScreenShottoPDF
from CoreScraperOutputModules import outputToFile
from CoreScraper_utils import removeDisallowedFilenameChars, print_memory_usage

def configure_reset_chrome_VPNextensions_cookies(params):
    """Configure Chrome instance by copying chrome set up and configuration files from a common folder to a folder local to the scraper
    To set up the Chrome confguration run the vpn_location_setup.py
    If a VPN is not specified and params['resetCookies'] is set it copies a set of default params whch also is set up with vpn_location_setup.py
    Otherwise if the VPN is not specified the chrome user-data-dir is not set
    Updates the params from a folder in the chrome user data dir. 
    """
    #fetch the directory of the python file
    dir_path = os.path.dirname(os.path.realpath(__file__))
    parent_path = str(Path(dir_path).parent)
    #choose the VPN if a list of VPNs is specified:
    if len(params['vpn_location_list']) > 0:
        params['vpn_location']  = random.choice(params['vpn_location_list'])

    #if using vpn set the arg_string to get cookies from the local folder
    #when using VPN always reset the cookies each time the driver is reset / reconfifgured
    if params['vpn_location'] != '':
        #when running several scrapers in parallel google chrome locks some of the files in the "cookies user data" folder
        #so approach is copy vpn cookies from master vpn location cookies to folder local to scraperto pick up cookies
        target_chrome_data_dir = str(Path( params['opfpath'])  / ( MASTER_CHROME_DATA_DIR + params['vpn_location']))
        source_chrome_data_dir = str(Path( parent_path + MASTER_CHROME_DATA_PATH) / ( MASTER_CHROME_DATA_DIR + params['vpn_location']))
        copy_cookies(target_chrome_data_dir, source_chrome_data_dir )
        arg_string = "--user-data-dir=" + target_chrome_data_dir
        params['user-data-dir'] = arg_string
        logging.info(('Configuring Chrome vpn_location arg string', arg_string))
        #wait, sometimes have an issue with the vpn extension not starting properly, maybe due to copy
        logging.info('Waiting for pre newdriver wait before exiting configure_reset_chrome_VPNextensions_cookies {}'.format(params['pre_newdriver_wait']))
        time.sleep( params['pre_newdriver_wait'])
    ##if not using vpn, and resetting cookies copy from a default MASTER_CHROME_DATA_DIR
    elif params['vpn_location'] == '' and params['resetCookies']:
        target_chrome_data_dir = str(Path( params['opfpath'])  / ( MASTER_CHROME_DATA_DIR ))
        source_chrome_data_dir = str(Path( parent_path +  MASTER_CHROME_DATA_PATH ) / ( MASTER_CHROME_DATA_DIR ))
        #source_chrome_data_dir = str(Path( MASTER_CHROME_DATA_PATH)  / ( MASTER_CHROME_DATA_DIR ))
        copy_cookies(target_chrome_data_dir, source_chrome_data_dir )
        arg_string = "--user-data-dir=" + target_chrome_data_dir
        params['user-data-dir'] = arg_string
        logging.info(('Configuring Chrome resetting cookies arg string', arg_string))
        #wait, sometimes have an issue with the vpn extension not starting properly, maybe due to copy
        logging.info('Waiting for pre newdriver wait before exiting configure_reset_chrome_VPNextensions_cookies {}'.format(params['pre_newdriver_wait']))
        time.sleep( params['pre_newdriver_wait'])
    else:
        #not configuring Chrome cookies or extensions, do not set the user-data-dir
        params['user-data-dir'] = arg_string = ""

    return params

def get_ip_from_ipleak(driver, params):
    """Fetches the ip and location details from the ipelak.net website
    Opens https://ipleak.net/, waits for 2 seconds, fetches the html and extracts the ip details
    """
    webPage = 'https://ipleak.net/'
    driver.get(webPage)
    #force a wait to give some time for the vpn extension or luminati proxy to connect
    logging.info(('Checking ip location, waiting for 2 secs for page to load'))
    time.sleep(2)
    html_page = driver.page_source
    webSoup = bs4.BeautifulSoup(html_page , params['htmlParser'])
    table = webSoup.find('table', { 'class' : "properties" } )
    if table != None:
        rows = table.find_all('td')
        ip = rows[1].get_text()
        country = rows[9].get_text()
        city = rows[11].get_text()
        logging.info('ip location : country: {} , city: {} , ip: {} '.format(country, city, ip))
    else:
        logging.warning('Warning: issue with web driver ipleak.net page not opened')
        country = ""
        city = ""
        ip = ""
    return country, city, ip


def get_ip_from_whatsmyip(driver, params):
    """Fetches the ip and location details from the whatsmyipaddress website
    Opens https://whatismyipaddress.com/, closes buttons,  waits for 2 seconds, fetches the html and extracts the ip details
    """
    webPage = 'https://whatismyipaddress.com/'
    country = ""
    city = ""
    ip = ""
    try: #place in a try and except to trap when the renderer fails to open the page
        logging.info('Openning get_ip_from_whatsmyip')
        driver.get(webPage)
        #force a wait to give some time for the vpn extension or luminati proxy to connect
        logging.info(('Checking ip location, waiting for 2 secs for page to load'))
        time.sleep(2)
        #click the buttons to close cookies etc
        buttonXpaths = ['//*[@id="qc-cmp2-ui"]/div[2]/div/button[3]']
        for buttonXpath in buttonXpaths:
            try:
                acceptButton = driver.find_element_by_xpath(buttonXpath) #will wait up to the implicitly wait timeout for the element to be present
                acceptButton.click()
            except:
                logging.warning('WARNING whatsmyip Button not Clicked {} '.format(buttonXpath))
                pass

        html_page = driver.page_source
        webSoup = bs4.BeautifulSoup(html_page , params['htmlParser'])
        #find the ip
        ip_info = webSoup.find('div', {'id' : 'ipv4'})
    except:
        logging.warning('Failed to check ip in get_ip_from_whatsmyip')
        ip_info = None
   
   #process the returned page to find the ip etc     
    if ip_info != None:
        ip = ip_info.get_text()
    else:
        logging.warning('Warning: issue with web driver whatismyipaddress.com page not opened')
        logging.info('page content {} '.format(webSoup.get_text()))
    #find the ip details
    table = webSoup.find('table')
    if table != None:
        rows = table.find_all('tr')
        if len(rows) > 3:
            isp = rows[0].get_text()
            country = rows[3].get_text()
            city = rows[1].get_text()
            logging.info('ip location : isp {} , country: {} , city: {} , ip: {} '.format(isp, country, city, ip))
        else:
            logging.warning('Warning: issue with web driver whatismyipaddress.com page not opened')

    return country, city, ip

def get_ip_from_identme(driver, params):
    """Fetches the ip and location details from the identme website
    Opens https://ident.me, closes buttons,  waits for 2 seconds, fetches the html and extracts the ip details
    """
    webPage = 'https://ident.me'
    country = ""
    city = ""
    try: #place in a try and except to trap when the renderer fails to open the page
        logging.info('Openning get_ip_from_identme')
        driver.get(webPage)
        #force a wait to give some time for the vpn extension or luminati proxy to connect
        logging.info(('Checking ip location, waiting for 2 secs for page to load'))
        time.sleep(2)
        html_page = driver.page_source
        webSoup = bs4.BeautifulSoup(html_page , params['htmlParser'])
        ip = webSoup.get_text()
    except:
        logging.warning('Failed to check ip in get_ip_from_identme')
        ip = ""

    if ip != "":
        logging.info('ip : {} '.format( ip))
    else:
        logging.warning('Warning: issue with web driver https://ident.me page not opened')

    return country, city, ip

def check_display_iplocation(driver, params, attempt):
    """Fetches the ip and location details
    """
    #country, city, ip = get_ip_from_ipleak(driver, params)
    ip = ""
    #if 1st attempt try whatsmyip - use for testing 
    if attempt == 0:
        #country, city, ip = get_ip_from_whatsmyip(driver, params)
        country, city, ip = get_ip_from_identme(driver, params)
    #if whatsmy ip did not find the ip, sometimes it denies access after multiple requests in a reasonably short time, use the simpler identme
    if ip == "":   
        country, city, ip = get_ip_from_identme(driver, params)
    return country, city, ip

def configureNewDriver(params,driver):
    """Configure a new web driver, close if already open.
    Set up the connection via the chrome vpn extension or the luminati server
    Wait until the driver has been established before continuing
    Check that the new driver has been established by checking the ip , if no connection no ip will be returned
    """
    #close any existing driver
    closeDriver(driver)

    #reset the chrome configuration if reset_cookies is set
    if  params['resetCookies']:
        params = configure_reset_chrome_VPNextensions_cookies(params)

    #set up driver options and fetch the directory of the python file
    options = webdriver.ChromeOptions()
    #capabilities = DesiredCapabilities.CHROME.copy()
    #configure proxy settings
    if params['proxy_location'] != '':
        msg = 'ERROR - proxy location not currently supported use vpn or luminati'
        logging.info(msg)
        sys.exit(msg)

    if params['luminati_proxy'] != '':
        lmp_proxy = ''
        if params['luminati_proxy'] == 'GB':
            lmp_proxy = 'http://127.0.0.1:24000'
        if params['luminati_proxy'] == 'FR':
            lmp_proxy = 'http://127.0.0.1:24001'
        if params['luminati_proxy'] == 'GB_R':
            lmp_proxy = 'http://127.0.0.1:24002'
        options.add_argument('--proxy-server=' + lmp_proxy)  # setting the proxy as an argument   

    #set chrome options
    if params['user-data-dir'] != "":
        options.add_argument(params['user-data-dir']) 
        logging.info(('Adding user-data-dir to chrome options', params['user-data-dir']))
    options.add_argument("--user-agent=" + USER_AGENT)                       
    # NOT ALLOWED options.add_experimental_option('w3c', False)
    options.add_argument('log-level=3') # do not generate INFO CONSOLE warnings
    #options.add_argument('log-level=0') # generate info logging
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--unsafe-inline') #gooutdoors would not load generated a chrome error
    options.add_experimental_option("excludeSwitches", ["ignore-certificate-errors", "safebrowsing-disable-download-protection", "safebrowsing-disable-auto-update",
                                "disable-client-side-phishing-detection", 'enable-automation']) # enable automation removes the experimental software info bar
    options.add_argument("--window-size=1600,900") #set the window size
    #turn off display if set
    if params ['displayOff']  : options.add_argument('headless')
    #disable loading images if set
    if params['disable_image_loading']: 
        prefs = {"profile.managed_default_content_settings.images": 2}
        options.add_experimental_option("prefs", prefs)

    #d = DesiredCapabilities.CHROME
    #d['goog:loggingPrefs'] = {'performance': 'ALL', 'browser' : 'ALL'}
    
    #new code using ChromeDriverManager - sometimes the chromedriver fails to download the driver, in this case get the the driver from an older cache
    logging.info('Using ChromeDriverManager to copy/install chromedriver')
    try:
        driver_path = ChromeDriverManager(cache_valid_range=1).install()
        logging.info('ChromeDriverManager installed')
    except:
        logging.info('Problem Installing ChromeDriverManager trying cache_valid_range = 2')
        driver_path = ChromeDriverManager(cache_valid_range=2).install()

    max_attempts = 30
    attempt = 0
    ip = ""
    #set up new driver until can obtain ip details, helping to solve issues where the vpn does not connect and asks for a user name, or on the
    #VM for some reason there is an issue in establishing a connection
    while attempt < max_attempts and ip == "":
        #close any existing driver
        closeDriver(driver)
        logging.info('Configuring new webdriver, attempt {} '.format(attempt))
        try:
            #driver = webdriver.Chrome(executable_path=driver_path, options=options, desired_capabilities=d)
            driver = webdriver.Chrome(executable_path=driver_path, options=options)
            logging.info(('Set up web driver, waiting for post driver wait', params['post_newdriver_wait']))
            time.sleep(params['post_newdriver_wait'])
            if platform.system() == "Windows": #for some reason Ubuntu has an issue with minimised windows, only minimise if Windows
                if params['displayMinimised']: driver.minimize_window()
            driver.set_page_load_timeout(60)
            #check the ip location, country and city
            country, city, ip = check_display_iplocation(driver, params, attempt)
        except Exception as e:
            #chrome driver has failed to start for some reason
            logging.warning('Chrome driver failed to initialise with message: {} '.format(e))

        #if in multiple attempts try increasing the driver wait, cap at 10 secs
        if attempt > 0:
            params['pre_newdriver_wait'] = min( params['pre_newdriver_wait'] + 1, 10)
            params['post_newdriver_wait'] = min(params['post_newdriver_wait'] + 1, 10)
            logging.info('Attempt {} at configuring new driver, pre_newdriver_wait now {}, post_newdriver_wait now {}'.format(attempt,
                                                                        params['pre_newdriver_wait'] , params['post_newdriver_wait'] ))
        attempt = attempt + 1

        #if struggling for some reason, reset the chrome cookies, wait and try again
        if attempt in [5, 10, 15, 20, 25]:
            closeDriver(driver)
            logging.warning('Problems configuring new driver, resetting chrome VPN extensions and waiting 10 seconds')
            params = configure_reset_chrome_VPNextensions_cookies(params)
            time.sleep(10)
            logging.warning('Problems configuring new driver, resetting chrome VPN extensions FINISHED waiting 10 seconds')
        if attempt == max_attempts and ip == "":
            msg = "Unable to open the browser with connection and a valid ip, exiting"
            sys.exit(msg)
            
    return driver

def get_size(obj, seen=None):
    """Recursively finds size of objects"""
    size = sys.getsizeof(obj)
    if seen is None:
        seen = set()
    obj_id = id(obj)
    if obj_id in seen:
        return 0
    # Important mark as seen *before* entering recursion to gracefully handle
    # self-referential objects
    seen.add(obj_id)
    if isinstance(obj, dict):
        size += sum([get_size(v, seen) for v in obj.values()])
        size += sum([get_size(k, seen) for k in obj.keys()])
    elif hasattr(obj, '__dict__'): 
        size += get_size(obj.__dict__, seen)
    elif hasattr(obj, '__iter__') and not isinstance(obj, (str, bytes, bytearray)):
        size += sum([get_size(i, seen) for i in obj])
    return size

#get the html pages from the selected webpage using selenium and chrome web driver (requests is easily blocked by website server)
#optional parameter wait_for_page_load to not wait for page to complete loading in getWebPage, e.g. when loading product details
#optional parameter use_short_webPageWait to use a shorter webPageWait = minimum time to load page, e.g. when loading product details
def getHtml(webPage, driver, params, wait_for_page_load = True, use_short_webPageWait = False, 
            clear_popups = False,  process_html_pages = False):  
    """ returns either the html pages as a list of pages, or the products processed from the page
    controlled by process_html_pages, defaults to False set to True to return products df
    """
    #params['webdriverpath'] = CHROMEDRIVERPATH
    scrollDownPage = params['scrollDownPage']
    resetDriver = params['resetDriver']
    invalidRedirect = params ['invalidRedirect']
    MaxPageReloads = 20

    #logging.info('Html from', webPage)
    currentUrlValid = False
    pageReloads = 0
    #continue to fetch the web page until there is a url which does not contain the invalid redirection switch
    while not currentUrlValid and pageReloads < MaxPageReloads:
        htmlPages = []
        #check if existing driver, if provided is valid
        try:
            driver.session_id
        except: #driver is not valid, force a new driver
            driver = None

        if (resetDriver or driver == None) and webPage != "DO_NOT_RELOAD_PAGE": # set up a new driver window if reset web driver or not yet set
            logging.info(('In getHTML configuring new driver', driver, resetDriver, webPage))
            driver = configureNewDriver(params, driver)
        logging.info(('In getHtml - fetching getWebPage', webPage))
        htmlPages, currentUrl, driver, problemUrl = getWebPage(webPage, driver, params, scrollDownPage, wait_for_page_load , use_short_webPageWait, clear_popups) 

        #check if the url is valid, if not then close the driver - then causes the collection loop again, and forces a new driver
        if currentUrl.find(invalidRedirect) == -1 or invalidRedirect == '':  # e.g. ?dual_run=monty& is not in the current url, then it is valid
            currentUrlValid = True
            logging.info(('fetched web page from url ', currentUrl , len(htmlPages)))
        else: #close the driver and reset
            currentUrlValid = False
            closeDriver(driver)
            driver = None
            pageReloads = pageReloads + 1  
            logging.info(('Invalid Url Reloading page ' , pageReloads))
    
    if process_html_pages:
        logging.info(('XXXXXXXXXXXXXXXXXXXXXXXX Processing HTML Pages ', len(htmlPages)) )
        logging.info(('Size of html in MBytes ', get_size(htmlPages)/(1024*1024)))
        print_memory_usage()
    else:
        logging.info(('XXXXXXXXXXXXXXXXXXXXXXXX NOT Processing HTML Pages , returning original html', len(htmlPages)) )


    if pageReloads == MaxPageReloads:
        errmsg = 'Reloaded html page too many times ' + str(pageReloads) + ' ' + currentUrl
        sys.exit(errmsg)

    return htmlPages, driver, currentUrl, problemUrl

def clear_popups_cookies(driver, params, clear_popups, webPage):
    """
    Clear popups, clear the wrong country popups and click the buttons to remove cookies etc.
    """
    if clear_popups and params['popup_remove_button'] != []: 
        logging.info(('Waiting to remove pop ups popup_remove_button'))
        remove_popup(driver, params['popup_remove_button'], params['popup_remove_button_wait'])        

    #check to see if there is a wrong country pop up, if so close the driver and reload
    if clear_popups and params['wrong_country_popup'] != []:
        logging.info('Clearing wrong country popup ')
        wrong_popup_flag = True
        while wrong_popup_flag:
            wrong_country_popup = detect_wrong_country_popup(driver, params['wrong_country_popup'], params['popup_remove_button_wait'])
            if wrong_country_popup:
                driver.close()
                driver = configureNewDriver(params, driver)
                driver.get(webPage)
                if clear_popups and params['popup_remove_button'] != []: 
                    remove_popup(driver, params['popup_remove_button'], params['popup_remove_button_wait'])
            else:
                wrong_popup_flag = False

    # click any buttons to close cookies etc.
    logging.info('Clicking buttons to close cookies')
    clickButton(driver, params) 
    return

def reload_page_if_error(driver, params, clear_popups, webPage):
    #check to see if there has been a problem loading the page, if so retry
    driver_flag = 0
    # while '"status":"200"' not in str(driver.get_log('performance')) or "No internet" in str(driver.page_source) or "This site can't be reached" in str(driver.page_source) or "Aw, Snap!" in str(driver.page_source) or "Access Denied" in str(driver.page_source):
    while "No internet" in str(driver.page_source) or "This site can't be reached" in str(driver.page_source) or "Aw, Snap!" in str(driver.page_source) or "Access Denied" in str(driver.page_source):
        if driver_flag < 20:
            driver.close()
            logging.info(("Retrying, reloading page ", driver_flag + 1, 'waiting for page to reload'))
            driver = configureNewDriver(params, driver)
            time.sleep(params['wait_for_page_load'])
            driver.get(webPage)
            clear_popups_cookies(driver, params, clear_popups, webPage)
        else:
            break
        driver_flag += 1
    return

def scroll_down_page_end_page(newHtml, driver, params):
    """ Scroll down the page to the bottom of the page using the end page key, and add new html to html pages if it has changed
    """
    origHtml = driver.page_source
    logging.info(("Scroll to end of page"))
    driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.END)
    time.sleep(params['webPageScrollWait'])
    driver.find_element_by_tag_name('body').send_keys(Keys.PAGE_UP)
    time.sleep(params['webPageScrollWait'])
    driver.find_element_by_tag_name('body').send_keys(Keys.PAGE_DOWN)
    time.sleep(params['webPageScrollWait'])
    #append the latest html if it is different to the original html
    scrolledHtml = driver.page_source
    if scrolledHtml != origHtml:
        newHtml.append(scrolledHtml)
    return newHtml

def scroll_through_pages_end_page(newHtml, driver, params, currentUrl):
    """The default method of scrolling through pages
    default scroll is to scroll to the end of the page, , the page up and page down to force html to load and reload the html, 
    and continue until no more pages load
    """
    page_end = False
    page_height1 = 0
    page_height2 = 0
    scrollPage = 0
    while not page_end: #continue scrolling until end of page reached
        #scroll down page and collect html
        page_height1 = driver.execute_script('return document.documentElement.scrollHeight')
        logging.info('scrolling to end of page')
        driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.END)
        time.sleep(params['webPageScrollWait'])
        driver.find_element_by_tag_name('body').send_keys(Keys.PAGE_UP)
        time.sleep(params['webPageScrollWait'])
        driver.find_element_by_tag_name('body').send_keys(Keys.PAGE_DOWN)
        time.sleep(params['webPageScrollWait'])
        newHtml.append(driver.page_source)
        #check if end of page - no more scrolling
        scrollPage += 1
        page_height2 = driver.execute_script('return document.documentElement.scrollHeight')
        logging.info(("page_height1: ", page_height1, "page_height2: ", page_height2, 'pages ', scrollPage))
        if page_height1 == page_height2:
            page_end = True
        #check if runtime limit exceeded  e.g. stuck in an endless scroll 
        if exit_if_exceeded_run_time_limit(params, currentUrl, driver): 
            page_end = True
    return newHtml

def scroll_through_pages_slow_scroll(newHtml, driver, params, currentUrl):
    """Scroll down the pages, but using the slow scroll technique, paging down the window screen hgeight each scroll
    Takes potentially a long time and duplicates the html captured but ensures every product is visible at some point
    """
    page_end = False
    page_height = 0
    page_scrolled = 0
    count = 0
    origHtml = ""
    #java script for getting screen height loaded, and number of pickes scrolled down with each page down
    js_screen_doc_height = 'return Math.max( document.body.scrollHeight, document.body.offsetHeight,  document.documentElement.clientHeight,  document.documentElement.scrollHeight,  document.documentElement.offsetHeight);'
    if params['pgkey_page_scroll']:
        pg_dn_distannce = driver.execute_script('return document.documentElement.clientHeight')
        logging.info(("Scroll using page down key ", pg_dn_distannce))
    else:
        pg_dn_distannce = int(params['slow_page_scroll'] )
        logging.info(("Scroll using slow page scroll ", pg_dn_distannce))
    while not page_end:
        #scroll down page and collect html
        logging.info(('scroll by page down key'))
        driver.find_element_by_tag_name('body').send_keys(Keys.PAGE_DOWN)
        time.sleep(params['webPageScrollWait'] )
        #add the new html to the list of html pages if it is different to the previous page download
        scrolledHtml = driver.page_source
        if scrolledHtml != origHtml:
            logging.info(('New Html is different to original html'))
            newHtml.append(scrolledHtml)
            origHtml = scrolledHtml
        else:
            logging.info(('New html is same'))
        #check if end of page - no more scrolling
        page_scrolled = page_scrolled + pg_dn_distannce
        page_height = driver.execute_script(js_screen_doc_height)
        count = count + 1
        #if the distance scrolled is greater than the current page height then have reached the end of the page 
        # (make it try at least two scrolls past the page height )
        logging.info(("page_height: ", page_height, "distance scrolled : ", page_scrolled, 'count ', count))
        if page_scrolled > (page_height + (3 *  pg_dn_distannce)) :
            page_end = True
        #force an exit if runtime limit exceeded
        if exit_if_exceeded_run_time_limit(params, currentUrl, driver):
            page_end = True
    return newHtml

#function to get a web page and the url of the web page so that redirects can be detected
#When called from getHtml it is called potentially twice or more.  First time to set up the page and clear popups etc
#then subsquently to scroll down the page
#add a timeout in case the driver / renderer freezes
@timeout(1000)
def getWebPage(webPage, driver, params, scrollDownPage, wait_for_page_load , use_short_webPageWait, clear_popups):
    maxTries = 5 # max attempts at getting the web page
    timeToWaitNextAttempt = 10 # time to wait before trying again if a problem
    attempt = 0
    webPageHtmlComplete = False
    newHtml = [] 
    currentUrl = ''
    problemUrl = False
    #fetch the webpage , if there is a problem getting the web page, reset the driver, wait and try again
    while not webPageHtmlComplete and attempt < maxTries:
        try:
            #reset the problem url
            problemUrl = False
            t = time.time()
            #get the page referenced by webPage
            logging.info(('In getWebPage Fetching Webpage ', webPage))
            if webPage != "DO_NOT_RELOAD_PAGE":
                #try:  # get the page, handle no internet errors, popups blocking the page, cookies messages stopping page from loading
                logging.info(("Opening webpage now... waiting for page to load ", params['waitForPageLoad']))
                driver.get(webPage)
                time.sleep(params['waitForPageLoad'] )
                #clear any popups and cookie buttons
                clear_popups_cookies(driver, params, clear_popups, webPage)
                #reload the page if there is a problem loading, no internet, this site cannot be reached, Aw snap
                reload_page_if_error(driver, params, clear_popups, webPage)
                logging.info(('Fetched web page ', time.strftime("%Y-%m-%d %H:%M:%S", time.localtime() )))

            #fetch the html and append to the list of html  pages collected
            logging.info(('Feching html from page ', driver.current_url))
            newHtml.append(driver.page_source)
            #get the current url so that we can track any redirects
            currentUrl = driver.current_url

            #if scrolling down the web page, scroll down and collect new html
            #default scroll is to scroll to the end of the page, , the page up and page down to force html to load and reload the html, and continue until no more pages load
            if scrollDownPage and   (not params['slow_page_scroll'])  and (not params['pgkey_page_scroll']) and (not params['end_page_scroll']) :
                newHtml = scroll_through_pages_end_page(newHtml, driver, params, currentUrl)

            #optional scroll is slow scroll, scrolling down specified no of pixels at a time optional scroll is scrolling down using the page down key
            if scrollDownPage and (params['slow_page_scroll'] or params['pgkey_page_scroll']):
                newHtml = scroll_through_pages_slow_scroll(newHtml, driver, params, currentUrl)

            #simple scroll to the end of the page, then page up and page down to force html to load and reload the html
            if scrollDownPage and  params['end_page_scroll'] :
                newHtml = scroll_down_page_end_page(newHtml, driver, params)

            #successfully scraped the page without errors
            webPageHtmlComplete  = True

        except Exception as e:
            #This means something serious has gone wrong, e,g, a renderer timeout and it stops working, restart 
            logging.warning(('WARNING problem with fetching web page in getWebPage',  webPage, 'restarting ' , attempt))
            logging.warning(("get html error:", e))
            time.sleep(timeToWaitNextAttempt)
            driver = configureNewDriver(params,driver)
            attempt = attempt+1
            problemUrl = True

    #before exiting,  wait for some additional time so that min time between page loads is webPageWait
    elapsed = time.time() - t
    if use_short_webPageWait:
        wait = max(params['short_webPageWait'] - elapsed,0)
    else:
        wait = max(params['webPageWait'] - elapsed,0)
    logging.info(('getWebPage waiting for additional time ', wait))
    time.sleep(wait)

    return newHtml, currentUrl, driver, problemUrl



def checkTrueEndofPage(driver, params, link):
    #Tneeded for mountain warehouse
    # get current documenbt height
    orig_height = driver.execute_script( " return document.documentElement.scrollHeight" )
    # page end, page down, page up, het new height
    driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.END) 
    driver.find_element_by_tag_name('body').send_keys(Keys.PAGE_UP) 
    driver.find_element_by_tag_name('body').send_keys(Keys.PAGE_DOWN) 
    waitForPageToLoad(driver, params, link)
    new_height = driver.execute_script( " return document.documentElement.scrollHeight" )
    if new_height == orig_height:
        endOfWebPage = True    
    else:
        # it was not a true end of page
        endOfWebPage = False
    
        
    return  endOfWebPage

#capture a screen shot and save to file
def capture_save_screenshot(webPageCount, driver, params, fpath, fname, google_project, google_credentials):
    verbose = True   
    maxNumScreens = 20
    if platform.system() == "Windows": #for some reason Ubuntu has an issue with minimised windows
        if params['displayMinimised']: driver.minimize_window()

    #first make sure at the top of the page
    #driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.HOME)
    driver.find_element(By.TAG_NAME, "body").send_keys(Keys.CONTROL + Keys.HOME)

    # click any buttons to close cookies etc.
    clickButton(driver, params)
                
    # from here http://stackoverflow.com/questions/1145850/how-to-get-height-of-entire-document-with-javascript
    js = 'return Math.max( document.body.scrollHeight, document.body.offsetHeight,  document.documentElement.clientHeight,  document.documentElement.scrollHeight,  document.documentElement.offsetHeight);'


    if verbose : 
        logging.info(('Capture screen shot ', webPageCount, 'screen'))
        #logging.info('Capturing image scroll,  height of entire document ', scrollheight)

    slices = []
    offset = 0
    scrollPosition = 0
    numslices = 0
    last_screenshot = False
    new_scrollheight = 0
    while not last_screenshot and numslices < maxNumScreens:
        if verbose : logging.info(('Capturing image from screen', scrollPosition))
        #scroll to new position    
        if offset != 0 : 
            driver.execute_script("window.scrollBy(0, %s);" %offset)
            # click any buttons to close cookies etc.
            clickButton(driver, params)
            #or use page down key
            #driver.find_element_by_tag_name('body').send_keys(Keys.PAGE_DOWN) 
        # give the page a chance to update
        #set up a dummy link variable as not used in this context
        link = None
        time.sleep(params['waitForPageLoad'] )
        #waitForPageToLoad(driver, params, link)
        # click any buttons to close cookies etc.
        clickButton(driver, params)
        #get the screen shot
        tmp = BytesIO(driver.get_screenshot_as_png())
        img = Image.open(tmp)
        offset = img.size[1] 
        size_of_final_image = offset # incase exit because of max number of pages
        next_scrollPosition = scrollPosition + offset
        old_scrollheight = new_scrollheight
        new_scrollheight = driver.execute_script(js)
        logging.info(('old scroll height ' , old_scrollheight))
        logging.info(('new scroll height ' , new_scrollheight))
        logging.info(('next scroll position ' , next_scrollPosition))

        #if the scroll height has not changed and it is greater than the next scroll position then last page / screenshot
        if (old_scrollheight >= new_scrollheight) and (next_scrollPosition >= new_scrollheight ):
            size_of_final_image = new_scrollheight - scrollPosition
            last_screenshot = True
        else:
            #end of page not reached
            scrollPosition = next_scrollPosition
            numslices = numslices+1

        slices.append(img)
        

    image_height_size = (numslices * offset) + size_of_final_image
    screenshot = Image.new('RGB', (slices[0].size[0], image_height_size))
    paste_offset = 0
    for i, img in enumerate(slices):
        if i < len(slices)-1:
            screenshot.paste(img, (0, paste_offset))
            paste_offset = paste_offset + offset
        else:
            paste_offset = image_height_size - offset
            screenshot.paste(img, (0, paste_offset))

    #save the screenshot
    opDate = dt.datetime.today().strftime("%Y%m%d") 
    #upate the fname
    fullfname = opDate + fname + str(webPageCount) + '.pdf'   
    nodatefname = fname + str(webPageCount) + '.pdf'  
    if OUTPUT_OPTION == "LocalOnly" or OUTPUT_OPTION == "All":
        saveScreenShottoPDF(screenshot, fpath , nodatefname, "Local", google_project, google_credentials)
    if OUTPUT_OPTION == "GCSOnly" or OUTPUT_OPTION == "All":
        saveScreenShottoPDF(screenshot, fpath , fullfname,  "Google_Storage", google_project, google_credentials)

    return driver    


#function to click a button, e.g. to close the cookies request or to mimic human behaviour
#from selenium.webdriver.support.ui import WebDriverWait as wait    


def clickButton(driver, params):
    for buttonXpath in params['buttonXpaths']:
        try:
            #temp click the cookies button buttonXpath = '//*[@id="ukCookie"]/div/div[2]/button'
            #wait until the button is present and then click it
            acceptButton = driver.find_element_by_xpath(buttonXpath) #will wait up to the implicitly wait timeout for the element to be present
            acceptButton.click()
        except:
            #logging.info('WARNING Button not Clicked ', buttonXpath)
            pass
    return

#function to compare two html pages to see if they have the same products
def compareHtmlProducts(html1, html2, params, driver, link):
    #convert the html to bs4 soup
    webSoup1 = bs4.BeautifulSoup(html1 , params['htmlParser'])
    webSoup2 = bs4.BeautifulSoup(html2 , params['htmlParser'])
    #get the product items from the web soup
    webPage1 = WebPageItems(webSoup1, link, driver, params)
    webPage2 = WebPageItems(webSoup2, link, driver, params)
    webPage1.find_all_products()
    webPage2.find_all_products()
    listofProducts1 = webPage1.products
    listofProducts2 = webPage2.products
    #check to see if products  are equal
    if (listofProducts1 == listofProducts2)  :
        #logging.info('WEB PAGES 1 and 2 are equal')
        htmlPagesEqual = True
    else:
        logging.info('WEBPAGES 1 and 2 have different products')
        htmlPagesEqual = False
    return htmlPagesEqual
               
#function to compare two html pages to see if they have the same html
def compareHtmlDirect(html1, html2, params, driver, link):
    #convert the html to bs4 soup
    webSoup1 = bs4.BeautifulSoup(html1 , params['htmlParser'])
    webSoup2 = bs4.BeautifulSoup(html2 , params['htmlParser'])
    #check to see if html is equal
    if (webSoup1 == webSoup2)  :
        #logging.info('WEB PAGES 1 and 2 are equal')
        htmlPagesEqual = True
    else:
        logging.info('WEBPAGES 1 and 2 have different html')
        htmlPagesEqual = False
    return htmlPagesEqual 

#function to wait for a page to finish loading by examining the product details, pages with no products time out/wait for the maximum time
#wait for at least the minimum scroll pause time
def waitForPageToLoad(driver, params, link):
    logging.info('In waitForPageToLoad')
    maxLoops = 5
    waitForPageLoad = params['waitForPageLoad'] 
    oldHtml = driver.page_source
    newHtml = driver.page_source
    #click on any buttons that need to be pressed
    webSoup = bs4.BeautifulSoup(newHtml , params['htmlParser'])
    #get the product items from the web soup
    webPage = WebPageItems(webSoup, link, driver, params)
    #click on any buttons on the page that need to be clicked
    webPage.click_buttons_on_webpage()
    loop = 0
    #loop around until the page is fully loaded
    htmlPagesEqual = False

    #if params['popup_remove_button'] != []:
    #    remove_popup(driver, params['popup_remove_button'], params['popup_remove_button_wait'])

    while not htmlPagesEqual and loop < maxLoops :
        #logging.info('waitforPagetoLoad loop ', loop, waitForPageLoad)
        oldHtml = newHtml
        #wait to give the page an opportunity to load
        logging.info('WAITING FOR PAGE TO LOAD')
        time.sleep(waitForPageLoad)
        newHtml = driver.page_source
        if loop > 0: # on 2nd and subsequent views of the web page check to see if the web pages are equal
            #version 1 assume fully loaded if same products details on successive reloads
            #assumes cannot compare html as most sites have rolling banners etc
            #version 2 assume fully loaded if html is equal
            if params['comparehtml'] == 'direct':
                htmlPagesEqual = compareHtmlDirect(newHtml, oldHtml, params, driver, link)
            else:
                htmlPagesEqual = compareHtmlProducts(newHtml, oldHtml, params, driver, link)
        loop = loop+1
        
    return

#copy chrome cookies
def copy_cookies(target_chrome_data_dir, source_chrome_data_dir ):
    #will load in all cookies etc saved to the folder, but is not shareable by several instances, causes an error, so use a unique folder for each scrape
    #remove the existing temporary folder if it already exists
    if os.path.exists(target_chrome_data_dir):
        retries = 0
        cookies_deleted = False
        while retries < 5 and not cookies_deleted:  #incase temporarily locked e.g backup
            try:
                logging.info(('Attempting to Delete chrome dir',retries, target_chrome_data_dir ))
                shutil.rmtree(target_chrome_data_dir, ignore_errors=False, onerror=None)
                #shutil.rmtree(target_chrome_data_dir, ignore_errors=True)
                cookies_deleted = True
                logging.info(('Deleted chrome dir',retries, target_chrome_data_dir ))
            except Exception as e:
                logging.warning(('Unable to delete chrome dir',retries, target_chrome_data_dir ))
                logging.info(e)
                retries = retries +1
                time.sleep(10)

        if not cookies_deleted :
            logging.info('Unable to delete old google chrome dir')
            sys.exit()

    #shutil.copytree as did not wait to complete, seems to be solved with specifying copy function
    #sysstring = 'Xcopy ' + MASTER_CHROME_DATA_DIR + ' "' + target_chrome_data_dir + '" /E/H/C/I/R/Y' + ' > nul'
    destination = shutil.copytree(source_chrome_data_dir, target_chrome_data_dir, copy_function = shutil.copy)  
    #os.system( sysstring)  
    logging.info(('Finished copying the chrome data to ' , destination))
    return

################ Ip Info ###########

def get_proxy_info(proxy, **kwargs):
    try:
        proxy = {'https': proxy}
        logging.info(("in country", proxy))
        loc_url = 'https://mylocation.org/'
        response = requests.request('get', loc_url, proxies=proxy, timeout=5, **kwargs)
        data = {}
        logging.info(("in country", response.status_code))
        soup = bs4.BeautifulSoup(response.content, 'html.parser')
        tr = soup.find_all('div', {'class': 'info'})[0].find('table').find_all('tr')
        for row in tr:
            key = row.find_all('td')[0].text
            value = row.find_all('td')[1].text
            data[key.strip()] = value.strip()
        return data['Country']
    except Exception as e:
        logging.info(("No response found for country!", e))
        return ''


#########################################

def detect_wrong_country_popup(driver, popup_xpths, popup_wait):
    wrong_country_popup = False
    for popup_xpth in popup_xpths:
        try:
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, popup_xpth)))
            element = driver.find_element_by_xpath(popup_xpth)
            if element:
                wrong_country_popup = True
                break

        except:
            pass
    return wrong_country_popup


################ remove popup ###########

def remove_popup(driver, xpths, wait_time):
    logging.info('waiting for pop up to appear ')
    time.sleep(wait_time)
    for xpth in xpths:
        try:
            #WebDriverWait(driver, wait_time).until(EC.presence_of_element_located((By.XPATH, xpth)))
            # element = driver.find_element_by_class_name(cls)

            element = driver.find_element_by_xpath(xpth)
            element.click()
            # driver.execute_script("""var element = arguments[0];element.parentNode.removeChild(element);""", element)

        except:
            pass

#########################################

# Classes to process the webpages
# Updated with retailer specific methods
#TODO move the parameters that are associated from the product page to the WebPageItems
#TODO  then remove the mainurl, link, params
#TODO differentiate between the webpage websoup and the product websoup (may be a lot of work requiring edits) - or just leave, for each product instance it gets overwritten
class WebPageItems():
    def __init__(self, webSoup, link, driver, params):
        # store bs4 soup
        self.webSoup = webSoup  
        #set the page related variables
        self.link = link
        self.driver = driver
        self.params = params
        self.mainurl = params['mainurl']
        self.nextPage = None
        self.products = []
        
    def output_screen(self):
        logging.info(self.webSoup.prettify())
        
    def output_file(self, fpath, fname):
        prettyHTML = self.webSoup.prettify() 
        location = "Local"
        google_project = "na"
        google_credentials = "na"
        outputToFile(fpath, fname, prettyHTML, "w", location, google_project, google_credentials)   
                            
    def find_all_products(self):
        raise NotImplementedError("You need to define find_all_products") 
    
    def get_promo_message(self):
        return ""
        
    def find_next_page_from_webpage(self):
        raise NotImplementedError("You need to define find next page") 
        
    def click_buttons_on_webpage(self):
        pass


class ProductDataFrame(WebPageItems):
    def __init__(self, webSoup, link,  params, driver):
        # store bs4 soup 
        self.webSoup = webSoup
        #set the page related variables
        self.link = link
        self.driver = driver
        #self.webPage = link['webpage'] # set the webPage to the page actually returned
        self.webPage = link['topurl'] # set the webPage to the original link
        self.priority = link['priority']
        self.department = link['department']
        self.category = link['category']
        self.subcategory = link['subcategory']
        self.mainurl = params['mainurl']
        self.promomessage = None
        self.params = params
        #initialise the variables
        self.date = params['todayDate']
        self.skuid = None
        self.description = None
        self.ticketprice = 0
        self.prevprice = 0
        self.banner = None
        self.reviewscore = None
        self.reviewcount = None
        self.url = None
        self.imgurl = None

        self.brand = None
        self.productdf = None
        self.stock = None
        self.pos = None

        self.long_description = None
        self.colour = None
        self.size = None
        self.availability = None

        self.product_descriptiondf = pd.DataFrame()
        
    def createOutputRow(self):
        # make a request to get the url and save the response on the instance
        # STANDARD ORIGINAL SCRAPER - does not have the ASIN attribute
        if not hasattr(self, 'ASIN'):
            row = [ self.skuid,
                    self.date,
                    self.department,
                    self.category,
                    self.subcategory,
                    self.description,
                    self.brand,
                    self.ticketprice,
                    self.prevprice,
                    self.banner,
                    self.reviewscore,
                    self.reviewcount,
                    self.url,
                    self.imgurl,
                    self.promomessage,
                    self.webPage,
                    self.priority,
                    self.stock,
                    ]
            cols = ['skuid', 'date', 'department', 'category', 'subcategory', 'description', 'brand', 'ticketprice', 'prevprice', 'banner', 'reviewscore', 'reviewcount',
                    'url', 'imgurl', 'promomessage', 'webPage',  'priority', 'stock',]
        # NEW BALANCE AMAZON
        else:
            row = [ self.skuid,
                    self.date,
                    self.department,
                    self.category,
                    self.subcategory,
                    self.description,
                    self.brand,
                    self.ticketprice,
                    self.prevprice,
                    self.reviewscore,
                    self.reviewcount,
                    self.url,
                    self.imgurl,
                    self.webPage,
                    self.ASIN,
                    self.seller,
                    self.seller_link,
                    self.discontinued,
                    self.date_first_available,
                    self.item_model_no,
                    ]
            cols = ['skuid', 'date', 'department', 'category', 'subcategory', 'description', 'brand', 'ticketprice', 'prevprice', 'reviewscore', 'reviewcount',
                    'url', 'imgurl', 'webPage',  'ASIN', 'seller', 'seller_link', 'discontinued', 'date_first_available', 'item_model_no']

        productdf = pd.DataFrame([row], columns = cols)
        self.productdf = productdf
        
    def display_product_values(self):
        logging.info('XXX New Product XXX')
        logging.info(('description', self.description))
        logging.info(('skuid', self.skuid))
        logging.info(('url', self.url))
        logging.info(('imageurl', self.imgurl))
        logging.info(('ticket price', self.ticketprice))
        logging.info(('prev price', self.prevprice))
        logging.info(('brand', self.brand))
        logging.info(('banner', self.banner))
        logging.info(('reviewscore', self.reviewscore))
        logging.info(('reviewcount', self.reviewcount))
        logging.info(('stock', self.stock))
        
    def get_description(self):
        self.description = None
        
    def get_url(self):
        self.url = None
                
    def get_skuid(self):
        self.skuid = None
    
    def get_imageurl(self):
        self.imgurl = None

    def get_banner(self):
        self.banner = None

    def get_ticket_previous_price(self):
        self.ticketprice = 0
        self.prevprice = 0

    def get_reviewscore(self):
        self.reviewcount  = None
        
    def get_reviewcount(self):
        self.reviewcount = None
        
    def get_brand(self):
        self.brand = None
        
    def get_stock(self):
        self.stock = None

    def get_product_promo_message(self):
        #promo message by legacy was configured to store the promo message at the top of the web page
        #some scrapers use this field to capture other promo messages associated with the product, e.g. Chrono24
        pass

    #place holder for fetching product description from product url page
    def get_product_description(self):
        pass

    #place holder for fetching main product image from product url page
    def get_product_imgage_url(self):
        pass

    #output the image file if not previously collected and the product page exists (long description != "")   
    #VIP - this code assumes that output_description_file runs first as it will fetch the image url 
    # #add a timeout in case there is an issue
    #@timeout(30) #causing errors
    def output_image_file(self):
        local_params = self.params.copy()
        op_image_path = local_params['opfpath'] 
        #update the output image path if the retailer has been subdivided into multiple scrapers, denoted by '_' with a suffix identifying the sub scraper
        #e.g. RetailerABC split into 3 scrapers RetalierABC_1, RetailerABC_2, RetailerABC_3
        op_image_path = op_image_path.split('_')[0]
        url = self.url
        if url != None and self.long_description != ""  :
            image_fname = removeDisallowedFilenameChars(str(self.skuid) +'.jpg')
            imgurl = self.imgurl
            #for testing without copying to GCS change the location to local
            #location = "Local"
            location = "Google_Storage"
            logging.info(('Fetching Image Url ', imgurl, ' skuid ' , self.skuid))
            try:
                response = requests.get(imgurl, verify=False, timeout=10)
                status_code = response.status_code
                content_type = response.headers['Content-Type']
                if content_type.startswith("image") and status_code == 200 :
                    google_project = local_params['google_project']
                    google_credentials = local_params['google_credentials']
                    outputToFile(op_image_path, image_fname, response.content, "wb", location, google_project, google_credentials) 
                    logging.info(('Finshed fetching image url ', url  ))
                else:
                    raise ValueError('Not an image')
            except:
                logging.warning(('WARNING not able to fetch image at ', url))

    #open the prduct url and fetch the long description, other product attributes and the image url 
    #add a timeout in case of an issue
    #@timeout(30) #causing errors
    def output_description_file(self):
        local_params = self.params.copy()
        #op_image_path = params['opfpath'] 
        url = self.url
        driver = self.driver
        if url != None  :
            #fetch the product page html, sometimes the web link does not work / has reset and current url is the page url
            retries = 0
            currentUrl = ""
            self.long_description = ""
            #retry until the product description is found but limit retries sometimes the link on the page stops working !
            while (self.long_description == "") and retries < 5 : 
                logging.info(('Opening the product page', url, ' retries ' , retries))
                #turn off any scrolling and next page, get the html (No point in saving and resetting as these vars are local to the product class)
                local_params['scrollDownPage'] = False
                local_params['findNextPage'] = False
                #also turn off resetting the driver when getting the individual product pages
                local_params['resetDriver'] = False
                #get the product page
                htmlPages,  driver, currentUrl, problemUrl = getHtml(url, driver, local_params, wait_for_page_load = False, use_short_webPageWait = True) 
                self.driver = driver # store the driver incase it has been updated via configurenewdriver
                retries = retries + 1
                #continue processing if not a problem with the url and fetch the product description and image
                if not problemUrl:
                    html = htmlPages[0]
                    webSoup = bs4.BeautifulSoup(html , local_params['htmlParser'])
                    self.webSoup = webSoup
                    #get the text description values from the product page
                    self.colour = ""
                    #### RETAILER SPECIFIC - GET THE PRODUCT DESCRIPTION
                    self.get_product_description()
                    #### END OF RETAILER SPECIFIC
                    #placeholder for attributes and keywords derivation
                    self.attributes = ''
                    self.keywords = ''
                    #create the product description
                    if local_params['ADD_EBAY_PRODUCT_DESCRIPTIONS']:
                        product_descriptiondf = pd.DataFrame( {'skuid' : [self.skuid], 
                                        'url' : [currentUrl], # use the currentUrl just incase redirected
                                        'img_url' : [self.imgurl],
                                        'short_description' : [self.description],
                                        'long_description' : [self.long_description],
                                        'colour' : [self.colour],
                                        'size' : [self.size],
                                        'availability' : [self.availability],
                                        'attributes' : [self.attributes],
                                        'keywords' : [self.keywords],
                                        'department' : [self.department],
                                        'category' : [self.category],
                                        'subcategory' : [self.subcategory],
                                        'authenticity_guarantee' : [self.authenticity_guarantee],
                                        'money_back_guarantee' : [self.money_back_guarantee],
                                        'seller_url' : [self.seller_url],
                                        'seller_description' : [self.seller_description],
                                        'seller_reviews' : [self.seller_reviews],
                                        'registered_business_seller' : [self.registered_business_seller],
                                        'item_title' : [self.item_title],
                                        'item_condition' : [self.item_condition],
                                        'quantity_available' : [self.quantity_available],
                                        'seller_posts_from' : [self.seller_posts_from],
                                        'buy_it_now' : [self.buy_it_now],
                                        'submit_bid' : [self.submit_bid],
                                        'item_specifics' : [self.item_specifics],
                                        'ebay_item_number' : [self.ebay_item_number],
                                        'seller_business_info' : [self.seller_business_info],
                                        'seller_vat_no' : [self.seller_vat_no],
                                        })
                    else:
                        product_descriptiondf = pd.DataFrame( {'skuid' : [self.skuid], 
                                                            'url' : [currentUrl], # use the currentUrl just incase redirected
                                                            'img_url' : [self.imgurl],
                                                            'short_description' : [self.description],
                                                            'long_description' : [self.long_description],
                                                            'colour' : [self.colour],
                                                            'size' : [self.size],
                                                            'availability' : [self.availability],
                                                            'attributes' : [self.attributes],
                                                            'keywords' : [self.keywords],
                                                            'department' : [self.department],
                                                            'category' : [self.category],
                                                            'subcategory' : [self.subcategory],
                                                            })  
                    self.product_descriptiondf = product_descriptiondf
                    #### RETAILER SPECIFIC GET THE IMAGE URL
                    self.get_product_imgage_url()
                    #### END OF RETAILER SPECIFIC
                    #log a warning if the product description has not been found and give it time to load
                    if self.long_description == "":
                        logging.info(('WARNING Problem fetching product description ', url, 'retrying ', retries, 'waiting 0.5 sec'))
                        time.sleep(0.5)
            if problemUrl:
                logging.info(('WARNING problem with url', url,  problemUrl))
