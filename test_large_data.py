"""
Examples to test the read and write speed to google big query
1. Downloading using the big query api, making use of intermediate storage client.query(sql).to_dataframe(create_bqstorage_client=True)
Note not using the internediate storage is painfully slow for large datasets so not tested
tested both overwriting and append
2. Downloading using the big query api, forcing arrow storage df = client.query(sql).to_arrow(create_bqstorage_client=True).to_pandas()
3. Uploading using the big query api, client.load_table_from_dataframe(df, table_id, job_config=job_config)
4. Uploading using the approach adopted for ML, data > local parquet > google storage > big query   upload_df_to_BQ_via_local_GS 
 (more complex but quicker for large datasets)
5. Uploading using the pandas_gbq library - slow for larger datasets

 Testing the big query API - google libraries required
pip install --upgrade google-cloud-bigquery
pip install --upgrade google-cloud-bigquery-storage
pip install --upgrade pyarrow
pip install --upgrade google-cloud-core
pip install --upgrade google-api-core
"""

import google.auth
from google.cloud import bigquery
from google.cloud import storage
import pandas as pd
import time
import os
from datetime import datetime
import pandas_gbq
from retrying import retry
import math
from pathlib import Path

#configure the parameters required for upload_df_to_BQ_via_local_GS
GBQ_OUT_DATASET_DATAPREP = 'testcompetitive_data'
GCS_ORG_ID = "5710211792764928"
TEMP_BUCKET_FOLDER =  GCS_ORG_ID + '/temp/'
BUCKET_NAME = 'sparkbox-dev-ml'
if os.name == 'posix':
    temp_local_folder = "/home/kevin_d_blackmore/Web Scrape TEST/"
else:
    temp_local_folder =  'C:/Users/kevin/Data/Spb-data/temp/'


"""
 Using a BigQuery Storage API client to download results.
 Creating temporary storage speeds up the download
"""
def download_bq_df_standardbq_storage(sql, credentials, project_id):
    client = bigquery.Client(credentials=credentials, project=project_id)
    start_time = time.time()
    df = client.query(sql).to_dataframe(create_bqstorage_client=True)
    t = time.time() - start_time
    print('Time Taken to download bq storage', t)
    return df, t

"""
 Using a BigQuery Storage API client to download results.
 Alternative approach in google docs 
 Querying to arrow data format and then to Pandas
"""
def download_bq_df_arrow_data(sql, credentials, project_id):
    client = bigquery.Client(credentials=credentials, project=project_id)
    start_time = time.time()
    df = client.query(sql).to_arrow(create_bqstorage_client=True).to_pandas()
    t = time.time() - start_time
    print('Time Taken to download arrow', t)
    return df, t

"""
Using a BigQuery Storage API client to upload results.
Appending to existing table if exists
"""
def test_bqclient_upload_append(df, table_id):
    #force object columns to string to stop bq upload misclassifying type
    for c in df.columns:
        if df[c].dtypes == 'object':
            df[c] = df[c].astype('str')
    client = bigquery.Client()
    start_time = time.time()
    job_config = bigquery.LoadJobConfig(
        # Optionally, set the write disposition. BigQuery appends loaded rows
        # to an existing table by default, but with WRITE_TRUNCATE write
        # disposition it replaces the table with the loaded data.
        #write_disposition="WRITE_TRUNCATE",
    )
    job = client.load_table_from_dataframe(df, table_id, job_config=job_config)
    #wait for job to complete
    job.result()
    t = time.time() - start_time
    print('Time Taken to upload df appending', t)
    return t

"""
Using a BigQuery Storage API client to upload results.
Overwriting existing table if exists
"""
def test_bqclient_upload_replace(df, table_id):
    #force object columns to string to stop bq upload misclassifying type
    for c in df.columns:
        if df[c].dtypes == 'object':
            df[c] = df[c].astype('str')
    client = bigquery.Client()
    start_time = time.time()
    job_config = bigquery.LoadJobConfig(
        # Optionally, set the write disposition. BigQuery appends loaded rows
        # to an existing table by default, but with WRITE_TRUNCATE write
        # disposition it replaces the table with the loaded data.
        write_disposition="WRITE_TRUNCATE",
    )
    job = client.load_table_from_dataframe(df, table_id, job_config=job_config)
    #wait for job to complete
    job.result()
    t = time.time() - start_time
    print('Time Taken to upload df overwriting', t)
    return t

"""
Using pandas gbq library to upload results.
Overwriting existing table if exists
"""
def test_gbq_replace(df, table_id, project_id, credentials):
    #force object columns to string to stop bq upload misclassifying type so as compatible with bq tests
    for c in df.columns:
        if df[c].dtypes == 'object':
            df[c] = df[c].astype('str')
    if_exists_operation = "append"
    start_time = time.time()
    pandas_gbq.to_gbq(df, table_id , project_id=project_id, credentials=credentials, if_exists=if_exists_operation)
    t = time.time() - start_time
    print('Time Taken to upload df append using gbq', t)
    return t


"""
test deduplication of the append table 
"""
def deduplicate_append_table():
    dedup_sql  = """
                #deduplicate on sku_id and cal_date, keeping the latest using the created column
                CREATE OR REPLACE TABLE testcompetitive_data.test_dataupload_append
                AS
                SELECT dedup_table.* FROM (
                SELECT ARRAY_AGG(
                t ORDER BY t.created DESC LIMIT 1
                ) [OFFSET(0)] dedup_table
                FROM testcompetitive_data.test_dataupload_append t
                GROUP BY sku_id, cal_date
                )
        """
    # Make client nd run query job
    bqclient = bigquery.Client(credentials= credentials, project=project_id )
    bqclient._http.adapters['https://']._pool_connections=100
    bqclient._http.adapters['https://'].pool_maxsize=100
    print('De duplicating append output table')
    start_time = time.time()
    bq_job = bqclient.query(dedup_sql, project=project_id)
    bq_job.result()  # Waits for job to complete.
    t = time.time() - start_time
    return t


""" Function to upload file from local folder onto Google Storage destination
Upload file from source to destination
add the retry decorator as GCP occassionaly generates 503 internal not available errors
"""
@retry(stop_max_delay=20000, wait_fixed=1000) #try for 20 seconds, wait 1 second between tries
def upload_blob(source_file_name, destination_blob_name, bucket_name):
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)
    #WORKAROUND to prevent timeout for large files on anything but v. fast link
    if os.name == 'posix':
        chunk_size = 100 * 1024 * 1024 # default 100MB
    else:
        chunk_size = 10 * 1024 * 1024 # reduce to 10MB
    try:
        blob.chunk_size = chunk_size
        blob.upload_from_filename(source_file_name)
    except:  
        chunk_size = int(chunk_size /2)
        blob.chunk_size = chunk_size
        blob.upload_from_filename(source_file_name)
    return

"""
Function to iterate through all files in a local directory and upload ones that match the upload pattern to google storage
excludes folders """
def upload_files(upload_pattern, local_directory, bucket_directory, bucket_name ):
    for filename in os.listdir(local_directory):
        if upload_pattern == []:
            upload_file = True
        else:
            upload_file = False
            for pt in upload_pattern:
                if filename.find(pt) > -1:
                    upload_file = True
        fpath = os.path.join(local_directory, filename)
        if os.path.isdir(fpath):
            print(filename, 'is a folder, excluding')
        elif upload_file:
            print('uploading {} to {} bucket {}'.format(filename,bucket_directory, bucket_name ))
            upload_blob(os.path.join(local_directory, filename), bucket_directory + filename, bucket_name)
    return 



""" 
function to copy a parquet format file from google storage bucket to big query data table
if wait_for_bq_job_to_finish = False, control is returned to Python before the BQ load has completed
"""
def copy_from_GS_to_BQ_parquet(bq_dataset, bq_table, BQ_method, GS_bucket, GS_file, wait_for_bq_job_to_finish = False):
    client = bigquery.Client()
    table_ref = client.dataset(bq_dataset).table(bq_table)
    job_config = bigquery.LoadJobConfig()
    if BQ_method == "WRITE_TRUNCATE":
        job_config.write_disposition = bigquery.WriteDisposition.WRITE_TRUNCATE
    else:
        job_config.write_disposition = bigquery.WriteDisposition.WRITE_APPEND
    job_config.source_format = bigquery.SourceFormat.PARQUET
    uri = "gs://" + GS_bucket + GS_file
    load_job = client.load_table_from_uri(uri, table_ref, job_config=job_config)  # API request
     #print("Starting job {}".format(load_job.job_id))
    if wait_for_bq_job_to_finish:
        load_job.result()  # Waits for table load to complete.
        #check acessing the BQ table
        destination_table = client.get_table(table_ref)
        print("Checking Table - Loaded {} rows.".format(destination_table.num_rows))
    return


"""
function to save a dataframe as parquet, copy it to google storage, load from google storage to big query
also converts legacy non compliant big query column names to compliant 
"""
def upload_df_to_BQ_via_local_GS(df, temp_local_folder, bq_dataset, bq_table, temp_upload_bucket_folder, gs_bucket, wait_for_bq_job_to_finish):
    #force object to string (Parquet throws an error when an object column contains a value that can be interpreted as 
    # an integer (e.g. with first_season )
    for c in df.columns:
        if df[c].dtypes == 'object':
            df[c] = df[c].astype('str')

    #parquet google storage to big query api job load is okay for FCALLNDE (11GB) but fails on FR (30GB)
    #so split files into 15GB chunks (before compression)
    df_size = df.memory_usage().sum()
    chunk_mem_size = 15 * 1024 * 1024 * 1024
    num_chunks = math.ceil(df_size / chunk_mem_size)
    chunk_size = math.ceil(len(df) /num_chunks)

    # Save the data to Parquet files in chunks
    start_time = time.time()
    file_names = []
    for i in range(num_chunks):
        fname =  bq_table  + str(i).zfill(3) + '.parquet'
        opfn = str( Path(temp_local_folder)/  fname )
        df[i*chunk_size:(i+1)*chunk_size].to_parquet( opfn, index=False )
        file_names.append(fname)
    print('Time to write to Parquet file is ', time.time() - start_time)

    #upload the data to the gcs bucket parquet
    start_time = time.time()
    for upload_pattern in file_names:
        upload_files([upload_pattern], temp_local_folder, temp_upload_bucket_folder, gs_bucket )
    print('Time to save parquet to GS ', time.time() - start_time)

    start_time = time.time()
    for i, gs_file in enumerate(file_names):
        if i == 0:
            bq_method = "WRITE_TRUNCATE"
        else:
            bq_method = "WRITE_APPEND"
        copy_from_GS_to_BQ_parquet(bq_dataset, bq_table, bq_method,  gs_bucket + '/' + temp_upload_bucket_folder , gs_file, wait_for_bq_job_to_finish = wait_for_bq_job_to_finish )
    print('Time to transfer parquet from GS to  BQ ', time.time() - start_time)

    return
"""
Main process to test the different methods
"""
if __name__ == '__main__':
    # get google credentials
    credentials, project_id = google.auth.default()

    #define a sql request, simulating a simple date query
    master_sql = """
        SELECT * 
        FROM pimkie_data.FCALLNFR
        WHERE cal_date >= "2017-01-21"
        LIMIT LIMIT_ROWS
        """

    #test_rows = [10000, 100000, 1000000, 5000000]
    test_rows = [1000]
    repitions = 1
    increment_df = 1
    results = pd.DataFrame()

    #run the tests
    for r in range(0, repitions):
        for test in test_rows:
            row = {}
            print(' Test rows ', test)
            
            ##### DOWNLOAD TESTS
            sql = master_sql.replace('LIMIT_ROWS', str(test) )
            #download using standard bq storage
            df, t = download_bq_df_standardbq_storage(sql, credentials, project_id)
            row['dl std bq'] = t

            #download using the arrow data format
            df, t = download_bq_df_arrow_data(sql, credentials, project_id)
            row['dl arrow bq'] = t

            #### UPLOAD TESTS
            #before uploading create a dummy region id , integer stored as a string to check the type processing on upload
            df['region_id_test'] = 11
            df['region_id_test'] = df['region_id_test'].astype('object')
            #add a created date
            df['created'] = datetime.now().replace(microsecond=0)

            #create a large df
            row['num Rows DL'] = len(df)
            df_size = df.memory_usage().sum()/1000000
            row['df size MB DL'] = df_size
            new_df = pd.DataFrame()
            for i in range(0,increment_df):
                print('Creating larger dfg, Appending ', i)
                new_df = new_df.append(df, ignore_index = True)
            df = new_df
            row['num Rows UL'] = len(df)
            df_size = df.memory_usage().sum()/1000000
            row['df size MB UL'] = df_size
            print('new df created, rows', len(df), 'size' , df_size)

            #upload using the legacy BQ to df solution
            st = time.time()
            bq_table = 'test_df_to_bq_table_upload_legacy'
            wait_for_bq_job_to_finish = True
            upload_df_to_BQ_via_local_GS(df, temp_local_folder, GBQ_OUT_DATASET_DATAPREP, bq_table, TEMP_BUCKET_FOLDER, BUCKET_NAME, wait_for_bq_job_to_finish)
            row['df_to_GS_to_BQ'] = time.time() - st

            #upload using bq api - append
            table_id = 'testcompetitive_data.test_dataupload_append'
            t = test_bqclient_upload_append(df, table_id)
            row['ul bq app'] = t

            #upload using bq api - replace    
            table_id = 'testcompetitive_data.test_dataupload_overwrite'
            t = test_bqclient_upload_replace(df, table_id)
            row['ul bq rep'] = t

            #upload using pandas_gbq
            table_id = 'testcompetitive_data.test_dataupload_pandasgbq'
            t = test_gbq_replace(df, table_id, project_id, credentials)
            row['ul panda_gbq rep'] = t

            results = results.append( row, ignore_index=True)

    cols = ['num Rows DL', 'df size MB DL' , 'dl arrow bq',  'num Rows UL', 'df size MB UL', 'dl std bq',    'ul bq app',  'ul bq rep', 'df_to_GS_to_BQ', 'ul panda_gbq rep']    
    results = results[cols]
    print(results)
    results.to_csv('bq_data_timings.csv')


    #test deduplicating the append table
    t = deduplicate_append_table()
    print('Time to deduplicate ', t)




