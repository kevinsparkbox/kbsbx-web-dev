#set up the path in init to the scrapers and the core modules
import __init__

import time
import os
import sys

from selenium import webdriver
from selenium.webdriver.chrome.options import Options

from CoreScraperConstants import MASTER_CHROME_DATA_DIR
from webdriver_manager.chrome import ChromeDriverManager


def main():
    #vpn_loaction = input("Enter VPN location")
    vpn_loaction = "KBTEST"
    chrome_options = Options()
    arg_string = "--user-data-dir=" + MASTER_CHROME_DATA_DIR + vpn_loaction
    #uncomment to use the data dictionary
    #chrome_options.add_argument(arg_string)
    chrome_options.add_argument("--window-size=1600,900")
    chrome_options.add_argument(
        "--user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36")

    #test not loading images
    prefs = {"profile.managed_default_content_settings.images": 2}
    chrome_options.add_experimental_option("prefs", prefs)

    driver = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)
    #driver = webdriver.Chrome(executable_path=CHROMEDRIVERPATH, options=chrome_options)
    # open any website
    # driver.get('https://www.amazon.co.uk')
    #driver.get('https://chrome.google.com/webstore/detail/windscribe-free-proxy-and/hnmpcagpplmpfojmgmnngilcnanddlhb?hl=en')
    #driver.get()

    id = driver.session_id

    print('DRIVER ID', id)

    #driver.find_element_by_id(id).sendKeys("HELLO")

    print('Wait 10 minutes, go to websites and click buttons / credentials or edit cookie in settings, close browser when finished')
    # first time around add the nord vpn extension, then gets saved with the Chrome cookies
    time.sleep(60 * 10)

    driver.close()
    driver.quit()


if __name__ == '__main__':
    main()