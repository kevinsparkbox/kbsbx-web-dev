#set up the path in init to the scrapers and the core modules
import __init__

import os
from pathlib import Path
from instaScraper import mainInstaScrape

#path to OUTPUT FOLDER
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Insta Scrape/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-insta/'

#insta_urls = ['https://www.instagram.com/pimkie/?hl=en', 'https://www.instagram.com/boden_clothing/?hl=en' ]
insta_urls = ['https://www.instagram.com/pimkie/?hl=en' ]

MAX_BOXES = 9999
if __name__ == '__main__':
    #scrape instagram for each url
    retailer_from_fname = Path(__file__).name.split('.')[0].replace('insta_','').replace('Insta','')
    for url in insta_urls:
        mainInstaScrape( url, MAX_BOXES, toppath, retailer_from_fname)



