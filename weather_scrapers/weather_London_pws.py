#set up the path in init to the scrapers and the core modules
import __init__

import os
from pathlib import Path
from WeatherScraper import mainWeatherScrape
from CoreScraperConstants import default_params

#path to OUTPUT FOLDER
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Weather Scrape/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-weather/'

#finding personal weather stations - if map does not wor use google: wunderground personal weather station dashboard manchester
weather_urls = ['https://www.wunderground.com/dashboard/pws/IBERKSHI14/table/YYYY-MM-01/YYYY-MM-01/monthly' ]

params = default_params.copy()
params.update({
    'location': 'London_pws',
    'opfpath': toppath,
    'waitForPageLoad': 5,
    'start_year' : 2017,
    'start_month' : 1,
    'buttonXpaths': ['//*[@id="truste-consent-button"]'],  # a list of XPaths  //*[@id="truste-consent-required"]
    'vpn_location': 'FRWS',  # started giving incorrect results on GBWS on VM but okay on laptop - redirecting to a default dec 2016 date
})

if __name__ == '__main__':
    filename = Path(__file__).name
    #scrape instagram for each url
    location_from_fname = Path(__file__).name.split('.')[0].replace('weather_','').replace('Weather','')
    for url in weather_urls:
        mainWeatherScrape( url, params, toppath, location_from_fname, filename)



