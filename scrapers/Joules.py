import __init__ # set up the import paths
import bs4
import datetime as dt
import pandas as pd
import time
import json
from pathlib import Path
import requests
import html5lib
import os
import re
from math import ceil

from CoreScraper import scrapeWebPages, extractPrice
from CoreScraper_getHtml import WebPageItems, ProductDataFrame
from CoreScraperConstants import toppath,  default_params

#  RETAILER SPECIFIC CONTROL PARAMETERS
# Set up the top path as parent folder of results, Note this is overridden if supplied as an argument when called as a main function
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape/'
    
#set up multiple urls for finding links, first url is mainurl (code filtters duplicates)
linkurls = ['https://www.joules.com']

params = default_params.copy()
params.update( {
    'mainurl' : linkurls[0],
    'opfpath' : toppath, # can be overidden by arguments, applied in in scrapeWebPages
    #define valid links and how to interpret links
    'validtags' : ['joules'],  # a valid link must contain one of these strings
    'invalidtags' : ['checkout', 'account', 'signin'],  # if a link contains one of these it is invalid
    'resetduplicatelinks' : True, # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks' : ['https://www.tbc.com'], # force any duplicate links to be ignored
    'starting_links' : [],    #additional links to process  #set any starting links that will not be picked up by the find starting pages process
    'tagref' : 'a', # look for hrefs in this class
    'remove_switches_link' : True,  # if True, remove ? and # switches from link (recommended)
    'departmentlevel' : 3, #the nth level in the link
    'categorylevel' : 4,  #the nth level in the link
    'subcategorylevel' : 5,  #the nth level in the link
    'minimumdepth' : 4 ,  #a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    #define how the web page is accessed
    'useRequests' : False, # whether or not to use the requests module, if False use the chromium driver Browser
    'findNextPage' : True, # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    #'htmlParser' :  'lxml', # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    'htmlParser' :  'lxml', # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    #webdriver parameters
    'webdriverpath' : 'dummy' , # path to the webdriver executable SET UP IN CoreScraper.py
    'displayOff' : False,  # whether display is switched off or visible
    'displayMinimised' :  False,  # whether to minimise the window if display is on
    'resetDriver' : False,  #whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'waitForPageLoad' : 1, #typ set to half webPageWait, when scrolling, time to wait for page to load before comparing 2 subsequent loads to see if scrolled page has finished loading
    'webPageWait' : 2,  # time between subsequent web pages (next link, next page, next scroll down page)
    'scrollDownPage' : True,  # whether to scroll down the page to find more products, e.g. Topshop
    # url and redirect parameters
    'urlSwitch' :  '',    #add an option to the url,e.g. 48 items per page
    'invalidRedirect' : '', # if the redirected url contains this value the product pages do not load correctly
    #define any buttons to be clicked on screen
    'buttonXpaths' : ['//*[@id="cookie-close-btn"]' , '//*[@id="onetrust-accept-btn-handler"]'], # a list of XPaths
    'buttoniFrameXpaths' : [],
    'proxy_location': '',  # Give multiple country like this, 'GB|US'. country codes: https://laendercode.net/en/2-letter-list.html
    'vpn_location': 'GBWS',

    })
            
#%%  RETAILER SPECIFIC - REDEFINE THE PRODUCTDATAFRAME METHODS
def new_get_description(self):
    info = self.webSoup.find('h2')
    if info != None : 
        d = info.find('a')
        if d != None: 
            description = d.get_text().replace('\n', '').replace('\r', '').strip()
            self.description = description        
            
def new_get_url(self):
    info = self.webSoup
    if info != None : 
        u = info.find('a')
        if u != None :
            url = params['mainurl']+ u.get('href')
            self.url = url

def new_get_skuid(self):
    skuid = self.url
    if skuid != None:
        skuid = self.url.split('id=')[-1]  # skuid 205972|BLUELRGCHK|ONE
        # skuid = skuid.split('|')[0]  # commented this out to make the skuid unique
        if skuid != None:
            self.skuid = skuid
        
def new_get_imageurl(self):
    i = self.webSoup.find('img', { 'class' : "grid-item__image--primary"})
    if i != None:  
        imgurl = params['mainurl'] + i.get('src').replace("//", "/")
        self.imgurl = imgurl

def new_get_brand(self):
    info = self.webSoup.find('section', {'class': 'grid-item__seller'})
    if info != None : 
        d = info.get_text()
        if d != None: 
            self.brand = d
            
def new_get_stock(self):
    # info = self.webSoup.find('span' , {'class' : 'listProductLowStock'})
    # if info != None :
    #     d = info.get_text()
    #     if d != None:
    #         self.stock = d
    pass
            
def new_get_banner(self):
    info = self.webSoup.find('section', {'class' : "grid-item__badges"})
    if info != None : 
        b = info.find('img')
        if b != None: 
            banner = b.get('src')
            if banner != None:
                banner = banner.split('/')[-1].split('.')[0]
            self.banner = banner
            
def new_get_ticket_previous_price(self):
    ticketprice = None
    prevprice = None
    itemprice = self.webSoup.find('span', {'class' : "new-price"})
    if itemprice != None:
        price_val = itemprice.get('data-value')
        ticketprice = price_val
        prevprice = price_val
    itemprices = self.webSoup.find_all('span', {'class' : "old-price-strucked"})
    if len(itemprices) > 0:
        for itemprice in itemprices:
            price_val = itemprice.get('data-value')
            if price_val != None:
                prevprice = price_val
                if len(price_val) > 0 :
                    break
        
    self.ticketprice = extractPrice(ticketprice, "£")
    self.prevprice = extractPrice(prevprice, "£")
    
def new_get_reviewscore(self):
    info = self.webSoup.find('figure', {'class' : "product-item-rating"})
    if info != None : 
        r = info.get_text()
        if r != None :
            reviewscore = r
            self.reviewscore = reviewscore

def new_get_reviewcount(self):
    info = self.webSoup.find('figcaption', {'class' : "product-reviews-count"})
    if info != None : 
        r = info.get_text()
        if r != None :
            r = r.split('(')[1].split('R')[0]
            reviewcount = r.replace('\n', ' ').replace('\r', ' ').replace('(', ' ').replace(')', ' ').strip()
            self.reviewcount = reviewcount
        
def new_get_promo_message(self):
    #TODO
    info = self.webSoup.find('div', {'id' : 'header_espot'})
    if info != None : 
        pm = info.find('img')
        if pm != None: 
            self.promomessage = pm.get('alt')
            
def new_find_all_products(self):
    # products = self.webSoup.find_all('div', { 'class' : 'product-grid-item-inner' } )
    products = self.webSoup.find_all('article', { 'data-type' : 'PRODUCT' } )
    print('num products', len(products))
    self.products = products
    
#define the method that is used to find the product description from the individual product page url
def new_get_product_description(self):
    pi_t = ""
    product_info_text_block = self.webSoup.find('div', { 'id' : 'product-description'})
    if product_info_text_block == None:
        product_info_text_block = self.webSoup.find('p', { 'class' : 'product-summary'})
    if product_info_text_block != None:
        #print('product info', product_info_text_block.prettify())
        block1 = product_info_text_block
        if block1 != None:
            pi_t = block1.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
    self.long_description =   pi_t 
    #get the colour description - clothing products
    info = self.webSoup.find('div', {'class' : 'c-ColourSelector__selected'})
    if info != None:
        colour_info = info
        if colour_info != None:
            colour = colour_info.get_text()
            self.colour = colour.replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
    else:
        #home products / gifts
        #check there is a colour selector
        info = self.webSoup.find('h5', {'class' : 'item-colour'})
        if info != None:
            #get the colour from the data product id, it is the fourth element of the sku id
            data_id = self.skuid.split('|')
            if len(data_id) > 3:
                colour = data_id[3]
                self.colour = colour.replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()


    #TODO fetch the size description data = [list of strings]
    self.size = "size  desc"
    self.availability = "availability desc"

#define the method that is used to find the main image url from the individual product page url
def new_get_product_imgage_url(self):
    image_info = self.webSoup.find('section', {'class' : 'main-image'})
    if image_info != None:
        images = image_info.find('img', {'class' : 'primary-image'})
        if images != None:
            url = images.get("src")
            self.imgurl = 'https:' + url 
        #url2 = image_url.find('img')["srcset"]
        #if url != None:
            #url = 'https:' + url
        #self.imgurl = url


#find the next page link from the current page
def new_find_next_page_from_webpage(self):
    #driver = self.driver # for access to buttons on the page etc
    #for Joules add page=N to the end of the url
    link = self.link
    findNextPage = params['findNextPage']
    nextPage = None
    maxPages = 200 # just in case something goes wrong!!
    dataperpage = 1
    if findNextPage:    
        #find current page number if already set
        pagePos = link['webpage'].find('page=')
        if pagePos == -1:
            # page no currently not set
            nextPage = link['webpage'] + '?page=1'
        else:
            #increment the page no
            currPageNo = link['webpage'][pagePos:].split("=")[1]
            #remove any #tags added
            currPageNo = currPageNo.split("#")[0]
            nextPageNo = int(currPageNo) + dataperpage
            if nextPageNo <= maxPages:
                #then update to next page
                b = link['webpage'].find('page=')
                nextPage = link['webpage'][:b] + 'page=' + str(nextPageNo) 
            else:
                #finish
                nextPage = None
        #if there are no products on the page, have moved past the last page so stop the next page process
        #numProducts = len(self.webSoup.find_all('section', { 'class' : 'grid-item' }))
        #if numProducts == 0:
        #    print('No Products on Page')
        #    nextPage = None

        #Joules - if there are more pages there is a SHOW MORE button at the bottom of the page.
        #put this in as a test as some of the pages do not show nil products when moved past the last valid page (Dec 2020 update)
        buttons = self.webSoup.find_all('button', class_="product-grid__button")
        if len(buttons) == 0:
            #no button
            #print('No Show More Button, no next page')
            nextPage = None
        else:
            more_found = False
            for btn in buttons:
                btn_text = btn.get_text()
                print('Checking Button ', btn_text)
                if "more"  in btn_text:
                    print('Found a More button')
                    more_found = True
                    break
            if not more_found:
                print('More Button not found')
                nextPage = None
     
        print('Next Page' , nextPage)
        
    self.nextPage = nextPage
            

    
#redefine the methods specific to this retailer        
ProductDataFrame.get_description =  new_get_description
ProductDataFrame.get_url = new_get_url
ProductDataFrame.get_skuid = new_get_skuid
ProductDataFrame.get_imageurl = new_get_imageurl
ProductDataFrame.get_banner = new_get_banner
ProductDataFrame.get_brand = new_get_brand
ProductDataFrame.get_stock = new_get_stock
ProductDataFrame.get_reviewscore = new_get_reviewscore
ProductDataFrame.get_reviewcount = new_get_reviewcount
ProductDataFrame.get_ticket_previous_price = new_get_ticket_previous_price
ProductDataFrame.get_product_description = new_get_product_description
ProductDataFrame.get_product_imgage_url = new_get_product_imgage_url
WebPageItems.get_promo_message = new_get_promo_message
WebPageItems.find_all_products = new_find_all_products
WebPageItems.find_next_page_from_webpage = new_find_next_page_from_webpage
#new methods can be added using the same approach, e.g. WebPageItems.newmethod = newmethod


#%%    RETAILER SPECIFIC - IDENTIFY WHICH OPTIONAL METHODS TO RUN 

methods_to_run = {
        'output_product_to_screen' : False,
        'output_product_values_to_screen' : False,
        'output_image_to_file' : True,
        'output_description_to_file' : True,
        'output_webpage_to_file' : False,
        }
     
            
# %% Main Code

    
def main():
    filename = __file__
    #accessories
    startPage = 0  # 20 EDIT HERE FOR SET_UP AND TESTING to processs a sub range of weblinks
    endPage = 999 # 21 EDIT HERE
    scrapeWebPages( startPage, endPage, linkurls, params, filename, methods_to_run)
    return
    
 
if __name__ == '__main__':
    main()    