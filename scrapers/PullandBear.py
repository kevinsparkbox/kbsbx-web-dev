import __init__ # set up the import paths
import bs4
import datetime as dt
import pandas as pd
import time
import json
from pathlib import Path
import requests
import html5lib
import os
import re
from math import ceil

from CoreScraper import scrapeWebPages, extractPrice
from CoreScraper_getHtml import WebPageItems, ProductDataFrame
from CoreScraperConstants import toppath,  default_params

#  RETAILER SPECIFIC CONTROL PARAMETERS
# Set up the top path as parent folder of results, Note this is overridden if supplied as an argument when called as a main function
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape/'
    
# set up multiple urls for finding links, first url is mainurl (code filtters duplicates)
linkurls = ['https://www.pullandbear.com/fr/']

params = default_params.copy()
params.update({
    'mainurl': linkurls[0],
    'numscreenshots' : 1,
    'opfpath': toppath,  # can be overidden by arguments, applied in in scrapeWebPages
    # define valid links and how to interpret links
    'sitemap_links': [],  # if empty search through the linkurls
    'validtags': ['pullandbear'],  # a valid link must contain one of these strings
    'invalidtags': ['page', 'google', 'store', 'redirect', 'help', 'index', 'cookie', 'international', 'content', 'blog',
                    'all-edits'],  # if a link contains one of these it is invalid
    'resetduplicatelinks': True,
    # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks': ['https://www.tbc.com'],  # force any duplicate links to be ignored
    'starting_links': [],
    # additional links to process  #set any starting links that will not be picked up by the find starting pages process
    'tagref': 'a',  # look for hrefs in this class
    'remove_switches_link': False,  # if True, remove ? and # switches from link (recommended unless required)
    'departmentlevel': 4,  # the nth level in the link
    'categorylevel': 5,  # the nth level in the link
    'subcategorylevel': 6,  # the nth level in the link
    'minimumdepth': 5,
    # a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    'max_link_depth': 1,  # the depth of url links to search in the links searching process
    # define how the web page is accessed
    'useRequests': False,  # whether or not to use the requests module, if False use the chromium driver Browser
    'findNextPage': False,
    # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    'htmlParser': 'lxml',
    # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    # webdriver parameters
    'webdriverpath': 'dummy',  # path to the webdriver executable SET UP IN CoreScraper.py
    'displayOff': False,  # whether display is switched off or visible
    'displayMinimised': False,  # whether to minimise the window if display is on
    'resetDriver': False,  # whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'waitForPageLoad': 5, #time to wait for page to load
    'webPageWait': 2,  # time between subsequent web pages next page 
    'webPageScrollWait' : 5 , #wait between page scrolls
    'scrollDownPage': True,  # whether to scroll down the page to find more products,
    'pgkey_page_scroll' : True, # scroll down the page using the page down key, slow but effective
    # url and redirect parameters
    'urlSwitch': '',  # add an option to the url,e.g. 48 items per page
    'invalidRedirect': '',  # if the redirected url contains this value the product pages do not load correctly
    # define any buttons to be clicked on screen
    'buttonXpaths': ['//*[@id="panelModalBannerCookies"]/div/a', '//*[@id="saveStore"]/span', '//*[@id="onetrust-accept-btn-handler"]'],  # a list of XPaths
    # 'buttonXpaths' : [ '' ], # a list of XPaths
    'buttoniFrameXpaths': [],
    'proxy_location': '',
    # Give multiple country like this, 'GB|US'. country codes: https://laendercode.net/en/2-letter-list.html
    'popup_remove_button': ['//*[@id="v5_punlClose"]', '//*[@id="onetrust-accept-btn-handler"]'],  # give xpath of popup removal button
    'vpn_location': 'FRWS',

})

# %%  RETAILER SPECIFIC - REDEFINE THE PRODUCTDATAFRAME METHODS
def new_get_description(self):
    info = self.webSoup.find('div', {'class': 'product-info'})
    if info != None:
        d = info.find('div', {'class': 'name'})
        if d != None:
            description = d.get_text()
            self.description = description


def new_get_url(self):
    info = self.webSoup.find('a')
    if info != None:
        u = info.get("href")
        if u != None:
            url = u
            self.url = url


def new_get_skuid(self):
    info = self.webSoup.parent
    if info != None:
        sku = info.get('id')
        color = info.find('div', {'class': 'color-box'})
        if color != None:
            color_code = color.find('input', {'class': 'field'})
            if color_code != None:
                color_code_value = color_code.get('value')
                if color_code_value != None:
                    sku_color = str(sku) + str(color_code_value)
                    self.skuid = sku_color
        else:
            self.skuid = sku


def new_get_imageurl(self):
    i = self.webSoup.find('img')
    if i != None:
        imgurl = i.get('src')
        self.imgurl = imgurl


def new_get_brand(self):
    pass


def new_get_stock(self):
    pass


def new_get_banner(self):
    pass


def new_get_ticket_previous_price(self):
    ticketprice = None
    prevprice = None
    #priceinfo = self.webSoup.find('div', class_="product-price-range-sale")
    priceinfo = self.webSoup.find('div', class_="price-container")

    if priceinfo != None:
        # price_list = re.findall(r"(\d+,\d+) €", str(priceinfo))
        price_list = re.findall(r'[0-9]+,[0-9]+', str(priceinfo))


        if len(price_list) >= 2:
            prevprice = str(price_list[1]) #+ " €"
            ticketprice = str(price_list[0]) #+ " €"
        if len(price_list) == 1:
            ticketprice = str(price_list[0]) #+ " €"
            prevprice = ticketprice

    self.ticketprice = extractPrice(ticketprice, "€")
    self.prevprice = extractPrice(prevprice, "€")


def new_get_reviewcount(self):
    pass


def new_get_reviewscore(self):
    pass


def new_get_promo_message(self):
    pass


def new_find_all_products(self):
    try:
        products = self.webSoup.find_all('div', {'class': "c-tile--product"})
    except:
        products = []

    print('num products', len(products))
    self.products = products


#define the method that is used to find the product description from the individual product page url
def new_get_product_description(self):
    #long description
    text_block = self.webSoup.find('span', { 'class' : 'c-product-info--description-text'})
    if text_block != None:
        t = text_block.get_text()
        self.long_description = t.replace('\n', ' ').replace('\r', ' ').replace("  ", " ")
    #get the colour description
    info = self.webSoup.find('div', {'class' : 'product-card-color-selector--popup-colors'})
    if info != None:
        colour_info = info.find('img')
        if colour_info != None:
            colour = colour_info.get("alt")
            self.colour = colour
    #TODO fetch the size description data = [list of strings]
    self.size = "size  desc"
    self.availability = "availability desc"

#define the method that is used to find the main image url from the individual product page url
def new_get_product_imgage_url(self):
    image_info = self.webSoup.find('div', {'class' : 'grid'})
    if image_info != None:
        image = image_info.find('figure', {'class' : 'figure'})
        if image != None:
            url = image.find('img')
            if url != None:
                url = url.get('src')
                self.imgurl = url


# find the next page link from the current page
def new_find_next_page_from_webpage(self):
    # driver = self.driver # for access to buttons on the page etc
    self.nextPage = None


# redefine the methods specific to this retailer
ProductDataFrame.get_description = new_get_description
ProductDataFrame.get_url = new_get_url
ProductDataFrame.get_skuid = new_get_skuid
ProductDataFrame.get_imageurl = new_get_imageurl
ProductDataFrame.get_banner = new_get_banner
ProductDataFrame.get_brand = new_get_brand
ProductDataFrame.get_stock = new_get_stock
ProductDataFrame.get_reviewscore = new_get_reviewscore
ProductDataFrame.get_reviewcount = new_get_reviewcount
ProductDataFrame.get_ticket_previous_price = new_get_ticket_previous_price
ProductDataFrame.get_product_description = new_get_product_description
ProductDataFrame.get_product_imgage_url = new_get_product_imgage_url
WebPageItems.get_promo_message = new_get_promo_message
WebPageItems.find_all_products = new_find_all_products
WebPageItems.find_next_page_from_webpage = new_find_next_page_from_webpage
# new methods can be added using the same approach, e.g. WebPageItems.newmethod = newmethod


# %%    RETAILER SPECIFIC - IDENTIFY WHICH OPTIONAL METHODS TO RUN

methods_to_run = {
    'output_product_to_screen': False,
    'output_product_values_to_screen': False,
    'output_image_to_file': True,
    'output_description_to_file': True,
    'output_webpage_to_file': False,
}


# %% Main Code


def main():
    filename = __file__
    startPage = 0  # EDIT HERE FOR SET_UP AND TESTING to processs a sub range of weblinks
    endPage = 999  # EDIT HERE
    scrapeWebPages(startPage, endPage, linkurls, params, filename, methods_to_run)
    return


if __name__ == '__main__':
    main()