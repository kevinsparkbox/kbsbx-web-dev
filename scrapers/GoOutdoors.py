import __init__ # set up the import paths
import bs4
import datetime as dt
import pandas as pd
import time
import json
from pathlib import Path
import requests
import html5lib
import os
import re
from math import ceil

from CoreScraper import scrapeWebPages, extractPrice
from CoreScraper_getHtml import WebPageItems, ProductDataFrame
from CoreScraperConstants import toppath,  default_params

#  RETAILER SPECIFIC CONTROL PARAMETERS
# Set up the top path as parent folder of results, Note this is overridden if supplied as an argument when called as a main function
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape/'

# set up multiple urls for finding links, first url is mainurl (code filtters duplicates)
linkurls = ['https://www.gooutdoors.co.uk/']

params = default_params.copy()
params.update({
    'mainurl': 'https://www.gooutdoors.co.uk',
    'opfpath': toppath,  # can be overidden by arguments, applied in in scrapeWebPages
    # define valid links and how to interpret links
    'validtags': ['gooutdoors'],  # a valid link must contain one of these strings
    'invalidtags': ['help', 'service', 'returns', 'sustainability', 'instagram', 'facebook', 'twitter', 'youtube',
                    'legacy', 'cookie',
                    'google', 'review', 'https://www.gooutdoors.co.uk/new:view', '/clothing/new:view', ],
    # if a link contains one of these it is invalid
    'resetduplicatelinks': True,
    # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks': ['https://www.tbc.com'],  # force any duplicate links to be ignored
    'starting_links': [],
    # additional links to process  #set any starting links that will not be picked up by the find starting pages process
    'tagref': 'a',  # look for hrefs in this class
    'remove_switches_link': True,  # if True, remove ? and # switches from link (recommended unless required)
    'departmentlevel': 3,  # the nth level in the link
    'categorylevel': 4,  # the nth level in the link
    'subcategorylevel': 5,  # the nth level in the link
    'minimumdepth': 5,
    # a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    # define how the web page is accessed
    'useRequests': False,  # whether or not to use the requests module, if False use the chromium driver Browser
    'findNextPage': True,
    # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    'htmlParser': 'lxml',
    # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    # webdriver parameters
    'webdriverpath': 'dummy',  # path to the webdriver executable SET UP IN CoreScraper.py
    'displayOff': False,  # whether display is switched off or visible
    'displayMinimised': False,  # whether to minimise the window if display is on
    'resetDriver': False,  # whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'waitForPageLoad': 2,
    # typ set to half webPageWait, when scrolling, time to wait for page to load before comparing 2 subsequent loads to see if scrolled page has finished loading
    'webPageWait': 2,  # time between subsequent web pages (next link, next page, next scroll down page)
    'scrollDownPage': False,  # whether to scroll down the page to find more products, e.g. Topshop
    # url and redirect parameters
    'urlSwitch': '',  # add an option to the url,e.g. 48 items per page
    'invalidRedirect': '',  # if the redirected url contains this value the product pages do not load correctly
    # define any buttons to be clicked on screen
    'buttonXpaths': [''],  # a list of XPaths
    # 'buttonXpaths' : [ '' ], # a list of XPaths
    'buttoniFrameXpaths': [],
    'chrome_updated_version': True,
    'proxy_location': '',  # Give multiple country like this, 'GB|US'. country codes: https://laendercode.net/en/2-letter-list.html,
    'vpn_location': 'GBWS',
})


# %%  RETAILER SPECIFIC - REDEFINE THE PRODUCTDATAFRAME METHODS
def new_get_description(self):
    info = self.webSoup.find('span', {'class': re.compile('product-info-holder')})
    if info != None:
        d = info.find('h2')
        if d != None:
            d = d
            if d != None:
                description = d.get_text().replace('\n', '').replace('\r', '').strip()
                self.description = description


def new_get_url(self):
    info = self.webSoup.find('a')
    if info != None:
        info = info
        if info != None:
            u = info
            if u != None:
                url = u.get('href')
                self.url = self.mainurl + url


def new_get_skuid(self):
    info = self.webSoup
    if info != None:
        u = info
        if u != None:
            data_id = u.get('data-id')
            self.skuid = str(data_id)


def new_get_imageurl(self):
    i = self.webSoup.find('img')
    if i != None:
        imgurl = i.get('src')
        if imgurl != None:
            imgurl = imgurl[: imgurl.find(',')]
        self.imgurl = imgurl


def new_get_brand(self):
    info = self.webSoup.find('span', {'class': re.compile("brand")})
    if info != None:
        brand = info.get_text().replace('\n', '').replace('\r', '').strip()
        self.brand = brand


def new_get_stock(self):
    # not implemented
    self.stock = None


def new_get_banner(self):
    i = self.webSoup.find('b')
    if i != None:
        banner = i.get_text()
        self.banner = banner


def new_get_ticket_previous_price(self):
    ticketprice = 0
    prevprice = 0

    info = self.webSoup.find('div', {'class': "price-tab"})
    if info != None:
        priceinfo = info
        if priceinfo != None:
            prices = priceinfo.get_text().replace('Price', '').replace('Discount', '').replace('Retail',
                                                                                               '').strip().split()
            numbers = []
            for p in prices:
                if any(str.isdigit(c) for c in p):
                    numbers.append(p)

            if len(numbers) > 2:
                prevprice = numbers[0]
                ticketprice = numbers[1]
            else:
                ticketprice = numbers[0]
                prevprice = ticketprice

    self.ticketprice = extractPrice(ticketprice, "£")
    self.prevprice = extractPrice(prevprice, "£")


def new_get_reviewcount(self):
    # not implemented all 5 stars
    info = self.webSoup.find('span', {'class': "product-rating__review-count"})
    if info != None:
        r = info.get_text()
        if r != None:
            r = r.split('(')[1].split('r')[0]
            reviewcount = r.replace('\n', ' ').replace('\r', ' ').replace('(', ' ').replace(')', ' ').strip()
            self.reviewcount = reviewcount


def new_get_reviewscore(self):
    # not implemented all 5 stars
    info = self.webSoup.find('div', {'class': "rating-wrap"})
    if info != None:
        info = str(info)
        reviewpos = info.find('ratings r-') + len('ratings r-')
        reviewscore = info[reviewpos: reviewpos + 2].strip()
        if len(reviewscore) == 1:
            reviewscore = float(reviewscore)
        else:
            reviewscore = float(reviewscore) / 10

        self.reviewscore = reviewscore


def new_get_promo_message(self):
    # not implemented
    info = self.webSoup.find('div', {'id': 'header_espot'})
    if info != None:
        pm = info.find('img')
        if pm != None:
            self.promomessage = pm.get('alt')


def new_find_all_products(self):
    info = self.webSoup
    if info != None:
        products = info.find_all('article', {'class': 'product-item'})
    else:
        products = []
    print('num products', len(products))
    self.products = products


#define the method that is used to find the product description from the individual product page url
def new_get_product_description(self):
    info = self.webSoup.find('div', {'class' : 'content-tab'})
    if info != None:
        t = info.get_text()
        self.long_description = t.replace('\n', ' ').replace('\r', ' ').replace("  ", " ")
        #print('DESCRIPTION ', self.long_description)
    #colour
    #colour for go outdoors not usually / inconsistentlyrecorded in fetures table
    self.colour = "na"
    #TODO fetch the size description data = [list of strings]
    self.size = "size  desc"
    self.availability = "availability desc"

#define the method that is used to find the main image url from the individual product page url
def new_get_product_imgage_url(self):
    image_url = self.webSoup.find('div', {'class' : 'slick-slide slick-current slick-active' })
    if image_url != None:
        imgurl = image_url.find('img')["srcset"]
        #imgurl2 = image_url.find('img')["srcset"]
        if imgurl != None:
            #self.imgurl = 'https:' + imgurl
            self.imgurl =  imgurl

def new_find_next_page_from_webpage(self):
    # driver = self.driver # for access to buttons on the page etc
    # link = self.link
    findNextPage = params['findNextPage']
    nextPage = None
    if findNextPage:
        pagination = self.webSoup.find('nav', {'class': 'pagination'})
        if pagination != None:
            pagination_items = pagination.find('li', {'class': 'next-page'})

            if pagination_items != None:
                link = pagination_items.find('a')
                if link != None:
                    nextPage = 'https://www.gooutdoors.co.uk' + link.get('href')

        print('Next Page', nextPage)

    self.nextPage = nextPage


# redefine the methods specific to this retailer
ProductDataFrame.get_description = new_get_description
ProductDataFrame.get_url = new_get_url
ProductDataFrame.get_skuid = new_get_skuid
ProductDataFrame.get_imageurl = new_get_imageurl
ProductDataFrame.get_banner = new_get_banner
ProductDataFrame.get_brand = new_get_brand
ProductDataFrame.get_stock = new_get_stock
ProductDataFrame.get_reviewscore = new_get_reviewscore
ProductDataFrame.get_reviewcount = new_get_reviewcount
ProductDataFrame.get_ticket_previous_price = new_get_ticket_previous_price
ProductDataFrame.get_product_description = new_get_product_description
ProductDataFrame.get_product_imgage_url = new_get_product_imgage_url
WebPageItems.get_promo_message = new_get_promo_message
WebPageItems.find_all_products = new_find_all_products
WebPageItems.find_next_page_from_webpage = new_find_next_page_from_webpage
# new methods can be added using the same approach, e.g. WebPageItems.newmethod = newmethod


# %%    RETAILER SPECIFIC - IDENTIFY WHICH OPTIONAL METHODS TO RUN

methods_to_run = {
    'output_product_to_screen': False,
    'output_product_values_to_screen': False,
    'output_image_to_file': True,
    'output_description_to_file': True,
    'output_webpage_to_file': False,
}


# %% Main Code


def main():
    filename = __file__
    startPage = 0  # EDIT HERE FOR SET_UP AND TESTING to processs a sub range of weblinks
    endPage = 999  # EDIT HERE
    scrapeWebPages(startPage, endPage, linkurls, params, filename, methods_to_run)
    return


if __name__ == '__main__':
    main()
