
import __init__ # set up the import paths
import bs4
import datetime as dt
import pandas as pd
import time
import json
from pathlib import Path
import requests
import html5lib
import os
import re
from math import ceil

from CoreScraper import scrapeWebPages, extractPrice
from CoreScraper_getHtml import WebPageItems, ProductDataFrame
from CoreScraperConstants import toppath,  default_params

#  RETAILER SPECIFIC CONTROL PARAMETERS
# Set up the top path as parent folder of results, Note this is overridden if supplied as an argument when called as a main function
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape/'

#set up multiple urls for finding links, first url is mainurl (code filtters duplicates)
linkurls = [ 'https://www2.hm.com/en_gb/ladies.html', 'https://www2.hm.com']

params = default_params.copy()
params.update( {
    'mainurl' : linkurls[1],
    'opfpath' : toppath, # can be overidden by arguments, applied in in scrapeWebPages
    #define valid links and how to interpret links
    'validtags' : ["ladies", "men", "divided", "kids"],  # a valid link must contain one of these strings
    'invalidtags' : ["view-all", "new-arrivals", "shop-by-occasion", "shop-by-feature", "shop-by-concept" ], # if a link contains one of these it is invalid
    'resetduplicatelinks' : True, # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks' : ['https://www.tbc.com'], # force any duplicate links to be ignored
    'starting_links' : [],    #additional links to process  #set any starting links that will not be picked up by the find starting pages process
    'tagref' : 'a', # look for hrefs in this class
    'remove_switches_link' : True,  # if True, remove ? and # switches from link (recommended unless required)
    'departmentlevel' : 4, #the nth level in the link
    'categorylevel' : 5,  #the nth level in the link
    'subcategorylevel' : 6,  #the nth level in the link
    'minimumdepth' : 6 ,  #a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    #define how the web page is accessed
    'useRequests' : False, # whether or not to use the requests module, if False use the chromium driver Browser
    'findNextPage' : True, # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    'htmlParser' :  'lxml', # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    #webdriver parameters
    'webdriverpath' : 'dummy' , # path to the webdriver executable SET UP IN CoreScraper.py
    'displayOff' : False,  # whether display is switched off or visible
    'displayMinimised' :  False,  # whether to minimise the window if display is on
    'resetDriver' : False,  #whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'resetCookies' : False, #whether or not to reset the cookies on every page
    'waitForPageLoad' : 4, #typ set to half webPageWait, when scrolling, time to wait for page to load before comparing 2 subsequent loads to see if scrolled page has finished loading
    'webPageWait' : 4,  # time between subsequent web pages (next link, next page, next scroll down page)
    'scrollDownPage' : False,  # whether to scroll down the page to find more products, e.g. Topshop
    'short_webPageWait': 1,  # define a shorter time to wait for when loading individal product pages
    # url and redirect parameters
    'urlSwitch' :  '',    #sets home country to uk
    'invalidRedirect' : '', # if the redirected url contains this value the product pages do not load correctly
    #define any buttons to be clicked on screen
    'buttonXpaths' : [ '/html/body/div[1]/button' , '//*[@id="gdpr-modal"]/div[2]/button' ,
                '//*[@id="onetrust-accept-btn-handler"]'], # a list of XPaths
    'proxy_location': '',  # Give multiple country like this, 'GB|US'. country codes: https://laendercode.net/en/2-letter-list.html
    'vpn_location': 'GBWS',
    })
            
#%%  RETAILER SPECIFIC - REDEFINE THE PRODUCTDATAFRAME METHODS
def new_get_description(self):
    info = self.webSoup.find('a', class_="item-link")
    if info != None : 
        d = info.get("title")
        if d != None: 
            description = d.replace('\n', '').replace('\r', '').strip()
            self.description = description        
          
def new_get_url(self):
    info = self.webSoup.find('a', class_="item-link")
    if info != None :
        url = self.mainurl + info.get('href')
        self.url = url
        
def new_get_skuid(self):
    info = self.webSoup.find('article')
    if info != None : 
        skuid = info.get("data-articlecode")
        self.skuid = skuid
        
def new_get_imageurl(self):
    i = self.webSoup.find('img')
    if i != None:  
        #case 1
        imgurl =  i.get('src')
        #case 2
        if imgurl == None: imgurl =  i.get('data-src')
        self.imgurl = imgurl
        
def new_get_banner(self):
    info = self.webSoup.find('div', {'class' : 'new-product'})
    if info != None : 
        b = info
        if b != None: 
            banner = b.get_text()
            self.banner = banner
            
def new_get_ticket_previous_price(self):
    ticketprice = None
    prevprice = None
    itemprice = self.webSoup.find('strong', class_="item-price")
    if itemprice != None:
        prevprice = itemprice.find('span', class_="price regular")
        if prevprice != None:
            prevprice = prevprice.get_text()
        else:
            prevprice = 0
        ticketprice = itemprice.find('span', class_="price sale")
        if ticketprice != None:
            ticketprice = ticketprice.get_text()
        else:
            ticketprice = prevprice
    else:
        prevprice = 0
        ticketprice = prevprice   
    self.ticketprice = extractPrice(ticketprice, "£")
    self.prevprice = extractPrice(prevprice, "£")
        
def new_get_promo_message(self):
    self.promomessage = None
            
def new_find_all_products(self):
    products = self.webSoup.findAll('li',{ "class" : "product-item"} )
    print('num products', len(products))
    self.products = products
                                            

#find the next page link from the current page
def alt_find_next_page_from_webpage(self):
    #H&M - show first page, get number of items per page and total number of items
    #then next page is to show all items - some pages require rounded up to complete pages
    #driver = self.driver # for access to buttons on the page etc
    link = self.link
    findNextPage = params['findNextPage']
    mainurl = params['mainurl']
    webSoup = self.webSoup
    nextPage = None    
    info = webSoup.find('div', class_="load-more-products") 
    if info != None:
        dataitems = info.find('h2', class_="load-more-heading")
        totalitems = int(dataitems.get('data-total'))
        dataitemsshown = int(dataitems.get('data-items-shown'))
        dataitems = info.find('button')
        dataperpage = int(dataitems.get('data-per-page'))
        numberPages = int(ceil(totalitems / dataperpage))
        roundedtotalitems = numberPages * dataperpage
        urlparams = '?sort=stock&image-size=small&image=model&offset=0&page-size='

        #find current page number if already set, if already set then return as all items have been read in
        pagePos = link['webpage'].find('&page-size=')
        if pagePos > -1:
            nextPage = None
        else:
            nextPage = link['webpage'] + urlparams + str(roundedtotalitems)
    else:
        nextPage = None
        
    print('Next Page' , nextPage)
    self.nextPage = nextPage  
     
#define the method that is used to find the product description from the individual product page url
def new_get_product_description(self):
    info = self.webSoup.find('p', {'class' : 'pdp-description-text'})
    if info != None:
        t = info.get_text()
        self.long_description = t.replace('\n', ' ').replace('\r', ' ').replace("  ", " ")
    #colour
    info = self.webSoup.find('h3', {'class' : 'product-input-label'})
    if info != None:
        self.colour = info.get_text()
    #TODO fetch the size description data = [list of strings]
    self.size = "size  desc"
    self.availability = "availability desc"

#define the method that is used to find the main image url from the individual product page url
def new_get_product_imgage_url(self):
    image_url = self.webSoup.find('div', {'class' : 'product-detail-main-image-container'})
    if image_url != None:
        imgurl = image_url.find('img')["src"]
        #url2 = image_url.find('img')["srcset"]
        if imgurl != None:
            imgurl = 'http:' + imgurl
            self.imgurl =  imgurl


#redefine the methods specific to this retailer        
ProductDataFrame.get_description =  new_get_description
ProductDataFrame.get_url = new_get_url
ProductDataFrame.get_skuid = new_get_skuid
ProductDataFrame.get_imageurl = new_get_imageurl
ProductDataFrame.get_banner = new_get_banner
ProductDataFrame.get_ticket_previous_price = new_get_ticket_previous_price
ProductDataFrame.get_product_description = new_get_product_description
ProductDataFrame.get_product_imgage_url = new_get_product_imgage_url
#WebPageItems.get_promo_message = new_get_promo_message
WebPageItems.find_all_products = new_find_all_products
WebPageItems.find_next_page_from_webpage = alt_find_next_page_from_webpage
#new methods can be added using the same approach, e.g. WebPageItems.newmethod = newmethod


#%%    RETAILER SPECIFIC - IDENTIFY WHICH OPTIONAL METHODS TO RUN 

methods_to_run = {
    'output_product_to_screen' : False,
    'output_product_values_to_screen' : False,
    'output_image_to_file': True, # set True to output product images to GS
    'output_description_to_file' : True, # set True to output product descriptions to BQ
    'output_webpage_to_file' : False,
        }
     
            
# %% Main Code

    
def main():
    filename = __file__
    startPage = 0 # 13 to 14 is a good test set = Dresses
    endPage = 999 # EDIT HERE
    scrapeWebPages( startPage, endPage, linkurls, params, filename, methods_to_run)
    return
    
 
if __name__ == '__main__':
    main()