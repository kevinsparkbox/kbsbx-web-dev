import __init__ # set up the import paths
import bs4
import datetime as dt
import pandas as pd
import time
import json
from pathlib import Path
import requests
import html5lib
import os
import re
from math import ceil

from CoreScraper import scrapeWebPages, extractPrice
from CoreScraper_getHtml import WebPageItems, ProductDataFrame
from CoreScraperConstants import toppath,  default_params

#  RETAILER SPECIFIC CONTROL PARAMETERS
# Set up the top path as parent folder of results, Note this is overridden if supplied as an argument when called as a main function
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape/'
    

# set up multiple urls for finding links, first url is mainurl (code filtters duplicates)
linkurls = ['https://www.stradivarius.com/fr']

params = default_params.copy()
params.update({
    'mainurl': linkurls[0],
    'opfpath': toppath,  # can be overidden by arguments, applied in in scrapeWebPages
    # define valid links and how to interpret links
    'validtags': ['stradivarius'],  # a valid link must contain one of these strings
    'invalidtags': ['javascript', 'inditex', 'facebook', 'instagram', 'youtube', 'pinterest', 'twitter'],
    # if a link contains one of these it is invalid
    'resetduplicatelinks': True,
    # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks': ['https://www.tbc.com'],  # force any duplicate links to be ignored
    'sitemap_links' : [ 'https://www.stradivarius.com/fr/nouvelle-collection/v%C3%AAtements/voir-par-produit/tops-et-bottoms-c1020206052.html'],  #if empty search through the linkurls, if defined use the sitemap links - set to a dummy page redirects to list of sitemap links

    # control processing of links
    'tagref': 'a',  # look for hrefs in this class
    'remove_switches_link': False,  # if True, remove ? switches from link (e.g. Topshop )
    'departmentlevel': 4,  # the nth level in the link
    'categorylevel': 5,  # the nth level in the link
    'subcategorylevel': 6,  # the nth level in the link
    'minimumdepth': 5,
    # a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    # define how the web page is accessed
    'useRequests': False,  # whether or not to use the requests module, if False use the chromium driver Browser
    'findNextPage': False,
    # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    'htmlParser': 'lxml',
    # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    # webdriver parameters
    'webdriverpath': 'dummy',  # path to the webdriver executable SET UP IN CoreScraper.py
    'displayOff': False,  # whether display is switched off or visible
    'displayMinimised': False,  # whether to minimise the window if display is on
    'resetDriver': False,  # whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'waitForPageLoad': 4,# wait between pages
    'webPageScrollWait' : 4 , #wait between page scrolls
    'webPageWait': 4,  # time between subsequent web pages (next link, next page, next scroll down page)
    'short_webPageWait' : 2, #time to wait for product page to load
    'scrollDownPage': True,  # whether to scroll down the page to find more products, e.g. Topshop
    # url and redirect parameters
    'urlSwitch': '',  # add an option to the url,e.g. 48 items per page
    'invalidRedirect': '',  # if the redirected url contains this value the product pages do not load correctly
    # define any buttons to be clicked on screen
    'buttonXpaths': ['//*[@id="cookiesWarn"]/a/svg', '//*[@id="cookiesWarn"]', '//*[@id="cookiesWarn"]/a',
                     '//*[@id="cookiesWarn"]/a/svg/path', '//*[@id="sidebar-component"]/div/div[1]/div[1]/div[1]/svg/path',
                     '//*[@id="cookiesWarn"]/a/svg', '//*[@id="onetrust-accept-btn-handler"]', '//*[@id="onetrust-consent-sdk"]/div[1]'],  # a list of XPaths
    'buttoniFrameXpaths': [],
    'comparehtml': 'direct',
    'proxy_location': '',
    # Give multiple country like this, 'GB|US'. country codes: https://laendercode.net/en/2-letter-list.html
    'popup_remove_button': ['//*[@id="cookiesWarn"]/a/svg', '//*[@id="sidebar-component"]/div/div[1]/div[1]/div[1]/svg/path', 
                            '//*[@id="cookiesWarn"]/a/svg', '//*[@id="onetrust-accept-btn-handler"]', 
                            '//*[@id="modal-component"]/div/div/div[2]/div/button[1]/div'] , # give xpath of popup removal button
    'vpn_location': 'FRWS',
})


# %%  RETAILER SPECIFIC - REDEFINE THE PRODUCTDATAFRAME METHODS
def new_get_description(self):
    info = self.webSoup.find('div', {'class': re.compile('^title-product')})
    if info != None:
        d = info
        if d != None:
            description = d.text.replace('\n', '').replace('\r', '').strip()
            self.description = description


def new_get_url(self):
    info = self.webSoup.find('a')
    if info != None:
        u = info
        if u != None:
            url = u.get('href')
            self.url = url


# def new_get_skuid_url(self):
# there is an alpha numeric skuid but limited characters ? reused ?  so extract the numeric id from the url
# if self.url != None:
#    skuid = self.url.split('-')[-1]
#    self.skuid = skuid

def new_get_skuid_data(self):
    if self.url != None:
        colorid = str(self.url.split('=')[-1])
    else:
        colorid = "nocolour"
    info = self.webSoup.get("id")
    if info != None:
        skuid = str(info.split("_")[1]) + colorid
        self.skuid = skuid


def new_get_reviewscore(self):
    info = self.webSoup.find('div', {'class': 'mod-product-info'})
    if info != None:
        r = info.find('a', {'class': 'mod-product-link feefoProduct'})
        if r != None:
            reviewscore = r.get('feefo-score')
            self.reviewscore = reviewscore


def new_get_reviewcount(self):
    info = self.webSoup.find('div', {'class': 'mod-product-info'})
    if info != None:
        r = info.find('span', {'class': 'nbReviews'})
        if r != None:
            reviewcount = r.get_text().replace('\n', ' ').replace('\r', ' ').replace('(', ' ').replace(')', ' ').strip()
            self.reviewcount = reviewcount


def new_get_imageurl(self):
    i = self.webSoup.find('img')
    if i != None:
        imgurl = i.get('src')
        self.imgurl = imgurl


def new_get_banner(self):
    b = self.webSoup.find('div', {'class': 'dvtSticker--bottom pCategory desktop'})
    # case 2
    if b == None:
        b = self.webSoup.find('div', {'class': 'dvtSticker--r-bottom pCategory desktop'})
    # case 3
    if b == None:
        b = self.webSoup.find('div', {'class': 'dvtSticker--over pCategory desktop'})
    if b != None:
        banner = b.get_text().replace('\n', ' ').replace('\r', ' ').strip()
        self.banner = banner


def new_get_brand(self):
    b = self.webSoup.find('div', {'class': 'dvtSticker--under pCategory desktop'})
    if b != None:
        brand = b.get_text().replace('\n', ' ').replace('\r', ' ').strip()
        self.brand = brand


def new_get_ticket_previous_price(self):
    # TODO also a class mod-product-promo
    ticketprice = None
    prevprice = None
    info = self.webSoup.find('div', {'class': 'current-price'})
    if info != None:
        ticketprice = info.find('span').get_text()
        prevprice = ticketprice

    itemprice = self.webSoup.find('div', {'class': 'one-old-price'})
    if itemprice != None:
        prevprice = itemprice.find('span').get_text()

    self.ticketprice = extractPrice(ticketprice, "€")
    self.prevprice = extractPrice(prevprice, "€")


def new_get_promo_message(self):
    info = self.webSoup.find('div', {'id': 'header_espot'})
    if info != None:
        pm = info.find('img')
        if pm != None:
            self.promomessage = pm.get('alt')


def new_find_all_products(self):
    products = self.webSoup.find_all('div', {'id': re.compile("^ProductGridItem_")})
    print('num products', len(products))
    self.products = products


#define the method that is used to find the product description from the individual product page url
def new_get_product_description(self):
    #product title - fetch the title as a reasonable number of products do not have a description
    title = self.webSoup.find('h1', { 'class' : 'product-name-title'})
    if title != None:
        title_text = title.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ")
    else:
        title_text = None
    #long description
    text_block = self.webSoup.find('div', { 'class' : 'product-description'})
    if text_block != None:
        t = text_block.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ")
    else:
        t = None
    if title_text != None and t != None :
        self.long_description = title_text + ' : ' + t
    elif title_text != None:
        self.long_description = title_text 
    elif t != None:
        self.long_description =   t
    #get the colour description
    info = self.webSoup.find('span', {'class' : 'product-color-title-label'})
    if info != None:
        colour_info = info.get_text()
        if colour_info != None:
            colour = colour_info.split(':')[1].strip()
            self.colour = colour
    #print('WEBSOUP')
    #print(self.webSoup.prettify())
    #TODO fetch the size description data = [list of strings]
    self.size = "size  desc"
    self.availability = "availability desc"

#define the method that is used to find the main image url from the individual product page url
def new_get_product_imgage_url(self):
    image_info = self.webSoup.find('div', {'class' : 'image-container'})
    if image_info != None:
        image = image_info
        if image != None:
            url = image.find('img')
            if url != None:
                url = url.get("src")
        #url2 = image_url.find('img')["srcset"]
        #if url != None:
            #url = 'https:' + url
        self.imgurl = url


def new_find_next_page_from_webpage(self):
    # driver = self.driver # for access to buttons on the page etc
    # for Stradivarius all on one page
    link = self.link
    findNextPage = params['findNextPage']
    mainurl = params['mainurl']
    webSoup = self.webSoup
    nextPage = None

    self.nextPage = nextPage


# redefine the methods specific to this retailer
ProductDataFrame.get_description = new_get_description
ProductDataFrame.get_url = new_get_url
ProductDataFrame.get_skuid = new_get_skuid_data
ProductDataFrame.get_imageurl = new_get_imageurl
ProductDataFrame.get_banner = new_get_banner
ProductDataFrame.get_ticket_previous_price = new_get_ticket_previous_price
ProductDataFrame.get_brand = new_get_brand
ProductDataFrame.get_reviewscore = new_get_reviewscore
ProductDataFrame.get_reviewcount = new_get_reviewcount
ProductDataFrame.get_product_description = new_get_product_description
ProductDataFrame.get_product_imgage_url = new_get_product_imgage_url
WebPageItems.get_promo_message = new_get_promo_message
WebPageItems.find_all_products = new_find_all_products
WebPageItems.find_next_page_from_webpage = new_find_next_page_from_webpage
# new methods can be added using the same approach, e.g. WebPageItems.newmethod = newmethod


# %%    RETAILER SPECIFIC - IDENTIFY WHICH OPTIONAL METHODS TO RUN FOR EACH PRODUCT

methods_to_run = {
    'output_product_to_screen': False,
    'output_image_to_file': True,
    'output_description_to_file': True,
    'output_product_values_to_screen': False,
    'output_webpage_to_file': False,
}


# %% Main Code

def main():
    filename = __file__
    startPage = 0  # EDIT HERE FOR SET_UP AND TESTING to processs a sub range of weblinks
    endPage = 999  # EDIT HERE
    scrapeWebPages(startPage, endPage, linkurls, params, filename, methods_to_run)
    return


if __name__ == '__main__':
    main()
