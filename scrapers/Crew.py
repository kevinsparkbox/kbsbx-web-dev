import __init__ # set up the import paths
import bs4
import datetime as dt
import pandas as pd
import time
import json
from pathlib import Path
import requests
import html5lib
import os
import re
from math import ceil

from CoreScraper import scrapeWebPages, extractPrice
from CoreScraper_getHtml import WebPageItems, ProductDataFrame
from CoreScraperConstants import toppath,  default_params

#  RETAILER SPECIFIC CONTROL PARAMETERS
# Set up the top path as parent folder of results, Note this is overridden if supplied as an argument when called as a main function
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape/'
    

# set up multiple urls for finding links, first url is mainurl (code filtters duplicates)
linkurls = ['https://www.crewclothing.co.uk']

params = default_params.copy()
params.update({
    'mainurl': linkurls[0],
    'opfpath': toppath,
    # can be overidden by arguments, applied in in scrapeWebPages    #define valid links and how to interpret links
    # 'validtags' : ['http://www.topshop.com/en/tsuk/category'],  # a valid link must contain one of these strings
    'validtags': ['crew'],  # a valid link must contain one of these strings
    'invalidtags': ['javascript', 'about', 'customer', ],  # if a link contains one of these it is invalid
    'resetduplicatelinks': True,
    # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks': ['https://www.tbc.com'],  # force any duplicate links to be ignored
    'starting_links': [],
    # additional links to process  #set any starting links that will not be picked up by the find starting pages process
    'tagref': 'a',  # look for hrefs in this class
    'remove_switches_link': False,  # if True, remove ? and # switches from link (recommended unless required)
    'departmentlevel': 3,  # the nth level in the link
    'categorylevel': 4,  # the nth level in the link
    'subcategorylevel': 5,  # the nth level in the link
    'minimumdepth': 5,
    # a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    # define how the web page is accessed
    'useRequests': False,  # whether or not to use the requests module, if False use the chromium driver Browser
    'findNextPage': False,
    # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    'htmlParser': 'lxml',
    # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    # webdriver parameters
    'webdriverpath': 'dummy',  # path to the webdriver executable SET UP IN CoreScraper.py
    'displayOff': False,  # whether display is switched off or visible
    'displayMinimised': False,  # whether to minimise the window if display is on
    'resetDriver': False,  # whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'waitForPageLoad': 1,
    'webPageScrollWait' : 1 , #wait between page scrolls
    'webPageWait': 1,  # time between subsequent web pages
    'scrollDownPage': True,  # whether to scroll down the page to find more products, e.g. Topshop
    # url and redirect parameters
    'urlSwitch': '',  # add an option to the url,e.g. 48 items per page
    'invalidRedirect': '',  # if the redirected url contains this value the product pages do not load correctly
    # define any buttons to be clicked on screen
    'buttonXpaths': ['//*[@id="mainCookieBanner"]/a[2]', '//*[@id="icon-close"]/path'],  # a list of XPaths
    'buttoniFrameXpaths': [],
    'numscreenshots': 1,  # number of screnshots to save
    'max_link_depth': 1,
    'proxy_location' : '',
    'vpn_location': 'GBWS',
    # Give multiple country like this, 'GB|US'. country codes: https://laendercode.net/en/2-letter-list.html
    'popup_remove_button': ['//*[@id="ajaxNewsletter"]/div[1]'],  # give xpath of popup removal button
    #'pgkey_page_scroll' : True,
})


# %%  RETAILER SPECIFIC - REDEFINE THE PRODUCTDATAFRAME METHODS
def new_get_description(self):
    info = self.webSoup.find('a', {'data-listing': 'name'})
    if info != None:
        d = info.get_text()
        if d != None:
            description = d.replace('\n', '').replace('\r', '').strip()
            self.description = description


def new_get_url(self):
    info = self.webSoup.find('a', {'data-listing': 'name'})
    if info != None:
        u = info
        if u != None:
            url = u.get('href')
            self.url = url


# def new_get_skuid_url(self):
# there is an alpha numeric skuid but limited characters ? reused ?  so extract the numeric id from the url
# if self.url != None:
#    skuid = self.url.split('-')[-1]
#    self.skuid = skuid

def new_get_skuid_data(self):
    info = self.webSoup.find("span", {'data-listing': 'swatches'})
    if info != None:
        skuid = info.get("data-pfid")
        self.skuid = skuid


def new_get_reviewscore(self):
    info = self.webSoup.find('div', {'class': 'mod-product-info'})
    if info != None:
        r = info.find('a', {'class': 'mod-product-link feefoProduct'})
        if r != None:
            reviewscore = r.get('feefo-score')
            self.reviewscore = reviewscore


def new_get_reviewcount(self):
    info = self.webSoup.find('div', {'class': 'mod-product-info'})
    if info != None:
        r = info.find('span', {'class': 'nbReviews'})
        if r != None:
            reviewcount = r.get_text().replace('\n', ' ').replace('\r', ' ').replace('(', ' ').replace(')', ' ').strip()
            self.reviewcount = reviewcount


def new_get_imageurl(self):
    i = self.webSoup.find_all('img')
    if i != None:
        i = i[-1]
        imgurl = i.get('src')
        self.imgurl = self.mainurl + imgurl


def new_get_banner(self):
    info = self.webSoup.find('div', {'data-listing': 'promoB'})
    if info != None:
        d = info.find('img')
        if d != None:
            banner = d.get("src")
            self.banner = banner[banner.rfind('/'):]


def new_get_brand(self):
    b = self.webSoup.find('div', {'class': 'dvtSticker--under pCategory desktop'})
    if b != None:
        brand = b.get_text().replace('\n', ' ').replace('\r', ' ').strip()
        self.brand = brand


def new_get_ticket_previous_price(self):
    ticketprice = None
    prevprice = None
    info = self.webSoup.find('span', {'data-listing': 'price'})
    if info != None:
        ticketprice = info.find('span', {'class': 'linethrough'})
        if ticketprice != None:
            ticketprice = ticketprice.get_text().strip()
            prevprice = info.find('span', {'class': 'f-sale'}).get_text().strip()
        else:
            ticketprice = info.get_text().strip()
            prevprice = ticketprice
    self.ticketprice = extractPrice(ticketprice, "£")
    self.prevprice = extractPrice(prevprice, "£")


def new_get_promo_message(self):
    info = self.webSoup.find('div', {'id': 'header_espot'})
    if info != None:
        pm = info.find('img')
        if pm != None:
            self.promomessage = pm.get('alt')


def new_find_all_products(self):
    products = self.webSoup.find_all('div', {'id': re.compile('ui-product*')})
    print('num products', len(products))
    self.products = products


#define the method that is used to find the product description from the individual product page url
def new_get_product_description(self):
    pi_t = ""
    product_info_text_block = self.webSoup.find('div', { 'class' : 'stick-stage2'})
    if product_info_text_block != None:
        #print('product info', product_info_text_block.prettify())
        block1 = product_info_text_block.find('div' , {'class' : 'accordion-body'})
        if block1 != None:
            pi_t = block1.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
    self.long_description =   pi_t 
    #get the colour description
    info = self.webSoup.find('span', {'data-bind' : 'text:option1value'})
    if info != None:
        colour_info = info
        if colour_info != None:
            colour = colour_info.get_text()
            self.colour = colour.replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()

    #TODO fetch the size description data = [list of strings]
    self.size = "size  desc"
    self.availability = "availability desc"

#define the method that is used to find the main image url from the individual product page url
def new_get_product_imgage_url(self):
    image_info = self.webSoup.find('div', {'class' : 'img-pan-wrapper'})
    if image_info != None:
        images = image_info.find('img')
        if images != None:
            url = images.get("src")
            self.imgurl = self.mainurl + url 
        #url2 = image_url.find('img')["srcset"]
        #if url != None:
            #url = 'https:' + url
        #self.imgurl = url


# find the next page link from the current page
def new_find_next_page_from_webpage(self):
    # driver = self.driver # for access to buttons on the page etc
    # for crew add page=N to the end of the url
    link = self.link
    findNextPage = params['findNextPage']
    mainurl = params['mainurl']
    webSoup = self.webSoup
    nextPage = None
    maxPages = 1000  # just in case something goes wrong!!
    dataperpage = 1
    if findNextPage:
        # find current page number if already set
        pagePos = link['webpage'].find('page=')
        if pagePos == -1:
            # page no currently not set
            nextPage = link['webpage'] + '?page=1'
        else:
            # increment the page no
            currPageNo = link['webpage'][pagePos:].split("=")[1]
            # remove any #tags added or ending /
            currPageNo = currPageNo.split('/')[0]
            currPageNo = currPageNo.split("#")[0]
            nextPageNo = int(currPageNo) + dataperpage
            if nextPageNo <= maxPages:
                # then update to next page
                b = link['webpage'].find('page=')
                nextPage = link['webpage'][:b] + 'page=' + str(nextPageNo)
            else:
                # finish
                nextPage = None

    # if there are no products on the page, have moved past the last page so stop the next page process
    numProducts = len(self.webSoup.findAll('span', {"class": "listing relative"}))
    if numProducts == 0:
        print('No Products on Page')
        nextPage = None

    print('Next Page', nextPage)
    self.nextPage = nextPage


# redefine the methods specific to this retailer
ProductDataFrame.get_description = new_get_description
ProductDataFrame.get_url = new_get_url
ProductDataFrame.get_skuid = new_get_skuid_data
ProductDataFrame.get_imageurl = new_get_imageurl
ProductDataFrame.get_banner = new_get_banner
ProductDataFrame.get_ticket_previous_price = new_get_ticket_previous_price
ProductDataFrame.get_brand = new_get_brand
ProductDataFrame.get_reviewscore = new_get_reviewscore
ProductDataFrame.get_reviewcount = new_get_reviewcount
ProductDataFrame.get_product_description = new_get_product_description
ProductDataFrame.get_product_imgage_url = new_get_product_imgage_url
WebPageItems.get_promo_message = new_get_promo_message
WebPageItems.find_all_products = new_find_all_products
WebPageItems.find_next_page_from_webpage = new_find_next_page_from_webpage
# new methods can be added using the same approach, e.g. WebPageItems.newmethod = newmethod


# %%    RETAILER SPECIFIC - IDENTIFY WHICH OPTIONAL METHODS TO RUN FOR EACH PRODUCT

methods_to_run = {
    'output_product_to_screen': False,
    'output_image_to_file': True,
    'output_description_to_file': True,
    'output_product_values_to_screen': False,
    'output_webpage_to_file': False,
}


# %% Main Code

def main():
    filename = __file__
    startPage = 0  # EDIT HERE FOR SET_UP AND TESTING to processs a sub range of weblinks
    endPage = 999  # EDIT HERE
    scrapeWebPages(startPage, endPage, linkurls, params, filename, methods_to_run)
    return


if __name__ == '__main__':
    main()
