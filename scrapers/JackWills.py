import __init__ # set up the import paths
import bs4
import datetime as dt
import pandas as pd
import time
import json
from pathlib import Path
import requests
import html5lib
import os
import re
from math import ceil

from CoreScraper import scrapeWebPages, extractPrice
from CoreScraper_getHtml import WebPageItems, ProductDataFrame
from CoreScraperConstants import toppath,  default_params

#  RETAILER SPECIFIC CONTROL PARAMETERS
# Set up the top path as parent folder of results, Note this is overridden if supplied as an argument when called as a main function
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape/'

#set up multiple urls for finding links, first url is mainurl (code filtters duplicates)
linkurls = ['https://www.jackwills.com']


params = default_params.copy()
params.update({
    'mainurl' : linkurls[0],
    'opfpath' : toppath, # can be overidden by arguments, applied in in scrapeWebPages
    #define valid links and how to interpret links
    'validtags' : ['ladies', 'men', 'women', 'sale', 'offers'],  # a valid link must contain one of these strings
    'invalidtags' : ["javascript"], # if a link contains one of these it is invalid
    'resetduplicatelinks' : True, # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks' : ['https://www.tbc.com'], # force any duplicate links to be ignored
    'starting_links' : [],    #additional links to process  #set any starting links that will not be picked up by the find starting pages process
    'tagref' : 'a', # look for hrefs in this class
    'remove_switches_link' : True,  # if True, remove ? and # switches from link (recommended unless required)
    'departmentlevel' : 3, #the nth level in the link
    'categorylevel' : 4,  #the nth level in the link
    'subcategorylevel' : 5,  #the nth level in the link
    'minimumdepth' : 4 ,  #a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    'max_link_depth' : 2,# search for links from each page
    'findNextPage' : True, # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    'htmlParser' :  'lxml', # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    #webdriver parameters
    'displayOff' : False,  # whether display is switched off or visible
    'displayMinimised' :  False,  # whether to minimise the window if display is on
    'resetDriver' : False,  #whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'waitForPageLoad' : 3, #typ set to half webPageWait, when scrolling, time to wait for page to load before comparing 2 subsequent loads to see if scrolled page has finished loading
    'webPageWait' : 3,  # time between subsequent web pages (next link, next page, next scroll down page)
    'scrollDownPage' : False,  # whether to scroll down the page to find more products, e.g. Topshop
    # url and redirect parameters
    'urlSwitch' :  '',    #add an option to the url,e.g. 48 items per page
    'invalidRedirect' : '', # if the redirected url contains this value the product pages do not load correctly
    #define any buttons to be clicked on screen
    'buttonXpaths' : ['//*[@id="dm_closer"]', '//*[@id="inputAcceptCookies"]' ], # a list of XPaths
    'buttoniFrameXpaths' : [],
    'proxy_location': '',  # Give multiple country like this, 'GB|US'. country codes: https://laendercode.net/en/2-letter-list.html
    'show_proxy_info': False,
    'vpn_location': 'GBWS',
    'luminati_proxy': '',
    })
            
#%%  RETAILER SPECIFIC - REDEFINE THE PRODUCTDATAFRAME METHODS
def new_get_description(self):
    info = self.webSoup.find('span', {'class' : 'productdescriptionname'})
    if info != None : 
        d = info
        if d != None: 
            description = d.get_text()
            self.description = description        
            
def new_get_url(self):
    info = self.webSoup.find('div', {'class' : 's-productthumbtext'})
    if info != None : 
        u = info.find('a')
        if u != None :
            url = self.mainurl + u.get('href')
            self.url = url

#def new_get_skuid_url(self):
    #there is an alpha numeric skuid but limited characters ? reused ?  so extract the numeric id from the url
    #if self.url != None:
    #    skuid = self.url.split('-')[-1]
    #    self.skuid = skuid
        
def new_get_skuid_data(self):
    info = self.webSoup
    if info != None : 
        skuid = info.get("li-productid")
        self.skuid = skuid
        
def new_get_reviewscore(self):
    # info = self.webSoup.find('div', {'class' : 'mod-product-info'})
    # if info != None : 
    #     r = info.find('a', {'class' : 'mod-product-link feefoProduct'} )
    #     if r != None :
    #         reviewscore = r.get('feefo-score')
    #         self.reviewscore = reviewscore
    self.reviewscore = None

def new_get_reviewcount(self):
    # info = self.webSoup.find('div', {'class' : 'mod-product-info'})
    # if info != None : 
    #     r = info.find('span', {'class' : 'nbReviews'} )
    #     if r != None :
    #         reviewcount = r.get_text().replace('\n', ' ').replace('\r', ' ').replace('(', ' ').replace(')', ' ').strip()
    self.reviewcount = None
        
def new_get_imageurl(self):
    i = self.webSoup.find('img')
    if i != None:  
        imgurl =  i.get('src')
        self.imgurl = imgurl
      
def new_get_banner(self):
    # b = self.webSoup.find('div', class_="product-promo")   
    # if b != None : 
    #     banner =  b.get_text().replace('\n', ' ').replace('\r', ' ').strip()
    #     self.banner = banner
    self.banner = None
        
def new_get_brand(self):
    # b = self.webSoup.find('div', {'class' : 'dvtSticker--under pCategory desktop'})
    # if b != None : 
    #     brand = b.get_text().replace('\n', ' ').replace('\r', ' ').strip()
    info = self.webSoup
    if info != None : 
        brand = info.get("li-brand")
        self.brand = brand
            
def new_get_ticket_previous_price(self):

    info = self.webSoup
    if info != None : 
        ticketprice = info.get("li-price")
    else:
        ticketprice = None

    prevprice = info.find('div', {'class' : 'RefandPrice'})
    if prevprice != None:
        prevprice = prevprice.find('span').get_text()
    else:
        prevprice = ticketprice

    
    self.ticketprice = extractPrice(ticketprice, "£")
    self.prevprice = extractPrice(prevprice, "£")
         
def new_get_promo_message(self):
    info = self.webSoup.find('div', {'id' : 'header_espot'})
    if info != None : 
        pm = info.find('img')
        if pm != None: 
            self.promomessage = pm.get('alt')
            
def new_find_all_products(self):
    info = self.webSoup.find('ul', {'id' : 'navlist'}  )
    if info != None:
        products = info.find_all('li', recursive = False)
    else:
        products = []
  
    print('num products', len(products))
    self.products = products
    
    #orig
    # products = self.webSoup.find_all('div',class_="product-tile" )
    # print('num products', len(products))
    # self.products = products
    
#define the method that is used to find the product description from the individual product page url
def new_get_product_description(self):
    product_info_text_block = self.webSoup.find('span', { 'itemprop' : 'description'})
    if product_info_text_block != None:
        #print('product info', product_info_text_block.prettify())
        pi_t = product_info_text_block.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
    else:
        pi_t = ""
    self.long_description =   pi_t 
    #get the colour description
    info = self.webSoup.find('span', {'id' : 'colourName'})
    if info != None:
        colour_info = info.get_text()
        if colour_info != None:
            self.colour = colour_info.replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()

    #TODO fetch the size description data = [list of strings]
    self.size = "size  desc"
    self.availability = "availability desc"

#define the method that is used to find the main image url from the individual product page url
def new_get_product_imgage_url(self):
    image_info = self.webSoup.find('a', {'id' : 'zoomMainImage'})
    if image_info != None:
        image = image_info
        if image != None:
            url = image.find('img')["src"]
        #url2 = image_url.find('img')["srcset"]
        #if url != None:
            #url = 'https:' + url
        self.imgurl = url
        
#find the next page link from the current page
def new_find_next_page_from_webpage(self):
    #driver = self.driver # for access to buttons on the page etc
    #for jack wills from html
    link = self.link
    findNextPage = params['findNextPage']
    mainurl = params['mainurl']
    webSoup = self.webSoup
    nextPage = None
    if findNextPage:    
        page_info = webSoup.find("a", { 'class': "swipeNextClick NextLink" })
        if page_info != None:
            next_link = page_info.get("href")
            if next_link != None:
                nextPage = mainurl + next_link
            else:
                nextPage = None
    print('Next Page' , nextPage)
    self.nextPage = nextPage
    

#redefine the methods specific to this retailer        
ProductDataFrame.get_description =  new_get_description
ProductDataFrame.get_url = new_get_url
ProductDataFrame.get_skuid = new_get_skuid_data
ProductDataFrame.get_imageurl = new_get_imageurl
ProductDataFrame.get_banner = new_get_banner
ProductDataFrame.get_ticket_previous_price = new_get_ticket_previous_price
ProductDataFrame.get_brand = new_get_brand
ProductDataFrame.get_reviewscore = new_get_reviewscore
ProductDataFrame.get_reviewcount = new_get_reviewcount
ProductDataFrame.get_product_description = new_get_product_description
ProductDataFrame.get_product_imgage_url = new_get_product_imgage_url
WebPageItems.get_promo_message = new_get_promo_message
WebPageItems.find_all_products = new_find_all_products
WebPageItems.find_next_page_from_webpage = new_find_next_page_from_webpage
#new methods can be added using the same approach, e.g. WebPageItems.newmethod = newmethod


#%%    RETAILER SPECIFIC - IDENTIFY WHICH OPTIONAL METHODS TO RUN FOR EACH PRODUCT

methods_to_run = {
        'output_product_to_screen' : False,
        'output_image_to_file' : False, #okay on windows, stalls on same image on VM ubuntu
        'output_description_to_file' : False, #okay on windows, stalls on same image on VM ubuntu
        'output_product_values_to_screen' : False,
        'output_webpage_to_file' : False,
        }
     

# %% Main Code    
    
def main():
    filename = __file__
    startPage = 0  # EDIT HERE FOR SET_UP AND TESTING to processs a sub range of weblinks
    endPage = 999 # EDIT HERE
    scrapeWebPages( startPage, endPage, linkurls, params, filename, methods_to_run)
    return
    
 
if __name__ == '__main__':
    main()