import __init__ # set up the import paths
import bs4
import datetime as dt
import pandas as pd
import time
import json
from pathlib import Path
import requests
import html5lib
import os
import re
from math import ceil

from CoreScraper import scrapeWebPages, extractPrice
from CoreScraper_getHtml import WebPageItems, ProductDataFrame
from CoreScraperConstants import toppath,  default_params

#  RETAILER SPECIFIC CONTROL PARAMETERS
# Set up the top path as parent folder of results, Note this is overridden if supplied as an argument when called as a main function
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape/'
    

# set up multiple urls for finding links, first url is mainurl (code filtters duplicates)
linkurls = ['https://www.zara.com/fr/en']

params = default_params.copy()
params.update({
    'mainurl': linkurls[0],
    'opfpath': toppath,  # can be overidden by arguments, applied in in scrapeWebPages
    # define valid links and how to interpret links
    #'validtags': ["femme", "homne", "trf", "enfant"],  # a valid link must contain one of these strings
    'validtags': ["woman", "man", "divided", "kids"],  # a valid link must contain one of these strings
    'invalidtags': ["view-all"],  # if a link contains one of these it is invalid
    'resetduplicatelinks': True,
    # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks': ['https://www.tbc.com'],  # force any duplicate links to be ignored
    'starting_links': [],
    # additional links to process  #set any starting links that will not be picked up by the find starting pages process
    'tagref': 'a',  # look for hrefs in this class
    'remove_switches_link': True,  # if True, remove ? and # switches from link (recommended unless required)
    'departmentlevel': 4,  # the nth level in the link
    'categorylevel': 5,  # the nth level in the link
    'subcategorylevel': 6,  # the nth level in the link
    'minimumdepth': 1,
    # a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    # define how the web page is accessed
    'findNextPage': True,
    # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    'htmlParser': 'lxml',
    # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    # webdriver parameters
    'webdriverpath': 'dummy',  # path to the webdriver executable SET UP IN CoreScraper.py
    'displayOff': False,  # whether display is switched off or visible
    'displayMinimised': False,  # whether to minimise the window if display is on
    'resetDriver': False,  # whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'waitForPageLoad': 3,
    # typ set to half webPageWait, when scrolling, time to wait for page to load before comparing 2 subsequent loads to see if scrolled page has finished loading
    'webPageWait': 6,  # time between subsequent web pages (next link, next page, next scroll down page)
    'scrollDownPage': False,  # whether to scroll down the page to find more products, e.g. Topshop
    # url and redirect parameters
    'urlSwitch': '',  # sets home country to uk
    'invalidRedirect': '',  # if the redirected url contains this value the product pages do not load correctly
    # define any buttons to be clicked on screen
    'buttonXpaths': ['//*[@id="geolocation-popup"]/div[2]/div[3]/div/section/section/button[1]',
                     '//*[@id="geolocation-popup"]/div[2]/div[1]/div/span', '//*[@id="onetrust-accept-btn-handler"]',
                     '//*[@id="onetrust-close-btn-container"]/button'],
    # a list of XPaths (contunue on French Site, view)
    'buttoniFrameXpaths': [''],
    'proxy_location': '', # Give multiple country like this, 'GB|US'. country codes: https://laendercode.net/en/2-letter-list.html
    'popup_remove_button': ['//*[@id="popup-announcements"]/div'],  # give xpath of popup removal button,
    'vpn_location': 'FRWS',
})




# %%  RETAILER SPECIFIC - REDEFINE THE PRODUCTDATAFRAME METHODS
def new_get_description(self):
    info = self.webSoup.find('a', { 'class' : 'product-grid-product-info__name'})
    if info != None:
        info = info.find('span')
        if info != None: 
            d = info.get_text()
            if d != None:
                description = d.replace('\n', '').replace('\r', '').strip()
                self.description = description


def new_get_url(self):
    #info = self.webSoup.find('a', class_="name")
    info = self.webSoup.find('a', { 'class' : 'product-grid-product-info__name'})
    if info != None:
        url_info = info
        if url_info != None: 
            href = url_info.get('href')
            #extra_query = url_info.get('data-extraquery')
            self.url = href


def new_get_skuid(self):
    info = self.webSoup.get('data-productid')
    if info != None:
        skuid = str(info)
        self.skuid = skuid


def new_get_imageurl(self):
    i = self.webSoup.find('img')
    if i != None:
        # case 1
        imgurl = i.get('src')
        # case 2
        if imgurl == None: imgurl = i.get('data-src')
        self.imgurl = imgurl


def new_get_banner(self):
    info = self.webSoup.find('div', {'class': 'product-grid-product-info__tag'})
    if info != None:
        b = info
        if b != None:
            banner = b.get_text()
            self.banner = banner

def new_get_ticket_previous_price(self):
    ticketprice = None
    prevprice = None

    info = self.webSoup.find('div', class_="product-grid-product-info__price")
    if info != None:
        priceinfo = info
        if priceinfo != None:
            prices = priceinfo.get_text().replace('EUR', '').strip().split()
            numbers = []
            for p in prices:
                if any(str.isdigit(c) for c in p):
                    # p = p [ p.find('EUR'): ]
                    numbers.append(p)

            if len(numbers) == 1:
                ticketprice = numbers[0]
                prevprice = ticketprice
            elif len(numbers) > 1:
                prevprice = numbers[0]
                ticketprice = numbers[1]

    self.ticketprice = extractPrice(ticketprice, "£")
    self.prevprice = extractPrice(prevprice, "£")


def new_get_promo_message(self):
    self.promomessage = None


def new_find_all_products(self):
    info = self.webSoup.find('div', {'class': "product-groups"})
    if info != None:
        products = info.find_all('li')
    else:
        products = []
    print('num products', len(products))
    self.products = products


#define the method that is used to find the product description from the individual product page url
def new_get_product_description(self):
    product_info_text_block = self.webSoup.find('p', { 'class' : 'description'})
    if product_info_text_block != None:
        pi_t = product_info_text_block.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ")
    else:
        pi_t = ""
    #to get the composition - need to click on the composition button, fetch the page then look in the popup 
    product_comp_text_block = self.webSoup.find('ul', { 'class' : 'list-composition'})
    if product_comp_text_block != None:
        pi_c = product_comp_text_block.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ")
    else:
        pi_c = ""
    self.long_description =   pi_t + " " + pi_c
    #get the colour description
    info = self.webSoup.find('span', {'class' : '_colorName'})
    if info != None:
        colour_info = info.get_text()
        if colour_info != None:
            self.colour = colour_info
    #TODO fetch the size description data = [list of strings]
    self.size = "size  desc"
    self.availability = "availability desc"

#define the method that is used to find the main image url from the individual product page url
def new_get_product_imgage_url(self):
    image_info = self.webSoup.find('a', {'class' : '_seoImg main-image'})
    if image_info != None:
        image = image_info
        if image != None:
            url = image.find('img')["src"]
        #url2 = image_url.find('img')["srcset"]
        #if url != None:
            #url = 'https:' + url
        self.imgurl = url


# find the next page link from the current page
def new_find_next_page_from_webpage(self):
    # driver = self.driver # for access to buttons on the page etc
    # for abercrombie add start position to end of row
    link = self.link
    findNextPage = params['findNextPage']
    mainurl = params['mainurl']
    webSoup = self.webSoup
    info = webSoup.find('div', class_="load-more-products")
    if info != None:
        dataitems = info.find('h2', class_="load-more-heading")
        totalitems = int(dataitems.get('data-total'))
        dataitemsshown = int(dataitems.get('data-items-shown'))
        dataitems = info.find('button')
        # roundeditems = ceil(int(totalitems)/sz)*sz
        dataperpage = int(dataitems.get('data-per-page'))
        nextPageItems = dataitemsshown + dataperpage
        print('Next Page ', totalitems, dataitemsshown, dataperpage)
        if nextPageItems < totalitems:
            nextPage = link['topurl'] + '?offset=0&page-size=' + str(dataitemsshown + dataperpage)
        else:
            nextPage = None
    else:
        nextPage = None
    print('Next Page', nextPage)
    self.nextPage = nextPage


# redefine the methods specific to this retailer
ProductDataFrame.get_description = new_get_description
ProductDataFrame.get_url = new_get_url
ProductDataFrame.get_skuid = new_get_skuid
ProductDataFrame.get_imageurl = new_get_imageurl
ProductDataFrame.get_banner = new_get_banner
ProductDataFrame.get_ticket_previous_price = new_get_ticket_previous_price
ProductDataFrame.get_product_description = new_get_product_description
ProductDataFrame.get_product_imgage_url = new_get_product_imgage_url
# WebPageItems.get_promo_message = new_get_promo_message
WebPageItems.find_all_products = new_find_all_products
WebPageItems.find_next_page_from_webpage = new_find_next_page_from_webpage
# new methods can be added using the same approach, e.g. WebPageItems.newmethod = newmethod


# %%    RETAILER SPECIFIC - IDENTIFY WHICH OPTIONAL METHODS TO RUN FOR EACH PRODUCT

methods_to_run = {
    'output_product_to_screen': False,
    'output_product_values_to_screen': False,
    'output_image_to_file': True,
    'output_description_to_file': True,
    'output_webpage_to_file': False,
}


# %% Main Code


def main():
    filename = __file__
    startPage = 0  # EDIT HERE FOR SET_UP AND TESTING to processs a sub range of weblinks
    endPage = 999  # EDIT HERE
    scrapeWebPages(startPage, endPage, linkurls, params, filename, methods_to_run)
    return


if __name__ == '__main__':
    main()