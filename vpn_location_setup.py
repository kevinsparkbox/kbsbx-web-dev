#set up the path in init to the scrapers and the core modules
import __init__

import time
import os
import sys

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from pathlib import Path

from CoreScraperConstants import MASTER_CHROME_DATA_DIR, MASTER_CHROME_DATA_PATH, USER_AGENT
from webdriver_manager.chrome import ChromeDriverManager


def main():
    """Creates a folder to save the Chrome Browser user files in a folder below the current project folder
    User opens any Chrome extensionns required, e.g. VPN, connects and opens any websites if any Cookies are required to be installed
    When done User closes the opened Chrome browser
    """
    #user enters the VPN folder location
    vpn_location = input("Enter VPN location")

    #fetch the directory of the python file
    dir_path = os.path.dirname(os.path.realpath(__file__))
    #parent_path = str(Path(dir_path).parent)
    chrome_data_dir = str(Path( dir_path + MASTER_CHROME_DATA_PATH) / ( MASTER_CHROME_DATA_DIR + vpn_location ))
    print('Setting up Chrome extension and default cookies in ', chrome_data_dir)

    chrome_options = Options()
    #arg_string = "--user-data-dir=" + MASTER_CHROME_DATA_DIR + vpn_location
    arg_string = "--user-data-dir=" + chrome_data_dir
    chrome_options.add_argument(arg_string)
    chrome_options.add_argument("--window-size=1600,900")
    chrome_options.add_argument("--user-agent=" + USER_AGENT)  
   
    #test not loading images
    #prefs = {"profile.managed_default_content_settings.images": 2}
    #chrome_options.add_experimental_option("prefs", prefs)

    driver = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)
    #driver = webdriver.Chrome(executable_path=CHROMEDRIVERPATH, options=chrome_options)
    # open any website
    # driver.get('https://www.amazon.co.uk')
    driver.get('https://chrome.google.com/webstore/detail/windscribe-free-proxy-and/hnmpcagpplmpfojmgmnngilcnanddlhb?hl=en')
    #driver.get('https://chrome.google.com/webstore/detail/surfshark-vpn-extension-f/ailoabdmgclmfmhdagmlohpjlbpffblp')
    print('Wait 10 minutes, go to websites and click buttons / credentials or edit cookie in settings, close browser when finished')
    # first time around add the nord vpn extension, then gets saved with the Chrome cookies
    time.sleep(60 * 10)


    driver.close()
    driver.quit()


if __name__ == '__main__':
    main()