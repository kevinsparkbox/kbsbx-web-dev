

import selenium
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

import time
#set up the driver
#driver = webdriver.Chrome(driver_path)  deprecated
driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))


#open amazon web page
#driver.get("https://www.amazon.co.uk//New-Balance-Mens-Core-Trainers/dp/B01AZH8DWO/ref=sr_1_5?crid=Y46JDEAELSIE&keywords=new+balance+trainers&qid=1676464024&sprefix=new+balance%2Caps%2C136&sr=8-5")
driver.get('https://www.amazon.co.uk/s?k=new+balance+trainers&crid=Y46JDEAELSIE&sprefix=new+balance%2Caps%2C136&ref=nb_sb_noss_1')
#driver.maximize_window()

#get the driver to the web page
#first scroll down the page to make sure the product details load
driver.find_element_by_tag_name('body').send_keys(Keys.PAGE_DOWN)

#click on the new balance only brand checkbox
brand_box = driver.find_element(By.XPATH, '//*[@id="p_89/New Balance"]/span/a/span')

brand_box.click()



#clear the cookies
time.sleep(50)