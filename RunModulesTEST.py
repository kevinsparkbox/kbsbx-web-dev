#set up the path in init to the scrapers and the core modules
import __init__

from CoreScraperRunModules import scrapePagesInPriority
import os
import sys

#reads in pages to process from csv file on GCS in WEBSITES_FOLDER and WEBSITES_FILE, 
# from either TEST_GOOGLE_STORAGE_FILE_PATH if "TEST" in toppath or GOOGLE_STORAGE_FILE_PATH otherwise
pages_to_process = "C:/Users/kevin/OneDrive/Documents/Repos/kbsbx-web-dev/webpages_priorityTEST.csv"  # set to a local file to force a local set for testing, e.g. pages_to_process = 'C:/Users/kevin/sbx-web/webpages_priority_MED.csv'
#pages_to_process = "C:/Users/kevin/OneDrive/Documents/Repos/kbsbx-web-dev/webpages_priorityTESTInsta.csv"  # set to a local file to force a local set for testing, e.g. pages_to_process = 'C:/Users/kevin/sbx-web/webpages_priority_MED.csv'
#pages_to_process = "C:/Users/kevin/OneDrive/Documents/Repos/kbsbx-web-dev/webpages_priorityTESTWeather.csv"  # set to a local file to force a local set for testing, e.g. pages_to_process = 'C:/Users/kevin/sbx-web/webpages_priority_MED.csv'
#pages_to_process = '/home/kevin_d_blackmore/kbsbx-web-dev/webpages_priorityTESTInsta.csv'
#pages_to_process = ""


#Set up the top path as parent folder of results, NOTE this overwrites any toppath defined in the scraper
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape TEST/'


def main():
    scrapePagesInPriority(toppath, pages_to_process)

    
if __name__ == '__main__':
    main()
  

