# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
""" Testing the big query API
pip install --upgrade google-cloud-bigquery
pip install --upgrade google-cloud-bigquery-storage
pip install --upgrade pyarrow
pip install --upgrade google-cloud-core
pip install --upgrade google-api-core
google docs examples
google-cloud-bigquery==2.0.0
google-cloud-bigquery-storage==2.0.0
pandas==1.1.3
pandas-gbq==0.14.0
pyarrow==1.0.1
grpcio==1.32.0
"""


# %%
import google.auth
from google.cloud import bigquery
import pandas as pd
import vaex
#import modin.pandas as pd
import time
import os
from datetime import datetime
#import pandas_gbq

from copy_ml_utils import upload_df_to_BQ_via_local_GS




#configure the parameters required for upload_df_to_BQ_via_local_GS
GBQ_OUT_DATASET_DATAPREP = 'testcompetitive_data'
GCS_ORG_ID = "5710211792764928"
TEMP_BUCKET_FOLDER = 'temp'
BUCKET_NAME = 'sparkbox-dev-ml'

if os.name == 'posix':
    temp_local_folder = "/home/kevin_d_blackmore/Web Scrape TEST/"
else:
    temp_local_folder =  'C:/Users/kevin/Data/Spb-data/pimkie_test_recommendationsFRDE/'


destination_dataset = GBQ_OUT_DATASET_DATAPREP
bq_table = 'df_to_bq_table_upload'
temp_bucket_folder = GCS_ORG_ID + '/' + TEMP_BUCKET_FOLDER + '/'
gs_bucket = BUCKET_NAME 


def download_bq_df_standardbq_storage(sql, credentials, project_id):
    # Use a BigQuery Storage API client to download results.
    client = bigquery.Client(credentials=credentials, project=project_id)
    start_time = time.time()
    df = client.query(sql).to_dataframe(create_bqstorage_client=True)
    t = time.time() - start_time
    #print(df.info())
    print('Time Taken to download bq storage', t)
    return df, t


# %%
def download_bq_df_arrow_data(sql, credentials, project_id):
# use big query API with arrow data format
    client = bigquery.Client(credentials=credentials, project=project_id)
    start_time = time.time()
    df = client.query(sql).to_arrow(create_bqstorage_client=True).to_pandas()
    t = time.time() - start_time
    #print(df.info())
    print('Time Taken to download arrow', t)
    return df, t


# %%



def test_bqclient_upload_append(df, table_id):
    #force object columns to string to stop bq upload misclassifying type
    for c in df.columns:
        if df[c].dtypes == 'object':
            #print('object', c)
            df[c] = df[c].astype('str')
    #test appending
    client = bigquery.Client()
    start_time = time.time()
    job_config = bigquery.LoadJobConfig(
        # Optionally, set the write disposition. BigQuery appends loaded rows
        # to an existing table by default, but with WRITE_TRUNCATE write
        # disposition it replaces the table with the loaded data.
        #write_disposition="WRITE_TRUNCATE",
    )
    job = client.load_table_from_dataframe(df, table_id, job_config=job_config)
    #wait for job to complete
    job.result()
    t = time.time() - start_time
    print('Time Taken to upload df appending', t)
    return t

def test_bqclient_upload_replace(df, table_id):
    #force object columns to string to stop bq upload misclassifying type
    for c in df.columns:
        if df[c].dtypes == 'object':
            #print('object', c)
            df[c] = df[c].astype('str')
    client = bigquery.Client()
    #test creatiung new table
    start_time = time.time()
    job_config = bigquery.LoadJobConfig(
        # Optionally, set the write disposition. BigQuery appends loaded rows
        # to an existing table by default, but with WRITE_TRUNCATE write
        # disposition it replaces the table with the loaded data.
        write_disposition="WRITE_TRUNCATE",
    )
    job = client.load_table_from_dataframe(df, table_id, job_config=job_config)
    #wait for job to complete
    job.result()
    t = time.time() - start_time
    print('Time Taken to upload df overwriting', t)
    return t


# %%
## test using the pandas gbq library
def test_gbq_replace(df, table_id, project_id, credentials):
    #force object columns to string to stop bq upload misclassifying type so as compatible with bq tests
    for c in df.columns:
        if df[c].dtypes == 'object':
            #print('object', c)
            df[c] = df[c].astype('str')
    if_exists_operation = "append"
    start_time = time.time()
    pandas_gbq.to_gbq(df, table_id , project_id=project_id, credentials=credentials, if_exists=if_exists_operation)
    t = time.time() - start_time
    print('Time Taken to upload df append using gbq', t)
    return t

#  test deduplication of the append table 
def deduplicate_append_table():
    dedup_sql  = """
                #deduplicate on sku_id and cal_date, keeping the latest using the created column
                CREATE OR REPLACE TABLE testcompetitive_data.test_dataupload
                AS
                SELECT dedup_table.* FROM (
                SELECT ARRAY_AGG(
                t ORDER BY t.created DESC LIMIT 1
                ) [OFFSET(0)] dedup_table
                FROM testcompetitive_data.test_dataupload t
                GROUP BY sku_id, cal_date
                )
        """
    # Make client nd run query job
    bqclient = bigquery.Client(credentials= credentials, project=project_id )
    bqclient._http.adapters['https://']._pool_connections=100
    bqclient._http.adapters['https://'].pool_maxsize=100
    print('De duplicating append output table')
    start_time = time.time()
    bq_job = bqclient.query(dedup_sql, project=project_id)
    bq_job.result()  # Waits for job to complete.
    t = time.time() - start_time
    print('Time Taken to de duplicate', t)
    return t


# %%
# get google credentials
credentials, project_id = google.auth.default()

#define a sql request, simulating a simple date query
master_sql = """
    SELECT * 
    FROM pimkie_data.FCALLNFR
    WHERE cal_date >= "2017-01-21"
    LIMIT LIMIT_ROWS
    """

#test_rows = [10000, 100000, 1000000, 5000000]
test_rows = [50000]
repitions = 1
results = pd.DataFrame()

#run the tests
for r in range(0, repitions):
    for test in test_rows:
        row = {}
        print(' Test rows ', test)
        
        ##### DOWNLOAD TESTS
        sql = master_sql.replace('LIMIT_ROWS', str(test) )
        #download using standard bq storage
        df_pandas, t = download_bq_df_standardbq_storage(sql, credentials, project_id)

        row['dl std bq'] = t

        #convert to a vaex df
        df = vaex.from_pandas(df_pandas)

        #### UPLOAD TESTS
        #before uploading create a dummy region id , integer stored as a string to check the type processing on upload
        #df['region_id_test'] = 11
        #df['region_id_test'] = df['region_id_test'].astype('str')

        #create a large df
        row['num Rows DL'] = len(df)
        df_size = df.memory_usage().sum()/1000000
        row['df size MB DL'] = df_size
        starttime = time.time()
        new_df = pd.DataFrame()
        for i in range(0,2):
            print('Creating larger dfg, Appending ', i)
            new_df = new_df.append(df, ignore_index = True)
        df = new_df
        print('Time to create larger df is ', time.time() - starttime )
        row['num Rows UL'] = len(df)
        df_size = df.memory_usage().sum()/1000000
        row['df size MB UL'] = df_size
        print('new df created, rows', len(df), 'size' , df_size)


        #upload using bq api - replace    
        #table_id = 'testcompetitive_data.test_dataupload_modin'
        #t = test_bqclient_upload_replace(df, table_id)
        row['ul bq rep'] = t


 

        results = results.append( row, ignore_index=True)

cols = ['num Rows DL', 'df size MB DL' , 'dl std bq',  'num Rows UL', 'df size MB UL', 'dl std bq',  'ul bq rep']    
#cols = ['num Rows', 'df size' , 'dl std bq', 'df_to_GS_to_BQ']    
results = results[cols]
print(results)
#results.to_csv('bq_data_timings.csv')





# %%
#test deduplicating the append table
#t = deduplicate_append_table()


# %%



