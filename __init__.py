#init for top level
import os, sys 
from pathlib import Path
current_path = Path(os.path.dirname(os.path.realpath(__file__)))
#parent_path = (current_path.parent)
core_scraper_path = Path( str(current_path) + '//core_scraper')
scraper_path = Path( str(current_path) + '//scrapers')
insta_scraper_path = Path( str(current_path) + '//insta_scrapers')
test_scraper_path = Path( str(current_path) + '//test_scrapers')
weather_scraper_path = Path( str(current_path) + '//weather_scrapers')
sys.path.append(str(core_scraper_path))
sys.path.append(str(scraper_path))
sys.path.append(str(insta_scraper_path))
sys.path.append(str(test_scraper_path))
sys.path.append(str(weather_scraper_path))