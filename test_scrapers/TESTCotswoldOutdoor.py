import __init__ # set up the import paths
import bs4
import datetime as dt
import pandas as pd
import time
import json
from pathlib import Path
import requests
import html5lib
import os
import re
from math import ceil

from CoreScraper import scrapeWebPages, extractPrice
from CoreScraper_getHtml import WebPageItems, ProductDataFrame
from CoreScraperConstants import toppath,  default_params

#  RETAILER SPECIFIC CONTROL PARAMETERS
# Set up the top path as parent folder of results, Note this is overridden if supplied as an argument when called as a main function
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape TEST/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape TEST/'
    
# set up multiple urls for finding links, first url is mainurl (code filtters duplicates)
linkurls = ['https://www.cotswoldoutdoor.com/']

params = default_params.copy()
params.update({
    'mainurl': linkurls[0],
    'opfpath': toppath,  # can be overidden by arguments, applied in in scrapeWebPages
    # define valid links and how to interpret links
    'validtags': ['cotswoldoutdoor'],  # a valid link must contain one of these strings
    'invalidtags': ['facebook', 'instagram', 'twitter', 'youtube' 'cyclesurgery', 'runnersneed', 'snowandrock',
                    'wishlist', 'stores', 'delivery', 'klarna', 'evouchers', 'knowledge', 'help', 'index', 'cookie',
                    'international', 'content', 'blog'],  # if a link contains one of these it is invalid
    'resetduplicatelinks': True,
    # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks': ['https://www.tbc.com'],  # force any duplicate links to be ignored
    'starting_links': [],
    # additional links to process  #set any starting links that will not be picked up by the find starting pages process
    'tagref': 'a',  # look for hrefs in this class
    'remove_switches_link': False,  # if True, remove ? and # switches from link (recommended unless required)
    'departmentlevel': 4,  # the nth level in the link
    'categorylevel': 5,  # the nth level in the link
    'subcategorylevel': 6,  # the nth level in the link
    'minimumdepth': 5,
    # a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    # define how the web page is accessed
    'useRequests': False,  # whether or not to use the requests module, if False use the chromium driver Browser
    'findNextPage': True,
    # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    'htmlParser': 'lxml',
    # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    # webdriver parameters
    'webdriverpath': 'dummy',  # path to the webdriver executable SET UP IN CoreScraper.py
    'displayOff': False,  # whether display is switched off or visible
    'displayMinimised': False,  # whether to minimise the window if display is on
    'resetDriver': False,
    # whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'waitForPageLoad': 2,
    # typ set to half webPageWait, when scrolling, time to wait for page to load before comparing 2 subsequent loads to see if scrolled page has finished loading
    'webPageWait': 2,  # time between subsequent web pages (next link, next page, next scroll down page)
    'scrollDownPage': True,  # whether to scroll down the page to find more products, e.g. Topshop
    # url and redirect parameters
    'urlSwitch': '',  # add an option to the url,e.g. 48 items per page
    'invalidRedirect': '',  # if the redirected url contains this value the product pages do not load correctly
    # define any buttons to be clicked on screen
    'buttonXpaths': ['//*[@id="cookie-message"]/div/button', '//*[@id="accept-all-cookies"]'],  # a list of XPaths
    # 'buttonXpaths' : [ '' ], # a list of XPaths
    'buttoniFrameXpaths': [],
    'numscreenshots': 1,  # number of screnshots to save
    'max_link_depth': 1,
    'resetCookies': False,
    'proxy_location': '',
    # Give multiple country like this, 'GB|US'. country codes: https://laendercode.net/en/2-letter-list.html
    'vpn_location': 'GBWS',
})


# %%  RETAILER SPECIFIC - REDEFINE THE PRODUCTDATAFRAME METHODS
def new_get_description(self):
    info = self.webSoup.find('span', {'class': 'as-m-product-tile__name'})
    if info != None:
        d = info
        if d != None:
            description = d.get_text()
            self.description = description


def new_get_url(self):
    info = self.webSoup
    if info != None:
        u = info.find('a')
        if u != None:
            url = u.get('href')
            self.url = self.mainurl + url


def new_get_skuid(self):
    skuid = str(self.url.split('-')[-1]).split('.')[0]
    if skuid != None:
        self.skuid = skuid


def new_get_imageurl(self):
    i = self.webSoup.find('img')
    if i != None:
        imgurl = 'https:' + str(i.get('src'))
        self.imgurl = imgurl


def new_get_brand(self):
    pass


def new_get_stock(self):
    pass


def new_get_banner(self):
    info = self.webSoup.find('div', {'class': "as-a-tag--general"})
    if info != None:
        b = info.get_text()
        if b != None:
            banner = b
            self.banner = banner


def new_get_ticket_previous_price(self):
    ticketprice = None
    prevprice = None
    priceinfo = self.webSoup.find('div', class_="as-a-price--spacing-xs")

    if priceinfo != None:
        price_list = re.findall(r"[-+]?\d*\.\d+|\d+", str(priceinfo))

        if len(price_list) == 2:
            prevprice = "£ " + str(price_list[0])
            ticketprice = "£ " + str(price_list[1])
        if len(price_list) == 1:
            ticketprice = "£ " + str(price_list[0])
            prevprice = ticketprice

    self.ticketprice = extractPrice(ticketprice, "£")
    self.prevprice = extractPrice(prevprice, "£")


def new_get_reviewcount(self):
    info = self.webSoup.find('span', {'class': "as-a-text--xs"})
    if info != None:
        r = info.get_text()
        if r != None:
            reviewcount = r.strip()
            self.reviewcount = reviewcount


def new_get_reviewscore(self):
    count_full_rating = 0
    count_half_rating = 0
    info = self.webSoup.find('div', {'as-m-product-tile__rating'})
    if info != None:
        full_rating = info.find_all('i', {'as-a-rating-item--full'})
        half_rating = info.find_all('i', {'as-a-rating-item--half'})
        if full_rating != None:
            count_full_rating = int(len(full_rating))
        if half_rating != None:
            count_half_rating = int(len(half_rating))

        review_score = count_full_rating + (0.5 * count_half_rating)
        self.reviewscore = review_score


def new_get_promo_message(self):
    pass


def new_find_all_products(self):
    products = self.webSoup.find_all('div', {'class': re.compile('as-t-product-grid__item')})
    print('num products', len(products))
    self.products = products


#define the method that is used to find the product description from the individual product page url
def new_get_product_description(self):
    pi_t = ""
    product_info_text_block = self.webSoup.find('div', { 'data-qa' : 'product_description'})
    if product_info_text_block != None:
        print('YYYY product info', product_info_text_block.prettify())
        blocks = product_info_text_block.find_all('p')
        if len(blocks) > 0:
            for b in blocks:
                print('Block b', b)
                pi_t = pi_t + " " + b.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
        else:
            b = product_info_text_block.find( 'div' ,  { 'class'  : "as-m-collapse__body"} )
            if b != None:
                pi_t =  b.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()

    self.long_description =   pi_t 
    #get the colour description - clothing products
    info = self.webSoup.find('span', {'data-qa' : 'color_name'})
    if info != None:
        print('XXX TEMPLATE TABLE', info.prettify())
        colour = info.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
        self.colour = colour
                        

    #TODO fetch the size description data = [list of strings]
    self.size = "size  desc"
    self.availability = "availability desc"

#define the method that is used to find the main image url from the individual product page url
def new_get_product_imgage_url(self):
    image_info = self.webSoup.find('div', {'data-qa' : 'product_image'})
    if image_info != None:
        images = image_info.find('img')
        if images != None:
            url = images.get("src")
            self.imgurl =  'https:' + url 
        #url2 = image_url.find('img')["srcset"]
        #if url != None:
            #url = 'https:' + url
        #self.imgurl = url


# find the next page link from the current page
def new_find_next_page_from_webpage(self):
    # driver = self.driver # for access to buttons on the page etc
    # for Karen Millen add page=N to the end of the url
    # Laren Millen hgas changed page format is ?sz=60&start=60
    findNextPage = params['findNextPage']
    nextPage = None
    if findNextPage:
        # set next page if valid
        page_info = self.webSoup.find('div', {'class': 'as-m-pagination'})
        if page_info != None:
            info = page_info.find('a', {'rel': 'next'})
            if info != None:
                np = info
                if np != None:
                    nextPage = np.get("href")

    # with john lewis url page number is much larger than page number displayed on web site,
    # e.g. website displayed page 22 = url page 169

    # alo needs o scroll down to load the last 24 products on each page

    print('Next Page', nextPage)
    self.nextPage = nextPage


# redefine the methods specific to this retailer
ProductDataFrame.get_description = new_get_description
ProductDataFrame.get_url = new_get_url
ProductDataFrame.get_skuid = new_get_skuid
ProductDataFrame.get_imageurl = new_get_imageurl
ProductDataFrame.get_banner = new_get_banner
ProductDataFrame.get_brand = new_get_brand
ProductDataFrame.get_stock = new_get_stock
ProductDataFrame.get_reviewscore = new_get_reviewscore
ProductDataFrame.get_reviewcount = new_get_reviewcount
ProductDataFrame.get_ticket_previous_price = new_get_ticket_previous_price
ProductDataFrame.get_product_description = new_get_product_description
ProductDataFrame.get_product_imgage_url = new_get_product_imgage_url
WebPageItems.get_promo_message = new_get_promo_message
WebPageItems.find_all_products = new_find_all_products
WebPageItems.find_next_page_from_webpage = new_find_next_page_from_webpage
# new methods can be added using the same approach, e.g. WebPageItems.newmethod = newmethod


# %%    RETAILER SPECIFIC - IDENTIFY WHICH OPTIONAL METHODS TO RUN

methods_to_run = {
    'output_product_to_screen': False,
    'output_product_values_to_screen': False,
    'output_image_to_file': True,
    'output_description_to_file': True,
    'output_webpage_to_file': False,
}


# %% Main Code


def main():
    filename = __file__
    startPage = 61  # EDIT HERE FOR SET_UP AND TESTING to processs a sub range of weblinks
    endPage = 62  # EDIT HERE
    scrapeWebPages(startPage, endPage, linkurls, params, filename, methods_to_run)
    return


if __name__ == '__main__':
    main()
