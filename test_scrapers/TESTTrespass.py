import __init__ # set up the import paths
import bs4
import datetime as dt
import pandas as pd
import time
import json
from pathlib import Path
import requests
import html5lib
import os
import re
from math import ceil

from CoreScraper import scrapeWebPages, extractPrice
from CoreScraper_getHtml import WebPageItems, ProductDataFrame
from CoreScraperConstants import toppath,  default_params

#  RETAILER SPECIFIC CONTROL PARAMETERS
# Set up the top path as parent folder of results, Note this is overridden if supplied as an argument when called as a main function
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape TEST/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape TEST/'

# set up multiple urls for finding links, first url is mainurl (code filtters duplicates)
linkurls = ['https://www.trespass.com']

params = default_params.copy()
params.update( {
    'mainurl': linkurls[0],
    'url_to_prefix_to_link': 'https://www.trespass.com',
    'opfpath': toppath,  # can be overidden by arguments, applied in in scrapeWebPages
    # define valid links and how to interpret links
    'validtags': ['trespass'],  # a valid link must contain one of these strings
    'invalidtags': ['regionSelector'],  # if a link contains one of these it is invalid
    'resetduplicatelinks': True,
    # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks': ['https://www.tbc.com'],  # force any duplicate links to be ignored
    'starting_links': [],
    # additional links to process  #set any starting links that will not be picked up by the find starting pages process
    'tagref': 'a',  # look for hrefs in this class
    'remove_switches_link': False,  # if True, remove ? and # switches from link (recommended unless required)
    'departmentlevel': 3,  # the nth level in the link
    'categorylevel': 4,  # the nth level in the link
    'subcategorylevel': 5,  # the nth level in the link
    'minimumdepth': 5,
    'maximumdepth' : 999 ,  #a page link has not to exceed the maximum depth
    # a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    # define how the web page is accessed
    'useRequests': False,  # whether or not to use the requests module, if False use the chromium driver Browser
    'findNextPage': True,
    # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    'htmlParser': 'lxml',
    # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    # webdriver parameters
    'webdriverpath': 'dummy',  # path to the webdriver executable SET UP IN CoreScraper.py
    'displayOff': False,  # whether display is switched off or visible
    'displayMinimised': False,  # whether to minimise the window if display is on
    'resetDriver': False,  # whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'waitForPageLoad': 3,
    # typ set to half webPageWait, when scrolling, time to wait for page to load before comparing 2 subsequent loads to see if scrolled page has finished loading
    'webPageWait': 2,  # time between subsequent web pages (next link, next page, next scroll down page)
    'scrollDownPage': False,
    # whether to scroll down the page to find more products, True scroll s down using keys, Short scroll down window height
    # url and redirect parameters
    'urlSwitch': '',  # add an option to the url,e.g. 48 items per page
    'invalidRedirect': '',  # if the redirected url contains this value the product pages do not load correctly
    # define any buttons to be clicked on screen
    'buttonXpaths': ['/html/body/div[1]/div[4]/a/i'],  # a list of XPaths
    'buttoniFrameXpaths': [],
    'proxy_location': '', # Give multiple country like this, 'GB|US'. country codes: https://laendercode.net/en/2-letter-list.html
    'vpn_location': 'GBWS',

    'short_webPageWait' : 2, #  #define a shorter time to wait for when loading individal product pages

})


# %%  RETAILER SPECIFIC - REDEFINE THE PRODUCTDATAFRAME METHODS
def new_get_description(self):
    info = self.webSoup.find('strong', {'class': 'product-item-name'})
    if info != None:
        d = info.find('a')
        if d != None:
            description = d.get_text().replace('\n', '').replace('\r', '').strip()
            self.description = description


def new_get_url(self):
    info = self.webSoup
    if info != None:
        u = info.find('a')
        if u != None:
            url = u.get('href')
            if url != None:
                self.url = url


def new_get_skuid(self):
    info = self.webSoup.find('div', {'class': "price-box"})
    if info != None:
        skuid = info.get('data-product-id')
        self.skuid = skuid


def new_get_imageurl(self):
    i = self.webSoup.find('img')
    if i != None:
        imgurl = i.get('src')
        self.imgurl = imgurl


def new_get_brand(self):
    # not implemented
    # info = self.webSoup.find('p')
    # if info != None :
    #    d = info.find('a', {'class' : "productMainLink"})
    #    if d != None:
    self.brand = None


def new_get_stock(self):
    # not implemented
    # info = self.webSoup.find('span' , {'class' : 'listProductLowStock'})
    # if info != None :
    #    d = info.get_text()
    #    if d != None:
    self.stock = None


def new_get_banner(self):
    # TODO
    # info = self.webSoup.find('ul', {'class': "trespass_uspbar-uspbar-inner"})
    # print("banner info: ", info)
    # if info != None:
    #     b = info.find('div', {'class': 'trespass_uspbar-uspbar-item-content'})
    #     print('bbb', b)
    #     if b != None:
    #         banner = b.get_text().strip()
    #         print("banner", banner)
    #         self.banner = banner
    self.banner = None


def new_get_ticket_previous_price(self):
    ticketprice = None
    prevprice = None
    priceinfo = self.webSoup.find('div', {'class': "price-box"})

    if priceinfo != None:
        normal_price = priceinfo.find('span', {'class': 'normal-price'})
        if normal_price != None:
            ticketprice = normal_price.find('span').find('span').find('span')
            ticketprice = ticketprice.get_text()

        prev_price = priceinfo.find('span', {'class': 'sly-old-price'})
        if prev_price != None:
            prevprice = prev_price.find('span').find('span', {'id': re.compile("^old-price-")})
            prevprice = prevprice.get_text()
        else:
            prevprice = ticketprice

    self.ticketprice = extractPrice(ticketprice, "£")
    self.prevprice = extractPrice(prevprice, "£")



    # ticket_price = 0
    # prev_price = 0
    # price_info = self.webSoup.find('div', {'class': "price-box"})
    # if price_info != None:
    #     # check to see if on sale
    #     ticket_price = price_info.find('p', {'class': 'special-price'})
    #     if ticket_price != None:  # is on sale
    #         ticket_price = ticket_price.find('span', {'class': re.compile("price product-price*")})
    #         ticket_price = ticket_price.get_text()
    #         ticket_price = ticket_price[ticket_price.find("£"):]  # extract the number only
    #         prev_price = price_info.find('span', {'class': 'price', 'id': re.compile("old-price*")})
    #         if prev_price != None:
    #             prev_price = prev_price.get_text()
    #             prev_price = prev_price[
    #                          prev_price.find("£"):]  # extract the number only and remove any weird second orig price
    #     else:  # not on sale
    #         ticket_price = ticket_price.find('span', {'class': re.compile("price product-price*")})
    #         if ticket_price != None:
    #             ticket_price = ticket_price.get_text()
    #             ticket_price = ticket_price[ticket_price.find(
    #                 "£"):]  # extract the number only   and remove any weird second orig price
    #             prev_price = ticket_price
    #
    # self.ticketprice = extractPrice(ticket_price, "£")
    # self.prevprice = extractPrice(prev_price, "£")


def new_get_reviewcount(self):
    # not implemented
    # info = self.webSoup.find('span', {'class' : "product-rating__review-count"})
    # if info != None :
    #    r = info.get_text()
    #    if r != None :
    #        r = r.split('(')[1].split('r')[0]
    #        reviewcount = r.replace('\n', ' ').replace('\r', ' ').replace('(', ' ').replace(')', ' ').strip()
    #        self.reviewcount = reviewcount
    self.reviewcount = None


def new_get_reviewscore(self):
    # not implemented
    # info = self.webSoup.find('div', {'class' : "product-rating"})
    # if info != None :
    #    fullstar = info.find_all('img',  { 'alt' : 'Full Star' })
    #    halfstar = info.find_all('img',  { 'alt' : 'Half Star' })
    #    reviewscore = len(fullstar) + 0.5*len(halfstar)
    #    self.reviewscore = reviewscore
    self.reviewscore = None


def new_get_promo_message(self):
    # not implemented
    info = self.webSoup.find('div', {'id': 'header_espot'})
    if info != None:
        pm = info.find('img')
        if pm != None:
            self.promomessage = pm.get('alt')


def new_find_all_products(self):
    productsinfo = self.webSoup.find('ol', {"class": "product-items"})
    if productsinfo != None:
        products = productsinfo.find_all('li')
    else:
        products = []

    print('num products', len(products))
    self.products = products


#define the method that is used to find the product description from the individual product page url
def new_get_product_description(self):
    info = self.webSoup.find('div', {'class' : 'product-details-wrapper-inner'})
    if info != None:
        t = info.get_text()
        self.long_description = t.replace('\n', ' ').replace('\r', ' ').replace("  ", " ")
        #print('DESCRIPTION ', self.long_description)
    #colour
    info = self.webSoup.find('span', {'class' : 'swatch-attribute-selected-option'})
    if info != None:
        self.colour = info.get_text()
        #print('COLOUR ', self.colour)
    #TODO fetch the size description data = [list of strings]
    self.size = "size  desc"
    self.availability = "availability desc"

#define the method that is used to find the main image url from the individual product page url
def new_get_product_imgage_url(self):
    image_url = self.webSoup.find('div', {'class' : 'fotorama__stage__frame magnify-wheel-loaded fotorama_vertical_ratio fotorama__loaded fotorama__loaded--img fotorama__active'})
    if image_url != None:
        imgurl = image_url.find('img')["src"]
        #imgurl2 = image_url.find('img')["srcset"]
        if imgurl != None:
            #self.imgurl = 'https:' + imgurl
            self.imgurl =  imgurl


# find the next page link from the current page
def new_find_next_page_from_webpage(self):
    # driver = self.driver # for access to buttons on the page etc
    # link = self.link
    findNextPage = params['findNextPage']
    nextPage = None
    if findNextPage:
        # find out if there us aload more button
        navinfo = self.webSoup.find("li", {'class': 'pages-item-next'})
        if navinfo != None:
            link = navinfo.find('a')
            if link != None:
                nextPage = link.get('href')

        print('Next Page', nextPage)

    self.nextPage = nextPage


# click any buttons that should be clicked on the page, e.g. to show all the products
def new_click_buttons_on_webpage(self):
    # E.g. where need to click buttons on page to expose all the products
    driver = self.driver

    # close the pop up box to select the country if it exists
    try:
        acceptButton = driver.find_element_by_css_selector('=#close > a')
        print('CLICKING CLOSE ON ACCEPT COUNTRY BUTTON', acceptButton)
        acceptButton.click()
    except:
        pass

    # close the pop up box to select the country if it exists
    try:
        acceptButton = driver.find_element_by_css_selector('close')
        print('CLICKING CLOSE ON ACCEPT COUNTRY BUTTON', acceptButton)
        acceptButton.click()
    except:
        pass

    # click on an Accept buttons for the cookies etc. as this is hiding the other buttons, if they are there
    try:
        acceptButton = driver.find_element_by_css_selector(
            'body > div.brtn-lightbox.lb-vanilla.type-html.gdpr-lightbox.isnt-closeable.trap.is-active.opened > div.lb-fg > div > div > div.col-buttons > a')
        print('CLICKING ACCEPT COOKIES BUTTON', acceptButton)
        acceptButton.click()
    except:
        pass


# =============================================================================
#     # click on the view all link if it is available
#     moreProductsLink  = driver.find_elements_by_partial_link_text('View All')
#     for clicklink in moreProductsLink:
#         try:
#             clicklink.click()
#             print('clicking view all link and sleeping')
#             time.sleep(10)
#         except:
#             pass
# =============================================================================


# %%


# redefine the methods specific to this retailer
ProductDataFrame.get_description = new_get_description
ProductDataFrame.get_url = new_get_url
ProductDataFrame.get_skuid = new_get_skuid
ProductDataFrame.get_imageurl = new_get_imageurl
ProductDataFrame.get_banner = new_get_banner
ProductDataFrame.get_brand = new_get_brand
ProductDataFrame.get_stock = new_get_stock
ProductDataFrame.get_reviewscore = new_get_reviewscore
ProductDataFrame.get_reviewcount = new_get_reviewcount
ProductDataFrame.get_ticket_previous_price = new_get_ticket_previous_price
ProductDataFrame.get_product_description = new_get_product_description
ProductDataFrame.get_product_imgage_url = new_get_product_imgage_url
WebPageItems.get_promo_message = new_get_promo_message
WebPageItems.find_all_products = new_find_all_products
WebPageItems.find_next_page_from_webpage = new_find_next_page_from_webpage
# WebPageItems.click_buttons_on_webpage = new_click_buttons_on_webpage

# new methods can be added using the same approach, e.g. WebPageItems.newmethod = newmethod


# %%    RETAILER SPECIFIC - IDENTIFY WHICH OPTIONAL METHODS TO RUN

methods_to_run = {
    'output_product_to_screen': False,
    'output_product_values_to_screen': False,
    'output_image_to_file': True,
    'output_description_to_file': True,
    'output_webpage_to_file': False,
}


# %% Main Code


def main():
    filename = __file__
    startPage = 10  # EDIT HERE FOR SET_UP AND TESTING to processs a sub range of weblinks
    endPage = 11  # EDIT HERE
    scrapeWebPages(startPage, endPage, linkurls, params, filename, methods_to_run)
    return


if __name__ == '__main__':
    main()    