import __init__ # set up the import paths
import bs4
import datetime as dt
import pandas as pd
import time
import json
from pathlib import Path
import requests
import html5lib
import os
import re
from math import ceil

from CoreScraper_utils import  init_logging
from CoreScraper import scrapeWebPages, extractPrice
from CoreScraper_getHtml import WebPageItems, ProductDataFrame, clickButton, getHtml
from CoreScraperConstants import toppath,  default_params

#  RETAILER SPECIFIC CONTROL PARAMETERS
# Set up the top path as parent folder of results, Note this is overridden if supplied as an argument when called as a main function
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape TEST/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape TEST/'

#set up multiple urls for finding links, first url is mainurl (code filtters duplicates)
linkurls = ['https://www.asos.com']

params = default_params.copy()
params.update({
    'mainurl' : linkurls[0],
    'opfpath' : toppath, # can be overidden by arguments, applied in in scrapeWebPages
    #define valid links and how to interpret links
    #'validtags' : ['women/sale', 'men/sale','women/outlet', 'men/outlet'],  # sale only a valid link must contain one of these strings
    #'validtags' : ['product'],  # a valid link must contain one of these strings
    'validtags' : ['product'],  # a valid link must contain one of these strings
    'invalidtags' : ['a-to-z', 'face-body' , 'gifts', 'marketplace', 'living', 'home', 'size'],  # if a link contains one of these it is invalid
    #invalid did include                '/women/outlet/cat/', '/men/outlet/cat/', '/women/sale/cat/', '/men/sale/cat/'],  # if a link contains one of these it is invalid
    'resetduplicatelinks' : True, # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks' : ['https://www.tbc.com'], # force any duplicate links to be ignored
    'starting_links' : [],    #additional links to process  #set any starting links that will not be picked up by the find starting pages process
    'tagref' : 'a', # look for hrefs in this class
    'remove_switches_link' : False,  # if True, remove ? and # switches from link (recommended)
    'departmentlevel' : 3, #the nth level in the link
    'categorylevel' : 4,  #the nth level in the link
    'subcategorylevel' : 5,  #the nth level in the link
    'minimumdepth' : 6 ,  #a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    #define how the web page is accessed
    'useRequests' : False, # whether or not to use the requests module, if False use the chromium driver Browser
    'findNextPage' : True, # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    'htmlParser' :  'lxml', # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    #webdriver parameters
    'webdriverpath' : 'dummy' , # path to the webdriver executable SET UP IN CoreScraper.py
    'displayOff' : False,  # whether display is switched off or visible
    'displayMinimised' :  False,  # whether to minimise the window if display is on
    'resetDriver' : False,  #whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'waitForPageLoad' : 1, #typ set to half webPageWait, when scrolling, time to wait for page to load before comparing 2 subsequent loads to see if scrolled page has finished loading
    'webPageWait' : 5,  # Browser: time to wait for page to fully load (implicit wait) , Requests: time to wait between page loads
    'scrollDownPage' : False,  # whether to scroll down the page to find more products, e.g. Topshop
    # url and redirect parameters
    'urlSwitch' :  '',    #add an option to the url,e.g. 48 items per page
    'invalidRedirect' : '', # if the redirected url contains this value the product pages do not load correctly
    #define any buttons to be clicked on screen
    'buttonXpaths' : ['//*[@id="cookieAgreementSubmit"]/span', '/html/body/div[10]/div[1]/div[3]/div[2]/div[2]/p',
                    '//*[@id="chrome-header"]/header/div[1]/div/div/button', '//*[@id="onetrust-accept-btn-handler"]' ], # a list of XPaths
    'buttoniFrameXpaths' : [],
    'resetCookies' : True,
    'proxy_location': '',
    'vpn_location': 'GBWS',
    'luminati_proxy': '',
    })
            
#  RETAILER SPECIFIC - REDEFINE THE PRODUCTDATAFRAME METHODS
def new_get_description(self):
    info = self.webSoup.find('p')
    if info != None : 
        d = info
        if d != None: 
            description = d.text.replace('\n', '').replace('\r', '').strip()
            self.description = description        
            
def new_get_url(self):
    info = self.webSoup.find('a')
    if info != None : 
        u = info
        if u != None :
            url = u.get('href')
            self.url = url

def new_get_skuid(self):
    info = self.webSoup
    skuid = info.get('id')
    if skuid != None:
        skuid = skuid.split('-')[1]
        self.skuid = skuid
        
def new_get_imageurl(self):
    #also can get via data-productid
    i = self.webSoup.find('img')
    if i != None:  
        imgurl =  i.get('src')
        self.imgurl = imgurl
        
def new_get_banner(self):
    #TODO
    info = self.webSoup.find('div', {'class' : 'info'})
    if info != None : 
        b = info.find('p', class_="t200v2-message")
        if b != None: 
            banner = b.text
            self.banner = banner
            
def new_get_ticket_previous_price(self):
    ticketprice = None
    prevprice = None
    #sale price (ticket price)
    origprice = self.webSoup.find('span' , {'class' : re.compile("originalPrice") })
    saleprice = self.webSoup.find('span' , {'class' : re.compile("reducedPrice") })
    #origprice sometimes has the text RRP at the beginning
    if origprice != None:
        origprice = origprice.get_text().strip()
        origprice = origprice[origprice.find('£'):]
    if saleprice == None :
        ticketprice = origprice
        prevprice = origprice
    else:  
        ticketprice = saleprice.get_text().strip()
        prevprice = origprice         
    self.ticketprice = extractPrice(ticketprice, "£")
    self.prevprice = extractPrice(prevprice, "£")
        
def new_get_promo_message(self):
    #TODO
    info = self.webSoup.find('div', {'id' : 'header_espot'})
    if info != None : 
        pm = info.find('img')
        if pm != None: 
            self.promomessage = pm.get('alt')
            
def new_find_all_products(self):
    products = self.webSoup.findAll('article' )
    print('num products', len(products))
    self.products = products
    
def new_output_description_file(self):
    params = self.params
    opfpath = params['opfpath'] +'/Text'
    url = self.url
    driver = self.driver
    if url != None:
        fname = self.skuid +'.txt'
        fname = removeDisallowedFilenameChars(fname)
        output_name =  Path(opfpath)/ (fname)
        if not os.path.isfile(output_name): # fetch if not already stored
            #this is fhe best way, but takes a long time because of waiting for pages to load
            #quicker to use requests but requests does not work with Asos ?
            htmlPages,  driver, currentUrl, problemUrl = getHtml(url, driver, params) 
            html = htmlPages[0]
            #this is the quicker way using requests, but does not work for some websites
            #TODO try requestshtml
            #response = requests.get(url)
            #html = response.text
            webSoup = bs4.BeautifulSoup(html , params['htmlParser'])
            info = webSoup.find('div', {'class' : 'product-hero'})
            if info != None:
                t = info.get_text()
                output_text = t.replace('\n', '').replace('\r', '').strip()
                if t != None:
                    with open(output_name, 'w',  encoding='utf-8-sig') as f:
                        f.write(output_text)

def new_get_brand(self):
    global listofBrands
    description = self.description
    found = False
    if listofBrands != [] and description != None :
        for b in listofBrands:
            if b.lower() in description.lower():
                found = True
                brand = b
    if not found:
        brand = "na"
    self.brand =  brand    


#find the next page link from the current page
def new_find_next_page_from_webpage(self):
    #driver = self.driver # for access to buttons on the page etc
    nextPage = None
    if self.params['findNextPage'] == True:
        #info = self.webSoup.find("div", { 'class': 'zCgWNEA' })  
        info = self.webSoup.find("a", { 'data-auto-id': 'loadMoreProducts' }) 
        if info != None:
            next_link = info
            # print('next_link', next_link)
            if next_link != None:
                nextPage =  next_link.get("href")  
            #if the nextPage url contains "ocid" the nextPage has swicthed to a new category (cid) , e.g. occurs at the end of outlet/dresses, 
            # then switches to outlet all,
                if nextPage != None and nextPage.find("ocid") >=0 :
                    nextPage = None
        print('Next Page' , nextPage)
    self.nextPage = nextPage    
            

#%% Specific to Asos
    
def fetch_brand_names(params):    
    ### ASOS specific get a list of brands
    brandsurls = ['https://www.asos.com/women/a-to-z-of-brands/cat/?cid=1340&nlid=ww|brands|top+brands',
                 'https://www.asos.com/men/a-to-z-of-brands/cat/?cid=1361&nlid=mw|brands|top+brands' ]
    driver = None
    brands = []
    for webPage in brandsurls:
            print('processing category web page for menus ', webPage)
            #set up dummy link variable as not used in this context
            webHtml, driver, currentUrl, problemUrl =  getHtml(webPage+params['urlSwitch'], driver, params)
            webHtml = webHtml[0]
            webSoup = bs4.BeautifulSoup(webHtml , params['htmlParser'])
            #print(webSoup.prettify())
            brandlinks =  webSoup.find_all('li',{ "class" : "vqk6pTa"})
            for b in brandlinks:
                if b.text.strip() not in brands:
                    brands.append(b.text.strip())
    #driver.quit()
    brands = [x.lower() for x in brands]
    return brands

#%%

    
    
#redefine the methods specific to this retailer        
ProductDataFrame.get_description =  new_get_description
ProductDataFrame.get_url = new_get_url
ProductDataFrame.get_skuid = new_get_skuid
ProductDataFrame.get_imageurl = new_get_imageurl
ProductDataFrame.get_banner = new_get_banner
ProductDataFrame.get_brand = new_get_brand
ProductDataFrame.get_ticket_previous_price = new_get_ticket_previous_price
ProductDataFrame.output_description_file = new_output_description_file
WebPageItems.get_promo_message = new_get_promo_message
WebPageItems.find_all_products = new_find_all_products
WebPageItems.find_next_page_from_webpage = new_find_next_page_from_webpage
#new methods can be added using the same approach, e.g. WebPageItems.newmethod = newmethod


#%%    RETAILER SPECIFIC - IDENTIFY WHICH OPTIONAL METHODS TO RUN 

methods_to_run = {
        'output_product_to_screen' : False,
        'output_product_values_to_screen' : False,
        'output_image_to_file' : False,
        'output_description_to_file' : False,
        'output_webpage_to_file' : False,
        }
     
            
# %% Main Code

    
def main():
    filename = __file__
    startPage = 0 # EDIT HERE FOR SET_UP AND TESTING to processs a sub range of weblinks
    endPage = 1 # EDIT HERE
    logpath =  params['opfpath'] + 'AA_LOGS/'
    init_logging(filename, logpath)
    # ASOS SPECIFIC
    global listofBrands
    listofBrands = fetch_brand_names(params)
    #print(listofBrands)
    #END OF ASOS SPECIFIC
    scrapeWebPages( startPage, endPage, linkurls, params, filename, methods_to_run)
    return
    
 
if __name__ == '__main__':
    main()    