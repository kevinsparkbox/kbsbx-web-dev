import __init__ # set up the import paths
import bs4
import datetime as dt
import pandas as pd
import time
import json
from pathlib import Path
import requests
import html5lib
import os
import re
from math import ceil

from selenium.webdriver.common.keys import Keys

from CoreScraper import scrapeWebPages, extractPrice
from CoreScraper_getHtml import WebPageItems, ProductDataFrame
from CoreScraperConstants import toppath,  default_params

#  RETAILER SPECIFIC CONTROL PARAMETERS
# Set up the top path as parent folder of results, Note this is overridden if supplied as an argument when called as a main function
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape TEST/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape TEST/'


#set up multiple urls for finding links, first url is mainurl (code filtters duplicates)
linkurls = ['https://www.superdry.com']

params = default_params.copy()
params.update( {
    'mainurl' : linkurls[0],
    'opfpath' : toppath, # can be overidden by arguments, applied in in scrapeWebPages
    #define valid links and how to interpret links
    'sitemap_links': ['https://www.superdry.com/component/sitemap/?view=sitemap'],  # if empty search through the linkurls
    'validtags' : ['sale', 'outlet', 'womens', 'mens'],  # a valid link must contain one of these strings
    'invalidtags' : ["javascript"], # if a link contains one of these it is invalid
     'resetduplicatelinks' : True, # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks' : ['https://www.tbc.com'], # force any duplicate links to be ignored
    'starting_links' : [],    #additional links to process  #set any starting links that will not be picked up by the find starting pages process
    'tagref' : 'a', # look for hrefs in this class
    'remove_switches_link' : True,  # if True, remove ? and # switches from link (recommended unless required)
    'departmentlevel' : 3, #the nth level in the link
    'categorylevel' : 4,  #the nth level in the link
    'subcategorylevel' : 5,  #the nth level in the link
    'minimumdepth' : 4 ,  #a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    #define how the web page is accessed
    'useRequests' : False, # whether or not to use the requests module, if False use the chromium driver Browser
    'findNextPage' : True, # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    'htmlParser' :  'lxml', # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    #webdriver parameters
    'webdriverpath' : 'dummy' , # path to the webdriver executable SET UP IN CoreScraper.py
    'displayOff' : False,  # whether display is switched off or visible
    'displayMinimised' :  False,  # whether to minimise the window if display is on
    'resetDriver' : False,  #whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'waitForPageLoad' : 2, #typ set to half webPageWait, when scrolling, time to wait for page to load before comparing 2 subsequent loads to see if scrolled page has finished loading
    'webPageWait' : 2,  # time between subsequent web pages (next link, next page, next scroll down page)
    'webPageScrollWait' : 1 , #wait between page scrolls
    'scrollDownPage' : True,  # whether to scroll down the page to find more products
    'end_page_scroll' : True, #type of scroll
    # url and redirect parameters
    'urlSwitch' :  '',    #add an option to the url,e.g. 48 items per page
    'invalidRedirect' : '', # if the redirected url contains this value the product pages do not load correctly
    #define any buttons to be clicked on screen
    'buttonXpaths' : ['/html/body/div[2]/div/a[2]' , '//*[@id="bx-element-1253258-Wltw0Lj"]/button'], # a list of XPaths
    'buttoniFrameXpaths' : [],
    'proxy_location': '',  # Give multiple country like this, 'GB|US'. country codes: https://laendercode.net/en/2-letter-list.html
    # 'popup_remove_button': ['/html/body/div[13]/div[1]/div[1]', '/html/body/div[14]/div[1]/div[1]'],  # give xpath of popup removal button
    'slow_page_scroll': False,
    'show_proxy_info': True,
    'vpn_location': 'GBWS',
    'wrong_country_popup': ['/html/body/div[13]/div[1]/div[1]', '/html/body/div[14]/div[1]/div[1]'],  # give xpath of popup removal button
    'popup_remove_button': ['//*[@id="bx-element-1253258-Wltw0Lj"]/button'],  # give xpath of popup removal button
    'popup_remove_button_wait': 1 , #time to wait for pop up to appear and be removed
    'descriptions_reset_interval' : 50,

})

            
#%%  RETAILER SPECIFIC - REDEFINE THE PRODUCTDATAFRAME METHODS
def new_get_description(self):
    info = self.webSoup.find('img')
    if info != None : 
        d = info
        if d != None: 
            description = d.get("alt")
            self.description = description        
            
def new_get_url(self):
    info = self.webSoup.find('a', class_="product-details__name")
    if info != None : 
        u = info
        if u != None :
            url = self.mainurl + u.get('href')
            self.url = url

#def new_get_skuid_url(self):
    #there is an alpha numeric skuid but limited characters ? reused ?  so extract the numeric id from the url
    #if self.url != None:
    #    skuid = self.url.split('-')[-1]
    #    self.skuid = skuid
        
def new_get_skuid_data(self):
    info = self.webSoup
    if info != None : 
        skuid = info.get("id")
        if skuid != None:
            skuid = skuid[7: ]
        self.skuid = skuid
        
def new_get_reviewscore(self):
    info = self.webSoup.find('div', {'class' : 'mod-product-info'})
    if info != None : 
        r = info.find('a', {'class' : 'mod-product-link feefoProduct'} )
        if r != None :
            reviewscore = r.get('feefo-score')
            self.reviewscore = reviewscore

def new_get_reviewcount(self):
    info = self.webSoup.find('div', {'class' : 'mod-product-info'})
    if info != None : 
        r = info.find('span', {'class' : 'nbReviews'} )
        if r != None :
            reviewcount = r.get_text().replace('\n', ' ').replace('\r', ' ').replace('(', ' ').replace(')', ' ').strip()
            self.reviewcount = reviewcount
        
def new_get_imageurl(self):
    i = self.webSoup.find('img')
    if i != None:  
        if i.has_attr("data-src"):
            imgurl =  i.get('data-src')
        else:
            imgurl =  i.get('src')
        self.imgurl = imgurl
      
def new_get_banner(self):
    b = self.webSoup.find('div', class_="product-listing__badge")   
    if b != None : 
        banner =  b.get_text().replace('\n', ' ').replace('\r', ' ').strip()
        self.banner = banner
        
def new_get_brand(self):
    b = self.webSoup.find('div', {'class' : 'dvtSticker--under pCategory desktop'})
    if b != None : 
        brand = b.get_text().replace('\n', ' ').replace('\r', ' ').strip()
        self.brand = brand
            
def new_get_ticket_previous_price(self):
    ticketprice = None
    prevprice = None

    #for sale prices ...    
    wasprice = self.webSoup.find('del',  { 'class' : "was-price"  })
    currprice = self.webSoup.find('span',  { 'class' : "price"  })
    
    if wasprice != None:
        prevprice =  wasprice.get_text()
        prevprice = prevprice.split('£')[-1]
        if currprice != None:
            ticketprice =  currprice.get_text()
            ticketprice = ticketprice.split('£')[-1] 
    else:
        #regular prices
        currprice = self.webSoup.find('span',  { 'class' : "product-details__price"  })
        if currprice != None:
            ticketprice =  currprice.get_text()
            ticketprice = ticketprice.split('£')[-1]
            prevprice = ticketprice
  
    self.ticketprice = extractPrice(ticketprice, "£")
    self.prevprice = extractPrice(prevprice, "£") 
         
def new_get_promo_message(self):
    info = self.webSoup.find('div', {'id' : 'header_espot'})
    if info != None : 
        pm = info.find('img')
        if pm != None: 
            self.promomessage = pm.get('alt')
            
def new_find_all_products(self):
    #products = self.webSoup.find_all('div', { "data-product-id" : True } )
    
    productsinfo = self.webSoup.find('div', { "id" : "product-list" } )
    if productsinfo != None :
        products = productsinfo.find_all('div', {'class' :  re.compile("col*") , 
                                                 'id' : True })
    else:
        products =[]
    
    print('num products', len(products))
    self.products = products
    
#define the method that is used to find the product description from the individual product page url
def new_get_product_description(self):
    pi_t = ""
    product_info_text_block = self.webSoup.find('div', { 'class' : 'description-container'})
    if product_info_text_block != None:
        #print('YYYY product info', product_info_text_block.prettify())
        blocks = product_info_text_block.find_all('p')
        if len(blocks) > 0:
            pi_t =  blocks[-1].get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
    self.long_description =   pi_t 
    #get the colour description - clothing products
    info = self.webSoup.find('span', {'class' : 'product_color'})
    if info != None:
        #print('XXXX Colour Info', info)
        colour = info.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip().split('-')[-1]
        self.colour = colour
                        
    #TODO fetch the size description data = [list of strings]
    self.size = "size  desc"
    self.availability = "availability desc"

#define the method that is used to find the main image url from the individual product page url
def new_get_product_imgage_url(self):
    image_info = self.webSoup.find('div', {'class' : 'slick-active'})
    if image_info != None:
        images = image_info.find('img')
        if images != None:
            url = images.get("src")
            self.imgurl =  url 
        #url2 = image_url.find('img')["srcset"]
        #if url != None:
            #url = 'https:' + url
        #self.imgurl = url
#find the next page link from the current page, use the buttons to load the next link, then get the link and store as the Next Page
#for editing the url does not work for Superdry


def new_find_next_page_from_webpage(self):
    def find_click_load_more_button(driver):
        counter1 = 0
        counter2 = 0
        button_clicked = False
        try:
            load_more_counter = driver.find_element_by_id('load-more-counter').text
            # load_more_counter = driver.execute_script("return document.getElementById('load-more-counter').innerHTML")
            print('Load More Counter ' , load_more_counter)
            counter_list = re.findall(r'[0-9]+', str(load_more_counter))
            print('counter_list: ', counter_list)
            if len(counter_list) > 0:
                counter1 = int(counter_list[0])
                counter2 = int(counter_list[1])
            else:
                counter1 = 0
                counter2 = 0
        except:
            button_clicked = False
        if counter1 <= counter2 and counter1 > 0:
            try:
                driver.execute_script('categoryPageController.loadMore()')
                time.sleep(1)
                driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.END)
                time.sleep(1)
                driver.find_element_by_tag_name('body').send_keys(Keys.PAGE_UP)
                time.sleep(1)
                button_clicked = True
            except:
                button_clicked = False
        return button_clicked


            
    
    driver = self.driver # for access to buttons on the page etc

    #if there are more items to load, load them, else set Next Page to None:
    nextPage = None

    #NOTE: quite often the total is greater than the number of items loaded, which generates a "Load More X items" button at the end which does not work
    # as there are no items to load, this is captured, waiting for the url to upodate, but gracefuly exiting after 20 attempts if it does not

    print('Clicking the Load More Button ')
    button_clicked = find_click_load_more_button(driver) # returns false if the button is not present
    # button_clicked = False
    if not button_clicked:
        #try again just in case the page did not load properly, seems to happen with Superdry
        button_clicked = False
        attempts = 0
        while not button_clicked and attempts < 2:
        # while not button_clicked and attempts < 3:
            print('reclicking load more button, not yet ready')
            time.sleep(1)
            attempts = attempts + 1
            button_clicked = find_click_load_more_button(driver)   # returns true while the button is present and clicked , False if not present and loaded
    if button_clicked:
        print('clicked button')
        nextPage = "DO_NOT_RELOAD_PAGE"

    print('Next Page' , nextPage)
    self.nextPage = nextPage
    

#redefine the methods specific to this retailer        
ProductDataFrame.get_description =  new_get_description
ProductDataFrame.get_url = new_get_url
ProductDataFrame.get_skuid = new_get_skuid_data
ProductDataFrame.get_imageurl = new_get_imageurl
ProductDataFrame.get_banner = new_get_banner
ProductDataFrame.get_ticket_previous_price = new_get_ticket_previous_price
ProductDataFrame.get_brand = new_get_brand
ProductDataFrame.get_reviewscore = new_get_reviewscore
ProductDataFrame.get_reviewcount = new_get_reviewcount
ProductDataFrame.get_product_description = new_get_product_description
ProductDataFrame.get_product_imgage_url = new_get_product_imgage_url
WebPageItems.get_promo_message = new_get_promo_message
WebPageItems.find_all_products = new_find_all_products
WebPageItems.find_next_page_from_webpage = new_find_next_page_from_webpage
#new methods can be added using the same approach, e.g. WebPageItems.newmethod = newmethod


#%%    RETAILER SPECIFIC - IDENTIFY WHICH OPTIONAL METHODS TO RUN FOR EACH PRODUCT

methods_to_run = {
        'output_product_to_screen' : False,
        'output_image_to_file' : True,
        'output_description_to_file' : True,
        'output_product_values_to_screen' : False,
        'output_webpage_to_file' : False,
        }
     

# %% Main Code    
    
def main():
    filename = __file__
    startPage = 22  # EDIT HERE FOR SET_UP AND TESTING to processs a sub range of weblinks
    endPage = 23 # EDIT HERE
    scrapeWebPages( startPage, endPage, linkurls, params, filename, methods_to_run)
    return
    
 
if __name__ == '__main__':
    main()