import __init__ # set up the import paths
import bs4
import datetime as dt
import pandas as pd
import time
import json
from pathlib import Path
import requests
import html5lib
import os
import re
from math import ceil

from CoreScraper import scrapeWebPages, extractPrice
from CoreScraper_getHtml import WebPageItems, ProductDataFrame
from CoreScraperConstants import toppath,  default_params

#  RETAILER SPECIFIC CONTROL PARAMETERS
# Set up the top path as parent folder of results, Note this is overridden if supplied as an argument when called as a main function
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape TEST/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape TEST/'

# set up multiple urls for finding links, first url is mainurl (code filtters duplicates)
linkurls = ['https://www.johnlewis.com', 'https://www.johnlewis.com/clearance/c6000270062', 'https://www.johnlewis.com/home-garden/c500006',
'https://www.johnlewis.com/furniture-lights/c9780211221', 'https://www.johnlewis.com/electricals/c500001', 'https://www.johnlewis.com/women/c50000298',
"https://www.johnlewis.com/women/women's-lingerie-underwear/c600001812",
'https://www.johnlewis.com/men/c50000300', 'https://www.johnlewis.com/beauty/c50000296', 'https://www.johnlewis.com/baby-child/c5000010',
'https://www.johnlewis.com/sport-leisure/c5000011', 'https://www.johnlewis.com/gifts/c500009']

linkurls = ['https://www.johnlewis.com/women/c50000298' ]

params = default_params.copy()
params.update({
    'mainurl': 'https://www.johnlewis.com',
    'opfpath': toppath,  # can be overidden by arguments, applied in in scrapeWebPages
    # define valid links and how to interpret links
    'validtags': ['browse'],  # a valid link must contain one of these strings
    'invalidtags': ['store', 'customer-services', 'brand', 'insurance', 'content', 'survey'],  # if a link contains one of these it is invalid
    'resetduplicatelinks': True,
    # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks': ['https://www.tbc.com'],  # force any duplicate links to be ignored
    'starting_links': [],
    # additional links to process  #set any starting links that will not be picked up by the find starting pages process
    'tagref': 'a',  # look for hrefs in this class
    'remove_switches_link': True,  # if True, remove ? and # switches from link (recommended)
    'departmentlevel': 4,  # the nth level in the link
    'categorylevel': 5,  # the nth level in the link
    'subcategorylevel': 6,  # the nth level in the link
    'minimumdepth': 5,  # a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    'max_link_depth' : 1,  # the depth of url links to search in the links searching process
    # define how the web page is accessed
    'useRequests': False,  # whether or not to use the requests module, if False use the chromium driver Browser
    'findNextPage': True,
    # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    # 'htmlParser' :  'lxml', # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    'htmlParser': 'lxml',
    # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    # webdriver parameters
    'webdriverpath': 'dummy',  # path to the webdriver executable SET UP IN CoreScraper.py
    'displayOff': False,  # whether display is switched off or visible
    'displayMinimised': False,  # whether to minimise the window if display is on
    'resetDriver': True,  # whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'waitForPageLoad': 2.5,
    # typ set to half webPageWait, when scrolling, time to wait for page to load before comparing 2 subsequent loads to see if scrolled page has finished loading
    'webPageWait': 1,
    # Browser: time to wait for page to fully load (implicit wait) , Requests: time to wait between page loads
    'scrollDownPage': True,  # whether to scroll down the page to find more products, e.g. Topshop
    # url and redirect parameters
    'urlSwitch': '',  # add an option to the url,e.g. 48 items per page
    'invalidRedirect': '',  # if the redirected url contains this value the product pages do not load correctly
    # define any buttons to be clicked on screen
    'buttonXpaths': ['//*[@id="root"]/div[1]/div/a[1]', '//*[@id="close"]',
                     '//*[@id="pecr-cookie-banner-wrapper"]/div/div[1]/div/div[2]/button[1]',
                        '//*[@id="close"]'],  # a list of XPaths
    'buttoniFrameXpaths': [],
    'proxy_location': '',
    # Give multiple country like this, 'GB|US'. country codes: https://laendercode.net/en/2-letter-list.html
    'vpn_location': 'GBWS',

})


# %%  RETAILER SPECIFIC - REDEFINE THE PRODUCTDATAFRAME METHODS
def new_get_description(self):
    info = self.webSoup.find('h2')
    if info != None:
        d = info
        if d != None:
            description = d.get_text().replace('\n', '').replace('\r', '').strip()
            self.description = description


def new_get_url(self):
    info = self.webSoup
    if info != None:
        u = info.find('a')
        if u != None:
            url = u.get('href')
            self.url = self.mainurl + url


def new_get_skuid(self):
    info = self.webSoup
    skuid = info.get('data-product-id')
    if skuid != None:
        self.skuid = skuid


def new_get_imageurl(self):
    info = self.webSoup.find('img')
    if info != None:
        i = info
        if i != None:
            imgurl = i.get('src')
            self.imgurl = imgurl


def new_get_banner(self):
    banner_info = self.webSoup.find('p', {'class': re.compile("promo-messages")})
    if banner_info != None:
        self.banner = banner_info.get_text().strip()


def new_get_ticket_previous_price(self):
    """
    website price format has changed a couple of times
    
    """

    ticketprice = None
    prevprice = None

    product_info = self.webSoup.find('div', {'data-test': 'product-image-container'})
    if product_info != None:
        price_info = product_info.find('div', {'class': re.compile("price")})
        if price_info != None:
            #prices = price_info.get_text().strip().split(',')
            pricetext = price_info.get_text().strip()
            prices = pricetext.split(', ') #do not split on only comma as it is used as thousands separator as well as separating was then text
            if len(prices) == 1:
                ticketprice = prices[0]
                prevprice = ticketprice
            else:
                for p_text in prices:
                    if 'was' in p_text.lower():
                        prevprice = p_text
                    if 'now' in p_text.lower():
                        ticketprice = p_text

            #convert the ticket price and prev price to numeric float
            ticketprice = extractPrice(ticketprice, "£")
            prevprice = extractPrice(prevprice, "£")

            # if a Save X% (price includes saving x%) banner and there is no prev price defined - extract the % saving and calculate the prev price
            # or if the price is a £ off but no prev price defined calculate the prev price
            banner_info = self.webSoup.find('p', {'class': re.compile("promo-messages")})
            if banner_info != None:
                banner = banner_info.get_text().strip()
                if banner.find('%') > -1 and ( prevprice == ticketprice ):
                        price_text = banner.split()
                        for p in price_text:
                            if p.find("%") > -1:
                                priceval = p
                        # % off
                        pcoff = int(priceval[: priceval.find('%')])
                        orig_price = ticketprice / (1 - (pcoff / 100))
                        prevprice = round(orig_price, 2)

                if banner.find('£') > -1 and ( prevprice == ticketprice ) :
                        # £ off
                        price_text = banner.split()
                        for p in price_text:
                            if p.find("£") > -1:
                                save_price = p
                        orig_price = extractPrice(save_price, "£") + ticketprice
                        prevprice = orig_price

    self.ticketprice = extractPrice(ticketprice, "£")
    self.prevprice = extractPrice(prevprice, "£")


def new_get_promo_message(self):
    # TODO
    info = self.webSoup.find('div', {'id': 'header_espot'})
    if info != None:
        pm = info.find('img')
        if pm != None:
            self.promomessage = pm.get('alt')


def new_find_all_products(self):
    products = self.webSoup.find_all('section', {'class': re.compile('product-card')})
    print('num products', len(products))
    self.products = products


def new_output_description_file(self):
    # TODO
    pass


def new_get_brand(self):
    pass


# find the next page link from the current page
def new_find_next_page_from_webpage(self):
    # driver = self.driver # for access to buttons on the page etc
    findNextPage = params['findNextPage']
    nextPage = None
    if findNextPage:
        # set next page if valid
        page_info = self.webSoup.find('li', {'class': re.compile('pagination__next') })
        if page_info != None:
            info = page_info.find('a')
            if info != None:
                np = info
                if np != None:
                    nextPage = self.mainurl + np.get("href")

    # with john lewis url page number is much larger than page number displayed on web site,
    # e.g. website displayed page 22 = url page 169
    #(Although this seems to have changed Jan 2020 - now match)

    # needs to scroll down to load the last 24 products on each page

    print('Next Page', nextPage)
    self.nextPage = nextPage


# %%


# redefine the methods specific to this retailer
ProductDataFrame.get_description = new_get_description
ProductDataFrame.get_url = new_get_url
ProductDataFrame.get_skuid = new_get_skuid
ProductDataFrame.get_imageurl = new_get_imageurl
ProductDataFrame.get_banner = new_get_banner
ProductDataFrame.get_ticket_previous_price = new_get_ticket_previous_price
ProductDataFrame.output_description_file = new_output_description_file
WebPageItems.get_promo_message = new_get_promo_message
WebPageItems.find_all_products = new_find_all_products
WebPageItems.find_next_page_from_webpage = new_find_next_page_from_webpage
# new methods can be added using the same approach, e.g. WebPageItems.newmethod = newmethod


# %%    RETAILER SPECIFIC - IDENTIFY WHICH OPTIONAL METHODS TO RUN

methods_to_run = {
    'output_product_to_screen': False,
    'output_product_values_to_screen': False,
    'output_image_to_file': False,
    'output_description_to_file': False,
    'output_webpage_to_file': False,
}


# %% Main Code


def main():
    filename = __file__
    startPage = 5  # EDIT HERE FOR SET_UP AND TESTING to processs a sub range of weblinks
    endPage = 6  # EDIT HERE
    scrapeWebPages(startPage, endPage, linkurls, params, filename, methods_to_run)
    return


if __name__ == '__main__':
    main()
