import __init__ # set up the import paths
import bs4
import datetime as dt
import pandas as pd
import time
import json
from pathlib import Path
import requests
import html5lib
import os
import re
from math import ceil

from CoreScraper import scrapeWebPages, extractPrice
from CoreScraper_getHtml import WebPageItems, ProductDataFrame, clickButton
from CoreScraperConstants import toppath,  default_params

#  RETAILER SPECIFIC CONTROL PARAMETERS
# Set up the top path as parent folder of results, Note this is overridden if supplied as an argument when called as a main function
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape TEST/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape TEST/'


# set up multiple urls for finding links, first url is mainurl (code filtters duplicates)
linkurls = ['https://www.blacks.co.uk/']

params = default_params.copy()
params.update({
    'mainurl': linkurls[0],
    'opfpath': toppath,  # can be overidden by arguments, applied in in scrapeWebPages
    # define valid links and how to interpret links
    'sitemap_links': [],  # if empty search through the linkurls
    'validtags': ['blacks'],  # a valid link must contain one of these strings
    'invalidtags': ['service', 'paypal', 'faqs', 'memebrs', 'review', 'promise', 'redirect', 'help', 'index', 'cookie', 'international', 'content', 'blog'],
    # if a link contains one of these it is invalid
    'resetduplicatelinks': True,
    # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks': ['https://www.tbc.com'],  # force any duplicate links to be ignored
    'starting_links': [],
    # additional links to process  #set any starting links that will not be picked up by the find starting pages process
    'tagref': 'a',  # look for hrefs in this class
    'remove_switches_link': False,  # if True, remove ? and # switches from link (recommended unless required)
    'departmentlevel': 3,  # the nth level in the link
    'categorylevel': 4,  # the nth level in the link
    'subcategorylevel': 5,  # the nth level in the link
    'minimumdepth': 4,
    # a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    # define how the web page is accessed
    'useRequests': False,  # whether or not to use the requests module, if False use the chromium driver Browser
    'findNextPage': True,
    # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    'htmlParser': 'lxml',
    # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    # webdriver parameters
    'webdriverpath': 'dummy',  # path to the webdriver executable SET UP IN CoreScraper.py
    'displayOff': False,  # whether display is switched off or visible
    'displayMinimised': False,  # whether to minimise the window if display is on
    'resetDriver': True,  # whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'waitForPageLoad': 2,
    # typ set to half webPageWait, when scrolling, time to wait for page to load before comparing 2 subsequent loads to see if scrolled page has finished loading
    'webPageWait': 2,  # time between subsequent web pages (next link, next page, next scroll down page)
    'scrollDownPage': False,  # whether to scroll down the page to find more products, e.g. Topshop
    # url and redirect parameters
    'urlSwitch': '',  # add an option to the url,e.g. 48 items per page
    'invalidRedirect': '',  # if the redirected url contains this value the product pages do not load correctly
    # define any buttons to be clicked on screen
    'buttonXpaths': ['//*[@id="panelModalBannerCookies"]/div/a', ''],  # a list of XPaths
    # 'buttonXpaths' : [ '' ], # a list of XPaths
    'buttoniFrameXpaths': [],
    'proxy_location': '',  # Give multiple country like this, 'GB|US'. country codes: https://laendercode.net/en/2-letter-list.html
    'show_proxy_info': False,
    'vpn_location': 'GBWS',
    'luminati_proxy': '',
})


# %%  RETAILER SPECIFIC - REDEFINE THE PRODUCTDATAFRAME METHODS
def new_get_description(self):
    info = self.webSoup.find('span', {'class': 'product-info-holder'})
    if info != None:
        info = info.find('h2')
        if info != None:
            d = info.find_all('span')
            if d != None and len(d)>1:
                description = d[1].get_text()
                self.description = description


def new_get_url(self):
    info = self.webSoup.find('a')
    if info != None:
        u = info.get("href")
        if u != None:
            url = str(self.mainurl) + u
            self.url = url


def new_get_skuid(self):
    info = self.webSoup.parent
    if info != None:
        id = info.get("data-id")
        self.skuid = id


def new_get_imageurl(self):
    i = self.webSoup.find('img', {'class': 'product-img'})
    if i != None:
        imgurl = i.get('src')
        self.imgurl = imgurl


def new_get_brand(self):
    info = self.webSoup.find('span', {'class': 'product-info-holder'}).find('h2').find_all('span')[0]
    if info != None:
        d = info
        if d != None:
            brand = d.get_text()
            self.brand = brand


def new_get_stock(self):
    pass


def new_get_banner(self):
    info = self.webSoup.find('span', {'class': 'product-info-holder'}).find('span', {'class': 'save'})
    if info != None:
        banner = info.get_text()
        self.banner = banner


def new_get_ticket_previous_price(self):
    ticketprice = None
    prevprice = None
    priceinfo = self.webSoup.find('span', {'class': 'product-info-holder'})

    if priceinfo != None:
        #sale prices
        ticketprice_tag = priceinfo.find('span', {'class': 'special'})
        prevprice_tag = priceinfo.find('span', {'class': 'was-price'})
        if ticketprice_tag != None:
            if ticketprice_tag != None:
                ticketprice = ticketprice_tag.text
            if prevprice_tag != None:
                prevprice = prevprice_tag.text
        #regular price
        else:
            ticketprice_tag = priceinfo.find_all('span')
            if ticketprice_tag != None:
                ticketprice = ticketprice_tag[-1].text
                prevprice = ticketprice

    self.ticketprice = extractPrice(ticketprice, "£")
    self.prevprice = extractPrice(prevprice, "£")



def new_get_reviewcount(self):
    info = self.webSoup.find('span', {'class': 'product-info-holder'}).find('div', {'class': 'reviews'})
    if info != None:
        reviews = info.find('span', {'class': 'reviewers'})
        if reviews != None:
            r = reviews.get_text()
            if r != None:
                reviewcount = str(r).split(' ')[0]
                self.reviewcount = reviewcount

def new_get_reviewscore(self):
    info = self.webSoup.find('span', {'class': 'product-info-holder'}).find('div', {'class': 'reviews'})
    if info != None:
        reviews = info.find('div', {'class': 'review-stars'})
        if reviews != None:
            r = reviews.find('img')
            if r != None:
                reviewscore_text = r.get('alt')
                if reviewscore_text != None:
                    reviewscore = str(reviewscore_text).split(' ')[0]
                    self.reviewscore = reviewscore


def new_get_promo_message(self):
    pass


def new_find_all_products(self):
    try:
        products = self.webSoup.find_all('div', {'class': "product-item-holder"})
    except:
        products = []

    print('num products', len(products))
    self.products = products


#define the method that is used to find the product description from the individual product page url
def new_get_product_description(self):
    pi_t = ""
    product_info_text_block = self.webSoup.find('div', { 'class' : 'product-description'})
    if product_info_text_block != None:
        #print('YYYY product info', product_info_text_block.prettify())
        block1 = product_info_text_block.find('div', {'class' : 'group'})
        if block1 != None:
            pi_t = block1.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
            if len(pi_t) == 0: # no description, force to be a blank space so that it does not generate a warning
                pi_t = " "
    self.long_description =   pi_t 
    #get the colour description - clothing products
    info = self.webSoup.find('table', {'class' : 'template_table'})
    if info != None:
        #print('XXX TEMPLATE TABLE', info.prettify())
        features = info.find_all('tr')
        if features != None:
            for f in features:
                label = f.find('label')
                if label != None:
                    if label.get_text() == "Colour" or label.get_text() == "Colours" :
                        colour = f.find('td').get_text()
                        self.colour = colour.replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
                        break

    #TODO fetch the size description data = [list of strings]
    self.size = "size  desc"
    self.availability = "availability desc"

#define the method that is used to find the main image url from the individual product page url
def new_get_product_imgage_url(self):
    image_info = self.webSoup.find('div', {'class' : 'slick-active'})
    if image_info != None:
        images = image_info.find('img', {'class' : ' lazyloaded'})
        if images != None:
            url = images.get("src")
            self.imgurl =  url 
        #url2 = image_url.find('img')["srcset"]
        #if url != None:
            #url = 'https:' + url
        #self.imgurl = url


# find the next page link from the current page
def new_find_next_page_from_webpage(self):
    findNextPage = params['findNextPage']
    nextPage = None
    if findNextPage:
        # set next page if valid
        page_info = self.webSoup.find('li', {'class': 'next-page'})
        if page_info != None:
            info = page_info.find('a', {'class', 'next-prev'})
            if info != None:
                np = info
                if np != None:
                    nextPage = self.mainurl + np.get("href")

    print('Next Page', nextPage)
    self.nextPage = nextPage

# define the function to open the product url and extract text description and image if not already extracted
def new_output_text_and_image_file(self):
    pass


# redefine the methods specific to this retailer
ProductDataFrame.get_description = new_get_description
ProductDataFrame.get_url = new_get_url
ProductDataFrame.get_skuid = new_get_skuid
ProductDataFrame.get_imageurl = new_get_imageurl
ProductDataFrame.get_banner = new_get_banner
ProductDataFrame.get_brand = new_get_brand
ProductDataFrame.get_stock = new_get_stock
ProductDataFrame.get_reviewscore = new_get_reviewscore
ProductDataFrame.get_reviewcount = new_get_reviewcount
ProductDataFrame.get_ticket_previous_price = new_get_ticket_previous_price
ProductDataFrame.get_product_description = new_get_product_description
ProductDataFrame.get_product_imgage_url = new_get_product_imgage_url
WebPageItems.get_promo_message = new_get_promo_message
WebPageItems.find_all_products = new_find_all_products
WebPageItems.find_next_page_from_webpage = new_find_next_page_from_webpage
# new methods can be added using the same approach, e.g. WebPageItems.newmethod = newmethod


# %%    RETAILER SPECIFIC - IDENTIFY WHICH OPTIONAL METHODS TO RUN

methods_to_run = {
    'output_product_to_screen': False,
    'output_product_values_to_screen': False,
    'output_image_to_file': True,
    'output_description_to_file': True,
    'output_webpage_to_file': False,
}


# %% Main Code


def main():
    filename = __file__
    startPage = 43  # EDIT HERE FOR SET_UP AND TESTING to processs a sub range of weblinks
    endPage = 44  # EDIT HERE
    scrapeWebPages(startPage, endPage, linkurls, params, filename, methods_to_run)
    return


if __name__ == '__main__':
    main()