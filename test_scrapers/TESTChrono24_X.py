""" VIP NOTE THERE IS A LOGICAL ERROR ON THE CHRONO24 WEBSITE
The web page is dynamic, each time a page is opened the positions of the products can change.
But this change is not limited to moving location within the page, the products can move between pages, e.g. move from page 2 to page 1
When this happens a product can be scraped twice and the one it replaces missed.
E.g. product A is on page 1 when page 1 is scraped, and product B is on page 2.
After page 1 is acraped but before Before page 2 is acraped product A swaps position with product B
The page 2 scrape picks up product A again, but product B is missed as it is not on page 2 but has moved to page 1 which has already been scraped
"""
import __init__ # set up the import paths
import bs4
import datetime as dt
import pandas as pd
import time
import json
from pathlib import Path
import requests
import html5lib
import os
import re
from math import ceil

from CoreScraper import scrapeWebPages, extractPrice
from CoreScraper_getHtml import WebPageItems, ProductDataFrame, clickButton
from CoreScraperConstants import toppath,  default_params

#  RETAILER SPECIFIC CONTROL PARAMETERS
# Set up the top path as parent folder of results, Note this is overridden if supplied as an argument when called as a main function
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape TEST/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape TEST/'

# set up multiple urls for finding links, first url is mainurl for capturing screenshots  (code filtters duplicates)
linkurls = ['https://www.chrono24.co.uk/']

params = default_params.copy()
params.update({
    'mainurl': 'https://www.chrono24.co.uk',
    'numscreenshots' : 0,
    'opfpath': toppath,
    # can be overidden by arguments, applied in in scrapeWebPages    #define valid links and how to interpret links
    # 'validtags' : ['http://www.topshop.com/en/tsuk/category'],  # a valid link must contain one of these strings
    'sitemap_links': ['https://www.chrono24.co.uk/others/sitemap.htm'],
    'validtags': ['index'],  # a valid link must contain one of these strings
    'invalidtags': ["javascript", "statement", "Size-Guide", "Cookie", "user", "info", "search", "brandpage", "others", "news"],  # if a link contains one of these it is invalid
    'resetduplicatelinks': True,
    # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks': ['https://www.tbc.com'],  # force any duplicate links to be ignored
    'starting_links': [],
    # additional links to process  #set any starting links that will not be picked up by the find starting pages process
    'tagref': 'a',  # look for hrefs in this class
    'remove_switches_link': True,  # if True, remove ? and # switches from link (recommended unless required)
    'departmentlevel': 3,  # the nth level in the link
    'categorylevel': 4,  # the nth level in the link
    'subcategorylevel': 5,  # the nth level in the link
    'minimumdepth': 4, # a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    # define how the web page is accessed
    'findNextPage': True, # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    'htmlParser': 'lxml',
    # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    # webdriver parameters
    'webdriverpath': 'dummy',  # path to the webdriver executable SET UP IN CoreScraper.py
    'displayOff': False,  # whether display is switched off or visible
    'displayMinimised': False,  # whether to minimise the window if display is on
    'resetDriver': False,  # whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'resetCookies' : False, #whether or not to reset the cookies on every page
    'waitForPageLoad': 2,
    'webPageWait': 2,  # time between subsequent web pages (next link, next page, next scroll down page)
    'webPageScrollWait': 1,  # time to scroll up and down at the end of a scroll page
    'scrollDownPage': False,  # whether to scroll down the page to find more products, e.g. Topshop
    #'pgkey_page_scroll' : True, #scroll using page down
    'end_page_scroll' : True, #scroll to end
    'short_webPageWait': 1,  # define a shorter time to wait for when loading individal product pages
    'comparehtml'  : 'direct', # method for detecting page loaded
    'disable_image_loading' : True, # disable images from loading
    # url and redirect parameters
    'urlSwitch': '&pageSize=120',  # add an option to the url,e.g. 48 items per page
    'invalidRedirect': '',  # if the redirected url contains this value the product pages do not load correctly
    # define any buttons to be clicked on screen
    'buttonXpaths': ['//*[@id="jq-colorbox-container"]/a'],  # a list of XPaths
    'buttoniFrameXpaths': [],
    # set up variables to hold values between pages
    'num_products': 0,  # initialise the number of products found
    'page_url': '',
    'proxy_location': '',
    # Give multiple country like this, 'GB|US'. country codes: https://laendercode.net/en/2-letter-list.html
    'show_proxy_info': False,
    #'vpn_location': 'GB',
    #'vpn_location_list' : ['GB', 'FR'],
    #turn off processing attributes
    'process_attributes' : False,
    #configure output data tables
    'GBQ_TABLE_RESULTS' : 'watch_results',
    'GBQ_TABLE_DESCRIPTIONS' : 'watch_descriptions',
    'TEST_GBQ_TABLE_RESULTS' : 'test_watch_results',
    'TEST_GBQ_TABLE_DESCRIPTIONS' : 'test_watch_descriptions',
})


# %%  RETAILER SPECIFIC - REDEFINE THE PRODUCTDATAFRAME METHODS
def new_get_description(self):
    info = self.webSoup.find('div', {'class': 'article-title'})
    if info != None:
        d = info.get_text()
        if d != None:
            description = d.replace('\n', ' ').replace('\r', ' ').replace("  ", " ")
            self.description = description


def new_get_url(self):
    info = self.webSoup.find('a')
    if info != None:
        u = info.get("href")
        if u != None:
            url = self.mainurl + u
            self.url = url


def new_get_skuid(self):
    info = self.webSoup.find('a')
    if info != None:
        skuid = info.get("data-article-id")
        self.skuid = skuid


def new_get_reviewscore(self):
    info = self.webSoup.find('div', class_="js-popover pointer")
    if info != None:
        r = info.get("data-content")
        if r != None:
            rev = r.split('<br>') 
            reviewscore = rev[-2].split('out')[0].strip()
            #reviewcount = rev[-1].split(':')[-1].strip()
            self.reviewscore = reviewscore


def new_get_reviewcount(self):
    info = self.webSoup.find('div', class_="js-popover pointer")
    if info != None:
        r = info.get("data-content")
        if r != None:
            rev = r.split('<br>') 
            #rate = rev[-2].split('out')[0].strip()
            reviewcount = rev[-1].split(':')[-1].strip()
            self.reviewcount = reviewcount


def new_get_imageurl(self):
    info = self.webSoup.find('img')
    if info != None:
        imgurl = info.get("src")
        self.imgurl = imgurl


def new_get_product_promo_message(self):
    info = self.webSoup.find('span', class_="article-badge top")
    if info != None:
        self.promomessage = info.get_text().replace('\n', ' ').replace('\r', ' ').replace('(', ' ').replace(')', ' ').strip()


def new_get_brand(self):
    info = self.webSoup.find('a')
    if info != None:
        brand = info.get("data-manufacturer")
        self.brand = brand

def new_get_stock(self):
    info = self.webSoup.find('span', {'class' : 'caption'} )
    if info != None:
        stock = info.get_text().strip()
        self.stock = stock

def new_get_ticket_previous_price(self):
    #some products have price on request - at the moment these are not caputred - see Ferrari watches
    #they are rejected as ticket price and prev price = 0
    #  price.get_text() = "Price on request"
    priceinfo = self.webSoup.find('div', class_="article-price")
    if priceinfo != None:
        info = priceinfo.find('div')
        if info != None:
            price = info.find('strong')
            if price != None:
                text_price = price.get_text()
                if "request" in  text_price.lower():
                    self.ticketprice = -1
                    self.prevprice = -1
                else: 
                    self.ticketprice = extractPrice(text_price, "£")
                    self.prevprice = extractPrice(text_price, "£")

def new_get_dealer_info(self):
    dealerinfo = self.webSoup.find('div', class_="article-seller-name")
    if dealerinfo != None:
        banner = dealerinfo.get_text().replace('\n', ' ').replace('\r', ' ').replace('(', ' ').replace(')', ' ').strip()
        self.banner = banner



def new_find_all_products(self):
    product_info = self.webSoup
    if product_info != None:
        products = product_info.find_all('div', {'class': re.compile("article-item-container")})
    else:
        products = []
    print('num products', len(products))
    self.products = products


#find the next page link from the current page
def new_find_next_page_from_webpage(self):
    nextPage = None
    if self.params['findNextPage'] == True:
        info = self.webSoup.find("ul", { 'class': 'pagination' }) 
        if info != None:
            next_link = info.find_all('li', {'class': 'flex-grow'})
            if len(next_link) > 0 :
                last_link = next_link[-1]
                if last_link != None:
                    np =  last_link.find('a')
                    if np != None:
                        nextPage =  np.get("href")  
        print('Next Page' , nextPage)
    self.nextPage = nextPage    

#define the method that is used to find the product description from the individual product page url
def new_get_product_description(self):
    #hot views text
    potential_views = self.webSoup.find_all('div', {'class' : 'm-b-2'})
    view = None
    for v in potential_views:
        ihot = v.find('i', { 'class' : 'i-hot'})
        if ihot != None:
            view = v.find('div', {'class' : 'pull-left'})
            break
    if view != None:
        views_text = view.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ")
    else:
        views_text = "na"
    self.long_description = self.long_description +  ( '{' + 'views : ' + views_text + ' }' )
    #print('Hot Views', self.long_description)
    #print('Views', view)

    #block of summary table test
    content_text = ""
    title_text = ""
    product_info = self.webSoup.find('table' , {'class' : 'table'})
    if product_info != None:
        text_blocks = product_info.find_all('tr')
        for tb in text_blocks:
            title = tb.find('th')
            if title != None:
                title_text = title.get_text().strip()
            content = tb.find('td')
            if content != None:
                content_text = content.get_text().strip()
            self.long_description = self.long_description +  ( '{' +
                            " " + title_text.replace('\n', ' ').replace('\r', ' ').replace("  ", " ") + ':' + 
                            " " + content_text.replace('\n', ' ').replace('\r', ' ').replace("  ", " ") + ' }' )
    #shipping cost text
    shipping = self.webSoup.find('div', {'class' : 'js-shipping-cost'})
    if shipping != None:
         self.long_description = self.long_description +  ( '{' + 'shipping : ' +
                                        shipping.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ") + ' }' )

    #other basic info text
    product_info = self.webSoup.find('div' , {'class' : 'col-md-12'})
    if product_info != None: 
        text_blocks = product_info.find_all('tr')
        for tb in text_blocks:
            txt = tb.find_all('td')
            if len(txt) > 0:
                title_text = txt[0].get_text().strip()
                content_text = txt[-1].get_text().strip()
                self.long_description = self.long_description +  ( '{' +
                            " " + title_text.replace('\n', ' ').replace('\r', ' ').replace("  ", " ") + ':' + 
                            " " + content_text.replace('\n', ' ').replace('\r', ' ').replace("  ", " ") + ' }' )


    #get the colour description - not applicable for Chrono24
    #info = self.webSoup.find('div', {'class' : 'colour-name'})
    #if info != None:
    #    self.colour = info.get_text().strip()
    self.colour = "na"
    #TODO fetch the size description data = [list of strings]
    self.size = views_text
    self.availability = "availability desc"

#define the method that is used to find the main image url from the individual product page url
def new_get_product_imgage_url(self):
    image_block = self.webSoup.find('div', {'class' : 'detail-image'})
    if image_block != None:
        img_box = image_block.find_all('div')
        if len(img_box) > 0:
            img_div = img_box[-1]
            url = img_div.get('data-original')
            print('Image URL ', url)
            self.imgurl = url


# redefine the methods specific to this retailer
ProductDataFrame.get_description = new_get_description
ProductDataFrame.get_url = new_get_url
ProductDataFrame.get_skuid = new_get_skuid
ProductDataFrame.get_imageurl = new_get_imageurl
ProductDataFrame.get_banner = new_get_dealer_info
ProductDataFrame.get_ticket_previous_price = new_get_ticket_previous_price
ProductDataFrame.get_brand = new_get_brand
ProductDataFrame.get_reviewscore = new_get_reviewscore
ProductDataFrame.get_reviewcount = new_get_reviewcount
ProductDataFrame.get_stock = new_get_stock
ProductDataFrame.get_product_description = new_get_product_description
ProductDataFrame.get_product_imgage_url = new_get_product_imgage_url
ProductDataFrame.get_product_promo_message = new_get_product_promo_message
WebPageItems.find_all_products = new_find_all_products
WebPageItems.find_next_page_from_webpage = new_find_next_page_from_webpage
# new methods can be added using the same approach, e.g. WebPageItems.newmethod = newmethod


# %%    RETAILER SPECIFIC - IDENTIFY WHICH OPTIONAL METHODS TO RUN FOR EACH PRODUCT

methods_to_run = {
    'output_product_to_screen': False,
    'output_image_to_file': True, # set True to output product images to GS
    'output_description_to_file' : True, # set True to output product descriptions to BQ
    'output_product_values_to_screen': False,
    'output_webpage_to_file': False,
}


# %% Main Code    

def main():
    filename = __file__
    startPage = 315  # Mondia = 315 to 316 EDIT HERE FOR SET_UP AND TESTING to processs a sub range of weblinks
    endPage = 316  # EDIT HERE
    scrapeWebPages(startPage, endPage, linkurls, params, filename, methods_to_run)
    return


if __name__ == '__main__':
    main()
