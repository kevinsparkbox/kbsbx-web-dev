import __init__ # set up the import paths
import bs4
import datetime as dt
import pandas as pd
import time
import json
from pathlib import Path
import requests
import html5lib
import os
import re
from math import ceil

from CoreScraper import scrapeWebPages, extractPrice
from CoreScraper_getHtml import WebPageItems, ProductDataFrame
from CoreScraperConstants import toppath,  default_params

#  RETAILER SPECIFIC CONTROL PARAMETERS
# Set up the top path as parent folder of results, Note this is overridden if supplied as an argument when called as a main function
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape TEST/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape TEST/'

#set up multiple urls for finding links, first url is mainurl (code filtters duplicates)
linkurls = ['https://www.made.com/']

params = default_params.copy()
params.update( {
    'mainurl' : linkurls[0],
    'opfpath' : toppath, # can be overidden by arguments, applied in in scrapeWebPages
    #define valid links and how to interpret links
    'sitemap_links' : ['https://www.made.com/catalog/seo_sitemap/category/'],
    'validtags' : ['made'],  # a valid link must contain one of these strings
    'invalidtags' : [ 'help', 'index', 'cookie',  'international', 'content', 'blog'],  # if a link contains one of these it is invalid
    'resetduplicatelinks' : True, # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks' : ['https://www.tbc.com'], # force any duplicate links to be ignored
    'starting_links' : [],    #additional links to process  #set any starting links that will not be picked up by the find starting pages process
    'tagref' : 'a', # look for hrefs in this class
    'remove_switches_link' : False,  # if True, remove ? and # switches from link (recommended unless required)
    'departmentlevel' : 4, #the nth level in the link
    'categorylevel' : 5,  #the nth level in the link
    'subcategorylevel' : 6,  #the nth level in the link
    'minimumdepth' : 5 ,  #a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    #define how the web page is accessed
    'useRequests' : False, # whether or not to use the requests module, if False use the chromium driver Browser
    'findNextPage' : True, # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    'htmlParser' :  'lxml', # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    #webdriver parameters
    'webdriverpath' : 'dummy' , # path to the webdriver executable SET UP IN CoreScraper.py
    'displayOff' : False,  # whether display is switched off or visible
    'displayMinimised' :  False,  # whether to minimise the window if display is on
    'resetDriver' : True,  #whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'waitForPageLoad' : 2, #typ set to half webPageWait, when scrolling, time to wait for page to load before comparing 2 subsequent loads to see if scrolled page has finished loading
    'webPageWait' : 2,  # time between subsequent web pages (next link, next page, next scroll down page)
    'scrollDownPage' : True,  # whether to scroll down the page to find more products, e.g. Topshop
    # url and redirect parameters
    'urlSwitch' :  '',    #add an option to the url,e.g. 48 items per page
    'invalidRedirect' : '', # if the redirected url contains this value the product pages do not load correctly
    #define any buttons to be clicked on screen
    'buttonXpaths' : [ '/html/body/footer/div[3]/div/button/p', '/html/body/div[4]/div/div[1]/button'], # a list of XPaths
    #'buttonXpaths' : [ '' ], # a list of XPaths
    'buttoniFrameXpaths' : [],
    'numscreenshots' : 1, # number of screnshots to save
    'max_link_depth' : 1,
    'resetCookies' : False,
    'proxy_location': '',
    # Give multiple country like this, 'GB|US'. country codes: https://laendercode.net/en/2-letter-list.html
    'show_proxy_info': False,
    'vpn_location': '',
    'luminati_proxy': 'GB_R',
    } )
            
#%%  RETAILER SPECIFIC - REDEFINE THE PRODUCTDATAFRAME METHODS
def new_get_description(self):
    info = self.webSoup.find('div', {'class': 'styles__ProductDetails-sc-1cyyn88-6'})
    if info != None:
        d = info.find('p', {'class': 'ProductCardName__ProductName-kzxb95-0'})
        if d != None:
            description = d.get_text()
            self.description = description

            
def new_get_url(self):
    info = self.webSoup.find('a')
    if info != None:
        u = info.get("href")
        if u != None:
            url = u
            self.url = url

def new_get_skuid(self):
    info = self.webSoup
    if info != None:
        data_sku = info.get("data-product-sku")
        self.skuid = data_sku
        
def new_get_imageurl(self):
    i = self.webSoup.find('img')
    if i != None:
        imgurl = i.get('src')
        self.imgurl = imgurl

def new_get_brand(self):
    pass
            
def new_get_stock(self):
    pass
            
def new_get_banner(self):
    pass
            
def new_get_ticket_previous_price(self):
    ticketprice = None
    prevprice = None
    priceinfo = self.webSoup.find_all('span', class_="ProductPrice__Price-rtg8id-1")

    if priceinfo != None:
        if len(priceinfo) == 1:
            ticketprice = priceinfo[0].get_text()
            prevprice = ticketprice
        if len(priceinfo) == 2:
            ticketprice = priceinfo[0].get_text()
            prevprice = priceinfo[1].get_text()

    self.ticketprice = extractPrice(ticketprice, "£")
    self.prevprice = extractPrice(prevprice, "£")

def new_get_reviewcount(self):
    pass

def new_get_reviewscore(self):
    pass
        
def new_get_promo_message(self):
    pass
            
def new_find_all_products(self):
    products = self.webSoup.find_all('div', {'class': 'styles__ProductCardElement-sc-1cyyn88-0'})
    print('num products', len(products))
    self.products = products
    
def new_output_description_file(self):
    pass


#find the next page link from the current page
def new_find_next_page_from_webpage(self):
    nextPage = None
    load_mnore_class = self.webSoup.find('div', {'class' : 'LoadMoreButton__Wrapper-d542u2-0'})
    if load_mnore_class != None:
        load_more_link = load_mnore_class.find('a')
        if load_more_link != None:
            nextPage = load_more_link.get("href")

    print('Next Page', nextPage)
    self.nextPage = nextPage
                
    
#redefine the methods specific to this retailer        
ProductDataFrame.get_description =  new_get_description
ProductDataFrame.get_url = new_get_url
ProductDataFrame.get_skuid = new_get_skuid
ProductDataFrame.get_imageurl = new_get_imageurl
ProductDataFrame.get_banner = new_get_banner
ProductDataFrame.get_brand = new_get_brand
ProductDataFrame.get_stock = new_get_stock
ProductDataFrame.get_reviewscore = new_get_reviewscore
ProductDataFrame.get_reviewcount = new_get_reviewcount
ProductDataFrame.get_ticket_previous_price = new_get_ticket_previous_price
ProductDataFrame.output_description_file = new_output_description_file
WebPageItems.get_promo_message = new_get_promo_message
WebPageItems.find_all_products = new_find_all_products
WebPageItems.find_next_page_from_webpage = new_find_next_page_from_webpage
#new methods can be added using the same approach, e.g. WebPageItems.newmethod = newmethod


#%%    RETAILER SPECIFIC - IDENTIFY WHICH OPTIONAL METHODS TO RUN 

methods_to_run = {
        'output_product_to_screen' : False,
        'output_product_values_to_screen' : False,
        'output_image_to_file' : False,
        'output_description_to_file' : False,
        'output_webpage_to_file' : False,
        }
     
            
# %% Main Code

    
def main():
    filename = __file__
    startPage = 60  # EDIT HERE FOR SET_UP AND TESTING to processs a sub range of weblinks
    endPage = 61 # EDIT HERE
    scrapeWebPages( startPage, endPage, linkurls, params, filename, methods_to_run)
    return
    
 
if __name__ == '__main__':
    main()
