import __init__ # set up the import paths
import bs4
import datetime as dt
import pandas as pd
import time
import json
from pathlib import Path
import requests
import html5lib
import os
import re
from math import ceil

from CoreScraper import scrapeWebPages, extractPrice
from CoreScraper_getHtml import WebPageItems, ProductDataFrame
from CoreScraperConstants import toppath,  default_params

#  RETAILER SPECIFIC CONTROL PARAMETERS
# Set up the top path as parent folder of results, Note this is overridden if supplied as an argument when called as a main function
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape TEST/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape TEST/'
    
# set up multiple urls for finding links, first url is mainurl (code filtters duplicates)
linkurls = [ 'https://www.kurtgeiger.com']

params = default_params.copy()
params.update({
    'mainurl': 'https://www.kurtgeiger.com',
    'opfpath': toppath,  # can be overidden by arguments, applied in in scrapeWebPages
    'sitemap_links': ['https://www.kurtgeiger.com/site-map'],
    #'sitemap_links': [],
    'numscreenshots' : 0, # number of screnshots to save
    # define valid links and how to interpret links
    'validtags': ['sale'],  # a valid link must contain one of these strings
    'invalidtags': ['help', 'apple', 'account', 'brand', 'nhs', 'cookie'],  # if a link contains one of these it is invalid
    'resetduplicatelinks': True,
    # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks': ['https://www.tbc.com'],  # force any duplicate links to be ignored
    'starting_links': [],
    # additional links to process  #set any starting links that will not be picked up by the find starting pages process
    'tagref': 'a',  # look for hrefs in this class
    'remove_switches_link': False,  # if True, remove ? and # switches from link (recommended unless required)
    'departmentlevel': 4,  # the nth level in the link
    'categorylevel': 5,  # the nth level in the link
    'subcategorylevel': 6,  # the nth level in the link
    'minimumdepth': 5,
    # a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    # define how the web page is accessed
    'maximumdepth': 999,  # a page link has not to exceed the maximum depth
    'max_link_depth' : 1,
    'useRequests': False,  # whether or not to use the requests module, if False use the chromium driver Browser
    'findNextPage': False,
    # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    'htmlParser': 'lxml',
    # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    # webdriver parameters
    'webdriverpath': 'dummy',  # path to the webdriver executable SET UP IN CoreScraper.py
    'displayOff': False,  # whether display is switched off or visible
    'displayMinimised': False,  # whether to minimise the window if display is on
    'resetDriver': False,
    # whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'waitForPageLoad': 15,
    # typ set to half webPageWait, when scrolling, time to wait for page to load before comparing 2 subsequent loads to see if scrolled page has finished loading
    'webPageWait': 5,  # time between subsequent web pages (next link, next page, next scroll down page)
    'scrollDownPage': True,  # whether to scroll down the page to find more products, e.g. Topshop
    # url and redirect parameters
    'urlSwitch': '',  # add an option to the url,e.g. 48 items per page
    'invalidRedirect': '',  # if the redirected url contains this value the product pages do not load correctly
    # define any buttons to be clicked on screen
    'buttonXpaths': ['//*[@id="onetrust-accept-btn-handler"]' ],  # a list of XPaths
    # 'buttonXpaths' : [ '' ], # a list of XPaths
    'buttoniFrameXpaths': [],
    # "document.querySelector('#\31 556719216 > div.header > div > div.closeButton.closeSticky')"
    'chrome_updated_version': False,
    'slow_page_scroll': False,
    'proxy_location': '',
    # Give multiple country like this, 'GB|US'. country codes: https://laendercode.net/en/2-letter-list.html
    'popup_remove_button': [ ],  # give xpath of popup removal button
    'show_proxy_info': False,
    'vpn_location': 'GBWS',
    #'vpn_location': '',
    'wrong_country_popup': [],  # give xpath of popup removal button

})


# %%  RETAILER SPECIFIC - REDEFINE THE PRODUCTDATAFRAME METHODS
def new_get_description(self):
    description = ""
    info = self.webSoup.find('span' , {'class' : re.compile("StyledSRText") })
    if info is not None:
        info = info.get_text().replace('\n', '').replace('\r', '').strip()
        if info is not None:
            info = info.split(',')
            for d in info:
                if 'Product' in d or 'Colour' in d:
                    description = description + d
                    self.description = description


def new_get_url(self):
    info = self.webSoup.find_all('a'  )
    if info != None:
        u = info[0]
        if u != None:
            url = u.get('href')
            if url != None:
                #self.url =  params['mainurl'] + url[1:]
                self.url =  params['mainurl'] + url
                #print('XXXX ', url)


def new_get_skuid(self):
    info = self.webSoup.find('span' , {'class' : re.compile("StyledSRText") })
    if info != None:
        info = info.get_text().replace('\n', '').replace('\r', '').strip()
        if info is not None:
            info = info.split(',')
            for d in info:
                if 'Line' in d:
                    sku_id = d.split(':')[-1].strip()
                    sku_id = sku_id.replace('.', '')
                    self.skuid = sku_id


def new_get_imageurl(self):
    i = self.webSoup.find('img')
    if i != None:
        imgurl = i.get('src')
        self.imgurl = imgurl


def new_get_brand(self):
    info = self.webSoup.find('span' , {'class' : re.compile("StyledSRText") })
    if info != None:
        info = info.get_text().replace('\n', '').replace('\r', '').strip()
        if info is not None:
            info = info.split(',')
            for d in info:
                if 'Brand' in d:
                    brand = d.split(':')[-1]
                    self.brand = brand


def new_get_stock(self):
    # not implemented
    info = self.webSoup.find('span', {'class': 'listProductLowStock'})
    if info != None:
        d = info.get_text()
        if d != None:
            self.stock = None


def new_get_banner(self):
    info = self.webSoup.find('span', {'class': "price-save"})
    b = info
    if b != None:
        banner = b.get_text().strip()
        self.banner = banner


def new_get_ticket_previous_price(self):
    ticketprice = 0
    prevprice = 0
    now_price = None
    #was price
    was_price = self.webSoup.find('p', {'data-hookid': re.compile('plpProductPriceWasPrice') } )
    if was_price is not None:
        was_price = was_price.get_text().replace('\n', '').replace('\r', '').strip()
    #current price
    info = self.webSoup.find('span' , {'class' : re.compile("StyledSRText") })
    if info != None:
        info = info.get_text().replace('\n', '').replace('\r', '').strip()
        if info is not None:
            info = info.split(',')
            for d in info:
                if 'Price' in d:
                    now_price  = d.strip().split(' ')[1]

                  
    if was_price is not None:
        ticketprice = extractPrice(now_price, "£")
        prevprice = extractPrice(was_price, "£")
    elif now_price is not None:
        ticketprice = extractPrice(now_price, "£")
        prevprice = extractPrice(now_price, "£")
    
    self.ticketprice = ticketprice
    self.prevprice = prevprice


def new_get_reviewcount(self):
    # not implemented all 5 stars
    info = self.webSoup.find('span', {'class': "product-rating__review-count"})
    if info != None:
        r = info.get_text()
        if r != None:
            r = r.split('(')[1].split('r')[0]
            reviewcount = r.replace('\n', ' ').replace('\r', ' ').replace('(', ' ').replace(')', ' ').strip()
            self.reviewcount = reviewcount


def new_get_reviewscore(self):
    # not implemented all 5 stars
    info = self.webSoup.find('div', {'class': "product-rating"})
    if info != None:
        fullstar = info.find_all('img', {'alt': 'Full Star'})
        halfstar = info.find_all('img', {'alt': 'Half Star'})
        reviewscore = len(fullstar) + 0.5 * len(halfstar)
        self.reviewscore = reviewscore


def new_get_promo_message(self):
    # not implemented
    info = self.webSoup.find('div', {'id': 'header_espot'})
    if info != None:
        pm = info.find('img')
        if pm != None:
            self.promomessage = pm.get('alt')


def new_find_all_products(self):
    #products_all = self.webSoup.find('div', {'id': "results"})
    products_all = self.webSoup
    if products_all != None:
        products = products_all.find_all('li', {'data-hookid': "plpProductListItem"})
    else:
        products = []
    # products = self.webSoup.find_all('div', { 'class' : re.compile("product col*") } )
    print('num products', len(products))
    self.products = products


#define the method that is used to find the product description from the individual product page url
def new_get_product_description(self):
    pi_t = ""
    product_info_text_block = self.webSoup.find('p', { 'data-test' : 'prodInfo-productDescription'})
    if product_info_text_block != None:
        pi_t =  product_info_text_block.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
    self.long_description =   pi_t 

    #get the colour description - clothing products
    self.colour = 'Unknown'
                        
    #TODO fetch the size description data = [list of strings]
    self.size = "size  desc"
    self.availability = "availability desc"

    print('long description ' , pi_t)

#define the method that is used to find the main image url from the individual product page url
def new_get_product_imgage_url(self):
    image_info = self.webSoup.find('div', {'data-test' : 'carousel-zoom-container'})
    if image_info != None:
        images = image_info.find('img')
        if images != None:
            url = images.get("src")
            self.imgurl =  url 
            print('IMAGE url is ', url )
            #TODO it is a jfif file
        #url2 = image_url.find('img')["srcset"]
        #if url != None:
            #url = 'https:' + url
        #self.imgurl = url


# find the next page link from the current page
def new_find_next_page_from_webpage(self):
    # driver = self.driver # for access to buttons on the page etc
    # for white company add page=N to the end of the url
    link = self.link
    findNextPage = params['findNextPage']
    nextPage = None
    if findNextPage:
        # find current page number if already set
        pagePos = int(link['webpage'].find('p='))
        if pagePos == -1:
            # page no currently not set
            nextPage = link['webpage'] + '?p=2'
        else:  # page number has already been set
            currPage = int(link['webpage'][pagePos + 2:])
            nextPage = currPage + 1
            nextPage = link['webpage'].replace('p=' + str(currPage), 'p=' + str(nextPage))

        # override - if there are no products on the current page then end of page has been reached
        if len(self.products) == 0:
            nextPage = None

        print('Next Page', nextPage)

    self.nextPage = nextPage


# %%


# redefine the methods specific to this retailer
ProductDataFrame.get_description = new_get_description
ProductDataFrame.get_url = new_get_url
ProductDataFrame.get_skuid = new_get_skuid
ProductDataFrame.get_imageurl = new_get_imageurl
ProductDataFrame.get_banner = new_get_banner
ProductDataFrame.get_brand = new_get_brand
ProductDataFrame.get_stock = new_get_stock
ProductDataFrame.get_reviewscore = new_get_reviewscore
ProductDataFrame.get_reviewcount = new_get_reviewcount
ProductDataFrame.get_ticket_previous_price = new_get_ticket_previous_price
ProductDataFrame.get_product_description = new_get_product_description
ProductDataFrame.get_product_imgage_url = new_get_product_imgage_url
WebPageItems.get_promo_message = new_get_promo_message
WebPageItems.find_all_products = new_find_all_products
WebPageItems.find_next_page_from_webpage = new_find_next_page_from_webpage
# new methods can be added using the same approach, e.g. WebPageItems.newmethod = newmethod


# %%    RETAILER SPECIFIC - IDENTIFY WHICH OPTIONAL METHODS TO RUN

methods_to_run = {
    'output_product_to_screen': False,
    'output_product_values_to_screen': False,
    #'output_image_to_file': True,
    #'output_description_to_file': True,
    'output_image_to_file': False,
    'output_description_to_file': False,
    'output_webpage_to_file': False,
}


# %% Main Code


def main():
    filename = __file__
    startPage = 4  #  EDIT HERE FOR SET_UP AND TESTING to processs a sub range of weblinks
    endPage = 5  # EDIT HERE
    scrapeWebPages(startPage, endPage, linkurls, params, filename, methods_to_run)
    return


if __name__ == '__main__':
    main()
