import __init__ # set up the import paths
import bs4
import datetime as dt
import pandas as pd
import time
import json
from pathlib import Path
import requests
import html5lib
import os
import re
from math import ceil

from CoreScraper import scrapeWebPages, extractPrice
from CoreScraper_getHtml import WebPageItems, ProductDataFrame
from CoreScraperConstants import toppath,  default_params

#  RETAILER SPECIFIC CONTROL PARAMETERS
# Set up the top path as parent folder of results, Note this is overridden if supplied as an argument when called as a main function
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape TEST/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape TEST/'

# set up multiple urls for finding links, first url is mainurl (code filtters duplicates)
linkurls = ['http://www.boden.co.uk']

params = default_params.copy()
params.update({
    'mainurl': linkurls[0],
    'opfpath': toppath,  # can be overidden by arguments, applied in in scrapeWebPages
    # define valid links and how to interpret links
    'validtags': ['en-gb'],  # a valid link must contain one of these strings
    'invalidtags': ['checkout', 'account', 'signin', 'search', 'delivery', 'help', 'company', 'contact', 'returns'],
    # if a link contains one of these it is invalid
    'resetduplicatelinks': True,
    # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks': ['https://www.tbc.com'],  # force any duplicate links to be ignored
    'starting_links': [],
    # additional links to process  #set any starting links that will not be picked up by the find starting pages process
    'tagref': 'a',  # look for hrefs in this class
    'remove_switches_link': True,  # if True, remove ? and # switches from link (recommended)
    'departmentlevel': 4,  # the nth level in the link
    'categorylevel': 5,  # the nth level in the link
    'subcategorylevel': 6,  # the nth level in the link
    'minimumdepth': 4,
    # a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    # define how the web page is accessed
    'useRequests': False,  # whether or not to use the requests module, if False use the chromium driver Browser
    'findNextPage': True,
    # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    # 'htmlParser' :  'lxml', # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    'htmlParser': 'lxml',
    # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    # webdriver parameters
    'webdriverpath': 'dummy',  # path to the webdriver executable SET UP IN CoreScraper.py
    'displayOff': False,  # whether display is switched off or visible
    'displayMinimised': False,  # whether to minimise the window if display is on
    'resetDriver': True,  # whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'waitForPageLoad': 1,
    # typ set to half webPageWait, when scrolling, time to wait for page to load before comparing 2 subsequent loads to see if scrolled page has finished loading
    'webPageWait': 2,
    # Browser: time to wait for page to fully load (implicit wait) , Requests: time to wait between page loads
    'scrollDownPage': False,  # whether to scroll down the page to find more products, e.g. Topshop
    # url and redirect parameters
    'urlSwitch': '',  # add an option to the url,e.g. 48 items per page
    'invalidRedirect': '',  # if the redirected url contains this value the product pages do not load correctly
    # define any buttons to be clicked on screen
    'buttonXpaths': ['//*[@id="cookie-close-btn"]', '//*[@id="onetrust-accept-btn-handler"]'],  # a list of XPaths
    'buttoniFrameXpaths': [],
    'numscreenshots': 1,  # number of screnshots to save
    'max_link_depth': 1,
    'proxy_location': '',
    'vpn_location': 'GBWS',
    # Give multiple country like this, 'GB|US'. country codes: https://laendercode.net/en/2-letter-list.html
})


# %%  RETAILER SPECIFIC - REDEFINE THE PRODUCTDATAFRAME METHODS
def new_get_description(self):
    info = self.webSoup.find('h3', {'class': "product-item-title"})
    if info != None:
        d = info
        if d != None:
            description = d.text.replace('\n', '').replace('\r', '').strip()
            self.description = description


def new_get_url(self):
    info = self.webSoup.find('div', {'class': "product-item"})
    if info != None:
        u = info.find('a')
        if u != None:
            url = params['mainurl'] + u.get('href')
            self.url = url


def new_get_skuid(self):
    skuid = str(self.url.split('/')[-1]).split('?')[0]
    if skuid != None:
        self.skuid = skuid


def new_get_imageurl(self):
    i = self.webSoup.find('img')
    if i != None:
        imgurl = i.get('data-src')
        self.imgurl = imgurl


def new_get_banner(self):
    info = self.webSoup.find('span', {'class': "product-item-badge"})
    if info != None:
        b = info.get_text()
        if b != None:
            banner = b
            self.banner = banner


def new_get_ticket_previous_price(self):
    ticketprice = None
    prevprice = None
    itemprice = self.webSoup.find('span', {'class': "product-item-price-value"})
    if itemprice != None:
        itemprice = itemprice.get_text().strip()
        itemprice = itemprice.split('£')
        if len(itemprice) > 2:
            # markdown
            ticketprice = itemprice[2].strip()
            prevprice = itemprice[1].strip()
        # else:
        elif len(itemprice) == 2:
            # in-season
            ticketprice = itemprice[1].strip()
            prevprice = ticketprice

    self.ticketprice = extractPrice(ticketprice, "£")
    self.prevprice = extractPrice(prevprice, "£")


def new_get_reviewscore(self):
    info = self.webSoup.find('figure', {'class': "product-item-rating"})
    if info != None:
        r = info.get_text()
        if r != None:
            reviewscore = r
            self.reviewscore = reviewscore


def new_get_reviewcount(self):
    info = self.webSoup.find('figcaption', {'class': "product-reviews-count"})
    if info != None:
        r = info.get_text()
        if r != None:
            r = r.split('(')[1].split('R')[0]
            reviewcount = r.replace('\n', ' ').replace('\r', ' ').replace('(', ' ').replace(')', ' ').strip()
            self.reviewcount = reviewcount


def new_get_promo_message(self):
    # TODO
    info = self.webSoup.find('div', {'id': 'header_espot'})
    if info != None:
        pm = info.find('img')
        if pm != None:
            self.promomessage = pm.get('alt')


def new_find_all_products(self):
    products = self.webSoup.find_all('li', {'class': 'product-grid-item'})
    print('num products', len(products))
    self.products = products


#define the method that is used to find the product description from the individual product page url
def new_get_product_description(self):
    pi_t = ""
    pi_c = ""
    product_info_text_block = self.webSoup.find('p', { 'class' : 'product-description-content'})
    if product_info_text_block != None:
        #print('product info', product_info_text_block.prettify())
        block1 = product_info_text_block
        if block1 != None:
            pi_t = block1.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
    
    product_info_text_block = self.webSoup.find('ul', { 'class' : 'product-bullet-points'})
    if product_info_text_block != None:
        pi_c =    product_info_text_block.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
    self.long_description =   pi_t + " " + pi_c
    #get the colour description
    info = self.webSoup.find('ul', {'class' : 'product-detail-filter-list'})
    if info != None:
        colour_info = info.find('li', {'class' : 'is-active'})
        if colour_info != None:
            colour = colour_info.find('a').get("title")
            self.colour = colour.replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()

    #TODO fetch the size description data = [list of strings]
    self.size = "size  desc"
    self.availability = "availability desc"

#define the method that is used to find the main image url from the individual product page url
def new_get_product_imgage_url(self):
    image_info = self.webSoup.find('picture')
    if image_info != None:
        images = image_info.find_all('source')
        if len(images) >0:
            url = images[0].get("srcset")
            self.imgurl = url 
        #url2 = image_url.find('img')["srcset"]
        #if url != None:
            #url = 'https:' + url
        #self.imgurl = url


def new_get_brand(self):
    pass


# find the next page link from the current page
def new_find_next_page_from_webpage(self):
    # define a global variable to hold the previous page number
    # for Boden, it is difficult to detect the end of pages, and when an invalid page number is provided the server redirects to page 1
    global previousPageNo

    # driver = self.driver # for access to buttons on the page etc
    # for Boden add page=N to the end of the url
    # difficult to detect the last page, if an invalid page number is entered it reverts to 1
    link = self.link
    findNextPage = params['findNextPage']
    nextPage = None
    if findNextPage:
        # the very first time it is run, previousPageNo will not be initlaised
        try:
            if previousPageNo < 0:
                reset_page = True
            else:
                reset_page = False
        except:
            reset_page = True

        if reset_page:
            # page no currently not set and has not previously been set
            print('Setting next page to 1 and initialising previous page')
            nextPage = link['webpage'] + '?page=1'
            previousPageNo = 0
        else:
            # find current page number if already set
            pagePos = link['webpage'].find('page=')
            # get current page no and increment the page no TO GET NEXT PAGE NO
            if pagePos > -1:  # will be zero if redirected back to the first page with no page number
                currPageNo = link['webpage'][pagePos:].split("=")[1]
                # remove any #tags added
                currPageNo = int(currPageNo.split("#")[0])
                nextPageNo = currPageNo + 1
                print('In next Page, previous page no ', previousPageNo)
                print('Incremented to next page', nextPageNo, 'Current Page no ', currPageNo)
                # if the previous page no is less than current page, it is a valid page no
                if previousPageNo < currPageNo:
                    # then update to next page
                    previousPageNo = currPageNo
                    b = link['webpage'].find('page=')
                    nextPage = link['webpage'][:b] + 'page=' + str(nextPageNo)
                else:
                    # finish
                    nextPage = None
                    previousPageNo = -1
            else:
                nextPage = None
                previousPageNo = -1

    # if there are no products on the page, have moved past the last page so stop the next page process
    numProducts = len(self.webSoup.find_all('li', {'class': 'product-grid-item'}))
    if numProducts == 0:
        print('No Products on Page')
        nextPage = None

    print('Next Page', nextPage)
    self.nextPage = nextPage


# %%


# redefine the methods specific to this retailer
ProductDataFrame.get_description = new_get_description
ProductDataFrame.get_url = new_get_url
ProductDataFrame.get_skuid = new_get_skuid
ProductDataFrame.get_imageurl = new_get_imageurl
ProductDataFrame.get_banner = new_get_banner
ProductDataFrame.get_reviewscore = new_get_reviewscore
ProductDataFrame.get_reviewcount = new_get_reviewcount
ProductDataFrame.get_ticket_previous_price = new_get_ticket_previous_price
ProductDataFrame.get_product_description = new_get_product_description
ProductDataFrame.get_product_imgage_url = new_get_product_imgage_url
WebPageItems.get_promo_message = new_get_promo_message
WebPageItems.find_all_products = new_find_all_products
WebPageItems.find_next_page_from_webpage = new_find_next_page_from_webpage
# new methods can be added using the same approach, e.g. WebPageItems.newmethod = newmethod


# %%    RETAILER SPECIFIC - IDENTIFY WHICH OPTIONAL METHODS TO RUN

methods_to_run = {
    'output_product_to_screen': False,
    'output_product_values_to_screen': False,
    'output_image_to_file': True,
    'output_description_to_file': True,
    'output_webpage_to_file': False,
}


# %% Main Code


def main():
    filename = __file__
    startPage = 6  # EDIT HERE FOR SET_UP AND TESTING to processs a sub range of weblinks
    endPage = 7  # EDIT HERE
    scrapeWebPages(startPage, endPage, linkurls, params, filename, methods_to_run)
    return


if __name__ == '__main__':
    main()
