import __init__ # set up the import paths
import bs4
import datetime as dt
import pandas as pd
import time
import json
from pathlib import Path
import requests
import html5lib
import os
import re
from math import ceil
import logging

from selenium.webdriver.common.keys import Keys

from CoreScraper import scrapeWebPages, extractPrice
from CoreScraper_getHtml import WebPageItems, ProductDataFrame, clickButton
from CoreScraperConstants import toppath,  default_params

#  RETAILER SPECIFIC CONTROL PARAMETERS
# Set up the top path as parent folder of results, Note this is overridden if supplied as an argument when called as a main function
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape TEST/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape TEST/'
    
#set up multiple urls for finding links, first url is mainurl for capturing screenshots  (code filtters duplicates)
# linkurls = [ 'https://www.newlook.com/uk/sale', 'https://www.newlook.com/uk/womens','https://www.newlook.com/uk/mens']
linkurls = ['https://www.newlook.com']

params = default_params.copy()
params.update({
    'mainurl' : 'https://www.newlook.com',
    'opfpath' : toppath, # can be overidden by arguments, applied in in scrapeWebPages    #define valid links and how to interpret links
    #'validtags' : ['http://www.topshop.com/en/tsuk/category'],  # a valid link must contain one of these strings
    'sitemap_links': ['https://www.newlook.com/uk/sitemap'],
    'validtags' : ['newlook'],  # a valid link must contain one of these strings
    'invalidtags' : ['help', 'refund', "javascript", "Statement", "Size-Guide", "Cookie", 'store', 'security',
                    'girls', 'teens', 'home', 'info', '/p/' ], # if a link contains one of these it is invalid
    'resetduplicatelinks' : True, # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks' : ['https://www.newlook.com/uk/womens/c/uk-womens', 'https://www.newlook.com/uk/womens/c/uk-mens'], # force any duplicate links to be ignored
    'starting_links' : [],    #additional links to process  #set any starting links that will not be picked up by the find starting pages process
    'tagref' : 'a', # look for hrefs in this class
    'remove_switches_link' : True,  # if True, remove ? and # switches from link (recommended unless required)
    'departmentlevel' : 6, #the nth level in the link
    'categorylevel' : 7,  #the nth level in the link
    'subcategorylevel' : 8,  #the nth level in the link
    'minimumdepth' : 6 ,  #a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    #define how the web page is accessed
    'useRequests' : False, # whether or not to use the requests module, if False use the chromium driver Browser
    'findNextPage' : True, # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    'htmlParser' :  'lxml', # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    #webdriver parameters
    'webdriverpath' : 'dummy' , # path to the webdriver executable SET UP IN CoreScraper.py
    'displayOff' : False,  # whether display is switched off or visible
    'displayMinimised' :  False,  # whether to minimise the window if display is on
    'resetDriver' : False,  #whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'waitForPageLoad' : 1, #typ set to half webPageWait, when scrolling, time to wait for page to load before comparing 2 subsequent loads to see if scrolled page has finished loading
    'webPageWait' : 1,  # time between subsequent web pages (next link, next page, next scroll down page)
    'webPageScrollWait' : 1 , #wait between page scrolls
    'scrollDownPage' : False,  # whether to scroll down the page to find more products - scroll is handed in find next page for Newlook
    'end_page_scroll' : True, #type of scroll
    'disable_image_loading' : True, # do not load images
    # url and redirect parameters
    'urlSwitch' :  '',    #add an option to the url,e.g. 48 items per page
    'invalidRedirect' : '', # if the redirected url contains this value the product pages do not load correctly
    #define any buttons to be clicked on screen
    'buttonXpaths' : ['//*[@id="body"]/cookie-banner/div/div/div[2]/a', '//*[@id="body"]/div[10]/div/div/svg/path', '//*[@id="body"]/div[8]/div/div/svg',
                        '//*[@id="body"]/div[10]/div/div/svg',  '//*[@id="body"]/div[12]/div/div/svg'], # a list of XPaths
    'buttoniFrameXpaths' : [],
    #set up variables to hold values between pages
    'num_products' : 0, # initialise the number of products found
    'page_url'  :'',
    'numscreenshots' : 1, # number of screnshots to save
    'max_link_depth' : 2,
    'resetCookies' : True,
    'proxy_location': '',
    # Give multiple country like this, 'GB|US'. country codes: https://laendercode.net/en/2-letter-list.html
    'show_proxy_info': False,
    'vpn_location': 'GBWS',
    'luminati_proxy': '',    
    'popup_remove_button': ['//*[@id="body"]/div[12]/div/div/svg'],  # give xpath of popup removal button
    'popup_remove_button_wait': 2 , #time to wait for pop up to be removed
    })
            
#%%  RETAILER SPECIFIC - REDEFINE THE PRODUCTDATAFRAME METHODS
def new_get_description(self):
    info = self.webSoup.find('img')
    if info != None : 
        d = info
        if d != None: 
            description = d.get("alt")
            self.description = description        
            
def new_get_url(self):
    info = self.webSoup.find('a', class_="link link--nounderline product-item__quick-view-link ng-scope")
    if info == None:
        info = self.webSoup.find('a',{ 'class' : "link link--nounderline product-item__quick-view-link ng-scope mm-link"})
            
    if info != None : 
        u = info.get("href")
        if u != None :
            url = self.mainurl + u
            self.url = url

def new_get_skuid_url(self):
    if self.url != None:
        skuid = str(self.url).split('/')[-1]
        self.skuid = skuid
        
def new_get_skuid_data(self):
    info = self.webSoup
    if info != None : 
        skuid = info.get("data-itemid")
        self.skuid = skuid
        
def new_get_reviewscore(self):  
    info = self.webSoup.find('span', class_="bv-off-screen")
    if info != None : 
        r = info
        if r != None :
            reviewscore = r.get_text()
            self.reviewscore = reviewscore

def new_get_reviewcount(self):
    info = self.webSoup.find('div', {'class' : 'mod-product-info'})
    if info != None : 
        r = info.find('span', {'class' : 'nbReviews'} )
        if r != None :
            reviewcount = r.get_text().replace('\n', ' ').replace('\r', ' ').replace('(', ' ').replace(')', ' ').strip()
            self.reviewcount = reviewcount
        
def new_get_imageurl(self):
    imglinks = self.webSoup.find_all('img')
    found = False
    imgurl = None
    i = 0
    while i < len(imglinks) and not found:
        if imglinks[i].has_attr("data-img-src"):
            imgurl = imglinks[i]["data-img-src"]
            found = True
        elif imglinks[i].has_attr("src"):
            imgurl = imglinks[i]["src"]
            found = True
        i = i+1
    self.imgurl = imgurl
      
def new_get_banner(self):
    b = self.webSoup.find('img', class_="product-item__promo-image ng-scope")   
    if b != None : 
        banner =  b.get("src")
        self.banner = banner
        
def new_get_brand(self):
    b = self.webSoup.find('div', {'class' : 'dvtSticker--under pCategory desktop'})
    if b != None : 
        brand = b.get_text().replace('\n', ' ').replace('\r', ' ').strip()
        self.brand = brand
            
def new_get_ticket_previous_price(self):
    ticketprice = None
    prevprice = None    
    priceinfo = self.webSoup.find('p', class_="product-item__price")
    if priceinfo != None:
        ticketprice = priceinfo.find('span')
        if ticketprice != None:
            ticketprice = ticketprice.get_text()
        p1 = priceinfo.find('strike')
        prevprice = ticketprice if p1 == None else p1.get_text()
    
    self.ticketprice = extractPrice(ticketprice, "£")
    self.prevprice = extractPrice(prevprice, "£")
         
def new_get_promo_message(self):
    info = self.webSoup.find('div', {'id' : 'header_espot'})
    if info != None : 
        pm = info.find('img')
        if pm != None: 
            self.promomessage = pm.get('alt')
            
def new_find_all_products(self):
    products = self.webSoup.find_all('div', { 'class' :  re.compile("plp-item*") }  )
    print('num products', len(products))
    self.products = products
    
def new_output_description_file(self):
    #TODO check for new look
    pass   
        
#find the next page link from the current page, use the buttons to load the next link, then get the link and store as the Next Page
#for some reason editing and updating the url directly does not work for New Look
def new_find_next_page_from_webpage(self):

    #function to find the Load More /Next button , click on it and return True if successful, Note there are 2 different types of Load More button
    #one where the text is in a Span below the button definition
    def find_click_load_more_button(driver, attribute, attr_string):
        buttons = driver.find_elements_by_tag_name("button")
        selected_button = None
        for b in buttons:
            #print('Button ', b)
            #print('Type ', b.get_attribute('type'))
            #print('Inner Text ' , b.get_attribute('innerText'))
            #print('Text ', b.get_attribute('text'))
            #print('Inner html ' , b.get_attribute('innerHTML'))
            try:
                if attribute == "innerText" : 
                    attr_value = b.get_attribute('innerText')
                elif attribute == "innerHTML":
                    attr_value = b.get_attribute('innerHTML')

                if attr_string in attr_value :
                    selected_button = b
            except:
                #avoid the error
                #selenium.common.exceptions.StaleElementReferenceException: Message: stale element reference: element is not attached to the page document
                selected_button = None
        try:
            #driver.find_element_by_xpath('//button[contains(text(), "ladda")]').click() 
            selected_button.click()
            button_clicked = True
            logging.info('Clicking Button - success')  
        except:
            #Load more button not present / not clickable
            button_clicked = False
            logging.info('Button not clicked')
        return button_clicked           

    driver = self.driver # for access to buttons on the page etc
    params = self.params

    prevUrl = params['page_url']
    currentUrl = driver.current_url
    if prevUrl != currentUrl:
        #url has changed so reset the page counter
        params['num_products'] = 0
        params['page_url'] = currentUrl


    logging.info('Clicking the Load More Next Button ')
    #move down the page
    #num_pg_downs = 8
    #for x in range(0, num_pg_downs):
    #    driver.find_element_by_tag_name('body').send_keys(Keys.PAGE_DOWN)
    #move to bottom of page
    driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.END)
    time.sleep(params['waitForPageLoad'])
    #first look to see if the "View all" button is available
    button_clicked = find_click_load_more_button(driver, "innerText", "all") # returns false if the button is not present 
    #if not try the "load next" button
    if not button_clicked:
        button_clicked = find_click_load_more_button(driver, "innerText", "next") # returns false if the button is not present 

    if not button_clicked:
        #try again just in case the page did not load properly, seems to happen with New Look
        button_clicked = False
        attempts = 0
        while not button_clicked and attempts < 3:
            logging.info('reclicking load more button, not yet present')
            #try clearing any pop up boxes
            clickButton(driver, params)
            #try moving up a page 
            driver.find_element_by_tag_name('body').send_keys(Keys.PAGE_UP)
            time.sleep(params['waitForPageLoad']) 
            attempts = attempts + 1
            #first look to see if the "View all" button is available
            button_clicked = find_click_load_more_button(driver, "innerText", "all") # returns false if the button is not present 
            #if not try the "load next" button
            if not button_clicked:
                button_clicked = find_click_load_more_button(driver, "innerText", "next") # returns false if the button is not present 
            nextPage = None # set next page  None, if no more products to load the button is sometimes / usually not present
    if button_clicked:
        logging.info('clicked load more button')
        time.sleep(params['waitForPageLoad'])
        #nextPage = driver.current_url  
        nextPage = "DO_NOT_RELOAD_PAGE"   
    
    #as a backup also check for end_of_products
    #get the number of products before the load more button is clicked
    #find all products on the webpage
    self.find_all_products()
    new_num_products = len(self.products)
    prev_num_products = params['num_products']
    logging.info('Number of products before click {}'.format(prev_num_products))
    logging.info('Number of products after click {}'.format(new_num_products))
    params['num_products'] = new_num_products
    self.params = params
    if (new_num_products == prev_num_products) :
        logging.info('End of Products')
        end_of_products = True
        nextPage = None
        
    logging.info('Exiting find_next_page - Next Page {} '.format(nextPage))
    self.nextPage = nextPage



#redefine the methods specific to this retailer        
ProductDataFrame.get_description =  new_get_description
ProductDataFrame.get_url = new_get_url
ProductDataFrame.get_skuid = new_get_skuid_url
ProductDataFrame.get_imageurl = new_get_imageurl
ProductDataFrame.get_banner = new_get_banner
ProductDataFrame.get_ticket_previous_price = new_get_ticket_previous_price
ProductDataFrame.get_brand = new_get_brand
ProductDataFrame.get_reviewscore = new_get_reviewscore
ProductDataFrame.get_reviewcount = new_get_reviewcount
ProductDataFrame.output_description_file = new_output_description_file
WebPageItems.get_promo_message = new_get_promo_message
WebPageItems.find_all_products = new_find_all_products
WebPageItems.find_next_page_from_webpage = new_find_next_page_from_webpage
#new methods can be added using the same approach, e.g. WebPageItems.newmethod = newmethod


#%%    RETAILER SPECIFIC - IDENTIFY WHICH OPTIONAL METHODS TO RUN FOR EACH PRODUCT

methods_to_run = {
        'output_product_to_screen' : False,
        'output_image_to_file' : False,
        'output_description_to_file' : False,
        'output_product_values_to_screen' : False,
        'output_webpage_to_file' : False,
        }
     

# %% Main Code    
    
def main():
    filename = __file__
    startPage = 0  # EDIT HERE FOR SET_UP AND TESTING to processs a sub range of weblinks
    endPage = 1 # EDIT HERE
    scrapeWebPages( startPage, endPage, linkurls, params, filename, methods_to_run)
    return
    
 
if __name__ == '__main__':
    main()