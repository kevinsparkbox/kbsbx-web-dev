import __init__ # set up the import paths
import bs4
import datetime as dt
import pandas as pd
import time
import json
from pathlib import Path
import requests
import html5lib
import os
import re
from math import ceil

from CoreScraper import scrapeWebPages, extractPrice
from CoreScraper_getHtml import WebPageItems, ProductDataFrame
from CoreScraperConstants import toppath,  default_params

#  RETAILER SPECIFIC CONTROL PARAMETERS
# Set up the top path as parent folder of results, Note this is overridden if supplied as an argument when called as a main function
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape TEST/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape TEST/'
    

#set up multiple link urls if unreliable/variable response, e.g. Topshop, code sorts out duplicates
linkurls = ['https://www.pimkie.fr']

params = default_params.copy()
params.update({
    'mainurl' : linkurls[0],
    'opfpath' : toppath, # can be overidden by arguments, applied in in scrapeWebPages
    #define valid links and how to interpret links
    'validtags' : ["pimkie"],  # a valid link must contain one of these strings
    'invalidtags' : ["tous", "Tous", "toute", "Toute", ".html", ".pdf", "javascript" ], # if a link contains one of these it is invalid
    'resetduplicatelinks' : True, # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks' : ['https://www.tbc.com'], # force any duplicate links to be ignored
    'starting_links' : [],    #additional links to process  #set any starting links that will not be picked up by the find starting pages process
    'tagref' : 'a', # look for hrefs in this class
    'remove_switches_link' : True,  # if True, remove ? and # switches from link (recommended unless required)
    'departmentlevel' : 2, #the nth level in the link
    'categorylevel' : 3,  #the nth level in the link
    'subcategorylevel' : 4,  #the nth level in the link
    'minimumdepth' : 1 ,  #a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    #define how the web page is accessed
    'useRequests' : False, # whether or not to use the requests module, if False use the chromium driver Browser
    'findNextPage' : True, # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    'htmlParser' :  'lxml', # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    #webdriver parameters
    'webdriverpath' : 'dummy' , # path to the webdriver executable SET UP IN CoreScraper.py
    'displayOff' : False,  # whether display is switched off or visible
    'displayMinimised' :  False,  # whether to minimise the window if display is on
    'resetDriver' : False,  #whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'resetCookies' : True,
    'waitForPageLoad' : 3, #typ set to half webPageWait, when scrolling, time to wait for page to load before comparing 2 subsequent loads to see if scrolled page has finished loading
    'webPageWait' : 3,  # time between subsequent web pages (next link, next page, next scroll down page)
    'scrollDownPage' : False,  # whether to scroll down the page to find more products, e.g. Topshop
    # url and redirect parameters
    'urlSwitch' :  '',    #sets home country to uk
    'invalidRedirect' : '', # if the redirected url contains this value the product pages do not load correctly
    #define any buttons to be clicked on screen
    #'buttonXpaths' : [ '//*[@id="wrapper"]/div[4]/a'  ], # a list of XPaths
    'buttonXpaths' : [ '/html/body/div[4]/a'  , '//*[@id="js-cookies_valid"]', '//*[@id="didomi-notice-agree-button"]/span'], # a list of XPaths
    'buttoniFrameXpaths' : [],
    'proxy_location': '',
    'vpn_location': 'FRWS',
    'luminati_proxy': '',
    'numscreenshots' : 0,
    })
            
#%%  RETAILER SPECIFIC - REDEFINE THE PRODUCTDATAFRAME METHODS
def new_get_description(self):
    #convert the json product data to pythion dict
    json_data = self.webSoup.find('a', class_="selected")
    if json_data != None :
        data_product = json.loads( json_data["data-product"])
        if data_product != None : 
            d = data_product["name"]
            if d != None: 
                description = d.replace('\n', '').replace('\r', '').strip()
                self.description = description        
          
def new_get_url(self):
    info = self.webSoup.find('a', class_="thumb-link")
    if info != None :
        url = info.get('href')
        self.url = url
        
def new_get_skuid(self):
    json_data = self.webSoup.find('a', class_="selected")
    if json_data != None :
        data_product = json.loads( json_data["data-product"])
        if data_product != None : 
            skuid = data_product["id"]
            self.skuid = skuid
        
def new_get_imageurl(self):
    i = self.webSoup.find('img')
    if i != None:  
        #case 1
        imgurl =  i.get('src')
        #case 2
        if imgurl == None: imgurl =  i.get('data-src')
        self.imgurl = imgurl
        
def new_get_banner(self):
    info = self.webSoup.find('span', class_="product-flags")
    if info != None : 
        b = info.get_text()
        if b != None: 
            banner = b.replace('\n', '').replace('\r', '')
            self.banner = banner

def new_get_reviewscore(self):
    info = self.webSoup.find('div',  { 'class' : "product-name"} )
    if info != None:
        rs = info.find('div', {'class' :"ts-stars-fullBar"} )
        if rs != None : 
            r = rs.get("style")
            if r != None :
                reviewscore = r.split('(')[1].split('/')[0]
                self.reviewscore = reviewscore

def new_get_reviewcount(self):
    info = self.webSoup.find('div',  { 'class' : "product-name"} )
    if info != None:
        rc = info.find('span', { 'class' : "ts-stars-reviewCount" } )
        if rc != None : 
            r = rc.get_text()
            if r != None :
                reviewcount = r.replace('\n', ' ').replace('\r', ' ').replace('(', ' ').replace(')', ' ').strip()
                self.reviewcount = reviewcount


            
def new_get_ticket_previous_price(self):
    ticketprice = None
    prevprice = None
    json_data = self.webSoup.find('a', class_="selected")
    if json_data != None :
        data_product = json.loads( json_data["data-product"])
        ticketprice = data_product["price"]
        prevprice = self.webSoup.find('span', class_="product-standard-price crossed")
        if prevprice != None:
            prevprice = prevprice.text.replace('\n', '').replace('\r', '').strip()
            prevprice = extractPrice(prevprice, "€")
        else:
            prevprice = ticketprice
            prevprice = extractPrice(prevprice, "£")
    #ticket price are stored with decimal seperator, when present prev price is in  €format
    self.ticketprice = extractPrice(ticketprice, "£")
    self.prevprice = prevprice
    
        
def new_get_promo_message(self):
    self.promomessage = None
            
def new_find_all_products(self):
    products = self.webSoup.find_all('div',class_="product-tile")
    print('num products', len(products))
    self.products = products
    
#define the method that is used to find the product description from the individual product page url
def new_get_product_description(self):
    product_info_text_block = self.webSoup.find('div', { 'class' : 'product-tabs'})
    if product_info_text_block != None:
        pi_t = product_info_text_block.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ")
    else:
        pi_t = ""
    self.long_description =   pi_t 
    #get the colour description
    info = self.webSoup.find('li', {'class' : 'attribute color'})
    if info != None:
        colour_info = info.find('span', {'class' : 'text-color'})
        if colour_info != None:
            self.colour = colour_info.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ")
        else:
            colour_info = info.find('span', {'class' : 'option-color'})
            if colour_info != None:
                self.colour = colour_info.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ")
    #TODO fetch the size description data = [list of strings]
    self.size = "size  desc"
    self.availability = "availability desc"

#define the method that is used to find the main image url from the individual product page url
def new_get_product_imgage_url(self):
    image_info = self.webSoup.find('img', {'class' : 'primary-image'})
    if image_info != None:
        image = image_info.get("src")
        if image != None:
            url = image
        #url2 = image_url.find('img')["srcset"]
        #if url != None:
            #url = 'https:' + url
            self.imgurl = url

#find the next page link from the current page
def new_find_next_page_from_webpage(self):
    #driver = self.driver # for access to buttons on the page etc
    #for abercrombie link from html
    link = self.link
    findNextPage = params['findNextPage']
    mainurl = params['mainurl']
    webSoup = self.webSoup
    nextPage = None
    if findNextPage:    
        page_info = webSoup.find("div", { 'class': "pagelist-nav" })
        if page_info != None:
            next_link = page_info.find('a', class_='pagination-next-button')
            if next_link != None: 
                nextPage = next_link.get("href")
            else:
                nextPage = None
    print('Next Page' , nextPage)
    self.nextPage = nextPage

#redefine the methods specific to this retailer        
ProductDataFrame.get_description =  new_get_description
ProductDataFrame.get_url = new_get_url
ProductDataFrame.get_skuid = new_get_skuid
ProductDataFrame.get_imageurl = new_get_imageurl
ProductDataFrame.get_banner = new_get_banner
ProductDataFrame.get_ticket_previous_price = new_get_ticket_previous_price
#ProductDataFrame.get_brand = new_get_brand
ProductDataFrame.get_reviewscore = new_get_reviewscore
ProductDataFrame.get_reviewcount = new_get_reviewcount
ProductDataFrame.get_product_description = new_get_product_description
ProductDataFrame.get_product_imgage_url = new_get_product_imgage_url
WebPageItems.get_promo_message = new_get_promo_message
WebPageItems.find_all_products = new_find_all_products
WebPageItems.find_next_page_from_webpage = new_find_next_page_from_webpage
#new methods can be added using the same approach, e.g. WebPageItems.newmethod = newmethod


#%%    RETAILER SPECIFIC - IDENTIFY WHICH OPTIONAL METHODS TO RUN 

methods_to_run = {
        'output_product_to_screen' : False,
        'output_product_values_to_screen' : False,
        'output_image_to_file' : True,
        'output_description_to_file' : True,
        'output_webpage_to_file' : False,
        }
     
            
# %% Main Code

    
def main():
    filename = __file__
    startPage = 20  # EDIT HERE FOR SET_UP AND TESTING to processs a sub range of weblinks
    endPage = 21 # EDIT HERE
    scrapeWebPages( startPage, endPage, linkurls, params, filename, methods_to_run)
    return
    
 
if __name__ == '__main__':
    main()