import __init__ # set up the import paths
import bs4
import datetime as dt
import pandas as pd
import time
import json
from pathlib import Path
import requests
import html5lib
import os
import re
from math import ceil
import logging

from CoreScraper import scrapeWebPages, extractPrice
from CoreScraper_getHtml import WebPageItems, ProductDataFrame
from CoreScraperConstants import toppath,  default_params

#  RETAILER SPECIFIC CONTROL PARAMETERS
# Set up the top path as parent folder of results, Note this is overridden if supplied as an argument when called as a main function
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape TEST/'

# set up multiple urls for finding links, first url is mainurl (code filtters duplicates)
linkurls = ['https://www.fatface.com']

params = default_params.copy()
params.update({
    'mainurl': linkurls[0],
    'opfpath': toppath,  # can be overidden by arguments, applied in in scrapeWebPages
    # define valid links and how to interpret links
    'validtags': ['fatface'],  # a valid link must contain one of these strings
    'invalidtags': ['help', 'legal', '/filter/', 'corporate', 'order', 'about', 'policies', 'facebook', 'instagram',
                    'twitter', 'youtube', 'pinterest', 'store'],  # if a link contains one of these it is invalid
    'resetduplicatelinks': True,
    # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks': ['https://www.tbc.com'],  # force any duplicate links to be ignored
    'starting_links': [],
    # additional links to process  #set any starting links that will not be picked up by the find starting pages process
    'tagref': 'a',  # look for hrefs in this class
    'remove_switches_link': False,  # if True, remove ? and # switches from link (recommended unless required)
    'departmentlevel': 3,  # the nth level in the link
    'categorylevel': 4,  # the nth level in the link
    'subcategorylevel': 5,  # the nth level in the link
    'minimumdepth': 5,
    # a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    # define how the web page is accessed
    'useRequests': False,  # whether or not to use the requests module, if False use the chromium driver Browser
    'findNextPage': False,
    # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    'htmlParser': 'lxml',
    # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    # webdriver parameters
    'webdriverpath': 'dummy',  # path to the webdriver executable SET UP IN CoreScraper.py
    'displayOff': False,  # whether display is switched off or visible
    'displayMinimised': False,  # whether to minimise the window if display is on
    'resetDriver': True,  # whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'waitForPageLoad': 3,# typ set to half webPageWait, when scrolling, time to wait for page to load before comparing 2 subsequent loads to see if scrolled page has finished loading
    'webPageWait': 3,  # time between subsequent web pages (next link, next page, next scroll down page)
    'scrollDownPage': True,  # whether to scroll down the page to find more products, e.g. Topshop
    # url and redirect parameters
    # 'urlSwitch' :  '#pagination=true',    #add an option to the url,e.g. 48 items per page
    'urlSwitch': '',  # add an option to the url,e.g. 48 items per page
    'invalidRedirect': '',  # if the redirected url contains this value the product pages do not load correctly
    # define any buttons to be clicked on screen
    'buttonXpaths': ['/html/body/div[1]/div/div/div/div'],  # a list of XPaths
    # 'buttonXpaths' : [ '' ], # a list of XPaths
    'buttoniFrameXpaths': [],
    # "document.querySelector('#\31 556719216 > div.header > div > div.closeButton.closeSticky')"
    'proxy_location': '',
    # Give multiple country like this, 'GB|US'. country codes: https://laendercode.net/en/2-letter-list.html
    'vpn_location': 'GBWS',
})


# %%  RETAILER SPECIFIC - REDEFINE THE PRODUCTDATAFRAME METHODS
def new_get_description(self):
    info = self.webSoup.find('div', {'class': 'b-product-name'})
    if info != None:
        d = info.find('a')
        if d != None:
            description = d.get_text().replace('\n', '').replace('\r', '').strip()
            self.description = description


def new_get_url(self):
    info = self.webSoup
    if info != None:
        u = info.find('a')
        if u != None:
            url = u.get('href')
            #self.url = self.mainurl + url
            self.url =  url


def new_get_skuid(self):
    info = self.webSoup.find('div', {'class': re.compile("b-product-tile$")})
    if info != None:
        u = info
        if u != None:
            skuid = u.get('data-itemid')
            self.skuid = skuid


def new_get_imageurl(self):
    i = self.webSoup.find('img')
    if i != None:
        imgurl = i.get('data-src')
        self.imgurl = imgurl


def new_get_brand(self):
    # not implemented
    info = self.webSoup.find('p')
    if info != None:
        d = info.find('a', {'class': "productMainLink"})
        if d != None:
            self.brand = None


def new_get_stock(self):
    # not implemented
    info = self.webSoup.find('span', {'class': 'listProductLowStock'})
    if info != None:
        d = info.get_text()
        if d != None:
            self.stock = None


def new_get_banner(self):
    info = self.webSoup.find('span', {'class': "price-save"})
    b = info
    if b != None:
        banner = b.get_text().strip()
        self.banner = banner


def new_get_ticket_previous_price(self):
    ticketprice = 0
    prevprice = 0
    wasprice = None
    nowprice = None

    wp = self.webSoup.find('span', {'class': "b-price__value_old"})
    if wp != None:
        wasprice = wp.find('span', {'class': "b-price__digit"})
        if wasprice != None:
            wasprice = wasprice.get_text()
    # print('WASPRICE', wasprice)

    np = self.webSoup.find('span', {'class': re.compile("b-price__value_new$")})
    if np != None:
        nowprice = np.find('span', {'itemprop': 'price'})
        nowprice = np.get_text()
        # nowprice = nowprice[ nowprice.find("£"):]
    # print('NOWPRICE', nowprice)

    if wasprice == None:  # standrd price
        np = self.webSoup.find('span', {'itemprop': 'price'})
        if np != None:
            nowprice = np.get_text()

    if wasprice == None:
        ticketprice = extractPrice(nowprice, "£")
        prevprice = ticketprice
    else:
        ticketprice = extractPrice(nowprice, "£")
        prevprice = extractPrice(wasprice, "£")

    self.ticketprice = extractPrice(ticketprice, "£")
    self.prevprice = extractPrice(prevprice, "£")


def new_get_reviewcount(self):
    # not implemented all 5 stars
    info = self.webSoup.find('span', {'class': "product-rating__review-count"})
    if info != None:
        r = info.get_text()
        if r != None:
            r = r.split('(')[1].split('r')[0]
            reviewcount = r.replace('\n', ' ').replace('\r', ' ').replace('(', ' ').replace(')', ' ').strip()
            self.reviewcount = reviewcount


def new_get_reviewscore(self):
    # not implemented all 5 stars
    info = self.webSoup.find('div', {'class': "product-rating"})
    if info != None:
        fullstar = info.find_all('img', {'alt': 'Full Star'})
        halfstar = info.find_all('img', {'alt': 'Half Star'})
        reviewscore = len(fullstar) + 0.5 * len(halfstar)
        self.reviewscore = reviewscore


def new_get_promo_message(self):
    # not implemented
    info = self.webSoup.find('div', {'id': 'header_espot'})
    if info != None:
        pm = info.find('img')
        if pm != None:
            self.promomessage = pm.get('alt')


def new_find_all_products(self):    
    # products = self.webSoup.find_all('div', { 'class' : re.compile("product col*") } )
    #changed
    #products_all = self.webSoup.find('div', {'class': re.compile("l-plp-grid$")})
    #changed Feb 2021
    products_all = self.webSoup.find('div', {'class': 'js-search-result-items'})
    if products_all != None:
        products = products_all.find_all('div', {'class': re.compile("l-plp-grid__item$")}, recursive=False)
    else:
        products = []
    logging.info(('num products', len(products)))
    self.products = products


#define the method that is used to find the product description from the individual product page url
def new_get_product_description(self):
    pi_t = ""
    pi_c = ""
    product_info_text_block = self.webSoup.find('p', { 'class' : 'b-content-longdesc'})
    if product_info_text_block != None:
        #print('product info', product_info_text_block.prettify())
        block1 = product_info_text_block
        if block1 != None:
            pi_t = block1.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
    
    product_info_text_block = self.webSoup.find('div', { 'class' : 'b-content-bullets'})
    if product_info_text_block != None:
        pi_c =    product_info_text_block.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
    self.long_description =   pi_t + " " + pi_c
    #get the colour description
    info = self.webSoup.find('span', {'class' : 'b-product-variations__value'})
    if info != None:
        colour_info = info.get_text()
        if colour_info != None:
            self.colour = colour_info.replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()

    #TODO fetch the size description data = [list of strings]
    self.size = "size  desc"
    self.availability = "availability desc"

#define the method that is used to find the main image url from the individual product page url
def new_get_product_imgage_url(self):
    logging.info(('Fetching imnage URL'))
    image_info = self.webSoup.find('picture')
    if image_info != None:
        images = image_info.find_all('source')
        if len(images) ==1:
            url = images[0].get("srcset")
            if url != None: 
                self.imgurl = 'https:' + url.split('1x,')[0]
        elif len(images) > 1:
            url = images[1].get("srcset")
            if url != None:
                self.imgurl = 'https:' + url.split('1x,')[0]   
        #url2 = image_url.find('img')["srcset"]
        #if url != None:
            #url = 'https:' + url
        #self.imgurl = url

# find the next page link from the current page
def new_find_next_page_from_webpage(self):
    # driver = self.driver # for access to buttons on the page etc
    # for white company add page=N to the end of the url
    link = self.link
    findNextPage = params['findNextPage']
    nextPage = None
    if findNextPage:
        # find current page number if already set
        pagePos = int(link['webpage'].find('page='))
        if pagePos == -1:
            # page no currently not set
            nextPage = link['webpage'] + '?page=2'
        else:  # page number has already been set
            currPage = int(link['webpage'][pagePos + 5:])
            nextPage = currPage + 1
            nextPage = link['webpage'].replace('page=' + str(currPage), 'page=' + str(nextPage))

        # override - if there are no products on the current page then end of page has been reached
        if len(self.products) == 0:
            nextPage = None

        logging.info(('Next Page', nextPage))

    self.nextPage = nextPage


# %%


# redefine the methods specific to this retailer
ProductDataFrame.get_description = new_get_description
ProductDataFrame.get_url = new_get_url
ProductDataFrame.get_skuid = new_get_skuid
ProductDataFrame.get_imageurl = new_get_imageurl
ProductDataFrame.get_banner = new_get_banner
ProductDataFrame.get_brand = new_get_brand
ProductDataFrame.get_stock = new_get_stock
ProductDataFrame.get_reviewscore = new_get_reviewscore
ProductDataFrame.get_reviewcount = new_get_reviewcount
ProductDataFrame.get_ticket_previous_price = new_get_ticket_previous_price
ProductDataFrame.get_product_description = new_get_product_description
ProductDataFrame.get_product_imgage_url = new_get_product_imgage_url
WebPageItems.get_promo_message = new_get_promo_message
WebPageItems.find_all_products = new_find_all_products
WebPageItems.find_next_page_from_webpage = new_find_next_page_from_webpage
# new methods can be added using the same approach, e.g. WebPageItems.newmethod = newmethod


# %%    RETAILER SPECIFIC - IDENTIFY WHICH OPTIONAL METHODS TO RUN

methods_to_run = {
    'output_product_to_screen': False,
    'output_product_values_to_screen': False,
    'output_image_to_file': True,
    'output_description_to_file': True,
    'output_webpage_to_file': False,
}


# %% Main Code


def main():
    filename = __file__
    startPage = 0  # EDIT HERE FOR SET_UP AND TESTING to processs a sub range of weblinks
    endPage = 1  # EDIT HERE
    scrapeWebPages(startPage, endPage, linkurls, params, filename, methods_to_run)
    return


if __name__ == '__main__':
    main()
