import __init__ # set up the import paths
import bs4
import datetime as dt
import pandas as pd
import time
import json
from pathlib import Path
import requests
import html5lib
import os
import re
from math import ceil

from CoreScraper import scrapeWebPages, extractPrice
from CoreScraper_getHtml import WebPageItems, ProductDataFrame, clickButton
from CoreScraperConstants import toppath,  default_params

#  RETAILER SPECIFIC CONTROL PARAMETERS
# Set up the top path as parent folder of results, Note this is overridden if supplied as an argument when called as a main function
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape TEST/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape TEST/'

# set up multiple urls for finding links, first url is mainurl for capturing screenshots  (code filtters duplicates)
linkurls = ['https://www.marksandspencer.com/']

params = default_params.copy()
params.update({
    'mainurl': 'https://www.marksandspencer.com',
    'opfpath': toppath,
    # can be overidden by arguments, applied in in scrapeWebPages    #define valid links and how to interpret links
    'sitemap_links': ['https://www.marksandspencer.com/sitemaps#intid=footernav_sitemap'],
    'validtags': ['offer', 'sale',  'price', 'women', 'men', 'lingerie'],  # a valid link must contain one of these strings
    'invalidtags': ["javascript", "statement", "Size-Guide", "Cookie", 'bank'],  # if a link contains one of these it is invalid
    'resetduplicatelinks': True,
    # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks': ['https://www.tbc.com'],  # force any duplicate links to be ignored
    'starting_links': [],
    # additional links to process  #set any starting links that will not be picked up by the find starting pages process
    'tagref': 'a',  # look for hrefs in this class
    'remove_switches_link': True,  # if True, remove ? and # switches from link (recommended unless required)
    'departmentlevel': 4,  # the nth level in the link
    'categorylevel': 5,  # the nth level in the link
    'subcategorylevel': 6,  # the nth level in the link
    'minimumdepth': 5,
    # a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    # define how the web page is accessed
    'useRequests': False,  # whether or not to use the requests module, if False use the chromium driver Browser
    'findNextPage': True,
    # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    'htmlParser': 'lxml',
    # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    # webdriver parameters
    'webdriverpath': 'dummy',  # path to the webdriver executable SET UP IN CoreScraper.py
    'displayOff': False,  # whether display is switched off or visible
    'displayMinimised': False,  # whether to minimise the window if display is on
    'resetDriver': False,  # whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'resetCookies' : False, #whether or not to reset the cookies on every page
    'waitForPageLoad': 2,
    # typ set to half webPageWait, when scrolling, time to wait for page to load before comparing 2 subsequent loads to see if scrolled page has finished loading
    'webPageWait': 2,  # time between subsequent web pages (next link, next page, next scroll down page)
    'scrollDownPage': False,  # whether to scroll down the page to find more products, e.g. Topshop
    'short_webPageWait': 1,  # define a shorter time to wait for when loading individal product pages
    # url and redirect parameters
    'urlSwitch': '',  # add an option to the url,e.g. 48 items per page
    'invalidRedirect': '',  # if the redirected url contains this value the product pages do not load correctly
    # define any buttons to be clicked on screen
    'buttonXpaths': [ '//*[@id="1556718956"]/div[2]/div/div[2]' , '//*[@id="consent_prompt_submit"' , '//*[@id="consent_prompt_submit"]/b',
                        '//*[@id="common-hdr-main"]/div[4]/div[2]/div/b'],  # a list of XPaths
    'buttoniFrameXpaths': [],
    # set up variables to hold values between pages
    'num_products': 0,  # initialise the number of products found
    'page_url': '',
    'proxy_location': '',
    # Give multiple country like this, 'GB|US'. country codes: https://laendercode.net/en/2-letter-list.html
    'show_proxy_info': False,
    'vpn_location': 'GBWS',  # should be GB but got blocked on the London Tube 
})


# %%  RETAILER SPECIFIC - REDEFINE THE PRODUCTDATAFRAME METHODS
def new_get_description(self):
    info = self.webSoup.find('h3', {'class' : 'product__title'})
    if info != None : 
        d = info
        if d != None: 
            description = d.get_text().replace('\n', '').replace('\r', '').strip()
            self.description = description     
            
def new_get_url(self):
    info = self.webSoup
    if info != None : 
        u = info.find('a')
        if u != None :
            url = u.get('href')
            self.url = self.mainurl + url

def new_get_skuid(self):
    info = self.url
    skuid = info.split('/')[-1]
    self.skuid = skuid
        
def new_get_imageurl(self):
    i = self.webSoup.find('img')
    if i != None:  
        imgurl =  i.get('src')
        self.imgurl = imgurl

def new_get_brand(self):
    info = self.webSoup.find('div', {'class' : 'product__brand'})
    if info != None : 
        d = info.get_text()
        if d != None: 
            self.brand = d
            
def new_get_stock(self):
    #not implemented 
    info = self.webSoup.find('span' , {'class' : 'listProductLowStock'})
    if info != None : 
        d = info.get_text()
        if d != None: 
            self.stock = None
            
def new_get_banner(self):
    info = self.webSoup.find('div',  { 'class' : "product__badge" })
    if info != None:
        b = info.find('span')
        if b != None: 
            banner = b.get_text().strip()
            self.banner = banner
            
def new_get_ticket_previous_price(self):
    ticketprice = 0
    prevprice = 0
    price_info = self.webSoup.find('div' ,  { 'class' : "product__price" } )
    if price_info != None:
        prices = price_info.get_text().replace('EUR', '').strip().split()
        numbers = []
        for p in prices:
            #extract any word that contains a number
            if any(str.isdigit(c) for c in p):
                #replace anything that is not a digit +,- or dot with a space
                p = re.sub(r'[^-+\d.]', '', p)
                numbers.append(float(p))
        #print(prices, numbers)
        ticketprice = min(numbers)
        prevprice = max(numbers)
    else:
        price_info = self.webSoup.find('div' ,  { 'class' : "sale-price" } )
        if price_info != None:
            ticketprice = price_info.find('p', {'class' : 'price price--reduced'})
            prevprice = price_info.find('p', {'class' : 'price price--previous'})
            if prevprice != None:
                prevprice = prevprice.get_text()
                if prevprice != None:
                    prevprice = prevprice.split('£')[-1]
            if ticketprice != None:
                ticketprice = ticketprice.get_text()
                if ticketprice != None:
                    ticketprice = ticketprice.split('£')[-1]

    self.ticketprice = extractPrice(ticketprice, "£")
    self.prevprice = extractPrice(prevprice, "£")
    #tidy up - sometimes in sale items there is ticketprice but no previous price, occurs where there is a range of sale price
    if self.prevprice == 0:
        self.prevprice = self.ticketprice

    
def new_get_reviewcount(self):
    #not implemented all 5 stars
    info = self.webSoup.find('span', {'class' : "product-rating__review-count"})
    if info != None : 
        r = info.get_text()
        if r != None :
            r = r.split('(')[1].split('r')[0]
            reviewcount = r.replace('\n', ' ').replace('\r', ' ').replace('(', ' ').replace(')', ' ').strip()
            self.reviewcount = reviewcount

def new_get_reviewscore(self):
    #not implemented all 5 stars
    info = self.webSoup.find('div', {'class' : "product-rating"})
    if info != None : 
        fullstar = info.find_all('img',  { 'alt' : 'Full Star' })
        halfstar = info.find_all('img',  { 'alt' : 'Half Star' })
        reviewscore = len(fullstar) + 0.5*len(halfstar)
        self.reviewscore = reviewscore
        
def new_get_promo_message(self):
    info = self.webSoup.find('p', {'class' : 'product__offer'})
    if info != None : 
        pm = info.get_text()
        if pm != None: 
            self.promomessage = pm


def new_find_all_products(self):
    products = self.webSoup.find_all('li', { 'class' : re.compile("grid__tile*") } )
    print('num products', len(products))
    self.products = products


# find the next page link from the current page, use the buttons to load the next link, then get the link and store as the Next Page
# for some reason editing and updating the url directly does not work for New Look
def new_find_next_page_from_webpage(self):
    # function to find the Load More /Next button , click on it and return True if successful, Note there are 2 different types of Load More button
    # one where the text is in a Span below the button definition
    def find_click_load_more_button(driver):
        try:
            driver.find_element_by_xpath('//*[@id="fesk-find"]/section/div[6]/a[contains(text(), "Load")]').click()
            button_clicked = True
            print('Clicking Load Next button - button contains text')
        except:
            # Load more button not present / not clickable
            button_clicked = False
            print('Load more button not clicked')
        return button_clicked

    driver = self.driver  # for access to buttons on the page etc
    params = self.params

    print('Clicking the Load More Next Button ')
    # view all button not present / clickable so try the Load buttonn
    button_clicked = find_click_load_more_button(driver)  # returns false if the button is not present
    if not button_clicked:
        # try again just in case the page did not load properly, seems to happen with New Look
        button_clicked = False
        attempts = 0
        while not button_clicked and attempts < 5:
            print('reclicking load more button, not yet present')
            # try clearing any pop up boxes
            clickButton(driver, params)
            time.sleep(1)
            attempts = attempts + 1
            button_clicked = find_click_load_more_button(
                driver)  # returns true while the button is presen and clicked , False if not present and loaded
            nextPage = None  # set next page  None, if no more products to load the button is sometimes / usually not present
    if button_clicked:
        print('clicked load more button')
        time.sleep(params['waitForPageLoad'])
        # nextPage = driver.current_url
        nextPage = "DO_NOT_RELOAD_PAGE"

    print('Next Page', nextPage)
    self.nextPage = nextPage

#define the method that is used to find the product description from the individual product page url
def new_get_product_description(self):
    text_block = self.webSoup.find('p', { 'class' :  re.compile("product-description*") })
    if text_block == None:
        text_block = self.webSoup.find('div', { 'class' : re.compile("product-description*") })
        if text_block == None:
            text_block = self.webSoup.find('p', { 'class' : re.compile("product-information*") })
    if text_block != None:
        self.long_description = text_block.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ")
    #get the colour description
    info = self.webSoup.find('strong', {'class' : 'colour-picker__name'})
    if info != None:
        self.colour = info.get_text().strip()
    #TODO fetch the size description data = [list of strings]
    self.size = "size  desc"
    self.availability = "availability desc"

#define the method that is used to find the main image url from the individual product page url
def new_get_product_imgage_url(self):
    image_url = self.webSoup.find('div', {'class' : 'image-grid__item'})
    if image_url == None:
        image_url = self.webSoup.find('div', {'class' : 'pdp-carousel__images-holder'})
    if image_url != None:
        url = image_url.find('img')["src"]
        #url2 = image_url.find('img')["srcset"]
        #if url != None:
            #url = 'https:' + url
        self.imgurl = url


# redefine the methods specific to this retailer
ProductDataFrame.get_description = new_get_description
ProductDataFrame.get_url = new_get_url
ProductDataFrame.get_skuid = new_get_skuid
ProductDataFrame.get_imageurl = new_get_imageurl
ProductDataFrame.get_banner = new_get_banner
ProductDataFrame.get_ticket_previous_price = new_get_ticket_previous_price
ProductDataFrame.get_brand = new_get_brand
ProductDataFrame.get_reviewscore = new_get_reviewscore
ProductDataFrame.get_reviewcount = new_get_reviewcount
ProductDataFrame.get_product_description = new_get_product_description
ProductDataFrame.get_product_imgage_url = new_get_product_imgage_url
WebPageItems.get_promo_message = new_get_promo_message
WebPageItems.find_all_products = new_find_all_products
WebPageItems.find_next_page_from_webpage = new_find_next_page_from_webpage
# new methods can be added using the same approach, e.g. WebPageItems.newmethod = newmethod


# %%    RETAILER SPECIFIC - IDENTIFY WHICH OPTIONAL METHODS TO RUN FOR EACH PRODUCT

methods_to_run = {
    'output_product_to_screen': False,
    'output_image_to_file': True, # set True to output product images to GS
    'output_description_to_file' : True, # set True to output product descriptions to BQ
    'output_product_values_to_screen': False,
    'output_webpage_to_file': False,
}


# %% Main Code    

def main():
    filename = __file__
    startPage =86  # EDIT HERE FOR SET_UP AND TESTING to processs a sub range of weblinks
    endPage = 87  # EDIT HERE
    scrapeWebPages(startPage, endPage, linkurls, params, filename, methods_to_run)
    return


if __name__ == '__main__':
    main()
