import __init__ # set up the import paths
import bs4
import datetime as dt
import pandas as pd
import time
import json
from pathlib import Path
import requests
import html5lib
import os
import re
from math import ceil
import logging
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from CoreScraper import scrapeWebPages, extractPrice
from CoreScraper_getHtml import WebPageItems, ProductDataFrame
from CoreScraperConstants import toppath,  default_params

#  RETAILER SPECIFIC CONTROL PARAMETERS
# Set up the top path as parent folder of results, Note this is overridden if supplied as an argument when called as a main function
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape TEST/'

# set up multiple urls for finding links, first url is mainurl (code filtters duplicates)
linkurls = ['https://www.amazon.co.uk/']



params = default_params.copy()
params.update({
    'mainurl': linkurls[0],
    'opfpath': toppath,  # can be overidden by arguments, applied in in scrapeWebPages
    'numscreenshots' : 0, # number of screnshots to save
    # define valid links and how to interpret links
    'validtags': ['somethingtomakesurewedontfindanyotherlinks'],  # a valid link must contain one of these strings
    'invalidtags': ['help'],  # if a link contains one of these it is invalid
    'resetduplicatelinks': True,
    # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks': ['https://www.tbc.com'],  # force any duplicate links to be ignored
    #'#for Amazon the starting link needs to be a url created by the Amazon search box
    #'starting_links': ['https://www.amazon.co.uk/s?k=new+balance+trainers&crid=Y46JDEAELSIE&sprefix=new+balance%2Caps%2C136&ref=nb_sb_noss_1'], #New Balance Trainers
    'starting_links': ['https://www.amazon.co.uk/s?k=new+balance+mens+trainers&crid=HYA7T704SW6D&sprefix=%2Caps%2C76&ref=nb_sb_ss_recent_1_0_recent'], #Mens
    'starting_links': ['https://www.amazon.co.uk/s?k=new+balance+womens+trainers&crid=1NN90MQJJR6L0&sprefix=new+balance+womens+trainers%2Caps%2C87&ref=nb_sb_ss_ts-doa-p_1_27'], # New Balance Womens Trainers
    # additional links to process  #set any starting links that will not be picked up by the find starting pages process
    'tagref': 'a',  # look for hrefs in this class
    'remove_switches_link': False,  # if True, remove ? and # switches from link (recommended unless required)
    'departmentlevel': 2,  # the nth level in the link
    'categorylevel': 3,  # the nth level in the link
    'subcategorylevel': 4,  # the nth level in the link
    'minimumdepth': 5,
    # a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    # define how the web page is accessed
    'useRequests': False,  # whether or not to use the requests module, if False use the chromium driver Browser
    'findNextPage': True,
    # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    'htmlParser': 'lxml',
    # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    # webdriver parameters
    'webdriverpath': 'dummy',  # path to the webdriver executable SET UP IN CoreScraper.py
    'displayOff': False,  # whether display is switched off or visible
    'displayMinimised': False,  # whether to minimise the window if display is on
    'resetDriver': False,  # whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'waitForPageLoad': 3,# typ set to half webPageWait, when scrolling, time to wait for page to load before comparing 2 subsequent loads to see if scrolled page has finished loading
    'webPageWait': 3,  # time between subsequent web pages (next link, next page, next scroll down page)
    'scrollDownPage': False,  # whether to scroll down the page to find more products, e.g. Topshop
    # url and redirect parameters
    # 'urlSwitch' :  '#pagination=true',    #add an option to the url,e.g. 48 items per page
    'urlSwitch': '',  # add an option to the url,e.g. 48 items per page
    'invalidRedirect': '',  # if the redirected url contains this value the product pages do not load correctly
    # define any buttons to be clicked on screen
    'buttonXpaths': ['//*[@id="sp-cc-accept"]', '//*[@id="sp-cc-accept"]', '//*[@id="sp-cc-rejectall-link"]'],  # a list of XPaths
    # 'buttonXpaths' : [ '' ], # a list of XPaths
    'buttoniFrameXpaths': [],
    # "document.querySelector('#\31 556719216 > div.header > div > div.closeButton.closeSticky')"
    'proxy_location': '',
    # Give multiple country like this, 'GB|US'. country codes: https://laendercode.net/en/2-letter-list.html
    'vpn_location': 'GBWS',
    #turn off processing attributes
    'process_attributes' : True,
    #configure output data tables
    'GBQ_TABLE_RESULTS' : 'NB_Amazon_results',
    'GBQ_TABLE_DESCRIPTIONS' : 'NB_Amazon_descriptions',
    'TEST_GBQ_TABLE_RESULTS' : 'test_NB_Amazon_results',
    'TEST_GBQ_TABLE_DESCRIPTIONS' : 'test_NB_Amazon_descriptions',
    #extra description info - if False searches for the default product info, if True searches for the additional ebay product info
    'ADD_EBAY_PRODUCT_DESCRIPTIONS' : False,
})


# %%  RETAILER SPECIFIC - REDEFINE THE PRODUCTDATAFRAME METHODS
def new_get_description(self):
    #item title
    item_title = None
    itm_info =  self.webSoup.find('h2')
    if itm_info is not None:
        item_title = itm_info.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
    self.description = item_title

    self.long_description =   None


def new_get_url(self):
    info = self.webSoup
    if info != None:
        u = info.find('h2')
        if u != None:
            url = u.find('a').get('href')
            self.url = self.mainurl + url
            #self.url =  url


def new_get_skuid(self):
    url = self.imgurl
    skuid_info = url.split('/')
    if len(skuid_info) > 1:
        sku_id1 = skuid_info[-1]
        sku_id2 = sku_id1.split('.')
        sku_id = sku_id2[0]
    else:
        sku_id = 'Unknown' + self.description
    self.skuid = sku_id


def new_get_imageurl(self):
    i = self.webSoup.find('img')
    if i != None:
        imgurl = i.get('src')
        self.imgurl = imgurl


def new_get_brand(self):
    #item brand
    item_brand= None
    itm_info =  self.webSoup.find('h5' )
    if itm_info is not None :
        item_brand = itm_info.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
    self.brand = item_brand

def new_get_stock(self):
    # implemented as seller info
    pass


def new_get_banner(self):
    # implemented as ASIN
    pass


def new_get_ticket_previous_price(self):
    wasprice = 0
    nowprice = 0

    #extract  price string
    np = self.webSoup.findAll('span', { 'class' : 'a-price'})

    #get the first price, the now price, 
    if len(np) > 0:
        nowprice_info = np[0].findAll('span')
        if len(nowprice_info) == 1:
            nowprice = nowprice_info[0].get_text()
            #incase multiple prices extract first of multiple prices
            nowprice = nowprice.strip().split(' ')[0]
            wasprice = nowprice
        else:
            nowprice = nowprice_info[0].get_text()
            #incase multiple prices extract first of multiple prices
            nowprice = nowprice.strip().split(' ')[0]
            wasprice = nowprice

    if len(np) > 1:  #check to see if there is more than one price
        wasprice_info = np[1].findAll('span')
        # take price from 1st span
        if len(wasprice_info) > 0:
            wasprice = wasprice_info[0].get_text()
            #incase multiple prices extract first of multiple prices
            wasprice = wasprice.strip().split(' ')[0]


    self.ticketprice = extractPrice(nowprice, "£")
    self.prevprice = extractPrice(wasprice, "£")


def new_get_reviewcount(self):
    #find all the possible review data sections
    info = self.webSoup.findAll('div', { 'class' : 'a-row'})
    #search each possible to find the one containing review info
    review_count = ""
    for info_row in info:
        info_text = info_row.get_text()
        if 'stars' in info_text:
            review_count = info_text.split('stars')[1].strip()
    self.reviewcount = review_count

def new_get_reviewscore(self):
    #find all the possible review data sections
    info = self.webSoup.findAll('div', { 'class' : 'a-row'})
    #search each possible to find the one containing review info
    review_score = ""
    for info_row in info:
        info_text = info_row.get_text()
        if 'stars' in info_text:
            review_score = info_text.split('stars')[0].strip()
    self.reviewscore = review_score

def new_get_promo_message(self):
    # not implemented
    pass

def new_find_all_products(self):
    """
    find new products
    NOTE: the standard approach of using self.webSoup and the commands to search the beautiful soup objects doesnot
    work as the product data fails to load, reloading here with the webSoup approach does not work either
    The approach that does work is to use the driver.find_elemsnts method
    """
    #get the driver to the web page
    driver = self.driver
    #first scroll down the page to make sure the product details load
    #driver.find_element_by_tag_name('body').send_keys(Keys.PAGE_DOWN)
    driver.find_element(By.TAG_NAME, "body").send_keys(Keys.PAGE_DOWN)

    #find if new balance brand check box already clicked
    try:
        check_box = driver.find_element(By.XPATH, '//*[@id="p_89/New Balance"]/span/a/div/label/input')
        #print(check_box.get_attribute('innerHTML'))
        #print(check_box.get_attribute('outerHTML'))
        if 'checked=' in check_box.get_attribute('outerHTML'):
            check_box_clicked = True
        else:
            check_box_clicked = False
    except:
        check_box_clicked=False

    # if check box not already selected, find and click on the new balance only brand checkbox
    if not check_box_clicked:
        #wait until the element is present 
        nb_item = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="p_89/New Balance"]/span/a/div/label/i')))
        #print(nb_item.get_attribute('innerHTML'))
        #print(nb_item.get_attribute('outerHTML'))
        #wait for a bit as often get not clickable error even though we have the wait command above
        time.sleep(1)
        #click the checkbox
        nb_item.click()
        #scroll down to see the check box and first set of products
        driver.find_element(By.TAG_NAME, "body").send_keys(Keys.PAGE_DOWN)

    products = []
    #find all of the item details
    all_items = driver.find_elements(By.XPATH, "//div[@class='a-section a-spacing-base a-text-center']")
    for item in all_items:
        #item_html = item.get_attribute('innerHTML')
        #put in try except loop - cope with  error
        #selenium.common.exceptions.StaleElementReferenceException: Message: stale element reference: element is not attached to the page document
        try:
            item_html2 = item.get_attribute('outerHTML')
            #print(item_html2)
            #convert the html to beautiful soup object
            item_websoup = bs4.BeautifulSoup(item_html2 , params['htmlParser'])
            #print(item_websoup)
            products.append(item_websoup  )
        except:
            pass

    logging.info(('num products', len(products)))

    self.products = products


#define the method that is used to find the product description from the individual product page url
def new_get_product_description(self):
    #TBD - get the details from the individual product page

    #webSoup does not work - need to use find_element
    #print(self.webSoup.prettify())

    #get the driver to the web page
    driver = self.driver
    #first scroll down the page to make sure the product details load
    #driver.find_element_by_tag_name('body').send_keys(Keys.PAGE_DOWN)
    seller_info_found = False
    attempts = 0
    while not seller_info_found and attempts < 3:
        try:
            #find the seller info if available
            buybox_item = driver.find_element(By.ID, "tabular-buybox")
            item_html2 = buybox_item.get_attribute('outerHTML')
            #print(item_html2)
            seller_info_found=True
        except Exception as e:
            #wait and try again
            time.sleep(1)
            attempts = attempts + 1
            item_html2 = ""

    # if seller info has not been found, select a size if available to force the seller info
    # some cases there is no seller info until a size has been selected
    if not seller_info_found:
        #if seller info not available select a size to make it available, put intry except incase not available
        try:
            size_box = driver.find_element_by_id('native_dropdown_selected_size_name')
        except:
            size_box = None
        if size_box is not None:    
            select = Select(size_box)
            list_of_size_values = []
            for item in select.options:
                list_of_size_values.append(item.get_attribute('innerText').strip())
            #drop the first element of the list as it is the 'select' keyword
            list_of_size_values = list_of_size_values[1:]
            #select a size - to force the delivery info to load
            buy_box_found = False
            item_html2 = "" #incase exceed maximum attempts or size values not available
            #for size in list_of_size_values:
            i = 0
            while i < len(list_of_size_values) and not buy_box_found:
                i = i+1
                try:
                    select.select_by_visible_text(list_of_size_values[i])
                    select_success = True
                except Exception as e:
                    select_success = False
                if select_success:
                    #fetch the delivery info, but wait for the web page to populate
                    attempts = 0
                    while not buy_box_found and attempts < 3 :
                        try:
                            #find the seller info if available
                            time.sleep(1)
                            buybox_item = driver.find_element(By.ID, "tabular-buybox")
                            item_html2 = buybox_item.get_attribute('outerHTML')
                            buy_box_found = True
                            seller_info_found = True
                        except Exception  as e:
                            attempts = attempts + 1
                            time.sleep(0.5)

    #if seller info not found - see if currently unavailable message on page
    unavail_text = ""
    if not seller_info_found:
        try:
            unavailable_item = driver.find_element(By.ID, "availability")
            unavail_text = unavailable_item.get_attribute('innerText').strip()
        except Exception as e:
            unavail_text = ""

    #convert the delivery info html into beautiful soup format and search for the Sold By text
    item_websoup = bs4.BeautifulSoup(item_html2 , params['htmlParser'])
    #print(item_websoup.prettify())
    soldby_items = item_websoup.findAll('div', {'tabular-attribute-name' : 'Sold by'})
    item_text = ""
    seller_link = ""
    for item in soldby_items:
        item_text = item_text + " " + item.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
        #if the item also contains a href - link to the supplier then extract it as well
        seller_link = item.find('a')
        if seller_link is not None:
            seller_link = seller_link.get('href')

    if len(item_text) > 0:
        self.seller = item_text.strip()
    elif len(unavail_text) > 0:
        self.seller = unavail_text.strip()
    else:
        self.seller = 'no seller info'  

    #save the supplier link
    if seller_link is not None:
        self.seller_link = self.mainurl + seller_link 
    else:
        self.seller_link = ""

    #get the product details for ASIN
    #id="detailBullets_feature_div"
    ASIN = ""
    discontinued = ""
    date_first_available = ""
    item_model_no = ""
    try:
        prod_details_element = driver.find_element(By.ID, "detailBullets_feature_div")
        item_html2 = prod_details_element.get_attribute('outerHTML')
        item_websoup = bs4.BeautifulSoup(item_html2 , params['htmlParser'])
        li_items = item_websoup.findAll('li')
        for li in li_items:
            li_text = li.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
            if "ASIN" in li_text:
                ASIN_text = li_text
                ASIN = ASIN_text.split('\u200e')[-1].strip()
            if "Discontinued" in li_text:
                discontinued_text = li_text
                discontinued = discontinued_text.split('\u200e')[-1].strip()    
            if "Date First" in li_text:
                date_text = li_text
                date_first_available = date_text.split('\u200e')[-1].strip() 
            if "Item model" in li_text:
                item_text = li_text
                item_model_no = item_text.split('\u200e')[-1].strip() 
    except:
        print('No Product details')
    self.ASIN = ASIN
    self.discontinued = discontinued
    self.date_first_available = date_first_available
    self.item_model_no = item_model_no

    self.long_description =   None

#define the method that is used to find the main image url from the individual product page url
def new_get_product_imgage_url(self):
    #TBD - get the details from the individual product page
    #do not need this ?
    pass

    # image_info = self.webSoup.find('div' , {'class' : 'ux-main-image-carousel'})
    # #print(image_info.prettify())
    # if image_info != None:
    #     images = image_info.findAll('img')
    #     if len(images) >=1:
    #         url = images[0].get("src")
    #         if url != None: 
    #             self.imgurl = url



# find the next page link from the current page
def new_find_next_page_from_webpage(self):

    def find_click_next_button(driver):
        """ click on the next page button if available"""
        try:
            time.sleep(2)
            next_button = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.CLASS_NAME, 's-pagination-next')))
            button_html = next_button.get_attribute('outerHTML')
            next_button.click()
            button_clicked = True
            print('Clicking Next Button')
            #print(button_html)
        except:
            # Load more button not present / not clickable
            button_clicked = False
            print('Next button not clicked')
            button_html = ""
        return button_clicked, button_html

    #click the next page button if there
    driver = self.driver
    #scroll down a couple of pages
    for i in range (0,2):
        #scroll down to bottom of page to see next page when testing
        driver.find_element(By.TAG_NAME, "body").send_keys(Keys.PAGE_DOWN)
    #find and click the next page button
    next_button_clicked, button_html = find_click_next_button(driver)

    if next_button_clicked:
        nextPage = "DO_NOT_RELOAD_PAGE"
    else:
        nextPage = None

    # keep track of page numbers
    try:
        prev_page = params['prev_page']
    except:
        prev_page = 0 # first iteration
    curr_page = prev_page + 1
    params['prev_page'] = curr_page

    #check to see if end of page has been reached
    #convert the html to beautiful soup object
    button_soup = bs4.BeautifulSoup(button_html , params['htmlParser'])
    print(button_soup.prettify())
    page_label = button_soup.find('a')
    if page_label is not None:
        page_label = page_label.get('aria-label')
        page_no = int(page_label.split('page')[-1].strip())
        print(f'Next Page No Page {page_no}')
    else:
        print('No Next Page Info')
        page_no = curr_page
    
    # # testing temporarily stop after 1 page
    # if curr_page == 2:
    #     nextPage = None

    #stop if the page number in the curr_page is greater than the page in the pagination text
    if curr_page >= page_no:
        nextPage = None

    # #always stop - debugging
    # nextPage = None

    logging.info(('Next Page', nextPage))

    self.nextPage = nextPage



# redefine the methods specific to this retailer
ProductDataFrame.get_description = new_get_description
ProductDataFrame.get_url = new_get_url
ProductDataFrame.get_skuid = new_get_skuid
ProductDataFrame.get_imageurl = new_get_imageurl
ProductDataFrame.get_banner = new_get_banner
ProductDataFrame.get_brand = new_get_brand
ProductDataFrame.get_stock = new_get_stock
ProductDataFrame.get_reviewscore = new_get_reviewscore
ProductDataFrame.get_reviewcount = new_get_reviewcount
ProductDataFrame.get_ticket_previous_price = new_get_ticket_previous_price
ProductDataFrame.get_product_description = new_get_product_description
ProductDataFrame.get_product_imgage_url = new_get_product_imgage_url
WebPageItems.get_promo_message = new_get_promo_message
WebPageItems.find_all_products = new_find_all_products
WebPageItems.find_next_page_from_webpage = new_find_next_page_from_webpage
# new methods can be added using the same approach, e.g. WebPageItems.newmethod = newmethod


# %%    RETAILER SPECIFIC - IDENTIFY WHICH OPTIONAL METHODS TO RUN

methods_to_run = {
    'output_product_to_screen': False,
    'output_product_values_to_screen': False,
    'output_image_to_file': True,
    'output_description_to_file': True,
    'output_webpage_to_file': False,
}



def main():
    filename = __file__
    startPage = 0  # EDIT HERE FOR SET_UP AND TESTING to processs a sub range of weblinks
    endPage = 999  # EDIT HERE
    scrapeWebPages(startPage, endPage, linkurls, params, filename, methods_to_run)
    return


if __name__ == '__main__':
    main()
