import __init__ # set up the import paths
import bs4
import datetime as dt
import pandas as pd
import time
import json
from pathlib import Path
import requests
import html5lib
import os
import re
from math import ceil
import logging

from CoreScraper import scrapeWebPages, extractPrice
from CoreScraper_getHtml import WebPageItems, ProductDataFrame
from CoreScraperConstants import toppath,  default_params

#  RETAILER SPECIFIC CONTROL PARAMETERS
# Set up the top path as parent folder of results, Note this is overridden if supplied as an argument when called as a main function
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape TEST/'

# set up multiple urls for finding links, first url is mainurl (code filtters duplicates)
linkurls = ['https://en.zalando.de/new-balance/']



params = default_params.copy()
params.update({
    'mainurl': linkurls[0],
    'opfpath': toppath,  # can be overidden by arguments, applied in in scrapeWebPages
    'numscreenshots' : 0, # number of screnshots to save
    # define valid links and how to interpret links
    'validtags': ['somethingtomakesurewedontfindanyotherlinks'],  # a valid link must contain one of these strings
    'invalidtags': ['help'],  # if a link contains one of these it is invalid
    'resetduplicatelinks': True,
    # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks': ['https://www.tbc.com'],  # force any duplicate links to be ignored
    #'starting_links': ['https://www.ebay.co.uk/sch/11450/i.html?_from=R40&_nkw=new+balance&LH_TitleDesc=0&_oac=1'], #all conditions
    'starting_links': ['https://en.zalando.de/new-balance/'], #New Balance
    #test the sports direct image
    #'starting_links' : ['https://www.ebay.co.uk/sch/i.html?_from=R40&_trksid=p2380057.m570.l1313&_nkw=New+Balance+Womens+London+Edition+Printed+Impact+Run+Light+Pack+Jacket+Outerwear&_sacat=0'],
    # additional links to process  #set any starting links that will not be picked up by the find starting pages process
    'tagref': 'a',  # look for hrefs in this class
    'remove_switches_link': False,  # if True, remove ? and # switches from link (recommended unless required)
    'departmentlevel': 2,  # the nth level in the link
    'categorylevel': 3,  # the nth level in the link
    'subcategorylevel': 4,  # the nth level in the link
    'minimumdepth': 5,
    # a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    # define how the web page is accessed
    'useRequests': False,  # whether or not to use the requests module, if False use the chromium driver Browser
    'findNextPage': True,
    # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    'htmlParser': 'lxml',
    # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    # webdriver parameters
    'webdriverpath': 'dummy',  # path to the webdriver executable SET UP IN CoreScraper.py
    'displayOff': False,  # whether display is switched off or visible
    'displayMinimised': False,  # whether to minimise the window if display is on
    'resetDriver': False,  # whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'waitForPageLoad': 2,# typ set to half webPageWait, when scrolling, time to wait for page to load before comparing 2 subsequent loads to see if scrolled page has finished loading
    'webPageWait': 2,  # time between subsequent web pages (next link, next page, next scroll down page)
    'scrollDownPage': False,  # whether to scroll down the page to find more products, e.g. Topshop
    # url and redirect parameters
    # 'urlSwitch' :  '#pagination=true',    #add an option to the url,e.g. 48 items per page
    'urlSwitch': '',  # add an option to the url,e.g. 48 items per page
    'invalidRedirect': '',  # if the redirected url contains this value the product pages do not load correctly
    # define any buttons to be clicked on screen
    'buttonXpaths': ['//*[@id="gdpr-banner-decline"]'],  # a list of XPaths
    # 'buttonXpaths' : [ '' ], # a list of XPaths
    'buttoniFrameXpaths': [],
    # "document.querySelector('#\31 556719216 > div.header > div > div.closeButton.closeSticky')"
    'proxy_location': '',
    # Give multiple country like this, 'GB|US'. country codes: https://laendercode.net/en/2-letter-list.html
    'vpn_location': 'GBWS',
    #turn off processing attributes
    'process_attributes' : True,
    #configure output data tables
    'GBQ_TABLE_RESULTS' : 'NB_results',
    'GBQ_TABLE_DESCRIPTIONS' : 'NB_descriptions',
    'TEST_GBQ_TABLE_RESULTS' : 'test_NB_results',
    'TEST_GBQ_TABLE_DESCRIPTIONS' : 'test_NB_descriptions',
    #extra description info - if False searches for the default product info, if True searches for the additional ebay product info
    'ADD_EBAY_PRODUCT_DESCRIPTIONS' : False,
})


# %%  RETAILER SPECIFIC - REDEFINE THE PRODUCTDATAFRAME METHODS
def new_get_description(self):
    #item title
    item_title = None
    itm_info =  self.webSoup.findAll('h3'  )
    if len(itm_info) == 2 :
        item_title = itm_info[1].get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
    self.description = item_title

    self.long_description =   None


def new_get_url(self):
    info = self.webSoup
    if info != None:
        u = info.find('a')
        if u != None:
            url = u.get('href')
            #self.url = self.mainurl + url
            self.url =  url


def new_get_skuid(self):
    url = self.url
    skuid_info = url.split('-')
    if len(skuid_info) > 1:
        sku_id = skuid_info[-2] + skuid_info[-1]
        sku_id = sku_id.replace('.html', '').upper()
    else:
        sku_id = 'Unknown'
    self.skuid = sku_id


def new_get_imageurl(self):
    i = self.webSoup.find('img')
    if i != None:
        imgurl = i.get('src')
        self.imgurl = imgurl


def new_get_brand(self):
    #item brand
    item_brand= None
    itm_info =  self.webSoup.findAll('h3' )
    if len(itm_info) == 2 :
        item_brand = itm_info[0].get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
    self.brand = item_brand

def new_get_stock(self):
    # not implemented
    pass


def new_get_banner(self):
    #not implemented
    pass


def new_get_ticket_previous_price(self):
    wasprice = 0
    nowprice = 0

    #extract  price string
    np = self.webSoup.findAll('p')

    #get the first price, the now price, 
    if len(np) > 0:
        nowprice_info = np[0].findAll('span')
        if len(nowprice_info) == 1:
            nowprice = nowprice_info[0].get_text()
            #incase multiple prices extract first of multiple prices
            nowprice = nowprice.strip().split(' ')[0]
            wasprice = nowprice
        else:
            nowprice = nowprice_info[0].get_text()
            #incase multiple prices extract first of multiple prices
            nowprice = nowprice.strip().split(' ')[0]
            wasprice = nowprice

    if len(np) > 1:  #check to see if there is more than one price
        wasprice_info = np[1].findAll('span')
        #if there are 'span' in the wasprice_info, the wasprice is the 2nd element, 1st element is text 'Originally'
        if len(wasprice_info) > 1:
            wasprice = wasprice_info[1].get_text()
            #incase multiple prices extract first of multiple prices
            wasprice = wasprice.strip().split(' ')[0]


    self.ticketprice = extractPrice(nowprice, "€")
    self.prevprice = extractPrice(wasprice, "€")


def new_get_reviewcount(self):
    # not implemented here - obtained from product description 
    pass

def new_get_reviewscore(self):
    # not implemented  here - obtained from product description 
    pass

def new_get_promo_message(self):
    # not implemented
    pass

def new_find_all_products(self):
    """find new products  """
    product_info = self.webSoup
    products = product_info.find_all('article', { 'class' :  re.compile("_0mW-4D") } )
    logging.info(('num products', len(products)))
    #print(products[0].prettify())

    self.products = products


#define the method that is used to find the product description from the individual product page url
def new_get_product_description(self):
    #TBD - get the details from the individual product page

    #review info
    review_count = 0
    review_score = ""
    reviews = self.webSoup.find('div' , {'id' : re.compile("reviews") })
    if reviews != None:
        review_count = reviews.find('h5')
        if review_count != None:
            review_count = review_count.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
            review_count = review_count.replace('(','').replace(')','')
            
        review_score = reviews.find('span')
        if review_score != None:
            review_score = review_score.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
            

    self.reviewcount = review_count
    self.reviewscore = review_score
    
    self.long_description =   None

    #put the vendor into the stock column
    #<div class="oX3eAU _2LebSa QMtjIA ibou8b fc1m10 agLu8- Pb4Ja8"><span class=""><p class="_0Qm8W1 u-6V88 FxZV-M pVrzNP zN9KaA">Sold by </p><p class="_0Qm8W1 u-6V88 dgII7d wu3klO Md_Vex zN9KaA" aria-haspopup="true" aria-expanded="false" tabindex="0">New Balance</p><p class="_0Qm8W1 u-6V88 FxZV-M pVrzNP zN9KaA"> and Zalando, shipped by </p><p class="_0Qm8W1 u-6V88 dgII7d wu3klO Md_Vex zN9KaA" aria-haspopup="true" aria-expanded="false" tabindex="0">New Balance</p></span></div>
    delivery_details = self.webSoup.findAll('div' , {'class' : re.compile("oX3eAU") })
    #loop through each delivery detail to find any sold by info
    delivery_text_output = ""
    seller_info = False
    for deliv_detail in delivery_details:
        for deliv in deliv_detail:
            deliv_data = deliv.findAll('p')
            #search all elements - sometimes Sold is down the list, sometimes appears several times
            if len(deliv_data) > 0 and not seller_info:
                for d in deliv_data :
                    deliv_text = d.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
                    if "Sold" in deliv_text:
                        seller_info = True
                        #look to see if there is an image of zalando colour logo
                        zcl = deliv_detail.find('u')
                        if zcl is not None:
                            zcl_text = zcl.get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
                            deliv_text = deliv_text + " " + zcl_text
                    if seller_info:
                        delivery_text_output = delivery_text_output + " " + deliv_text
                    

    if len(delivery_text_output) > 0:
        self.stock = delivery_text_output . strip()
    else:
        self.stock = 'no seller info'




#define the method that is used to find the main image url from the individual product page url
def new_get_product_imgage_url(self):
    #TBD - get the details from the individual product page
    #do not need this ?
    pass

    # image_info = self.webSoup.find('div' , {'class' : 'ux-main-image-carousel'})
    # #print(image_info.prettify())
    # if image_info != None:
    #     images = image_info.findAll('img')
    #     if len(images) >=1:
    #         url = images[0].get("src")
    #         if url != None: 
    #             self.imgurl = url



# find the next page link from the current page
def new_find_next_page_from_webpage(self):
    # driver = self.driver # for access to buttons on the page etc
    # for white company add page=N to the end of the url
    # try:
    #     prevPage = params['prev_page']
    # except:
    #     prevPage = 0 # first iteration
    link = self.link
    findNextPage = params['findNextPage']
    nextPage = None
    if findNextPage:
        # find current page number if already set
        pagePos = int(link['webpage'].find('?p='))
        if pagePos == -1:
            # page no currently not set
            nextPage = link['webpage'] + '?p=2'
            currPage = 1
            params['prev_page'] = currPage
            prevPage = 0
        else:  # page number has already been set
            prevPage = params['prev_page']
            currPage = int(link['webpage'][pagePos + 3:])
            params['prev_page'] = currPage
            nextPage = currPage + 1
            nextPage = link['webpage'].replace('?p=' + str(currPage), '?p=' + str(nextPage))

        # override - if there are no products on the current page then end of page has been reached
        # ebay has a limitation of approx 10,000 results returned per search
        if len(self.products) == 0:
            nextPage = None

        #save the current page
        params['prev_page'] = currPage

        # # testing temporarily stop after 1 page
        # if currPage == 2:
        #     nextPage = None

        #stop if the page number has not changed
        if currPage == prevPage:
            nextPage = None

        logging.info(('Next Page', nextPage))

    self.nextPage = nextPage



# redefine the methods specific to this retailer
ProductDataFrame.get_description = new_get_description
ProductDataFrame.get_url = new_get_url
ProductDataFrame.get_skuid = new_get_skuid
ProductDataFrame.get_imageurl = new_get_imageurl
ProductDataFrame.get_banner = new_get_banner
ProductDataFrame.get_brand = new_get_brand
ProductDataFrame.get_stock = new_get_stock
ProductDataFrame.get_reviewscore = new_get_reviewscore
ProductDataFrame.get_reviewcount = new_get_reviewcount
ProductDataFrame.get_ticket_previous_price = new_get_ticket_previous_price
ProductDataFrame.get_product_description = new_get_product_description
ProductDataFrame.get_product_imgage_url = new_get_product_imgage_url
WebPageItems.get_promo_message = new_get_promo_message
WebPageItems.find_all_products = new_find_all_products
WebPageItems.find_next_page_from_webpage = new_find_next_page_from_webpage
# new methods can be added using the same approach, e.g. WebPageItems.newmethod = newmethod


# %%    RETAILER SPECIFIC - IDENTIFY WHICH OPTIONAL METHODS TO RUN

methods_to_run = {
    'output_product_to_screen': False,
    'output_product_values_to_screen': False,
    'output_image_to_file': True,
    'output_description_to_file': True,
    'output_webpage_to_file': False,
}



def main():
    filename = __file__
    startPage = 0  # EDIT HERE FOR SET_UP AND TESTING to processs a sub range of weblinks
    endPage = 1  # EDIT HERE
    scrapeWebPages(startPage, endPage, linkurls, params, filename, methods_to_run)
    return


if __name__ == '__main__':
    main()
