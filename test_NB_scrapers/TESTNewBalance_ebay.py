import __init__ # set up the import paths
import bs4
import datetime as dt
import pandas as pd
import time
import json
from pathlib import Path
import requests
import html5lib
import os
import re
from math import ceil
import logging

from CoreScraper import scrapeWebPages, extractPrice
from CoreScraper_getHtml import WebPageItems, ProductDataFrame
from CoreScraperConstants import toppath,  default_params

#  RETAILER SPECIFIC CONTROL PARAMETERS
# Set up the top path as parent folder of results, Note this is overridden if supplied as an argument when called as a main function
if os.name == 'posix':
    toppath = "/home/kevin_d_blackmore/Web Scrape/"
else:
    toppath = 'C:/Users/kevin/Data/Spb-web/Web Scrape TEST/'

# set up multiple urls for finding links, first url is mainurl (code filtters duplicates)
linkurls = ['https://www.ebay.co.uk/sch/i.html?_from=R40&_nkw=new+balance&_sacat=0&LH_ItemCondition=1000%7C1500%7C1750&rt=nc&LH_PrefLoc=1']



params = default_params.copy()
params.update({
    'mainurl': linkurls[0],
    'opfpath': toppath,  # can be overidden by arguments, applied in in scrapeWebPages
    'numscreenshots' : 0, # number of screnshots to save
    # define valid links and how to interpret links
    'validtags': ['somethingtomakesurewedontfindanyotherlinks'],  # a valid link must contain one of these strings
    'invalidtags': ['help'],  # if a link contains one of these it is invalid
    'resetduplicatelinks': True,
    # set to True to reset the duplicate links using the latest(current) results (false reads in duplicates from csv)
    'duplicatelinks': ['https://www.tbc.com'],  # force any duplicate links to be ignored
    #'starting_links': ['https://www.ebay.co.uk/sch/11450/i.html?_from=R40&_nkw=new+balance&LH_TitleDesc=0&_oac=1'], #all conditions
    'starting_links': ['https://www.ebay.co.uk/sch/i.html?_from=R40&_nkw=new+balance&_sacat=0&LH_ItemCondition=1000%7C1500%7C1750&rt=nc&LH_PrefLoc=1'], #new UK only
    #test the sports direct image
    #'starting_links' : ['https://www.ebay.co.uk/sch/i.html?_from=R40&_trksid=p2380057.m570.l1313&_nkw=New+Balance+Womens+London+Edition+Printed+Impact+Run+Light+Pack+Jacket+Outerwear&_sacat=0'],
    # additional links to process  #set any starting links that will not be picked up by the find starting pages process
    'tagref': 'a',  # look for hrefs in this class
    'remove_switches_link': False,  # if True, remove ? and # switches from link (recommended unless required)
    'departmentlevel': 2,  # the nth level in the link
    'categorylevel': 3,  # the nth level in the link
    'subcategorylevel': 4,  # the nth level in the link
    'minimumdepth': 5,
    # a page link has to have at least the minimum depth to be included, to exclude the top level "all clothing" type menus
    # define how the web page is accessed
    'useRequests': False,  # whether or not to use the requests module, if False use the chromium driver Browser
    'findNextPage': True,
    # whether all products are on one page, e.g. Topshop = False, or on multiple pages, e.g. JDW = True
    'htmlParser': 'lxml',
    # select the html parser, either 'lxml' or 'html.parser or 'lxml' (pref choice) or html5lib (best but slowest)
    # webdriver parameters
    'webdriverpath': 'dummy',  # path to the webdriver executable SET UP IN CoreScraper.py
    'displayOff': False,  # whether display is switched off or visible
    'displayMinimised': False,  # whether to minimise the window if display is on
    'resetDriver': False,  # whether driver is reset each time or same driver is used. e.g. Asos need to reset each time
    'waitForPageLoad': 2,# typ set to half webPageWait, when scrolling, time to wait for page to load before comparing 2 subsequent loads to see if scrolled page has finished loading
    'webPageWait': 2,  # time between subsequent web pages (next link, next page, next scroll down page)
    'scrollDownPage': False,  # whether to scroll down the page to find more products, e.g. Topshop
    # url and redirect parameters
    # 'urlSwitch' :  '#pagination=true',    #add an option to the url,e.g. 48 items per page
    'urlSwitch': '',  # add an option to the url,e.g. 48 items per page
    'invalidRedirect': '',  # if the redirected url contains this value the product pages do not load correctly
    # define any buttons to be clicked on screen
    'buttonXpaths': ['//*[@id="gdpr-banner-decline"]'],  # a list of XPaths
    # 'buttonXpaths' : [ '' ], # a list of XPaths
    'buttoniFrameXpaths': [],
    # "document.querySelector('#\31 556719216 > div.header > div > div.closeButton.closeSticky')"
    'proxy_location': '',
    # Give multiple country like this, 'GB|US'. country codes: https://laendercode.net/en/2-letter-list.html
    'vpn_location': 'GBWS',
    #turn off processing attributes
    'process_attributes' : True,
    #configure output data tables
    'GBQ_TABLE_RESULTS' : 'NB_results',
    'GBQ_TABLE_DESCRIPTIONS' : 'NB_descriptions',
    'TEST_GBQ_TABLE_RESULTS' : 'test_NB_results',
    'TEST_GBQ_TABLE_DESCRIPTIONS' : 'test_NB_descriptions',
    #extra description info - if False searches for the default product info, if True searches for the additional ebay product info
    'ADD_EBAY_PRODUCT_DESCRIPTIONS' : True,
})


# %%  RETAILER SPECIFIC - REDEFINE THE PRODUCTDATAFRAME METHODS
def new_get_description(self):
    info = self.webSoup.find('div', {'class': 's-item__title'})
    if info != None:
        d = info.find('span')
        if d != None:
            description = d.get_text().replace('\n', '').replace('\r', '').strip()
            self.description = description


def new_get_url(self):
    info = self.webSoup
    if info != None:
        u = info.find('a')
        if u != None:
            url = u.get('href')
            #self.url = self.mainurl + url
            self.url =  url


def new_get_skuid(self):
    url = self.url
    skuid_info = url.split('?')[0]
    skuid = skuid_info.split('/')[-1]
    self.skuid = skuid


def new_get_imageurl(self):
    i = self.webSoup.find('img')
    if i != None:
        imgurl = i.get('src')
        self.imgurl = imgurl


def new_get_brand(self):
    # not implemented
    info = self.webSoup.find('p')
    if info != None:
        d = info.find('a', {'class': "productMainLink"})
        if d != None:
            self.brand = None


def new_get_stock(self):
    # not implemented
    info = self.webSoup.find('span', {'class': 'listProductLowStock'})
    if info != None:
        d = info.get_text()
        if d != None:
            self.stock = None


def new_get_banner(self):
    #not implemented
    info = self.webSoup.find('span', {'class': "price-save"})
    b = info
    if b != None:
        banner = b.get_text().strip()
        self.banner = banner


def new_get_ticket_previous_price(self):
    ticketprice = 0
    prevprice = 0
    wasprice = None
    nowprice = None

    #extract  price string
    np = self.webSoup.find('span', {'class': "s-item__price"})
    nowprice = np.get_text()

    #incase multiple prices extract first of multiple prices
    nowprice = nowprice.strip().split(' ')[0]

    if wasprice == None:
        ticketprice = extractPrice(nowprice, "£")
        prevprice = ticketprice
    else:
        ticketprice = extractPrice(nowprice, "£")
        prevprice = extractPrice(wasprice, "£")

    self.ticketprice = extractPrice(ticketprice, "£")
    self.prevprice = extractPrice(prevprice, "£")


def new_get_reviewcount(self):
    # not implemented 
    info = self.webSoup.find('span', {'class': "product-rating__review-count"})
    if info != None:
        r = info.get_text()
        if r != None:
            r = r.split('(')[1].split('r')[0]
            reviewcount = r.replace('\n', ' ').replace('\r', ' ').replace('(', ' ').replace(')', ' ').strip()
            self.reviewcount = reviewcount


def new_get_reviewscore(self):
    # not implemented 
    info = self.webSoup.find('div', {'class': "product-rating"})
    if info != None:
        fullstar = info.find_all('img', {'alt': 'Full Star'})
        halfstar = info.find_all('img', {'alt': 'Half Star'})
        reviewscore = len(fullstar) + 0.5 * len(halfstar)
        self.reviewscore = reviewscore


def new_get_promo_message(self):
    # not implemented
    info = self.webSoup.find('div', {'id': 'header_espot'})
    if info != None:
        pm = info.find('img')
        if pm != None:
            self.promomessage = pm.get('alt')


def new_find_all_products(self):
    """find new products  """
    products_all = self.webSoup.find('ul', {'class': 'srp-results'})
    if products_all != None:
        products = products_all.find_all('li', {'class': re.compile("s-item")}, recursive=False)
    else:
        products = []
    logging.info(('num products', len(products)))
    self.products = products


#define the method that is used to find the product description from the individual product page url
def new_get_product_description(self):

    #shop with confidence - authencity and money back
    authenticity_guarantee = False
    money_back_guarantee = False
    shp_info = self.webSoup.find('div' , {'class' : re.compile("shop-with-confidence") })
    #print(shp_info.prettify())
    if shp_info is not None:
        shp_options = shp_info.findAll('span' , {'class' : re.compile("ux-textspans") })
        for shp in shp_options:
            shp_txt = shp.get_text()
            #print(shp_txt)
            if "Authenticity" in shp_txt:
                authenticity_guarantee = True
            if "Money Back" in shp_txt:
                money_back_guarantee = True
    self.authenticity_guarantee = authenticity_guarantee
    self.money_back_guarantee = money_back_guarantee
        

    #seller info
    seller_url = None
    seller_descr = None
    seller_reviews = None
    registered_business_seller = None
    sell_info = self.webSoup.find('div' , {'class' : re.compile("about-this-seller") })
    #print(sell_info.prettify())
    if sell_info is not None:
        sell_sect = sell_info.find('li', {'class' : re.compile("ux-seller-section") })
        sell_details = sell_sect.findAll('a')
        seller_url = sell_details[0].get('href')
        seller_descr = sell_details[0].find('span').get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
        seller_reviews = sell_details[1].find('span').get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()

        sell_options = sell_info.findAll('span' , {'class' : re.compile("ux-textspans") }, recursive=True)
        registered_business_seller = False
        for sell in sell_options:
            sell_txt = sell.get_text()
            if "business seller" in sell_txt:
                registered_business_seller = True

    self.seller_url = seller_url
    self.seller_description = seller_descr
    self.seller_reviews = seller_reviews
    self.registered_business_seller = registered_business_seller

    #item title
    item_title = None
    itm_info =  self.webSoup.find('div' , {'class' : re.compile("item-title") })
    if itm_info is not None:
        item_title = itm_info.find('h1').get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
    self.item_title = item_title

    #item condition
    item_condition = None
    itm_info =  self.webSoup.find('div' , {'class' : re.compile("item-condition-text") })
    if itm_info is not None:
        item_condition = itm_info.find('span' , {'class' : re.compile("ux-textspans") }).get_text().replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
    self.item_condition = item_condition

    #quantity available
    quantity_available = 0
    quant_info = self.webSoup.find('div' , {'class' : re.compile("vi-quantity-wrapper") })
    if quant_info is not None:
        #nested spans
        quant_info2 = quant_info.findAll('span')
        for quant in quant_info2:
            quant_text = quant.get_text()
            if 'available' in quant_text:
                qty_avail_txt = quant_text.replace('\n', ' ').replace('\r', ' ').replace("  ", " ").strip()
                quantity_available = qty_avail_txt.split('/')[0].strip()
    self.quantity_available = quantity_available

    #why to buy
    posts_from = "not known"
    w2b_info = self.webSoup.find('div' , {'id' : re.compile("why2buy") })
    if w2b_info is not None:
        w2b_opts = w2b_info.findAll('div')
        for w2b in w2b_opts:
            w2b_txt = w2b.get_text()
            if "Posts" in w2b_txt:
                posts_from = w2b_txt
    self.seller_posts_from = posts_from

    #buy box CTA
    buy_it_now = False
    buy_info = self.webSoup.find('ul' , {'class' : re.compile("buybox") })
    if buy_info is not None:
        cta_opts = buy_info.findAll('span', {'class' : re.compile("ux-call-to-action__text") } )
        for cta in cta_opts:
            cta_txt = cta.get_text()
            if "Buy" in cta_txt:
                buy_it_now = True
    self.buy_it_now = buy_it_now

    #buy box bid
    submit_bid = True
    bid_info = self.webSoup.find('div' , {'class' : re.compile("bid-action") })
    if bid_info is not None:
        bid_opts = bid_info.findAll('span', {'class' : re.compile("ux-call-to-action__text") } )
        for bid in bid_opts:
            bid_txt = bid.get_text()
            if "Bid" in bid_txt:
                submit_bid = True
    self.submit_bid = submit_bid

    #item specifics
    item_info = self.webSoup.find('div' , {'class' : re.compile("--features") })
    item_text_dict = {}
    if item_info is not None:
        itm_opts = item_info.findAll('div', {'class' : re.compile("__row") } )
        for idx, itm in enumerate(itm_opts):
            itm_txt_dict  = {}
            itm_parts = itm.findAll('span', {'class' : 'ux-textspans'})
            for prt, part in enumerate(itm_parts):
                itm_txt_dict['col'+str(prt)] = part.get_text()
            item_text_dict[ 'row'+str(idx)] = itm_txt_dict
    self.item_specifics = item_text_dict


    #long description
    #TODO NOT WORKING  - this opens a link to an iframe which needs to be opened to get the info
    long_info_1_txt = ""
    long_info_2_txt = ""
    # info = self.webSoup.find('div' , {'class' : re.compile("d-item-description") })
    # #print(info.prettify())
    # #THIS IS WITHIN THE iframe 
    # long_info_1 = info.find('div' , {'class' : "product__desc-text" })
    # if long_info_1 is not None:
    #     long_info_1_txt = long_info_1.get_text()
    # long_info_2 = info.find('div' , {'class' : re.compile("product__attributes") })
    # if long_info_2 is not None:
    #     long_info_2_txt = long_info_2.get_text()

    # self.long_description =   long_info_1_txt + " " + long_info_2_txt
    self.long_description =   None

    #get the ebay item number
    ebay_item_number = 0
    info = self.webSoup.find('div' , {'class' : re.compile("d-item-details-tab-header") })
    if info is not None:
        #print(info.prettify())
        item_nos = info.findAll( 'span', {'class' : 'ux-textspans'})
        for idx, item in enumerate(item_nos):
            if 'item number' in item.get_text():
                ebay_item_number = int(item_nos[idx+1].get_text())
        #print('EBAY item Number ' , ebay_item_number)
    self.ebay_item_number = ebay_item_number

    #get the business information
    bus_info = self.webSoup.find('div' , {'class' : re.compile("ux-section--nameAndAddress") })
    bus_info_dict = {}
    if bus_info is not None:
        bus_opts = bus_info.findAll('span', {'class' : re.compile('ux-textspans') } )
        for idx, businf in enumerate(bus_opts):
            bus_info_dict[ 'row'+str(idx)] = businf.get_text()
    self.seller_business_info = bus_info_dict

    #VAT info
    vat_number = 0
    vat_info = self.webSoup.find('ul' , {'class' : re.compile("ux-section--vatInformation") })
    if vat_info is not None:
        vat_number = vat_info.get_text()
        bus_info_dict[ 'row_vat'] = vat_number #starts with GB
    self.seller_vat_no = vat_number

    #get the colour description
    #TODO 
    self.colour = None  #find in item specifics

    #TODO fetch the size description data = [list of strings]
    self.size = None
    self.availability = None

#define the method that is used to find the main image url from the individual product page url
def new_get_product_imgage_url(self):
    logging.info(('Fetching imnage URL'))

    image_info = self.webSoup.find('div' , {'class' : 'ux-main-image-carousel'})
    #print(image_info.prettify())
    if image_info != None:
        images = image_info.findAll('img')
        if len(images) >=1:
            url = images[0].get("src")
            if url != None: 
                self.imgurl = url



# find the next page link from the current page
def new_find_next_page_from_webpage(self):
    # driver = self.driver # for access to buttons on the page etc
    # for white company add page=N to the end of the url
    link = self.link
    findNextPage = params['findNextPage']
    nextPage = None
    if findNextPage:
        # find current page number if already set
        pagePos = int(link['webpage'].find('&_pgn='))
        if pagePos == -1:
            # page no currently not set
            nextPage = link['webpage'] + '&_pgn=2'
            currPage = 1
        else:  # page number has already been set
            currPage = int(link['webpage'][pagePos + 6:])
            nextPage = currPage + 1
            nextPage = link['webpage'].replace('pgn=' + str(currPage), 'pgn=' + str(nextPage))

        # override - if there are no products on the current page then end of page has been reached
        # ebay has a limitation of approx 10,000 results returned per search
        if len(self.products) == 0:
            nextPage = None

        # #TODO temporarily stop after 1 page
        # if currPage == 1:
        #     nextPage = None

        logging.info(('Next Page', nextPage))

    self.nextPage = nextPage



# redefine the methods specific to this retailer
ProductDataFrame.get_description = new_get_description
ProductDataFrame.get_url = new_get_url
ProductDataFrame.get_skuid = new_get_skuid
ProductDataFrame.get_imageurl = new_get_imageurl
ProductDataFrame.get_banner = new_get_banner
ProductDataFrame.get_brand = new_get_brand
ProductDataFrame.get_stock = new_get_stock
ProductDataFrame.get_reviewscore = new_get_reviewscore
ProductDataFrame.get_reviewcount = new_get_reviewcount
ProductDataFrame.get_ticket_previous_price = new_get_ticket_previous_price
ProductDataFrame.get_product_description = new_get_product_description
ProductDataFrame.get_product_imgage_url = new_get_product_imgage_url
WebPageItems.get_promo_message = new_get_promo_message
WebPageItems.find_all_products = new_find_all_products
WebPageItems.find_next_page_from_webpage = new_find_next_page_from_webpage
# new methods can be added using the same approach, e.g. WebPageItems.newmethod = newmethod


# %%    RETAILER SPECIFIC - IDENTIFY WHICH OPTIONAL METHODS TO RUN

methods_to_run = {
    'output_product_to_screen': False,
    'output_product_values_to_screen': False,
    'output_image_to_file': True,
    'output_description_to_file': True,
    'output_webpage_to_file': False,
}



def main():
    filename = __file__
    startPage = 0  # EDIT HERE FOR SET_UP AND TESTING to processs a sub range of weblinks
    endPage = 1  # EDIT HERE
    scrapeWebPages(startPage, endPage, linkurls, params, filename, methods_to_run)
    return


if __name__ == '__main__':
    main()
